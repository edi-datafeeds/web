use wca;
drop table if exists wca2.evf_loadingtable;
create table wca2.evf_loadingtable(
select distinct
'preference amount outstanding' as event,
case when bochg.bochgid is not null 
     then concat(lpad(scexh.scexhid,7,'0'),bochg.bochgid)
     else concat(lpad(scexh.scexhid,7,'0'),scmst.secid) 
     end as caref,
case when bochg.bochgid is not null
     then bochg.bochgid
     else scmst.secid
     end as eventid,
case when bochg.bochgid is not null then bochg.announcedate
     else scmst.announcedate
     end as created,
case when bochg.bochgid is not null then bochg.acttime
     else scmst.acttime
     end as changed,
case when bochg.actflag = 'U' then 'Updated'
     when bochg.actflag = 'I' then 'Inserted'
     when bochg.actflag = 'D' then 'Deleted'
     when bochg.actflag = 'C' then 'Cancelled'
     when scmst.actflag = 'U' then 'Updated'
     when scmst.actflag = 'I' then 'Inserted'
     when scmst.actflag = 'D' then 'Deleted'
     when scmst.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
ifnull(sbx.sedol,'') as sedol,
ifnull(sbx.rcntrycd,'') as sedol_register_country,
ifnull(sbx.curencd,'') as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
ifnull(sbx.bbgcompid,'') as composite_global_id,
ifnull(sbx.bbgcomptk,'') as bloomberg_composite_ticker,
ifnull(sbx.bbgexhid,'') as bloomberg_global_id,
ifnull(sbx.bbgexhtk,'') as bloomberg_exchange_ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
bochg.oldoutdate as old_amount_date,
bochg.newoutdate as new_amount_date,
bochg.oldoutvalue as old_amount,
bochg.newoutvalue as new_amount,
'Mandatory' as choice,
case when bochg.eventtype is null then ''
     when event.eventname is null and bochg.eventtype <> ''
     then concat('[',bochg.eventtype,'] not found')
     else event.eventname
     end as related_event,
case when bochg.eventtype<>'' and bochg.eventtype<>'CLEAN' 
     then concat(releventid,'-',evlook.eventname)
     else '' end as event_link,
'bochg|bochgid|bochgnotes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid,
case when scmst.sectycd='BND' then 'Yes' else '' end as fi_prf
from scmst
inner join bochg on scmst.secid = bochg.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
left outer join event as evlook on bochg.eventtype = evlook.eventtype
left outer join event on bochg.eventtype = event.eventtype
where
bochg.actflag<>'D'
and ((wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (bochg.acttime > '2015/01/01' or bochg.acttime is null)
and (effectivedate < scexh.acttime or scexh.liststatus<>'D' or effectivedate is null)
and scexh.actflag<>'D'
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`trading_currency`,`sedol_register_country`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`),
add index `ix_exchgcd`(`exchange_code`);
drop table if exists wca2.evf_preference_amount_outstanding;
rename table wca2.evf_loadingtable to wca2.evf_preference_amount_outstanding;

drop table if exists wca2.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'preference redemption' as event,
concat(lpad(scexh.scexhid,7,'0'),redem.redemid) as caref,
redemid as eventid,
redem.announcedate as created,
redem.acttime as changed,
case when redem.actflag = 'U' then 'Updated'
     when redem.actflag = 'I' then 'Inserted'
     when redem.actflag = 'D' then 'Deleted'
     when redem.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
ifnull(sbx.sedol,'') as sedol,
ifnull(sbx.rcntrycd,'') as sedol_register_country,
ifnull(sbx.curencd,'') as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
ifnull(sbx.bbgcompid,'') as composite_global_id,
ifnull(sbx.bbgcomptk,'') as bloomberg_composite_ticker,
ifnull(sbx.bbgexhid,'') as bloomberg_global_id,
ifnull(sbx.bbgexhtk,'') as bloomberg_exchange_ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
case when redem.redemtype = '' then ''
     when redemtype.lookup is null
     then concat('[',redem.redemtype,'] not found')
     else redemtype.lookup
     end as redemption_type,
'price' as rate_type,
redem.redemprice as rate,
redem.curencd as currency,
redem.amountredeemed as amount_redeemed,
redem.redemdate as redemption_date,
case when redem.partfinal='p' then 'Part'
     when redem.partfinal='f' then 'Final'
     else ''
     end as part_final,
case when redem.mandoptflag = 'm' then 'Mandatory'
     when redem.mandoptflag = 'o' then 'Voluntary'
     else '' end as choice,
redem.redempremium as redemption_premium,
redem.priceaspercent as price_as_percent,
redem.premiumaspercent as premium_as_percent,
rd.recdate as record_date,
redem.poolfactor as pool_factor,
'redem|redemid|redemnotes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid,
case when scmst.sectycd='BND' then 'Yes' else '' end as fi_prf
from redem
inner join scmst on redem.secid = scmst.secid
left outer join bond on redem.secid = bond.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join rd on redem.rdid = rd.rdid
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
left outer join lookup as redemtype on redem.redemtype = redemtype.code
                      and 'redemtype' = redemtype.typegroup
where 
redem.acttime > '2015/01/01'
and redem.actflag<>'D'
and sectygrp.secgrpid is not null
and (3>sectygrp.secgrpid or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (redemdate < scexh.acttime or scexh.liststatus<>'D' or redemdate is null)
and scexh.actflag<>'D'
and redemtype<>'BB' and redemtype<>'BBED' and redemtype<>'BBRD'
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`trading_currency`,`sedol_register_country`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`
);
drop table if exists wca2.evf_preference_redemption;
rename table wca2.evf_loadingtable to wca2.evf_preference_redemption;

drop table if exists wca2.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'preference offer' as event,
concat(lpad(scexh.scexhid,7,'0'),redem.redemid) as caref,
redemid as eventid,
redem.announcedate as created,
redem.acttime as changed,
case when redem.actflag = 'U' then 'Updated'
     when redem.actflag = 'I' then 'Inserted'
     when redem.actflag = 'D' then 'Deleted'
     when redem.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
ifnull(sbx.sedol,'') as sedol,
ifnull(sbx.rcntrycd,'') as sedol_register_country,
ifnull(sbx.curencd,'') as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
ifnull(sbx.bbgcompid,'') as composite_global_id,
ifnull(sbx.bbgcomptk,'') as bloomberg_composite_ticker,
ifnull(sbx.bbgexhid,'') as bloomberg_global_id,
ifnull(sbx.bbgexhtk,'') as bloomberg_exchange_ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
case when redem.redemtype = '' then ''
     when redemtype.lookup is null
     then concat('[',redem.redemtype,'] not found')
     else redemtype.lookup
     end as offer_type,
'price' as rate_type,
redem.redemprice as rate,
redem.curencd as currency,
redem.AmountRedeemed as offer_amount,
redem.redemdate as redemption_date,
case when redem.partfinal='p' then 'Part'
     when redem.partfinal='f' then 'Final'
     else ''
     end as part_final,
case when redem.mandoptflag = 'm' then 'Mandatory'
     when redem.mandoptflag = 'o' then 'Voluntary'
     else '' end as choice,
redem.redempremium as redemption_premium,
redem.priceaspercent as price_as_percent,
redem.premiumaspercent as premium_as_percent,
rd.recdate as record_date,
redem.tenderopendate as tender_open_date,
redem.tenderclosedate as tender_close_date,
redem.tenderofferor as tender_offeror,
redem.poolfactor as pool_factor,
'redem|redemid|redemnotes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid,
case when scmst.sectycd='BND' then 'Yes' else '' end as fi_prf
from redem
inner join scmst on redem.secid = scmst.secid
left outer join bond on redem.secid = bond.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join rd on redem.rdid = rd.rdid
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
left outer join lookup as redemtype on redem.redemtype = redemtype.code
                      and 'redemtype' = redemtype.typegroup
where 
redem.acttime > '2015/01/01'
and redem.actflag<>'D'
and sectygrp.secgrpid is not null
and (3>sectygrp.secgrpid or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (redemdate < scexh.acttime or scexh.liststatus<>'D' or redemdate is null)
and scexh.actflag<>'D'
and (redemtype='BB' or redemtype='BBED' or redemtype='BBRD')
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`trading_currency`,`sedol_register_country`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`
);
drop table if exists wca2.evf_preference_offer;
rename table wca2.evf_loadingtable to wca2.evf_preference_offer;

drop table if exists wca2.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'preference conversion' as event,
concat(lpad(scexh.scexhid,7,'0'),conv.convid) as caref,
conv.convid as eventid,
conv.announcedate as created,
conv.acttime as changed,
case when conv.actflag = 'U' then 'Updated'
     when conv.actflag = 'I' then 'Inserted'
     when conv.actflag = 'D' then 'Deleted'
     when conv.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
ifnull(sbx.sedol,'') as sedol,
ifnull(sbx.rcntrycd,'') as sedol_register_country,
ifnull(sbx.curencd,'') as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
ifnull(sbx.bbgcompid,'') as composite_global_id,
ifnull(sbx.bbgcomptk,'') as bloomberg_composite_ticker,
ifnull(sbx.bbgexhid,'') as bloomberg_global_id,
ifnull(sbx.bbgexhtk,'') as bloomberg_exchange_ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
case when conv.convtype = '' then ''
     when convtype.lookup is null
     then concat('[',conv.convtype,'] not found')
     else convtype.lookup
     end as conversion_type,
case when conv.rationew is null or conv.rationew = ''
     then ''
     else concat(conv.rationew,':',conv.ratioold)
     end as ratio,
case when conv.fractions = '' then ''
     when irfractions.lookup is null and conv.fractions <> ''
     then concat('[',conv.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
'price' as rate_type,
conv.price as rate,
conv.curencd as currency,
conv.amountconverted as amount_converted,
conv.fromdate as period_start_date,
conv.todate as period_end_date,
conv.settlementdate as settlement_date,
rd.recdate as record_date,
conv.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
case when ressedol.sedol is null then '' else ressedol.sedol end as resultant_sedol,
case when conv.ressectycd is null then ''
     when secty.securitydescriptor is null and conv.ressectycd <> ''
     then concat('[',conv.ressectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
case when conv.mandoptflag = 'm' then 'Mandatory'
     when conv.mandoptflag = 'o' then 'Voluntary'
     else ''
     end as choice,
case when conv.partfinalflag='p' then 'Part'
     when conv.partfinalflag='f' then 'Final'
     else ''
     end as part_final,
'conv|convid|convnotes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid,
case when scmst.sectycd='BND' then 'Yes' else '' end as fi_prf
from conv
inner join scmst on conv.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join rd on conv.rdid = rd.rdid
left outer join irfractions on conv.fractions = irfractions.code
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
left outer join scmst as resscmst on conv.ressecid = resscmst.secid
left outer join sedol as ressedol on conv.ressecid =ressedol.secid
                   and exchg.cntrycd=ressedol.cntrycd
                   and 'D'<>ressedol.actflag
left outer join secty on conv.ressectycd = secty.sectycd
left outer join lookup as convtype on conv.convtype = convtype.code
                      and 'convtype' = convtype.typegroup
where
conv.acttime > '2015/01/01'
and substring(conv.convtype,1,2)<>'BB'
and conv.convtype<>'TENDER'
and sectygrp.secgrpid is not null
and (3>sectygrp.secgrpid or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (fromdate < scexh.acttime or scexh.liststatus<>'D' or fromdate is null)
and scexh.actflag<>'D'
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`trading_currency`,`sedol_register_country`,`resultant_sedol`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`
);
drop table if exists wca2.evf_preference_conversion;
rename table wca2.evf_loadingtable to wca2.evf_preference_conversion;

drop table if exists wca2.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'preference_exchange_offer' as event,
concat(lpad(scexh.scexhid,7,'0'),conv.convid) as caref,
conv.convid as eventid,
conv.announcedate as created,
conv.acttime as changed,
case when conv.actflag = 'U' then 'Updated'
     when conv.actflag = 'I' then 'Inserted'
     when conv.actflag = 'D' then 'Deleted'
     when conv.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
ifnull(sbx.sedol,'') as sedol,
ifnull(sbx.rcntrycd,'') as sedol_register_country,
ifnull(sbx.curencd,'') as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
ifnull(sbx.bbgcompid,'') as composite_global_id,
ifnull(sbx.bbgcomptk,'') as bloomberg_composite_ticker,
ifnull(sbx.bbgexhid,'') as bloomberg_global_id,
ifnull(sbx.bbgexhtk,'') as bloomberg_exchange_ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
'Exchange Offer' as offer_type,
case when conv.rationew is null or conv.rationew = ''
     then ''
     else concat(conv.rationew,':',conv.ratioold)
     end as ratio,
case when conv.fractions = '' then ''
     when irfractions.lookup is null and conv.fractions <> ''
     then concat('[',conv.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
'price' as rate_type,
conv.price as rate,
conv.curencd as currency,
conv.amountconverted as offer_amount,
conv.fromdate as period_start_date,
conv.todate as period_end_date,
conv.settlementdate as settlement_date,
rd.recdate as record_date,
conv.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
case when ressedol.sedol is null then '' else ressedol.sedol end as resultant_sedol,
case when conv.ressectycd is null then ''
     when secty.securitydescriptor is null and conv.ressectycd <> ''
     then concat('[',conv.ressectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
case when conv.mandoptflag = 'm' then 'Mandatory'
     when conv.mandoptflag = 'o' then 'Voluntary'
     else ''
     end as choice,
case when conv.partfinalflag='p' then 'Part'
     when conv.partfinalflag='f' then 'Final'
     else ''
     end as part_final,
'conv|convid|convnotes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid,
case when scmst.sectycd='BND' then 'Yes' else '' end as fi_prf
from conv
inner join scmst on conv.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join rd on conv.rdid = rd.rdid
left outer join irfractions on conv.fractions = irfractions.code
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
left outer join scmst as resscmst on conv.ressecid = resscmst.secid
left outer join sedol as ressedol on conv.ressecid =ressedol.secid
                   and exchg.cntrycd=ressedol.cntrycd
                   and 'D'<>ressedol.actflag
left outer join secty on conv.ressectycd = secty.sectycd
where
conv.acttime > '2015/01/01'
and (conv.convtype='BB' or conv.convtype='TENDER')
and sectygrp.secgrpid is not null
and (3>sectygrp.secgrpid or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (fromdate < scexh.acttime or scexh.liststatus<>'D' or fromdate is null)
and scexh.actflag<>'D'
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`trading_currency`,`sedol_register_country`,`resultant_sedol`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`
);
drop table if exists wca2.evf_preference_exchange_offer;
rename table wca2.evf_loadingtable to wca2.evf_preference_exchange_offer;
