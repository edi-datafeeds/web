
drop table if exists wca2.sedolbbg;
use wca;
create table wca2.sedolbbg
(
select
main.secid,
main.cntrycd,
main.curencd,
rcntrycd,
case when exchgcd is null then '' else exchgcd end as exchgcd,
sedol,
bbgcompid,
bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk
from sedol as main
inner join scmst on main.secid = scmst.secid and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and (3>sectygrp.secgrpid or 7=sectygrp.secgrpid)
left outer join bbc on main.secid = bbc.secid and main.cntrycd=bbc.cntrycd and main.curencd=bbc.curencd and 'D'<>bbc.actflag
left outer join bbe on main.secid = bbe.secid and main.cntrycd = substring(bbe.exchgcd,1,2) and  main.curencd=bbe.curencd and 'D'<>bbe.actflag
where
main.actflag<>'D'
and main.sedol<>''
and main.cntrycd<>''
and main.cntrycd<>'XS'
and main.cntrycd<>'ZZ'
and main.defunct<>'T'
);
alter table `wca2`.`sedolbbg`
add primary key (`secid`,`cntrycd`,`curencd`,`rcntrycd`,`exchgcd`);
insert into wca2.sedolbbg
select
main.secid,
main.cntrycd,
main.curencd,
'' as rcntrycd,
case when exchgcd is null then '' else exchgcd end as exchgcd,
'' as sedol,
bbgcompid,
bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk
from bbc as main
inner join scmst on main.secid = scmst.secid and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and (3>sectygrp.secgrpid or 7=sectygrp.secgrpid)
left outer join bbe on main.secid = bbe.secid and main.cntrycd = substring(bbe.exchgcd,1,2) and main.curencd=bbe.curencd and 'D'<>bbe.actflag
left outer join sedol on main.secid = sedol.secid and main.cntrycd=sedol.cntrycd and main.curencd=sedol.curencd and 'T'<>sedol.defunct and 'D'<>sedol.actflag and ''<>sedol.sedol
where
main.actflag<>'D'
and sedol.secid is null;
