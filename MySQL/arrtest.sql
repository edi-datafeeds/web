drop table if exists wca2.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'arrangement' as event,
case when optionid is not null 
     then concat(lpad(scexh.scexhid,7,'0'),lpad(optionid,2,'0'),lpad(serialid,2,'0'),rd.rdid)
     else concat(lpad(scexh.scexhid,7,'0'),'0101',rd.rdid) 
     end as caref,
arr.rdid as eventid,
arr.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > arr.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > arr.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > arr.acttime) then pexdt.acttime
     else arr.acttime
     end as changed,
case when arr.actflag = 'U' then 'Updated'
     when arr.actflag = 'I' then 'Inserted'
     when arr.actflag = 'D' then 'Deleted'
     when arr.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as security_type,
scmst.structcd as security_structure,
case when (icc.releventid is null or icc.oldisin = '') then scmst.isin
     else icc.oldisin
     end as isin,
case when (icc.releventid is null or icc.olduscode = '') then scmst.uscode
     else icc.olduscode
     end as us_code,
case when (sdchg.releventid is not null and sdchg.oldsedol <> '') then sdchg.oldsedol
     when sbx.sedol is not null then sbx.sedol
     else ''
     end as sedol,
case when (sdchg.releventid is not null and sdchg.oldsedol <> '') then sdchg.rcntrycd
     when sbx.sedol is not null then sbx.rcntrycd
     else ''
     end as sedol_register_country,
case when (sdchg.releventid is not null and sdchg.oldsedol <> '') then sdchg.oldcurencd
     when sbx.secid is not null then sbx.curencd
     else ''
     end as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
case when (lcc.releventid is null or lcc.oldlocalcode = '') then scexh.localcode
     else lcc.oldlocalcode
     end as local_code,
ifnull(sbx.bbgcompid,'') as composite_global_id,
ifnull(sbx.bbgcomptk,'') as bloomberg_composite_ticker,
case when ifnull(sbx.bbgexhid,'')='' then ifnull(bbe.bbgexhid,'') else ifnull(sbx.bbgexhid,'') end as bloomberg_global_id,
case when ifnull(sbx.bbgexhid,'')='' then ifnull(bbe.bbgexhtk,'') else ifnull(sbx.bbgexhid,'') end as bloomberg_exchange_ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.rdid is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.rdid is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
arr.expcompletiondate as expected_completion_date,
case when pexdt.duebillsredemdate is not null
     then pexdt.duebillsredemdate
     else exdt.duebillsredemdate
     end as duebills_redemption_date,
case when mpay.actflag = 'U' then 'Updated'
     when mpay.actflag = 'I' then 'Inserted'
     when mpay.actflag = 'D' then 'Deleted'
     when mpay.actflag = 'C' then 'Cancelled'
     else '' end as Pay_actflag,
case when optionid is not null 
     then concat(lpad(optionid,2,'0'),',',lpad(serialid,2,'0'))
     else ''
     end as option_key,
case when mpay.paytype is null then ''
     when irpaytype.lookup is null and mpay.actflag <> ''
     then concat('[',mpay.actflag,'] not found')
     else irpaytype.lookup
     end as pay_type,
case when mpay.sectycd is null then ''
     when secty.securitydescriptor is null and mpay.sectycd <> ''
     then concat('[',mpay.sectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
case when mpay.rationew is null or mpay.rationew = ''
     then ''
     else concat(mpay.rationew,':',mpay.ratioold)
     end as ratio,
case when mpay.fractions = '' then ''
     when irfractions.lookup is null and mpay.fractions <> ''
     then concat('[',mpay.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
mpay.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
case when ressedol.sedol is null then '' else ressedol.sedol end as resultant_sedol,
tlelkscmst.isin as Temporary_ISIN,
tlelkscmst.securitydesc as Temporary_Description,
tlelkscexh.localcode as Temporary_Localcode,
' ' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'arr|rdid|arrnotes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid,
ifnull(tlelkscexh.secid,0) as tlelksecid,
rd.rdid,
case when scmst.sectycd='BND' then 'Yes' else '' end as fi_prf
from arr
inner join rd on arr.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
left outer join bbe on scmst.secid=bbe.secid and ''=bbe.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'ARR' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'ARR' = pexdt.eventtype
left outer join icc on arr.rdid = icc.releventid and 'ARR' = icc.eventtype
       and icc.actflag<>'c'
       and icc.actflag<>'d'
left outer join sdchg on arr.rdid = sdchg.releventid and 'ARR' = sdchg.eventtype
       and scexh.secid = sdchg.secid
       and exchg.cntrycd = sdchg.cntrycd
       and sbx.rcntrycd = sdchg.rcntrycd
       and sbx.curencd = sdchg.oldcurencd
       and sdchg.actflag<>'c'
       and sdchg.actflag<>'d'
left outer join lcc on arr.rdid = lcc.releventid and 'ARR' = lcc.eventtype
       and scexh.exchgcd = lcc.exchgcd
       and lcc.actflag<>'c'
       and lcc.actflag<>'d'
left outer join tlelk on 'ARR'=tlelk.eventtype and arr.rdid=tlelk.eventid
left outer join scexh as tlelkscexh on tlelk.secid = tlelkscexh.secid and scexh.exchgcd = tlelkscexh.exchgcd
left outer join scmst as tlelkscmst on tlelkscexh.secid = tlelkscmst.secid
left outer join mpay on arr.rdid = mpay.eventid and 'ARR' = mpay.sevent
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
left outer join sedol as ressedol on mpay.ressecid =ressedol.secid
                   and exchg.cntrycd=ressedol.cntrycd
                   and 'D'<>ressedol.actflag
left outer join secty on mpay.sectycd = secty.sectycd
left outer join irfractions on mpay.fractions = irfractions.code
left outer join irpaytype on mpay.paytype = irpaytype.code
left outer join iraction as iractionmpay on mpay.actflag = iractionmpay.code
where
arr.acttime > '2015/01/01'
and sectygrp.secgrpid is not null
and (3>sectygrp.secgrpid or (wca.scmst.sectycd='BND' and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57 and wca.exchg.cntrycd='US'))
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
and scexh.actflag<>'D'
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`trading_currency`,`sedol_register_country`,`resultant_sedol`,`tlelksecid`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`
);
drop table if exists wca2.evf_arrangement;
rename table wca2.evf_loadingtable to wca2.evf_arrangement;
