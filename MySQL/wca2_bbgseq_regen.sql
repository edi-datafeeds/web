use wca2;
drop table if exists bbgseq;
CREATE TABLE `bbgseq` (
  `autoid` int(11) NOT NULL AUTO_INCREMENT,
  `bbcacttime` datetime DEFAULT NULL,
  `bbeacttime` datetime DEFAULT NULL,
  `secid` int(11) NOT NULL DEFAULT '0',
  `cntrycd` char(2) NOT NULL DEFAULT '',
  `curencd` char(3) NOT NULL DEFAULT '',
  `exchgcd` char(6) NOT NULL DEFAULT '',
  `bbgcompid` varchar(12) DEFAULT NULL,
  `bbgcomptk` varchar(40) DEFAULT NULL,
  `bbgexhid` char(12) DEFAULT NULL,
  `bbgexhtk` varchar(40) DEFAULT NULL,
  `bbcid` int(11) DEFAULT NULL,
  `bbeid` int(11) DEFAULT NULL,
  PRIMARY KEY (`autoid`),
  KEY `ix_bbcid` (`bbcid`),
  KEY `ix_bbeid` (`bbeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `wca2`.`bbgseq` 
ADD UNIQUE INDEX `ix_distinct` (`secid` ASC, `exchgcd` ASC, `curencd` ASC);
use wca;
insert ignore into wca2.bbgseq
select
null,
bbc.acttime,
bbc.acttime,
bbc.secid,
bbc.cntrycd,
bbc.curencd,
bbe.exchgcd,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
bbc.bbcid,
bbe.bbeid
from bbe
inner join scmst on bbe.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
inner join bbc on bbe.secid=bbc.secid
                   and substring(bbe.exchgcd,1,2)=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and 'D'<>ifnull(bbc.actflag,'')
where
bbe.exchgcd<>''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and bbe.actflag<>'D'
and bbe.bbgexhid<>''
and ifnull(bbe.bbgexhid,'')
not in (select ifnull(subbbgseq.bbgexhid,'')
         from wca2.bbgseq as subbbgseq
         where 
         subbbgseq.secid=bbe.secid);
insert ignore into wca2.bbgseq
select
null,
bbc.acttime,
null as acttime,
bbc.secid,
bbc.cntrycd,
bbc.curencd,
scexh.exchgcd,
bbc.bbgcompid,
bbc.bbgcomptk,
'' as bbgexhid,
'' as bbgexhtk,
bbc.bbcid,
null as bbeid
from bbc
inner join scmst on bbc.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join scexh on bbc.secid=scexh.secid
                and bbc.cntrycd=substring(scexh.exchgcd,1,2)
                and 'D'<>scexh.actflag
inner join exchg on scexh.exchgcd=exchg.exchgcd
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
where
scexh.actflag<>'D'
and bbc.actflag<>'D'
and bbc.bbgcompid<>''
and bbc.bbgcompid<>''
and bbc.bbgcomptk<>''
and bbc.cntrycd<>'XG'
and bbc.cntrycd<>'XH'
and bbc.cntrycd<>'XZ'
and scexh.exchgcd<>'CNSZHK'
and scexh.exchgcd<>'CNSGHK'
and scexh.exchgcd<>'HKHKSG'
and scexh.exchgcd<>'HKHKSZ'
and ifnull(bbc.bbgcompid,'')
not in (select ifnull(subbbgseq.bbgcompid,'')
         from wca2.bbgseq as subbbgseq
         where 
         subbbgseq.secid=bbc.secid);

