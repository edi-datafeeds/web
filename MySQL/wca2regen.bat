#@# 01/01/13 | YM |2013010310000011
Start Log= 			O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs Start Regen_MySQL_LIQ i
Start Log= 			O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs Start Regen_MySQL_LIQ_FTP i

# 21/03/2013 - DJ - 2013022710000064 
#----------------------------------------------
Get Workstation name=		powershell.exe O:\AUTO\Scripts\Powershell\IDworkstation.ps1 Regen_MySQL_LIQ i
#----------------------------------------------

#Regeneration WCA2
#------------------------------
Regen WCA2 Sedolbbg_Regen=	O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wca2_sedolbbg_regen.sql Regen_MySQL_LIQ WCA2_Sedolbbg_Regen i
#------------------------------
Web Loading Flag=		O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wca2_Loading.sql Regen_MySQL_LIQ Div_Loading_Flag i
#------------------------------
Regen WCA2 div_regen=		O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wca2_div_regen.sql Regen_MySQL_LIQ WCA2_div_regen i
#------------------------------
Run Div Inc Emailer=		O:\AUTO\Scripts\vbs\GENERIC\Emailer_LIQ.vbs a ann WDI Regen_MySQL_LIQ i
#Run Div Inc Emailer=		O:\AUTO\Scripts\vbs\GENERIC\Emailer_LIQ_20140106.vbs a ann WDI Regen_MySQL_LIQ 1
#------------------------------
Tidy Emailer attachments=	powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a tidy -s O:\AUTO\Apps\Emailers\edi_port_emailer -t htm -logfile Regen_MySQL_LIQ -i y	
#Tidy Emailer attachments=	powershell.exe O:\AUTO\Scripts\Powershell\File_Tidy.ps1 O:\AUTO\Apps\Emailers\edi_port_emailer .htm -i y
#------------------------------
Regen WCA2 adjsos_regen=	O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wca2_adjsos_regen.sql Regen_MySQL_LIQ WCA2_adjsos_regen i
Regen WCA2 ca_Regen=		O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wca2_ca_regen.sql Regen_MySQL_LIQ WCA2_ca_Regen i
#------------------------------
Run WCA Inc Emailer=		O:\AUTO\Scripts\vbs\GENERIC\Emailer_LIQ.vbs a ann WCA Regen_MySQL_LIQ i
#Run WCA Inc Emailer=		O:\AUTO\Scripts\vbs\GENERIC\Emailer_LIQ_20140106.vbs a ann WDI Regen_MySQL_LIQ 1

Tidy Emailer attachments=	powershell.exe O:\AUTO\Scripts\Powershell\File_Handler.ps1 -a tidy -s O:\AUTO\Apps\Emailers\edi_port_emailer -t htm -logfile Regen_MySQL_LIQ -i y	
#Tidy Emailer attachments=	powershell.exe O:\AUTO\Scripts\Powershell\File_Tidy.ps1 O:\AUTO\Apps\Emailers\edi_port_emailer .htm -i y
Web Complete Flag=		O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wca2_Completed.sql Regen_MySQL_LIQ Div_Complete_Flag i
#------------------------------

Web Loading Flag=		O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wfi_Loading.sql Regen_MySQL_LIQ WFI_Loading_Flag i

#------------------------------
Regen WFI Reference=		O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wfi_regen_reference.sql Regen_MySQL_LIQ wfi_regen_reference i
Regen WFI qtable=		O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wfi_regen_qtable.sql Regen_MySQL_LIQ wfi_regen_qtable i
Regen WFI other=		O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wfi_regen_other.sql Regen_MySQL_LIQ wfi_regen_other i
Regen WFI static=		O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wfi_regen_static.sql Regen_MySQL_LIQ wfi_regen_static i
Regen WFI sys_lookup=		O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wfi_regen_sys_lookup.sql Regen_MySQL_LIQ wfi_regen_sys_lookup i
#------------------------------

Web Complete Flag=		O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\wfi_Completed.sql Regen_MySQL_LIQ WFI_Complete_Flag i

#------------------------------

Regen UDR=			O:\AUTO\Scripts\vbs\GENERIC\MySQL32.vbs Liquid_MySql o:\auto\scripts\web\mysql\udr_regen.sql Regen_MySQL_LIQ udr_regen i
#------------------------------

## 08/10/2013 - DJ - 2013100810000012 
#----------------------------------------------
Transfer Ops Log=		O:\AUTO\Apps\DbTrans\DbTrans.exe -s MyDev -ds HAT_MY_Diesel -db AutoOps -tbl opslog -dt acttime
#----------------------------------------------

End Log= 			O:\AUTO\Scripts\vbs\GENERIC\FilesEx.vbs Finish Regen_MySQL_LIQ i