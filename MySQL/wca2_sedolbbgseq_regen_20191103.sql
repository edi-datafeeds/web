use wca2;
drop table if exists sedolbbgseq;
CREATE TABLE `sedolbbgseq` (
  `autoid` int(11) NOT NULL AUTO_INCREMENT,
  `sedolacttime` datetime DEFAULT NULL,
  `bbcacttime` datetime DEFAULT NULL,
  `bbeacttime` datetime DEFAULT NULL,
  `secid` int(11) NOT NULL DEFAULT '0',
  `cntrycd` char(2) NOT NULL DEFAULT '',
  `curencd` char(3) NOT NULL DEFAULT '',
  `rcntrycd` char(2) NOT NULL DEFAULT '',
  `exchgcd` char(6) NOT NULL DEFAULT '',
  `sedol` char(7) NOT NULL DEFAULT '',
  `sedoldefunct` char(1) NOT NULL DEFAULT '',
  `bbgcompid` varchar(12) DEFAULT NULL,
  `bbgcomptk` varchar(40) DEFAULT NULL,
  `bbgexhid` char(12) DEFAULT NULL,
  `bbgexhtk` varchar(40) DEFAULT NULL,
  `sedolid` int(11) DEFAULT NULL,
  `bbcid` int(11) DEFAULT NULL,
  `bbeid` int(11) DEFAULT NULL,
  PRIMARY KEY (`autoid`),
  KEY `ix_sedolid` (`sedolid`),
  KEY `ix_bbcid` (`bbcid`),
  KEY `ix_bbeid` (`bbeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `wca2`.`sedolbbgseq` 
ADD UNIQUE INDEX `ix_distinct` (`secid` ASC, `cntrycd` ASC, `rcntrycd` ASC, `exchgcd` ASC, `curencd` ASC);
use wca;
insert ignore into wca2.sedolbbgseq
select
null,
sedol.acttime,
bbc.acttime,
scexh.acttime,
scexh.secid,
'US' as cntrycd,
'USD' as curencd,
case when sedol.rcntrycd is null then '' else sedol.rcntrycd end as rcntrycd,
scexh.exchgcd,
case when sedol.sedol is null then '' else sedol.sedol end as sedol,
case when sedol.defunct is null then '' else sedol.defunct end as defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
case when bbe.bbgexhid is null then '' else bbe.bbgexhid end as bbgexhid,
case when bbe.bbgexhtk is null then '' else bbe.bbgexhtk end as bbgexhtk,
sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from scexh as scexh
inner join scmst on scexh.secid=scmst.secid
            and 'D'<>scmst.actflag
left outer join sedol on scexh.secid=sedol.secid
            and ('US'=ifnull(sedol.cntrycd,'') or 'ZZ'=ifnull(sedol.cntrycd,''))
            and 'D'<>ifnull(sedol.actflag,'')
            and ''<>ifnull(sedol.sedol,'')
left outer join bbe on scexh.secid=bbe.secid
            and ''<>ifnull(bbe.bbgexhid,'')
            and 'D'<>ifnull(bbe.actflag,'')
left outer join bbc on scexh.secid=bbc.secid
            and 'US'=ifnull(bbc.cntrycd,'')
            and ''<>ifnull(bbc.bbgcompid,'')
            and 'D'<>ifnull(bbc.actflag,'')
where
scexh.actflag<>'D'
and (((wca.scmst.sectycd='BND') 
and ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57)
and (substring(scexh.exchgcd,1,2)='US' and scexh.exchgcd<>'USTRCE' and scexh.exchgcd<>'USBND' and scexh.exchgcd<>'USOTC'))
and ord(substring(uscode,7,1)) between 48 and 57 
and ord(substring(uscode,8,1)) between 48 and 57
and (sedol.sedolid is not null or bbc.bbcid is not null or bbe.bbeid is not null)
and concat(ifnull(sedol.sedolid,''),ifnull(bbc.bbgcompid,''),ifnull(bbe.bbgexhid,''))
not in (select concat(ifnull(subsdseq.sedolid,''),ifnull(subsdseq.bbgcompid,''),ifnull(subsdseq.bbgexhid,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=scexh.secid);
insert ignore into wca2.sedolbbgseq
select
null,
sedol.acttime,
bbc.acttime,
bbe.acttime,
sedol.secid,
sedol.cntrycd,
sedol.curencd,
sedol.rcntrycd,
bbe.exchgcd,
sedol.sedol,
sedol.defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from wca_other.sedol
inner join bbe on sedol.secid=bbe.secid
              and sedol.cntrycd=substring(bbe.exchgcd,1,2)
              and sedol.curencd=bbe.curencd
              and 'D'<>bbe.actflag
              and ''<>bbe.bbgexhid
inner join scmst on sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
left outer join bbc on bbe.secid=bbc.secid
                   and sedol.cntrycd=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and 'D'<>bbc.actflag
where
sedol.actflag<>'D'
and bbe.exchgcd<>''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and sedol.sedol<>''
and sedol.cntrycd<>''
and sedol.cntrycd<>'XS'
and sedol.cntrycd<>'ZZ'
and sedol.rcntrycd<>'XG'
and sedol.rcntrycd<>'XH'
and sedol.rcntrycd<>'XZ';
insert ignore into wca2.sedolbbgseq
select
null,
sedol.acttime,
bbc.acttime,
bbe.acttime,
bbe.secid,
substring(bbe.exchgcd,1,2) as cntrycd,
sedol.curencd,
sedol.rcntrycd,
bbe.exchgcd,
sedol.sedol,
sedol.defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from bbe
inner join sedol on bbe.secid=sedol.secid
            and substring(bbe.exchgcd,1,2)=sedol.cntrycd
            and ''<>ifnull(sedol.curencd,'')
            and 'D'<>ifnull(sedol.actflag,'')
            and ''<>ifnull(sedol.sedol,'')
            and sedol.cntrycd<>'XS'
            and sedol.cntrycd<>'ZZ'
            and sedol.rcntrycd<>'XG'
            and sedol.rcntrycd<>'XH'
            and sedol.rcntrycd<>'XZ'
inner join scmst on bbe.secid=scmst.secid
                and 'D'<>scmst.actflag
left outer join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
left outer join bbc on bbe.secid=bbc.secid
                   and substring(bbe.exchgcd,1,2)=bbc.cntrycd
                   and 'D'<>ifnull(bbc.actflag,'')
where
bbe.curencd=''
and bbe.exchgcd<>''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and bbe.bbgexhid<>''
and bbe.actflag<>'D'
and concat(ifnull(sedol.sedolid,''),ifnull(bbe.bbgexhid,''))
not in (select concat(ifnull(subsdseq.sedolid,''),ifnull(subsdseq.bbgexhid,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=bbe.secid);
insert ignore into wca2.sedolbbgseq
select
null,
sedol.acttime,
bbc.acttime,
bbe.acttime,
sedol.secid,
sedol.cntrycd,
bbe.curencd,
sedol.rcntrycd,
bbe.exchgcd,
sedol.sedol,
sedol.defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
case when bbe.bbgexhid is null then '' else bbe.bbgexhid end as bbgexhid,
case when bbe.bbgexhtk is null then '' else bbe.bbgexhtk end as bbgexhtk,
sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from wca_other.sedol
inner join bbe on sedol.secid=bbe.secid
                   and sedol.cntrycd=substring(bbe.exchgcd,1,2)
                   and ''<>ifnull(bbe.curencd,'')
                   and 'D'<>ifnull(bbe.actflag,'')
inner join scmst on sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
left outer join bbc on bbe.secid=bbc.secid
                   and sedol.cntrycd=ifnull(bbc.cntrycd,'')
                   and 'D'<>ifnull(bbc.actflag,'')
where
sedol.curencd=''
and bbe.exchgcd<>''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and sedol.actflag<>'D'
and sedol.sedol<>''
and sedol.cntrycd<>''
and sedol.cntrycd<>'XS'
and sedol.cntrycd<>'ZZ'
and sedol.rcntrycd<>'XG'
and sedol.rcntrycd<>'XH'
and sedol.rcntrycd<>'XZ'
and concat(ifnull(sedol.sedolid,''),ifnull(bbe.bbgexhid,''))
not in (select concat(ifnull(subsdseq.sedolid,''),ifnull(subsdseq.bbgexhid,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=bbe.secid);
insert ignore into wca2.sedolbbgseq
select
null,
sedol.acttime,
bbc.acttime,
bbe.acttime,
bbe.secid,
substring(bbe.exchgcd,1,2) as cntrycd,
bbe.curencd,
'' as rcntrycd,
bbe.exchgcd,
'' as sedol,
'' as defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from bbe
left outer join sedol on bbe.secid=sedol.secid
            and substring(bbe.exchgcd,1,2)=ifnull(sedol.cntrycd,'')
            and 'D'<>ifnull(sedol.actflag,'')
            and ''<>ifnull(sedol.sedol,'')
            and ifnull(sedol.cntrycd,'')<>'XS'
            and ifnull(sedol.cntrycd,'')<>'ZZ'
            and ifnull(sedol.rcntrycd,'')<>'XG'
            and ifnull(sedol.rcntrycd,'')<>'XH'
            and ifnull(sedol.rcntrycd,'')<>'XZ'
inner join scmst on bbe.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
left outer join bbc on bbe.secid=bbc.secid
                   and substring(bbe.exchgcd,1,2)=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and 'D'<>ifnull(bbc.actflag,'')
where
sedol.sedolid is null
and bbe.exchgcd<>''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and bbe.actflag<>'D'
and bbe.exchgcd<>''
and bbe.bbgexhid<>''
and concat(ifnull(sedol.sedolid,''),ifnull(bbe.bbgexhid,''))
not in (select concat(ifnull(subsdseq.sedolid,''),ifnull(subsdseq.bbgexhid,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=bbe.secid);
insert ignore into wca2.sedolbbgseq
select distinct
null,
sedol.acttime,
bbc.acttime,
bbe.acttime,
sedol.secid,
sedol.cntrycd,
sedol.curencd,
sedol.rcntrycd,
scexh.exchgcd,
sedol.sedol,
sedol.defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
'' as bbgexhid,
'' as bbgexhtk,
sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from wca_other.sedol
left outer join bbe on sedol.secid=bbe.secid
                   and sedol.cntrycd=substring(bbe.exchgcd,1,2)
                   and sedol.curencd=bbe.curencd
                   and 'D'<>ifnull(bbe.actflag,'')
                   and ''<>ifnull(bbe.bbgexhid,'')
left outer join bbc on sedol.secid=bbc.secid
                   and sedol.cntrycd=bbc.cntrycd
                   and sedol.curencd=bbc.curencd
                   and 'D'<>ifnull(bbc.actflag,'')
                   and ''<>ifnull(bbc.bbgcompid,'')
inner join scmst on sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join scexh on sedol.secid=scexh.secid
                and sedol.cntrycd=substring(scexh.exchgcd,1,2)
                and 'D'<>scexh.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
where
sedol.actflag<>'D'
and scexh.exchgcd<>'CNSZHK'
and scexh.exchgcd<>'CNSGHK'
and scexh.exchgcd<>'HKHKSG'
and scexh.exchgcd<>'HKHKSZ'
and bbe.secid is null
and sedol.sedol<>''
and sedol.cntrycd<>''
and sedol.cntrycd<>'XS'
and sedol.cntrycd<>'ZZ'
and sedol.rcntrycd<>'XG'
and sedol.rcntrycd<>'XH'
and sedol.rcntrycd<>'XZ'
and sedol.sedolid
not in (select subsdseq.sedolid
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=sedol.secid);
insert ignore into wca2.sedolbbgseq
select
null,
sedol.acttime,
bbc.acttime,
scexh.acttime,
scexh.secid,
exchg.cntrycd,
case when ifnull(bbc.curencd,'')<>'' then bbc.curencd when ifnull(sedol.curencd,'')<>'' then sedol.curencd else '' end,
case when sedol.rcntrycd is null then '' else sedol.rcntrycd end as rcntrycd,
scexh.exchgcd,
case when sedol.sedol is null then '' else sedol.sedol end as sedol,
case when sedol.defunct is null then '' else sedol.defunct end as defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
'' as bbgexhid,
'' as bbgexhtk,
sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from scexh
inner join scmst on scexh.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join exchg on scexh.exchgcd=exchg.exchgcd
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
left outer join bbc on scexh.secid=bbc.secid
                   and exchg.cntrycd=bbc.cntrycd
                   and 'D'<>ifnull(bbc.actflag,'')
                   and ''<>ifnull(bbc.bbgcompid,'')
left outer join sedol on scexh.secid=sedol.secid
            and exchg.cntrycd=ifnull(sedol.cntrycd,'')
            and bbc.curencd=sedol.curencd
            and 'D'<>ifnull(sedol.actflag,'')
            and ''<>ifnull(sedol.sedol,'')
            and ifnull(sedol.cntrycd,'')<>'XS'
            and ifnull(sedol.cntrycd,'')<>'ZZ'
            and ifnull(sedol.rcntrycd,'')<>'XG'
            and ifnull(sedol.rcntrycd,'')<>'XH'
            and ifnull(sedol.rcntrycd,'')<>'XZ'
left outer join bbe on sedol.secid=bbe.secid
                   and sedol.cntrycd=substring(bbe.exchgcd,1,2)
                   and sedol.curencd=bbe.curencd
                   and 'D'<>ifnull(bbe.actflag,'')
                   and ''<>ifnull(bbe.bbgexhid,'')
where
scexh.actflag<>'D'
and scexh.exchgcd<>'CNSZHK'
and scexh.exchgcd<>'CNSGHK'
and scexh.exchgcd<>'HKHKSG'
and scexh.exchgcd<>'HKHKSZ'
and bbe.bbeid is null
and (bbc.bbcid is not null or sedol.sedolid is not null)
and concat(ifnull(sedol.sedolid,''),ifnull(bbc.bbgcompid,''))
not in (select concat(ifnull(subsdseq.sedolid,''),ifnull(subsdseq.bbgcompid,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=scexh.secid);
insert ignore into wca2.sedolbbgseq
select distinct
null,
sedol.acttime,
bbc.acttime,
bbe.acttime,
bbe.secid,
substring(bbe.exchgcd,1,2) as cntrycd,
bbe.curencd,
'' as rcntrycd,
bbe.exchgcd,
'' as sedol,
'' as defunct,
bbc.bbgcompid,
bbc.bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from bbe
inner join scmst on bbe.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
left outer join bbc on bbe.secid=bbc.secid
                   and substring(bbe.exchgcd,1,2)=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and ''<>ifnull(bbc.bbgcompid,'')
                   and 'D'<>ifnull(bbc.actflag,'')
left outer join sedol on bbe.secid=sedol.secid
            and substring(bbe.exchgcd,1,2)=ifnull(sedol.cntrycd,'')
            and 'D'<>ifnull(sedol.actflag,'')
            and ''<>ifnull(sedol.sedol,'')
            and ifnull(sedol.cntrycd,'')<>'XS'
            and ifnull(sedol.cntrycd,'')<>'ZZ'
            and ifnull(sedol.rcntrycd,'')<>'XG'
            and ifnull(sedol.rcntrycd,'')<>'XH'
            and ifnull(sedol.rcntrycd,'')<>'XZ'
where
sedolid is null
and bbe.exchgcd<>''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and bbe.actflag<>'D'
and bbe.bbgexhtk<>''
and bbe.exchgcd<>''
and bbe.bbgexhid<>''
and concat(ifnull(bbc.bbgcompid,''),ifnull(bbe.bbgexhid,''))
not in (select concat(ifnull(subsdseq.bbgcompid,''),ifnull(subsdseq.bbgexhid,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=scmst.secid);

