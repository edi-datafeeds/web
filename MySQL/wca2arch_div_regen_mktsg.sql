drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2arch.evf_loadingtable(
select distinct
'dividend' as event,
case when optionid is not null
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),div_my.divid)
     else concat(lpad(scexhid,7,'0'),'01',div_my.divid)
     end as caref,
div_my.divid as eventid,
div_my.announcedate as created,
case when (divpy.acttime is not null) and (divpy.acttime > div_my.acttime) and (divpy.acttime > rd.acttime) and (divpy.acttime > exdt.acttime) and (divpy.acttime > pexdt.acttime) then divpy.acttime
     when (rd.acttime is not null) and (rd.acttime > div_my.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > div_my.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > div_my.acttime) then pexdt.acttime
     else div_my.acttime
     end as changed,
case when div_my.actflag = 'U' then 'Updated'
     when div_my.actflag = 'I' then 'Inserted'
     when div_my.actflag = 'D' then 'Deleted'
     when div_my.actflag = 'C' then 'Cancelled'
     when div_my.actflag = 'U' then 'Updated'
     when div_my.actflag = 'I' then 'Inserted'
     when div_my.actflag = 'D' then 'Deleted'
     when div_my.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sbx.secid is not null then sbx.sedol when sbc.secid is not null then sbc.sedol else '' end as sedol,
case when sbx.secid is not null then sbx.rcntrycd when sbc.secid is not null then sbc.rcntrycd else '' end as sedol_register_country,
case when sbx.secid is not null then sbx.curencd when sbc.secid is not null then sbc.curencd else '' end as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
case when sbx.secid is not null then sbx.bbgcompid when sbc.secid is not null then sbc.bbgcompid else '' end as composite_global_id,
case when sbx.secid is not null then sbx.bbgcomptk when sbc.secid is not null then sbc.bbgcomptk else '' end as bloomberg_composite_ticker,
case when sbx.secid is not null then sbx.bbgexhid else '' end as bloomberg_global_id,
case when sbx.secid is not null then sbx.bbgexhtk else '' end as bloomberg_exchange_ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
rd.registrationdate as registration_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
div_my.fyedate as fin_year_date,
case when div_my.divperiodcd= 'mnt' then 'monthly'
     when div_my.divperiodcd= 'sma' then 'semi-annual'
     when div_my.divperiodcd= 'ins' then 'installment'
     when div_my.divperiodcd= 'int' then 'interim'
     when div_my.divperiodcd= 'qtr' then 'quarterly'
     when div_my.divperiodcd= 'fnl' then 'final'
     when div_my.divperiodcd= 'anl' then 'annual'
     when div_my.divperiodcd= 'reg' then 'regular'
     when div_my.divperiodcd= 'un'  then 'unspecified'
     when div_my.divperiodcd= 'bim' then 'bi-monthly'
     when div_my.divperiodcd= 'spl' then 'special'
     when div_my.divperiodcd= 'trm' then 'trimesterly'
     when div_my.divperiodcd= 'mem' then 'memorial'
     when div_my.divperiodcd= 'sup' then 'supplemental'
     when div_my.divperiodcd= 'isc' then 'interest on sgc'
     else ''
     end as dividend_period,
case when div_my.tbaflag= 't' then 'yes'
     else ''
     end as to_be_announced,
case when div_my.nildividend= 't' then 'yes'
     else ''
     end as nil_dividend,
case when optionid is not null 
     then concat(lpad(optionid,2,'0'),',01')
     else ''
     end as option_key,
case when divpy.defaultopt = 't' then 'yes'
     else ''
     end as default_option,
divpy.optelectiondate as option_election_date,
iractiondivpy.lookup as option_status,
case when div_my.nildividend= 'y' then 'nildividend'
     when divpy.divtype= 'b' then 'cash & stock'
     when divpy.divtype= 's' then 'stock'
     when divpy.divtype= 'c' then 'cash'
     else 'unspecified'
     end as dividend_type,
case when divpy.recindcashdiv='t' then '0' else divpy.grossdividend
     end as gross_dividend,
case when divpy.recindcashdiv='t' then '0' else divpy.netdividend
     end as net_dividend,
case when divpy.curencd is null then ''
     else divpy.curencd
     end as currency,
case when divpy.recindcashdiv= 't' then concat('yes: gross was: ',divpy.grossdividend,' net was: ',divpy.netdividend,' as on changed date')
     else ''
     end as cash_dividend_rescinded,
divpy.taxrate as tax_rate,
case when divpy.approxflag= 't' then 'yes'
     else ''
     end as approximate_dividend,
divpy.usdratetocurrency as usd_rate_to_currency,
case when divpy.fractions = '' then ''
     when irfractions.lookup is null and divpy.fractions <> ''
     then concat('[',divpy.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
divpy.coupon as coupon,
divpy.couponid as coupon_id,
divpy.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
'' as resultant_sedol,
case when divpy.rationew is null or divpy.rationew = ''
     then ''
     else concat(divpy.rationew,':',divpy.ratioold)
     end as ratio,
exdt.paydate2 as stock_pay_date,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'div|divid|divnotes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid,
rd.rdid
from div_my
inner join rd on div_my.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid
       and scexh.exchgcd = exdt.exchgcd and 'DIV' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid
       and scmst.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
left outer join divpy on div_my.divid = divpy.divid
left outer join scmst as resscmst on divpy.ressecid = resscmst.secid
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
left outer join wca2.sedolbbg as sbc on scmst.secid = sbc.secid
                   and '' = sbc.exchgcd
                   and exchg.cntrycd = sbc.cntrycd
left outer join irfractions on divpy.fractions = irfractions.code
left outer join iraction as iractiondivpy on divpy.actflag = iractiondivpy.code
where
div_my.acttime < '2011/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2arch`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`trading_currency`,`sedol_register_country`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`);
drop table if exists wca2arch.evf_dividend;
rename table wca2arch.evf_loadingtable to wca2arch.evf_dividend;
