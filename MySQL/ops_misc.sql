INSERT INTO probate.uit2(
	citiCode, 
	mexCode, 
	SEDOL, 
	ISIN, 
	shareName, 
	currency, 
	management, 
	category, 
	innAcc, 
	xdFlag, 
	valuationDate, 
	bidPrice, 
	offerPrice, 
	midPrice, 
	yield, 
	acttime
)
SELECT 
	citiCode, 
	mexCode, 
	SEDOL, 
	ISIN, 
	shareName, 
	currency, 
	management, 
	category, 
	innAcc, 
	xdFlag, 
	CASE WHEN STR_TO_DATE(valuationDate,'%d/%m/%Y') IS NULL THEN
		valuationDate 
	ELSE
		STR_TO_DATE(valuationDate,'%d/%m/%Y')
	End As valuationDate, 
	bidPrice, 
	offerPrice, 
	midPrice, 
	yield, 
	NOW() AS acttime
FROM probate.uit 
	WHERE valuationDate !='0000-00-00' ORDER BY ut_ref ASC