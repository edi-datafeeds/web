drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select
case when qtable.redemdate = '1800/01/01' then null else qtable.redemdate end as redemption_date,
case when mandoptflag='m' then 'mandatory'
     when mandoptflag='o' then 'optional'
     else mandoptflag end as status, 
case when partfinal='p' then 'part'
     when partfinal='f' then 'final'
     else partfinal end as part_or_final, 
case when redemtype.lookup is null then qtable.redemtype else redemtype.lookup end as redemption_type,
case when indef.defaulttype<>'' then 'in default' else '' end as redemption_default,
poolfactor as pool_factor,
redempercent as percentage_redeemed,
qtable.curencd as redemption_currency,
qtable.amountredeemed as amount_redeemed,
qtable.redemprice as redemption_price,
qtable.priceaspercent as price_as_percent,
qtable.redempremium as redemption_premium,
qtable.premiumaspercent as premium_as_percent,
rd.recdate as record_date,
qtable.tenderopendate as tender_open_date,
qtable.tenderclosedate as tender_close_date,
qtable.TenderOfferor as Tender_Offeror,
case when qtable.DutchAuction = 'T' THEN 'True'
     when qtable.DutchAuction = 'F' THEN 'False'	
ELSE qtable.DutchAuction end as Dutch_Auction,
qtable.DutchMinPrice as Dutch_Auction_Min_Price,
qtable.DutchMinAmount as Dutch_Auction_Min_Amount,
qtable.DutchMaxPrice as Dutch_Auction_Max_Price,
qtable.DutchMaxAmount as Dutch_Auction_Max_Amount,
indef.notes as default_notes,
'wca.dbo.redem|redemid|redemnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.redemid as firef,
qtable.secid,
qtable.redemid
from redem as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join rd on qtable.rdid = rd.rdid
left outer join wfi.sys_lookup as redemtype on qtable.redemtype = redemtype.code
                      and 'redemtype' = redemtype.typegroup
left outer join indef on qtable.secid = indef.secid and qtable.redemdate = indef.defaultdate and 'reddefa' = indef.defaulttype and 'd' <> indef.actflag
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and redemtype<>'tender'
and redemtype<>'clean'
and redemtype<>'corr'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.redemption;
rename table wfi.loadingtable to wfi.redemption;
-----------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select
case when qtable.redemdate = '1800/01/01' then null else qtable.redemdate end as redemption_date,
case when mandoptflag='m' then 'mandatory'
     when mandoptflag='o' then 'optional'
     else mandoptflag end as status, 
case when partfinal='p' then 'part'
     when partfinal='f' then 'final'
     else partfinal end as part_or_final, 
case when redemtype.lookup is null then qtable.redemtype else redemtype.lookup end as redemption_type,
case when indef.defaulttype<>'' then 'in default' else '' end as redemption_default,
poolfactor as pool_factor,
redempercent as percentage_redeemed,
qtable.curencd as redemption_currency,
qtable.amountredeemed as amount_redeemed,
qtable.redemprice as redemption_price,
qtable.priceaspercent as price_as_percent,
qtable.redempremium as redemption_premium,
qtable.premiumaspercent as premium_as_percent,
rd.recdate as record_date,
qtable.tenderopendate as tender_open_date,
qtable.tenderclosedate as tender_close_date,
qtable.TenderOfferor as Tender_Offeror,
case when qtable.DutchAuction = 'T' THEN 'True'
     when qtable.DutchAuction = 'F' THEN 'False'	
ELSE qtable.DutchAuction end as Dutch_Auction,
qtable.DutchMinPrice as Dutch_Auction_Min_Price,
qtable.DutchMinAmount as Dutch_Auction_Min_Amount,
qtable.DutchMaxPrice as Dutch_Auction_Max_Price,
qtable.DutchMaxAmount as Dutch_Auction_Max_Amount,
indef.notes as default_notes,
'wca.dbo.redem|redemid|redemnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.redemid as firef,
qtable.secid,
qtable.redemid
from redem as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join rd on qtable.rdid = rd.rdid
left outer join wfi.sys_lookup as redemtype on qtable.redemtype = redemtype.code
                      and 'redemtype' = redemtype.typegroup
left outer join indef on qtable.secid = indef.secid and qtable.redemdate = indef.defaultdate and 'reddefa' = indef.defaulttype and 'd' <> indef.actflag
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and redemtype<>'tender'
and redemtype<>'clean'
and redemtype<>'corr'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.redemption;
rename table wfimatured.loadingtable to wfimatured.redemption;
--
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select
redemptiondate as redemption_date,
case when mandoptflag='m' then 'mandatory'
     when mandoptflag='o' then 'optional'
     else mandoptflag end as status, 
case when partfinal='p' then 'part'
     when partfinal='f' then 'final'
     else partfinal end as part_or_final, 
qtable.curencd as redemption_currency,
qtable.redemptionprice as redemption_price,
qtable.priceaspercent as price_as_percent,
qtable.redemptionpremium as redemption_premium,
qtable.premiumaspercent as premium_as_percent,
qtable.redemptionamount as redemption_amount,
qtable.redeminpercent as redemption_in_percent,
'wca.dbo.redmt|redmtid|redmtnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.redmtid as firef,
qtable.secid,
qtable.redmtid
from redmt as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.redemption_terms;
rename table wfi.loadingtable to wfi.redemption_terms;
----------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select
redemptiondate as redemption_date,
case when mandoptflag='m' then 'mandatory'
     when mandoptflag='o' then 'optional'
     else mandoptflag end as status, 
case when partfinal='p' then 'part'
     when partfinal='f' then 'final'
     else partfinal end as part_or_final, 
qtable.curencd as redemption_currency,
qtable.redemptionprice as redemption_price,
qtable.priceaspercent as price_as_percent,
qtable.redemptionpremium as redemption_premium,
qtable.premiumaspercent as premium_as_percent,
qtable.redemptionamount as redemption_amount,
qtable.redeminpercent as redemption_in_percent,
'wca.dbo.redmt|redmtid|redmtnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.redmtid as firef,
qtable.secid,
qtable.redmtid
from redmt as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.redemption_terms;
rename table wfimatured.loadingtable to wfimatured.redemption_terms;
----------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.effectivedate as effective_date,
qtable.oldoutvalue as old_os_value_in_millions,
qtable.newoutvalue as new_os_value_in_millions,
qtable.oldoutdate as old_outstanding_date,
qtable.newoutdate as new_outstanding_date,
'wca.dbo.bochg|bochgid|bochgnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.bochgid as firef,
qtable.secid,
qtable.bochgid
from bochg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid 
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.outstanding_amount;
rename table wfi.loadingtable to wfi.outstanding_amount;
-----------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.effectivedate as effective_date,
qtable.oldoutvalue as old_os_value_in_millions,
qtable.newoutvalue as new_os_value_in_millions,
qtable.oldoutdate as old_outstanding_date,
qtable.newoutdate as new_outstanding_date,
'wca.dbo.bochg|bochgid|bochgnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.bochgid as firef,
qtable.secid,
qtable.bochgid
from bochg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid 
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.outstanding_amount;
rename table wfimatured.loadingtable to wfimatured.outstanding_amount;
------------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.notificationdate as notification_date,
qtable.oldmaturitydate as old_maturity_date,
qtable.newmaturitydate as new_maturity_date,
case when oldmatbnhmrk.lookup is null then qtable.oldmaturitybenchmark else oldmatbnhmrk.lookup end as old_maturity_benchmark,
case when newmatbnhmrk.lookup is null then qtable.newmaturitybenchmark else newmatbnhmrk.lookup end as new_maturity_benchmark,
'wca.dbo.mtchg|mtchgid|notes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.mtchgid as firef,
qtable.secid,
qtable.mtchgid
from mtchg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldmatbnhmrk on qtable.oldmaturitybenchmark = oldmatbnhmrk.code
                      and 'matbnhmrk' = oldmatbnhmrk.typegroup
left outer join wfi.sys_lookup as newmatbnhmrk on qtable.newmaturitybenchmark = newmatbnhmrk.code
                      and 'matbnhmrk' = newmatbnhmrk.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.maturity_change;
rename table wfi.loadingtable to wfi.maturity_change;
-------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.notificationdate as notification_date,
qtable.oldmaturitydate as old_maturity_date,
qtable.newmaturitydate as new_maturity_date,
case when oldmatbnhmrk.lookup is null then qtable.oldmaturitybenchmark else oldmatbnhmrk.lookup end as old_maturity_benchmark,
case when newmatbnhmrk.lookup is null then qtable.newmaturitybenchmark else newmatbnhmrk.lookup end as new_maturity_benchmark,
'wca.dbo.mtchg|mtchgid|notes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.mtchgid as firef,
qtable.secid,
qtable.mtchgid
from mtchg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldmatbnhmrk on qtable.oldmaturitybenchmark = oldmatbnhmrk.code
                      and 'matbnhmrk' = oldmatbnhmrk.typegroup
left outer join wfi.sys_lookup as newmatbnhmrk on qtable.newmaturitybenchmark = newmatbnhmrk.code
                      and 'matbnhmrk' = newmatbnhmrk.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.maturity_change;
rename table wfimatured.loadingtable to wfimatured.maturity_change;
---------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
qtable.fromdate as from_date,
qtable.todate as to_date,
qtable.rationew as new_ratio,
qtable.ratioold as old_ratio,
qtable.curencd as conversion_currency,
qtable.curpair as currency_pair,
qtable.price as price,
case when mandoptflag='m' then 'mandatory'
     when mandoptflag='o' then 'optional'
     else mandoptflag end as status, 
qtable.ressecid,
resscmst.isin as resultant_isin,
case when sectycd.lookup is null then qtable.sectycd else sectycd.lookup end as resultant_type,
case when fractions.lookup is null then qtable.fractions else fractions.lookup end as fractions,
qtable.fxrate as foreign_exchange_rate,
case when partfinalflag='p' then 'part'
     when partfinalflag='f' then 'final'
     else partfinalflag end as part_final_flag,
qtable.priceaspercent as price_as_percent,
qtable.MinPrice as minimum_price,
qtable.MaxPrice as maximum_price,
'wca.dbo.convt|convtid|convtnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.convtid as firef,
qtable.secid,
qtable.convtid
from convt as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join scmst as resscmst on qtable.ressecid = resscmst.secid
left outer join wfi.sys_lookup as fractions on qtable.fractions = fractions.code
                      and 'fractions' = fractions.typegroup
left outer join wfi.sys_lookup as sectycd on qtable.sectycd = sectycd.code
                      and 'sectycd' = sectycd.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.conversion_terms;
rename table wfi.loadingtable to wfi.conversion_terms;
--------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
qtable.fromdate as from_date,
qtable.todate as to_date,
qtable.rationew as new_ratio,
qtable.ratioold as old_ratio,
qtable.curencd as conversion_currency,
qtable.curpair as currency_pair,
qtable.price as price,
case when mandoptflag='m' then 'mandatory'
     when mandoptflag='o' then 'optional'
     else mandoptflag end as status, 
qtable.ressecid,
resscmst.isin as resultant_isin,
case when sectycd.lookup is null then qtable.sectycd else sectycd.lookup end as resultant_type,
case when fractions.lookup is null then qtable.fractions else fractions.lookup end as fractions,
qtable.fxrate as foreign_exchange_rate,
case when partfinalflag='p' then 'part'
     when partfinalflag='f' then 'final'
     else partfinalflag end as part_final_flag,
qtable.priceaspercent as price_as_percent,
qtable.MinPrice as minimum_price,
qtable.MaxPrice as maximum_price,
'wca.dbo.convt|convtid|convtnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.convtid as firef,
qtable.secid,
qtable.convtid
from convt as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join scmst as resscmst on qtable.ressecid = resscmst.secid
left outer join wfi.sys_lookup as fractions on qtable.fractions = fractions.code
                      and 'fractions' = fractions.typegroup
left outer join wfi.sys_lookup as sectycd on qtable.sectycd = sectycd.code
                      and 'sectycd' = sectycd.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.conversion_terms;
rename table wfimatured.loadingtable to wfimatured.conversion_terms;
----------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
qtable.fromdate as from_date,
qtable.todate as to_date,
qtable.rationew as new_ratio,
qtable.ratioold as old_ratio,
qtable.curencd as conversion_currency,
qtable.curpair as currency_pair,
qtable.price as price,
case when mandoptflag='m' then 'mandatory'
     when mandoptflag='o' then 'optional'
     else mandoptflag end as status, 
qtable.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
case when sectycd.lookup is null then qtable.ressectycd else sectycd.lookup end as resultant_type,
case when fractions.lookup is null then qtable.fractions else fractions.lookup end as fractions,
qtable.fxrate as foreign_exchange_rate,
case when partfinalflag='p' then 'part'
     when partfinalflag='f' then 'final'
     else partfinalflag end as part_final_flag, 
qtable.convtype as conversion_type,
rd.recdate as record_date,
qtable.priceaspercent as price_as_percent,
qtable.amountconverted as amount_converted,
qtable.settlementdate as settlement_date,
'wca.dbo.conv|convid|convnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.convid as firef,
qtable.secid,
qtable.convid
from conv as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join scmst as resscmst on qtable.ressecid = resscmst.secid
left outer join rd on qtable.rdid = rd.rdid
left outer join wfi.sys_lookup as fractions on qtable.fractions = fractions.code
                      and 'fractions' = fractions.typegroup
left outer join wfi.sys_lookup as sectycd on qtable.ressectycd = sectycd.code
                      and 'sectycd' = sectycd.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and convtype<>'tender'
and convtype<>'bb'
and convtype<>'corr'
and convtype<>'clean'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.conversion;
rename table wfi.loadingtable to wfi.conversion;
----------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
qtable.fromdate as from_date,
qtable.todate as to_date,
qtable.rationew as new_ratio,
qtable.ratioold as old_ratio,
qtable.curencd as conversion_currency,
qtable.curpair as currency_pair,
qtable.price as price,
case when mandoptflag='m' then 'mandatory'
     when mandoptflag='o' then 'optional'
     else mandoptflag end as status, 
qtable.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
case when sectycd.lookup is null then qtable.ressectycd else sectycd.lookup end as resultant_type,
case when fractions.lookup is null then qtable.fractions else fractions.lookup end as fractions,
qtable.fxrate as foreign_exchange_rate,
case when partfinalflag='p' then 'part'
     when partfinalflag='f' then 'final'
     else partfinalflag end as part_final_flag, 
qtable.convtype as conversion_type,
rd.recdate as record_date,
qtable.priceaspercent as price_as_percent,
qtable.amountconverted as amount_converted,
qtable.settlementdate as settlement_date,
'wca.dbo.conv|convid|convnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.convid as firef,
qtable.secid,
qtable.convid
from conv as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join scmst as resscmst on qtable.ressecid = resscmst.secid
left outer join rd on qtable.rdid = rd.rdid
left outer join wfi.sys_lookup as fractions on qtable.fractions = fractions.code
                      and 'fractions' = fractions.typegroup
left outer join wfi.sys_lookup as sectycd on qtable.ressectycd = sectycd.code
                      and 'sectycd' = sectycd.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and convtype<>'tender'
and convtype<>'bb'
and convtype<>'corr'
and convtype<>'clean'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.conversion;
rename table wfimatured.loadingtable to wfimatured.conversion;
-----------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
qtable.effectivedate as effective_date,
case when oldintaccrual.lookup is null then qtable.oldinterestaccrualconvention else oldintaccrual.lookup end as old_accrual_convention,
case when newintaccrual.lookup is null then qtable.newinterestaccrualconvention else newintaccrual.lookup end as new_accrual_convention,
case when oldcmethod.lookup is null then qtable.oldconvmethod else oldcmethod.lookup end as old_convention_method,
case when newcmethod.lookup is null then qtable.newconvmethod else newcmethod.lookup end as new_convention_method,
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
'wca.dbo.rconv|rconvid|notes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.rconvid as firef,
qtable.secid,
qtable.rconvid
from rconv as qtable
inner join bond on qtable.secid = bond .secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as oldintaccrual on qtable.oldinterestaccrualconvention = oldintaccrual.code
                      and 'intaccrual' = oldintaccrual.typegroup
left outer join wfi.sys_lookup as newintaccrual on qtable.newinterestaccrualconvention = newintaccrual.code
                      and 'intaccrual' = newintaccrual.typegroup
left outer join wfi.sys_lookup as oldcmethod on qtable.oldconvmethod = oldcmethod.code
                      and 'cmethod' = oldcmethod.typegroup
left outer join wfi.sys_lookup as newcmethod on qtable.newconvmethod = newcmethod.code
                      and 'cmethod' = newcmethod.typegroup
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.reconvention;
rename table wfi.loadingtable to wfi.reconvention;
----------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
qtable.effectivedate as effective_date,
case when oldintaccrual.lookup is null then qtable.oldinterestaccrualconvention else oldintaccrual.lookup end as old_accrual_convention,
case when newintaccrual.lookup is null then qtable.newinterestaccrualconvention else newintaccrual.lookup end as new_accrual_convention,
case when oldcmethod.lookup is null then qtable.oldconvmethod else oldcmethod.lookup end as old_convention_method,
case when newcmethod.lookup is null then qtable.newconvmethod else newcmethod.lookup end as new_convention_method,
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
'wca.dbo.rconv|rconvid|notes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.rconvid as firef,
qtable.secid,
qtable.rconvid
from rconv as qtable
inner join bond on qtable.secid = bond .secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as oldintaccrual on qtable.oldinterestaccrualconvention = oldintaccrual.code
                      and 'intaccrual' = oldintaccrual.typegroup
left outer join wfi.sys_lookup as newintaccrual on qtable.newinterestaccrualconvention = newintaccrual.code
                      and 'intaccrual' = newintaccrual.typegroup
left outer join wfi.sys_lookup as oldcmethod on qtable.oldconvmethod = oldcmethod.code
                      and 'cmethod' = oldcmethod.typegroup
left outer join wfi.sys_lookup as newcmethod on qtable.newconvmethod = newcmethod.code
                      and 'cmethod' = newcmethod.typegroup
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.reconvention;
rename table wfimatured.loadingtable to wfimatured.reconvention;
-----------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
qtable.effectivedate as effective_date,
qtable.olddenomination1 as old_denom1,
qtable.olddenomination2 as old_denom2,
qtable.olddenomination3 as old_denom3,
qtable.olddenomination4 as old_denom4,
qtable.olddenomination5 as old_denom5,
qtable.olddenomination6 as old_denom6,
qtable.olddenomination7 as old_denom7,
qtable.oldminimumdenomination as old_min_denom,
qtable.olddenominationmultiple as old_denom_multiple,
qtable.newdenomination1 as new_denom1,
qtable.newdenomination2 as new_denom2,
qtable.newdenomination3 as new_denom3,
qtable.newdenomination4 as new_denom4,
qtable.newdenomination5 as new_denom5,
qtable.newdenomination6 as new_denom6,
qtable.newdenomination7 as new_denom7,
qtable.newminimumdenomination as new_min_denom,
qtable.newdenominationmultiple as new_denom_multiple,
'wca.dbo.rdnom|rdnomid|notes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.rdnomid as firef,
qtable.secid,
qtable.rdnomid
from rdnom as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.redenomination;
rename table wfi.loadingtable to wfi.redenomination;
------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
qtable.effectivedate as effective_date,
qtable.olddenomination1 as old_denom1,
qtable.olddenomination2 as old_denom2,
qtable.olddenomination3 as old_denom3,
qtable.olddenomination4 as old_denom4,
qtable.olddenomination5 as old_denom5,
qtable.olddenomination6 as old_denom6,
qtable.olddenomination7 as old_denom7,
qtable.oldminimumdenomination as old_min_denom,
qtable.olddenominationmultiple as old_denom_multiple,
qtable.newdenomination1 as new_denom1,
qtable.newdenomination2 as new_denom2,
qtable.newdenomination3 as new_denom3,
qtable.newdenomination4 as new_denom4,
qtable.newdenomination5 as new_denom5,
qtable.newdenomination6 as new_denom6,
qtable.newdenomination7 as new_denom7,
qtable.newminimumdenomination as new_min_denom,
qtable.newdenominationmultiple as new_denom_multiple,
'wca.dbo.rdnom|rdnomid|notes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.rdnomid as firef,
qtable.secid,
qtable.rdnomid
from rdnom as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.redenomination;
rename table wfimatured.loadingtable to wfimatured.redenomination;
--------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
qtable.effectivedate as effective_date,
qtable.oldinterestrate as old_interest_rate,
qtable.newinterestrate as new_interest_rate,
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
'wca.dbo.irchg|irchgid|notes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.irchgid as firef,
qtable.secid,
qtable.irchgid
from irchg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.int_rate_change;
rename table wfi.loadingtable to wfi.int_rate_change;
-------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
qtable.effectivedate as effective_date,
qtable.oldinterestrate as old_interest_rate,
qtable.newinterestrate as new_interest_rate,
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
'wca.dbo.irchg|irchgid|notes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.irchgid as firef,
qtable.secid,
qtable.irchgid
from irchg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.int_rate_change;
rename table wfimatured.loadingtable to wfimatured.int_rate_change;
--------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
qtable.fromdate as from_date,
qtable.todate as to_date,
qtable.rationew as new_ratio,
qtable.ratioold as old_ratio,
qtable.curencd as conversion_currency,
qtable.curpair as currency_pair,
qtable.price,
case when mandoptflag='m' then 'mandatory'
     when mandoptflag='o' then 'optional'
     else mandoptflag end as status, 
qtable.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
case when sectycd.lookup is null then qtable.ressectycd else sectycd.lookup end as resultant_type,
case when fractions.lookup is null then qtable.fractions else fractions.lookup end as fractions,
qtable.fxrate as foreign_exchange_rate,
case when partfinalflag='p' then 'part'
     when partfinalflag='f' then 'final'
     else partfinalflag end as part_final_flag, 
rd.recdate as record_date,
qtable.priceaspercent as price_as_percent,
qtable.amountconverted as amount_converted,
qtable.settlementdate as settlement_date,
'wca.dbo.conv|convid|convnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.convid as firef,
qtable.secid,
qtable.convid
from conv as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join scmst as resscmst on qtable.ressecid = resscmst.secid
left outer join rd on qtable.rdid = rd.rdid
left outer join wfi.sys_lookup as fractions on qtable.fractions = fractions.code
                      and 'fractions' = fractions.typegroup
left outer join wfi.sys_lookup as sectycd on qtable.ressectycd = sectycd.code
                      and 'sectycd' = sectycd.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and convtype='bb'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.exchange_offer;
rename table wfi.loadingtable to wfi.exchange_offer;
------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
qtable.fromdate as from_date,
qtable.todate as to_date,
qtable.rationew as new_ratio,
qtable.ratioold as old_ratio,
qtable.curencd as conversion_currency,
qtable.curpair as currency_pair,
qtable.price,
case when mandoptflag='m' then 'mandatory'
     when mandoptflag='o' then 'optional'
     else mandoptflag end as status, 
qtable.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
case when sectycd.lookup is null then qtable.ressectycd else sectycd.lookup end as resultant_type,
case when fractions.lookup is null then qtable.fractions else fractions.lookup end as fractions,
qtable.fxrate as foreign_exchange_rate,
case when partfinalflag='p' then 'part'
     when partfinalflag='f' then 'final'
     else partfinalflag end as part_final_flag, 
rd.recdate as record_date,
qtable.priceaspercent as price_as_percent,
qtable.amountconverted as amount_converted,
qtable.settlementdate as settlement_date,
'wca.dbo.conv|convid|convnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.convid as firef,
qtable.secid,
qtable.convid
from conv as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join scmst as resscmst on qtable.ressecid = resscmst.secid
left outer join rd on qtable.rdid = rd.rdid
left outer join wfi.sys_lookup as fractions on qtable.fractions = fractions.code
                      and 'fractions' = fractions.typegroup
left outer join wfi.sys_lookup as sectycd on qtable.ressectycd = sectycd.code
                      and 'sectycd' = sectycd.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and convtype='bb'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.exchange_offer;
rename table wfimatured.loadingtable to wfimatured.exchange_offer;
---------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
qtable.effectivedate as effective_date,
qtable.oldfromdate as old_from_date,
qtable.newfromdate as new_from_date,
qtable.oldtodate as old_to_date,
qtable.newtodate as new_to_date,
qtable.oldresultantratio as old_resultant_ratio,
qtable.newresultantratio as new_resultant_ratio,
qtable.oldsecurityratio as old_security_ratio,
qtable.newsecurityratio as new_security_ratio,
qtable.oldcurrency as old_currency,
qtable.newcurrency as new_currency,
qtable.oldcurpair as old_currency_pair,
qtable.newcurpair as new_currency_pair,
qtable.oldconversionprice as old_conversionprice,
qtable.newconversionprice as new_conversionprice,
qtable.oldressecid as old_resultant_secid,
oldres.isin as old_resultant_isin,
qtable.newressecid as new_resultant_secid,
newres.isin as new_resultant_isin,
case when sectycd.lookup is null then qtable.ressectycd else sectycd.lookup end as resultant_type,
qtable.oldfxrate as old_foreign_exchange_rate,
qtable.newfxrate as new_foreign_exchange_rate,
qtable.oldpriceaspercent as old_price_as_percent,
qtable.newpriceaspercent as new_price_as_percent,
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.releventid as related_eventid,
qtable.convtid as conversion_terms_id,
'wca.dbo.ctchg|ctchgid|notes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.ctchgid as firef,
qtable.secid,
qtable.ctchgid
from ctchg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join scmst as oldres on qtable.oldressecid = oldres.secid
left outer join scmst as newres on qtable.newressecid = newres.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as sectycd on qtable.ressectycd = sectycd.code
                      and 'sectycd' = sectycd.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.conversion_terms_change;
rename table wfi.loadingtable to wfi.conversion_terms_change;
----------------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
qtable.effectivedate as effective_date,
qtable.oldfromdate as old_from_date,
qtable.newfromdate as new_from_date,
qtable.oldtodate as old_to_date,
qtable.newtodate as new_to_date,
qtable.oldresultantratio as old_resultant_ratio,
qtable.newresultantratio as new_resultant_ratio,
qtable.oldsecurityratio as old_security_ratio,
qtable.newsecurityratio as new_security_ratio,
qtable.oldcurrency as old_currency,
qtable.newcurrency as new_currency,
qtable.oldcurpair as old_currency_pair,
qtable.newcurpair as new_currency_pair,
qtable.oldconversionprice as old_conversionprice,
qtable.newconversionprice as new_conversionprice,
qtable.oldressecid as old_resultant_secid,
oldres.isin as old_resultant_isin,
qtable.newressecid as new_resultant_secid,
newres.isin as new_resultant_isin,
case when sectycd.lookup is null then qtable.ressectycd else sectycd.lookup end as resultant_type,
qtable.oldfxrate as old_foreign_exchange_rate,
qtable.newfxrate as new_foreign_exchange_rate,
qtable.oldpriceaspercent as old_price_as_percent,
qtable.newpriceaspercent as new_price_as_percent,
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.releventid as related_eventid,
qtable.convtid as conversion_terms_id,
'wca.dbo.ctchg|ctchgid|notes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.ctchgid as firef,
qtable.secid,
qtable.ctchgid
from ctchg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join scmst as oldres on qtable.oldressecid = oldres.secid
left outer join scmst as newres on qtable.newressecid = newres.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as sectycd on qtable.ressectycd = sectycd.code
                      and 'sectycd' = sectycd.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.conversion_terms_change;
rename table wfimatured.loadingtable to wfimatured.conversion_terms_change;
-------------------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
rd.recdate as record_date,
qtable.offerorname as offeror_name,
case when qtable.hostile='t' then 'yes'
     when qtable.hostile='f' then 'no'
     else qtable.hostile end as hostile, 
qtable.opendate as open_date,
qtable.closedate as close_date,
case when tkovrstat.lookup is null then qtable.tkovrstatus else tkovrstat.lookup end as takeover_status,
qtable.preofferqty as pre_offer_quantity,
qtable.preofferpercent as pre_offer_percent,
qtable.targetquantity as target_quantity,
qtable.targetpercent as target_percent,
qtable.unconditionaldate as unconditional_date,
qtable.cmacqdate as compulsory_acquisition_date,
qtable.minacpqty as min_acceptance_quantity,
qtable.maxacpqty as max_acceptance_quantity,
case when qtable.minitkovr='t' then 'yes'
     when qtable.minitkovr='f' then 'no'
     else qtable.minitkovr end as mini_takeover, 
'wca.dbo.tkovr|tkovrid|tkovrnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.tkovrid as firef,
qtable.secid,
qtable.tkovrid
from tkovr as qtable 
left outer join rd on qtable.rdid = rd.rdid
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as tkovrstat on qtable.tkovrstatus = tkovrstat.code
                      and 'tkovrstat' = tkovrstat.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.takeover;
rename table wfi.loadingtable to wfi.takeover;
--------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
rd.recdate as record_date,
qtable.offerorname as offeror_name,
case when qtable.hostile='t' then 'yes'
     when qtable.hostile='f' then 'no'
     else qtable.hostile end as hostile, 
qtable.opendate as open_date,
qtable.closedate as close_date,
case when tkovrstat.lookup is null then qtable.tkovrstatus else tkovrstat.lookup end as takeover_status,
qtable.preofferqty as pre_offer_quantity,
qtable.preofferpercent as pre_offer_percent,
qtable.targetquantity as target_quantity,
qtable.targetpercent as target_percent,
qtable.unconditionaldate as unconditional_date,
qtable.cmacqdate as compulsory_acquisition_date,
qtable.minacpqty as min_acceptance_quantity,
qtable.maxacpqty as max_acceptance_quantity,
case when qtable.minitkovr='t' then 'yes'
     when qtable.minitkovr='f' then 'no'
     else qtable.minitkovr end as mini_takeover, 
'wca.dbo.tkovr|tkovrid|tkovrnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.tkovrid as firef,
qtable.secid,
qtable.tkovrid
from tkovr as qtable 
left outer join rd on qtable.rdid = rd.rdid
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as tkovrstat on qtable.tkovrstatus = tkovrstat.code
                      and 'tkovrstat' = tkovrstat.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.takeover;
rename table wfimatured.loadingtable to wfimatured.takeover;
---------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
qtable.effectivedate as effective_date,
qtable.oldcurencd as old_redemption_currency,
qtable.newcurencd as new_redemption_currency,
qtable.oldparvalue as old_par_value,
qtable.newparvalue as new_par_value,
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
'wca.dbo.currd|currdid|currdnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.currdid as firef,
qtable.secid,
qtable.currdid
from currd as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.cur_redenomination;
rename table wfi.loadingtable to wfi.cur_redenomination;
-----------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
qtable.effectivedate as effective_date,
qtable.oldcurencd as old_redemption_currency,
qtable.newcurencd as new_redemption_currency,
qtable.oldparvalue as old_par_value,
qtable.newparvalue as new_par_value,
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
'wca.dbo.currd|currdid|currdnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.currdid as firef,
qtable.secid,
qtable.currdid
from currd as qtable  
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.cur_redenomination;
rename table wfimatured.loadingtable to wfimatured.cur_redenomination;
-------------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
rd.recdate as record_date,
case when onoffflag.lookup is null then qtable.onoffflag else onoffflag.lookup end as on_off_market,
qtable.startdate as start_date,
qtable.enddate as end_date,
qtable.minacpqty as min_acceptance_quantity,
qtable.maxacpqty as max_acceptance_quantity,
qtable.bbminpct as min_percent,
qtable.bbmaxpct as max_percent,
'wca.dbo.bb|bbid|bbnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.bbid as firef,
qtable.secid,
qtable.bbid
from bb as qtable
left outer join rd on qtable.rdid = rd.rdid
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as onoffflag on qtable.onoffflag = onoffflag.code
                      and 'onoffflag' = onoffflag.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.buy_back;
rename table wfi.loadingtable to wfi.buy_back;
----------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
rd.recdate as record_date,
case when onoffflag.lookup is null then qtable.onoffflag else onoffflag.lookup end as on_off_market,
qtable.startdate as start_date,
qtable.enddate as end_date,
qtable.minacpqty as min_acceptance_quantity,
qtable.maxacpqty as max_acceptance_quantity,
qtable.bbminpct as min_percent,
qtable.bbmaxpct as max_percent,
'wca.dbo.bb|bbid|bbnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.bbid as firef,
qtable.secid,
qtable.bbid
from bb as qtable
left outer join rd on qtable.rdid = rd.rdid
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as onoffflag on qtable.onoffflag = onoffflag.code
                      and 'onoffflag' = onoffflag.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.buy_back;
rename table wfimatured.loadingtable to wfimatured.buy_back;
---------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
qtable.effectivedate as effective_date,
qtable.curencd as redemption_currency,
qtable.oldparvalue as old_par_value,
qtable.newparvalue as new_par_value,
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
'wca.dbo.pvrd|pvrdid|pvrdnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.pvrdid as firef,
qtable.secid,
qtable.pvrdid
from pvrd as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid =scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfi`.loadingtable
add primary key (`firef`),
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.pv_redenomination;
rename table wfi.loadingtable to wfi.pv_redenomination;
----------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
qtable.effectivedate as effective_date,
qtable.curencd as redemption_currency,
qtable.oldparvalue as old_par_value,
qtable.newparvalue as new_par_value,
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
'wca.dbo.pvrd|pvrdid|pvrdnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.pvrdid as firef,
qtable.secid,
qtable.pvrdid
from pvrd as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid =scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.pv_redenomination;
rename table wfimatured.loadingtable to wfimatured.pv_redenomination;
----------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select
qtable.notificationdate as notification_date,
qtable.oldintpayfrqncy as old_interest_pay_freq,
qtable.oldintpaydate1 as old_payddmm_1,
qtable.oldintpaydate2 as old_payddmm_2,
qtable.oldintpaydate3 as old_payddmm_3,
qtable.oldintpaydate4 as old_payddmm_4,
qtable.newintpayfrqncy as new_interest_pay_freq,
qtable.newintpaydate1 as new_payddmm_1,
qtable.newintpaydate2 as new_payddmm_2,
qtable.newintpaydate3 as new_payddmm_3,
qtable.newintpaydate4 as new_payddmm_4,
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.oldvarintpaydate as old_var_interest_pay_date,
qtable.newvarintpaydate as new_var_interest_pay_date,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.ifchgid as firef,
qtable.secid,
qtable.ifchgid
from ifchg as qtable
inner join  bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as oldfreq on qtable.oldintpayfrqncy = oldfreq.code
                      and 'freq' = oldfreq.typegroup
left outer join wfi.sys_lookup as newfreq on qtable.newintpayfrqncy = newfreq.code
                      and 'freq' = newfreq.typegroup
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and bond.actflag<>'D'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.int_freq_change;
rename table wfi.loadingtable to wfi.int_freq_change;
--------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select
qtable.notificationdate as notification_date,
qtable.oldintpayfrqncy as old_interest_pay_freq,
qtable.oldintpaydate1 as old_payddmm_1,
qtable.oldintpaydate2 as old_payddmm_2,
qtable.oldintpaydate3 as old_payddmm_3,
qtable.oldintpaydate4 as old_payddmm_4,
qtable.newintpayfrqncy as new_interest_pay_freq,
qtable.newintpaydate1 as new_payddmm_1,
qtable.newintpaydate2 as new_payddmm_2,
qtable.newintpaydate3 as new_payddmm_3,
qtable.newintpaydate4 as new_payddmm_4,
case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.oldvarintpaydate as old_var_interest_pay_date,
qtable.newvarintpaydate as new_var_interest_pay_date,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.ifchgid as firef,
qtable.secid,
qtable.ifchgid
from ifchg as qtable
inner join  bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as oldfreq on qtable.oldintpayfrqncy = oldfreq.code
                      and 'freq' = oldfreq.typegroup
left outer join wfi.sys_lookup as newfreq on qtable.newintpayfrqncy = newfreq.code
                      and 'freq' = newfreq.typegroup
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and bond.actflag<>'D'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.int_freq_change;
rename table wfimatured.loadingtable to wfimatured.int_freq_change;
----------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select
agncy.RegistrarName as Agency_Name,
agncy.CntryCD as Agency_Country,
case when RELATION.Lookup is null then qtable.Relationship else RELATION.Lookup end as Relationship,
case when GTYPE.Lookup is null then qtable.GuaranteeType else GTYPE.Lookup end as Guarantee_Type,
qtable.SpStartDate as Surety_Start_Date,
qtable.SpEndDate as Surety_End_Date,
qtable.Actflag,
qtable.Acttime as changed,
qtable.AnnounceDate as created,
qtable.ScagyID as firef,
qtable.secid
from scagy as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join agncy on qtable.agncyid = agncy.agncyid
left outer join wfi.sys_lookup as RELATION on qtable.relationship = RELATION.Code
                      and 'RELATION' = RELATION.TypeGroup
left outer join wfi.sys_lookup as GTYPE on qtable.GuaranteeType = GTYPE.Code
                      and 'GTYPE' = GTYPE.TypeGroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfi.agency;
rename table wfi.loadingtable to wfi.agency;
---------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select
agncy.RegistrarName as Agency_Name,
agncy.CntryCD as Agency_Country,
case when RELATION.Lookup is null then qtable.Relationship else RELATION.Lookup end as Relationship,
case when GTYPE.Lookup is null then qtable.GuaranteeType else GTYPE.Lookup end as Guarantee_Type,
qtable.SpStartDate as Surety_Start_Date,
qtable.SpEndDate as Surety_End_Date,
qtable.Actflag,
qtable.Acttime as changed,
qtable.AnnounceDate as created,
qtable.ScagyID as firef,
qtable.secid
from scagy as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join agncy on qtable.agncyid = agncy.agncyid
left outer join wfi.sys_lookup as RELATION on qtable.relationship = RELATION.Code
                      and 'RELATION' = RELATION.TypeGroup
left outer join wfi.sys_lookup as GTYPE on qtable.GuaranteeType = GTYPE.Code
                      and 'GTYPE' = GTYPE.TypeGroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfimatured.agency;
rename table wfimatured.loadingtable to wfimatured.agency;
--------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select
cntr.CntryCD as Center_Country,
cntr.CentreName as Center_Name,
case when qtable.bdcappliedto='I' then 'Interest' when qtable.bdcappliedto='M' then 'Maturity' else qtable.bdcappliedto end as Applied_To,
'BDC|BdcID|Notes' as Link_Notes,
qtable.Actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.BdcID as firef,
qtable.secid,
qtable.BdcID
from bdc as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join cntr on qtable.cntrid = cntr.cntrid
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfi.center;
rename table wfi.loadingtable to wfi.center;
---------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select
cntr.CntryCD as Center_Country,
cntr.CentreName as Center_Name,
case when qtable.bdcappliedto='I' then 'Interest' when qtable.bdcappliedto='M' then 'Maturity' else qtable.bdcappliedto end as Applied_To,
'BDC|BdcID|Notes' as Link_Notes,
qtable.Actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.BdcID as firef,
qtable.secid,
qtable.BdcID
from bdc as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join cntr on qtable.cntrid = cntr.cntrid
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfimatured.center;
rename table wfimatured.loadingtable to wfimatured.center;
--------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select
qtable.cntrycd as restriction_country,
'selrt|selrtid|restriction' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.selrtid as firef,
qtable.secid,
qtable.selrtid
from selrt as qtable
inner join bond on qtable.secid =bond.secid
inner join scmst on bond.secid = scmst.secid
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfi.selling_restriction;
rename table wfi.loadingtable to wfi.selling_restriction;
--------------------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select
qtable.cntrycd as restriction_country,
'selrt|selrtid|restriction' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.selrtid as firef,
qtable.secid,
qtable.selrtid
from selrt as qtable
inner join bond on qtable.secid =bond.secid
inner join scmst on bond.secid = scmst.secid
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfimatured.selling_restriction;
rename table wfimatured.loadingtable to wfimatured.selling_restriction;
--------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select
case when qtable.callput = 'c' then 'call' when qtable.callput = 'p' then 'put' else qtable.callput end as call_or_put,
cptype as call_style,
case when qtable.mandatoryoptional='m' then 'mandatory'
     when qtable.mandatoryoptional='o' then 'optional'
     else qtable.mandatoryoptional end as status, 
qtable.currency as schedule_currency, 
qtable.price as schedule_price,
qtable.priceaspercent as price_as_percent,
qtable.fromdate as from_date,
qtable.todate as to_date,
qtable.noticefrom as notice_from,
qtable.noticeto as notice_to,
qtable.minnoticedays as minimum_notice_days,
qtable.maxnoticedays as maximum_notice_days,
'wca.dbo.cpopn|cpopnid|notes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.cpoptid as firef,
qtable.secid,
cpopn.cpopnid
from cpopt as qtable
left outer join cpopn on qtable.secid = cpopn.secid and qtable.callput = cpopn.callput
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfi.schedule;
rename table wfi.loadingtable to wfi.schedule;
--------------------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select
case when qtable.callput = 'c' then 'call' when qtable.callput = 'p' then 'put' else qtable.callput end as call_or_put,
cptype as call_style,
case when qtable.mandatoryoptional='m' then 'mandatory'
     when qtable.mandatoryoptional='o' then 'optional'
     else qtable.mandatoryoptional end as status, 
qtable.currency as schedule_currency, 
qtable.price as schedule_price,
qtable.priceaspercent as price_as_percent,
qtable.fromdate as from_date,
qtable.todate as to_date,
qtable.noticefrom as notice_from,
qtable.noticeto as notice_to,
qtable.minnoticedays as minimum_notice_days,
qtable.maxnoticedays as maximum_notice_days,
'wca.dbo.cpopn|cpopnid|notes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.cpoptid as firef,
qtable.secid,
cpopn.cpopnid
from cpopt as qtable
left outer join cpopn on qtable.secid = cpopn.secid and qtable.callput = cpopn.callput
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfimatured.schedule;
rename table wfimatured.loadingtable to wfimatured.schedule;
--------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select
qtable.shochid as event_id,
-- case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.oldsos as old_shares_outstanding,
qtable.newsos as new_shares_outstanding,
qtable.effectivedate as effectivedate,
-- qtable.shochnotes as shochnotes,
-- qtable.releventid as releventid,
-- qtable.oldoutstandingdate,
-- qtable.newoutstandingdate,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.shochid as firef,
qtable.secid,
qtable.shochid
from wca.shoch as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfi.shares_outstanding_change;
rename table wfi.loadingtable to wfi.shares_outstanding_change;
--------------------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select
qtable.shochid as event_id,
-- case when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.oldsos as old_shares_outstanding,
qtable.newsos as new_shares_outstanding,
qtable.effectivedate as effectivedate,
-- qtable.shochnotes,
-- qtable.releventid,
-- qtable.oldoutstandingdate,
-- qtable.newoutstandingdate,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.shochid as firef,
qtable.secid,
qtable.shochid
from wca.shoch as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfimatured.shares_outstanding_change;
rename table wfimatured.loadingtable to wfimatured.shares_outstanding_change;
--------------------------------------------------------------------

drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select distinct
qtable.divid as event_id,
case when qtable.nildividend= 'y' then 'nildividend'
     when divpy.divtype= 'b' then 'cash & stock'
     when divpy.divtype= 's' then 'stock'
     when divpy.divtype= 'c' then 'cash'
     else 'unspecified'
     end as dividend_type,

case when divpy.recindcashdiv='t' then '0' else divpy.grossdividend
     end as gross_dividend,
     
     case when divpy.recindcashdiv='t' then '0' else divpy.netdividend
     end as net_dividend,
     
 divpy.taxrate as tax_rate, 
 
 case when divpy.curencd is null then ''
     else divpy.curencd
     end as curencd,
     
 rd.recdate as record_date,    


case when divpy.divtype= 's' and exdt.rdid is not null and exdt.acttime>'2014-07-21'
     then exdt.exdate2
     when divpy.divtype= 's' and pexdt.rdid is not null and pexdt.acttime>'2014-07-21'
     then pexdt.exdate2
     when exdt.rdid is not null
     then exdt.exdate
     else pexdt.exdate
     end as ex_date,
     
case when divpy.divtype= 's' and exdt.rdid is not null and exdt.acttime>'2014-07-21'
     then exdt.paydate2
     when divpy.divtype= 's' and pexdt.rdid is not null and pexdt.acttime>'2014-07-21'
     then pexdt.paydate2
     when exdt.rdid is not null
     then exdt.paydate
     else pexdt.paydate
     end as pay_date,
     
     
  case 
     
     when qtable.frequency= 'mnt' then 'monthly'
     when qtable.frequency= 'sma' then 'semi-annual'
     when qtable.frequency= 'ins' then 'installment'
     when qtable.frequency= 'int' then 'interim'
     when qtable.frequency= 'qtr' then 'quarterly'
     when qtable.frequency= 'fnl' then 'final'
     when qtable.frequency= 'anl' then 'annual'
     when qtable.frequency= 'reg' then 'regular'
     when qtable.frequency= 'un'  then 'unspecified'
     when qtable.frequency= 'bim' then 'bi-monthly'
     when qtable.frequency= 'spl' then 'special'
     when qtable.frequency= 'trm' then 'trimesterly'
     when qtable.frequency= 'mem' then 'memorial'
     when qtable.frequency= 'sup' then 'supplemental'
     when qtable.frequency= 'isc' then 'interest on sgc'
     else 'regular'
     end as frequency,   
     
     'div|divid|divnotes' as link_notes,
     
   case when optionid is not null
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),qtable.divid)
     else concat(lpad(scexhid,7,'0'),'01',qtable.divid)
     end as firef,  
     


qtable.actflag,



case when (divpy.acttime is not null) and (divpy.acttime > qtable.acttime) and (divpy.acttime > rd.acttime) and (divpy.acttime > exdt.acttime) and (divpy.acttime > pexdt.acttime) then divpy.acttime
     when (rd.acttime is not null) and (rd.acttime > qtable.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > qtable.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > qtable.acttime) then pexdt.acttime
     else qtable.acttime
     end as acttime,





scmst.secid,
qtable.divid

from wca.div_my as qtable
inner join rd on qtable.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join bond on scmst.secid = bond.secid

inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid
       and scexh.exchgcd = exdt.exchgcd and 'DIV' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid
      and scmst.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
left outer join divpy on qtable.divid = divpy.divid
                   and 'D' <> divpy.actflag
left outer join rdprt on qtable.rdid=rdprt.rdid and 'div'= rdprt.eventtype


where
scmst.secid in (select secid from wfi.reference)
and qtable.acttime > '2012/01/01'
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
and scexh.actflag<>'D'
and ifnull(divpy.optionserialno,1)=1
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfi.dividend;
rename table wfi.loadingtable to wfi.dividend;
--------------------------------------------------------------------


drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select distinct
qtable.divid as event_id,
case when qtable.nildividend= 'y' then 'nildividend'
     when divpy.divtype= 'b' then 'cash & stock'
     when divpy.divtype= 's' then 'stock'
     when divpy.divtype= 'c' then 'cash'
     else 'unspecified'
     end as dividend_type,

case when divpy.recindcashdiv='t' then '0' else divpy.grossdividend
     end as gross_dividend,
     
     case when divpy.recindcashdiv='t' then '0' else divpy.netdividend
     end as net_dividend,
     
 divpy.taxrate as tax_rate, 
 
 case when divpy.curencd is null then ''
     else divpy.curencd
     end as curencd,
     
 rd.recdate as record_date,    


case when divpy.divtype= 's' and exdt.rdid is not null and exdt.acttime>'2014-07-21'
     then exdt.exdate2
     when divpy.divtype= 's' and pexdt.rdid is not null and pexdt.acttime>'2014-07-21'
     then pexdt.exdate2
     when exdt.rdid is not null
     then exdt.exdate
     else pexdt.exdate
     end as ex_date,
     
case when divpy.divtype= 's' and exdt.rdid is not null and exdt.acttime>'2014-07-21'
     then exdt.paydate2
     when divpy.divtype= 's' and pexdt.rdid is not null and pexdt.acttime>'2014-07-21'
     then pexdt.paydate2
     when exdt.rdid is not null
     then exdt.paydate
     else pexdt.paydate
     end as pay_date,
     
     
  case 
     
     when qtable.frequency= 'mnt' then 'monthly'
     when qtable.frequency= 'sma' then 'semi-annual'
     when qtable.frequency= 'ins' then 'installment'
     when qtable.frequency= 'int' then 'interim'
     when qtable.frequency= 'qtr' then 'quarterly'
     when qtable.frequency= 'fnl' then 'final'
     when qtable.frequency= 'anl' then 'annual'
     when qtable.frequency= 'reg' then 'regular'
     when qtable.frequency= 'un'  then 'unspecified'
     when qtable.frequency= 'bim' then 'bi-monthly'
     when qtable.frequency= 'spl' then 'special'
     when qtable.frequency= 'trm' then 'trimesterly'
     when qtable.frequency= 'mem' then 'memorial'
     when qtable.frequency= 'sup' then 'supplemental'
     when qtable.frequency= 'isc' then 'interest on sgc'
     else 'regular'
     end as frequency,   
     
     'div|divid|divnotes' as link_notes,
     
   case when optionid is not null
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),qtable.divid)
     else concat(lpad(scexhid,7,'0'),'01',qtable.divid)
     end as firef,  
     


qtable.actflag,



case when (divpy.acttime is not null) and (divpy.acttime > qtable.acttime) and (divpy.acttime > rd.acttime) and (divpy.acttime > exdt.acttime) and (divpy.acttime > pexdt.acttime) then divpy.acttime
     when (rd.acttime is not null) and (rd.acttime > qtable.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > qtable.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > qtable.acttime) then pexdt.acttime
     else qtable.acttime
     end as acttime,





scmst.secid,
qtable.divid

from wca.div_my as qtable
inner join rd on qtable.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join bond on scmst.secid = bond.secid

inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid
       and scexh.exchgcd = exdt.exchgcd and 'DIV' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid
      and scmst.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
left outer join divpy on qtable.divid = divpy.divid
                   and 'D' <> divpy.actflag
left outer join rdprt on qtable.rdid=rdprt.rdid and 'div'= rdprt.eventtype


where
scmst.secid in (select secid from wfi.reference)
and qtable.acttime > '2012/01/01'
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
and scexh.actflag<>'D'
and ifnull(divpy.optionserialno,1)=1
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfimatured.dividend;
rename table wfimatured.loadingtable to wfimatured.dividend;


--------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(


select distinct
qtable.auctionid as event_id,
qtable.auctiondate as auction_date,
qtable.auctionamount as auction_amount,
qtable.issuedate as issue_date,
qtable.actflag,
'' as allotment_price_as_percent,
qtable.amountaccepted as amount_accepted,
qtable.AuctionAnnouncementDate as Auction_Announcement_Date,
qtable.AuctionStatus as Auction_Status,
qtable.BidAmount as Bid_Amount,
qtable.Bids,
qtable.NonCompetitiveBidAmount as Non_Competitive_Bid_Amount,
qtable.NonCompetitiveBids as Non_Competitive_Bids,
qtable.CompetitiveBidAmount as Competitive_Bid_Amount,
qtable.CompetitiveBids as Competitive_Bids,
qtable.BidsAccepted as Bids_Accepted,
qtable.NonCompetitiveAmountAccepted as Non_Competitive_Amount_Accepted,
qtable.NonCompetitiveBidsAccepted as Non_Competitive_Bids_Accepted,
qtable.CompetitiveAmountAccepted as Competitive_Amount_Accepted,
qtable.CompetitiveBidsAccepted as Competitive_Bids_Accepted,
qtable.IssuersQuotaAmount as Issuers_Quota_Amount,
qtable.CutOffPrice as Cut_Off_Price,
qtable.CutoffYield as Cut_off_Yield,
qtable.HighestAcceptedPrice as Highest_Accepted_Price,
qtable.LowestAcceptedYield as Lowest_Accepted_Yield,
qtable.AverageAcceptedPrice as Average_Accepted_Price,
qtable.AverageAcceptedYield as Average_Accepted_Yield,
qtable.LowestAcceptedPrice as Lowest_Accepted_Price,
qtable.HighestAcceptedYield as Highest_Accepted_Yield,
qtable.WeightedAveragePrice as Weighted_Average_Price,
qtable.WeightedAverageYield as Weighted_Average_Yield,
qtable.BidToCoverRatio as Bid_To_Cover_Ratio,
qtable.YieldTailAtAuction as Yield_Tail_At_Auction,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.auctionid as firef,
qtable.secid,
qtable.auctionid
from wca.auct as qtable


inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid

where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`secid`, `auction_date`),
add index `ix_firef` (`firef`);
drop table if exists wfi.auction;
rename table wfi.loadingtable to wfi.auction;
--------------------------------------------------------------------

drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(


select distinct
qtable.auctionid as event_id,
qtable.auctiondate as auction_date,
qtable.auctionamount as auction_amount,
qtable.issuedate as issue_date,
qtable.actflag,
'' as allotment_price_as_percent,
qtable.amountaccepted as amount_accepted,
qtable.AuctionAnnouncementDate as Auction_Announcement_Date,
qtable.AuctionStatus as Auction_Status,
qtable.BidAmount as Bid_Amount,
qtable.Bids,
qtable.NonCompetitiveBidAmount as Non_Competitive_Bid_Amount,
qtable.NonCompetitiveBids as Non_Competitive_Bids,
qtable.CompetitiveBidAmount as Competitive_Bid_Amount,
qtable.CompetitiveBids as Competitive_Bids,
qtable.BidsAccepted as Bids_Accepted,
qtable.NonCompetitiveAmountAccepted as Non_Competitive_Amount_Accepted,
qtable.NonCompetitiveBidsAccepted as Non_Competitive_Bids_Accepted,
qtable.CompetitiveAmountAccepted as Competitive_Amount_Accepted,
qtable.CompetitiveBidsAccepted as Competitive_Bids_Accepted,
qtable.IssuersQuotaAmount as Issuers_Quota_Amount,
qtable.CutOffPrice as Cut_Off_Price,
qtable.CutoffYield as Cut_off_Yield,
qtable.HighestAcceptedPrice as Highest_Accepted_Price,
qtable.LowestAcceptedYield as Lowest_Accepted_Yield,
qtable.AverageAcceptedPrice as Average_Accepted_Price,
qtable.AverageAcceptedYield as Average_Accepted_Yield,
qtable.LowestAcceptedPrice as Lowest_Accepted_Price,
qtable.HighestAcceptedYield as Highest_Accepted_Yield,
qtable.WeightedAveragePrice as Weighted_Average_Price,
qtable.WeightedAverageYield as Weighted_Average_Yield,
qtable.BidToCoverRatio as Bid_To_Cover_Ratio,
qtable.YieldTailAtAuction as Yield_Tail_At_Auction,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.auctionid as firef,
qtable.secid,
qtable.auctionid
from wca.auct as qtable


inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid

where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`secid`, `auction_date`),
add index `ix_firef` (`firef`);
drop table if exists wfimatured.auction;
rename table wfimatured.loadingtable to wfimatured.auction;
--------------------------------------------------------------------

drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(


select
bond.secid,
qtable.rdid,
qtable.bndlqid as event_id,
qtable.curencd as bond_liquidation_currency,
qtable.liquidationprice as liquidation_price,
qtable.actflag,
qtable.liqseq as liquidation_payment_sequence,

'wca.dbo.bndlq|bndlqid|notes' as link_notes,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.bndlqid as firef,

qtable.bndlqid
from wca.bndlq as qtable

inner join rd on qtable.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
inner join scmst on bond.secid = scmst.secid

where
bond.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfi.bond_liquidation;
rename table wfi.loadingtable to wfi.bond_liquidation;
--------------------------------------------------------------------

drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(


select
bond.secid,
qtable.rdid,
qtable.bndlqid as event_id,
qtable.curencd as bond_liquidation_currency,
qtable.liquidationprice as liquidation_price,
qtable.actflag,
qtable.liqseq as liquidation_payment_sequence,

'wca.dbo.bndlq|bndlqid|notes' as link_notes,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.bndlqid as firef,

qtable.bndlqid
from wca.bndlq as qtable

inner join rd on qtable.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
inner join scmst on bond.secid = scmst.secid

where
bond.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfimatured.bond_liquidation;
rename table wfimatured.loadingtable to wfimatured.bond_liquidation;
--------------------------------------------------------------------

drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(


select
qtable.secid,
qtable.lstatid as event_id,
qtable.notificationdate as notification_date,
qtable.effectivedate as effective_date,
#qtable.lstatstatus as status,

case when qtable.lstatstatus = 'D' then 'Delisted'
	 when qtable.lstatstatus = 'L' then 'Listed'
     when qtable.lstatstatus = 'N' then 'New Listing'
     when qtable.lstatstatus = 'R' then 'Resumed'
     when qtable.lstatstatus = 'S' then 'Suspended'
else qtable.lstatstatus end as status,     

qtable.eventtype as event_type,
qtable.actflag,
'wca.dbo.lstat|lstatid|reason' as link_notes,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.lstatid as firef,

qtable.lstatid
from wca.lstat as qtable

inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup

where
bond.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'clean'
and eventtype<>'corr'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfi.listing_status_change;
rename table wfi.loadingtable to wfi.listing_status_change;
--------------------------------------------------------------------

drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(


select
qtable.secid,
qtable.lstatid as event_id,
qtable.notificationdate as notification_date,
qtable.effectivedate as effective_date,
#qtable.lstatstatus as status,

case when qtable.lstatstatus = 'D' then 'Delisted'
	 when qtable.lstatstatus = 'L' then 'Listed'
     when qtable.lstatstatus = 'N' then 'New Listing'
     when qtable.lstatstatus = 'R' then 'Resumed'
     when qtable.lstatstatus = 'S' then 'Suspended'
else qtable.lstatstatus end as status,     

qtable.eventtype as event_type,
qtable.actflag,
'wca.dbo.lstat|lstatid|reason' as link_notes,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.lstatid as firef,

qtable.lstatid
from wca.lstat as qtable

inner join bond on qtable.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup

where
bond.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'clean'
and eventtype<>'corr'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfimatured.listing_status_change;
rename table wfimatured.loadingtable to wfimatured.listing_status_change;
--------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(


select distinct
scmst.secid,
qtable.lk144id as event_id,
qtable.secid144a as secid_144a,
s1.isin as isin_144a,
-- scmst.isin as isin_144a,
qtable.secidregs as secid_regs,
s2.isin as isin_regs,
-- s1.isin as isin_regs,
qtable.secidunrestricted as secid_unrestricted,
qtable.linkoutstandingamount as outstanding_amount,
qtable.linkoutstandingamountdate as outstanding_amount_date,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.lk144id as firef,
qtable.lk144id
from wca.lk144 as qtable

inner join scmst on qtable.SecID144A = scmst.secid or qtable.SecIDRegS = scmst.secid
inner join scmst as s1 on qtable.SecID144A = s1.secid
inner join scmst as s2 on qtable.SecIDRegS = s2.secid

-- inner join scmst on qtable.secid144a = scmst.secid
-- inner join scmst as s1 on qtable.secidregs = s1.secid
-- inner join bond on scmst.secid = bond.secid


where
s1.secid and s2.secid in (select secid from wfi.reference)
-- bond.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
-- add primary key (`firef`),
add index `ix_secid144a` (`secid_144a`),
add index `ix_secidregs` (`secid_regs`);
drop table if exists wfi.144a_regs_link;
rename table wfi.loadingtable to wfi.144a_regs_link;
--------------------------------------------------------------------

drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(


select distinct
scmst.secid,
qtable.lk144id as event_id,
qtable.secid144a as secid_144a,
s1.isin as isin_144a,
-- scmst.isin as isin_144a,
qtable.secidregs as secid_regs,
s2.isin as isin_regs,
-- s1.isin as isin_regs,
qtable.secidunrestricted as secid_unrestricted,
qtable.linkoutstandingamount as outstanding_amount,
qtable.linkoutstandingamountdate as outstanding_amount_date,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.lk144id as firef,
qtable.lk144id
from wca.lk144 as qtable

inner join scmst on qtable.SecID144A = scmst.secid or qtable.SecIDRegS = scmst.secid
inner join scmst as s1 on qtable.SecID144A = s1.secid
inner join scmst as s2 on qtable.SecIDRegS = s2.secid

-- inner join scmst on qtable.secid144a = scmst.secid
-- inner join scmst as s1 on qtable.secidregs = s1.secid
-- inner join bond on scmst.secid = bond.secid

where
s1.secid and s2.secid in (select secid from wfimatured.reference)
-- bond.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
-- add primary key (`firef`),
add index `ix_secid144a` (`secid_144a`),
add index `ix_secidregs` (`secid_regs`);
drop table if exists wfimatured.144a_regs_link;
rename table wfimatured.loadingtable to wfimatured.144a_regs_link;
--------------------------------------------------------------------
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(


select distinct
scmst.secid,
qtable.ch144id as event_id,
qtable.effectivedate as effective_date,
qtable.serialnumber as serial_number,
qtable.oldsecid144a as old_secid_144a,
qtable.newsecid144a as new_secid_144a,
s1.isin as isin_144a,
-- s1.isin as new_isin_144a,

qtable.oldsecidregs as old_secid_regs,
qtable.newsecidregs as new_secid_regs,
s2.isin as isin_regs,
-- s2.isin as new_isin_regs,

qtable.oldsecidunrestricted as old_secid_unrestricted,
qtable.newsecidunrestricted as new_secid_unrestricted,
qtable.oldlinkoutstandingamount as old_outstanding_amount,
qtable.newlinkoutstandingamount as new_outstanding_amount,
qtable.oldlinkoutstandingamountdate as old_outstanding_amount_date,
qtable.newlinkoutstandingamountdate as new_outstanding_amount_date,
qtable.eventtype as event_type,
qtable.releventid as related_eventid,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.ch144id as firef,
qtable.ch144id
from wca.ch144 as qtable

inner join scmst on qtable.oldsecid144a = scmst.secid or qtable.oldsecidregs = scmst.secid

inner join scmst as s1 on qtable.oldsecid144a = s1.secid
inner join scmst as s2 on qtable.oldsecidregs = s2.secid
-- inner join scmst as s3 on qtable.oldsecidregs = s3.secid
-- inner join scmst as s4 on qtable.newsecidregs = s4.secid

-- inner join scmst on qtable.oldsecid144a = scmst.secid
-- inner join scmst as s1 on qtable.oldsecidregs = s1.secid
-- inner join bond on scmst.secid = bond.secid

left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup

where
s1.secid and s2.secid in (select secid from wfi.reference)
-- bond.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'clean'
and eventtype<>'corr'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
-- add primary key (`firef`),
add index `ix_oldsecid144a` (`old_secid_144a`),
add index `ix_oldsecidregs` (`old_secid_regs`);
drop table if exists wfi.144a_regs_link_change;
rename table wfi.loadingtable to wfi.144a_regs_link_change;
--------------------------------------------------------------------

drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(


select distinct
scmst.secid,
qtable.ch144id as event_id,
qtable.effectivedate as effective_date,
qtable.serialnumber as serial_number,
qtable.oldsecid144a as old_secid_144a,
qtable.newsecid144a as new_secid_144a,
s1.isin as isin_144a,
-- s1.isin as new_isin_144a,

qtable.oldsecidregs as old_secid_regs,
qtable.newsecidregs as new_secid_regs,
s2.isin as isin_regs,
-- s2.isin as new_isin_regs,

qtable.oldsecidunrestricted as old_secid_unrestricted,
qtable.newsecidunrestricted as new_secid_unrestricted,
qtable.oldlinkoutstandingamount as old_outstanding_amount,
qtable.newlinkoutstandingamount as new_outstanding_amount,
qtable.oldlinkoutstandingamountdate as old_outstanding_amount_date,
qtable.newlinkoutstandingamountdate as new_outstanding_amount_date,
qtable.eventtype as event_type,
qtable.releventid as related_eventid,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.ch144id as firef,
qtable.ch144id
from wca.ch144 as qtable

inner join scmst on qtable.oldsecid144a = scmst.secid or qtable.oldsecidregs = scmst.secid

inner join scmst as s1 on qtable.oldsecid144a = s1.secid
inner join scmst as s2 on qtable.oldsecidregs = s2.secid
-- inner join scmst as s3 on qtable.oldsecidregs = s3.secid
-- inner join scmst as s4 on qtable.newsecidregs = s4.secid

-- inner join scmst on qtable.oldsecid144a = scmst.secid
-- inner join scmst as s1 on qtable.oldsecidregs = s1.secid
-- inner join bond on scmst.secid = bond.secid

left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
s1.secid and s2.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'clean'
and eventtype<>'corr'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
-- add primary key (`firef`),
add index `ix_oldsecid144a` (`old_secid_144a`),
add index `ix_oldsecidregs` (`old_secid_regs`);
drop table if exists wfimatured.144a_regs_link_change;
rename table wfimatured.loadingtable to wfimatured.144a_regs_link_change;
--------------------------------------------------------------------

drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(


select distinct
scmst.secid,
qtable.sfundid as event_id,
qtable.sfunddate as sinking_date,
qtable.curencd as sinking_currency,
qtable.Amount as sinking_amount,
qtable.AmountasPercent as sinking_amount_as_percent,
'wca.dbo.sfund|sfundid|sfundnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.sfundid as firef,
qtable.sfundid
from wca.sfund as qtable
inner join scmst on qtable.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where
bond.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`);
drop table if exists wfi.sinking_fund;
rename table wfi.loadingtable to wfi.sinking_fund;
--------------------------------------------------------------------

drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(


select distinct
scmst.secid,
qtable.sfundid as event_id,
qtable.sfunddate as sinking_date,
qtable.curencd as sinking_currency,
qtable.Amount as sinking_amount,
qtable.AmountasPercent as sinking_amount_as_percent,
'wca.dbo.sfund|sfundid|sfundnotes' as link_notes,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.sfundid as firef,
qtable.sfundid
from wca.sfund as qtable
inner join scmst on qtable.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where
bond.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`);
drop table if exists wfimatured.sinking_fund;
rename table wfimatured.loadingtable to wfimatured.sinking_fund;
--------------------------------------------------------------------

drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(


select distinct
scmst.secid,
qtable.leiid as event_id,
qtable.lei,
qtable.managinglou as managing_lou,
qtable.regstatus as lei_registration_status,
qtable.legalname as legal_name,
qtable.prevname as previous_name,
qtable.othername as other_names,
qtable.incorpdate as incorporation_date,
qtable.dissdate as dissolution_date,
qtable.roadd1 as registered_office_address_1,
qtable.roadd2 as registered_office_address_2,
qtable.roadd3 as registered_office_address_3,
qtable.roadd4 as registered_office_address_4,
qtable.roadd5 as registered_office_address_5,
qtable.rocity as registered_office_city,
qtable.rostate as registered_office_state,
qtable.rocntry as registered_office_country,
qtable.ropostcode as registered_office_postcode,
qtable.hoadd1 as head_office_address_1,
qtable.hoadd2 as head_office_address_2,
qtable.hoadd3 as head_office_address_3,
qtable.hoadd4 as head_office_address_4,
qtable.hoadd5 as head_office_address_5,
qtable.hocity as head_office_city,
qtable.hostate as head_office_state,
qtable.hocntry as head_office_country,
qtable.hopostcode as head_office_postcode,
qtable.regulator,
qtable.regulatorystatus as regulatory_status,
qtable.regulatoryid as regulatory_id,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.leiid as firef,
qtable.leiid
from wca.lei as qtable
inner join issur on qtable.lei = issur.lei
inner join scmst on issur.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
bond.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`secid`);
drop table if exists wfi.legal_entity_identifier;
rename table wfi.loadingtable to wfi.legal_entity_identifier;

--------------------------------------------------------------------

drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(


select distinct
scmst.secid,
qtable.leiid as event_id,
qtable.lei,
qtable.managinglou as managing_lou,
qtable.regstatus as lei_registration_status,
qtable.legalname as legal_name,
qtable.prevname as previous_name,
qtable.othername as other_names,
qtable.incorpdate as incorporation_date,
qtable.dissdate as dissolution_date,
qtable.roadd1 as registered_office_address_1,
qtable.roadd2 as registered_office_address_2,
qtable.roadd3 as registered_office_address_3,
qtable.roadd4 as registered_office_address_4,
qtable.roadd5 as registered_office_address_5,
qtable.rocity as registered_office_city,
qtable.rostate as registered_office_state,
qtable.rocntry as registered_office_country,
qtable.ropostcode as registered_office_postcode,
qtable.hoadd1 as head_office_address_1,
qtable.hoadd2 as head_office_address_2,
qtable.hoadd3 as head_office_address_3,
qtable.hoadd4 as head_office_address_4,
qtable.hoadd5 as head_office_address_5,
qtable.hocity as head_office_city,
qtable.hostate as head_office_state,
qtable.hocntry as head_office_country,
qtable.hopostcode as head_office_postcode,
qtable.regulator,
qtable.regulatorystatus as regulatory_status,
qtable.regulatoryid as regulatory_id,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.leiid as firef,
qtable.leiid
from wca.lei as qtable
inner join issur on qtable.lei = issur.lei
inner join scmst on issur.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
bond.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`secid`);
drop table if exists wfimatured.legal_entity_identifier;
rename table wfimatured.loadingtable to wfimatured.legal_entity_identifier;

--------------------------------------------------------------------

drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(


select distinct
scmst.secid,
qtable.leihrid as event_id,
qtable.blei as base_issuer_lei,
qtable.dplei as direct_parent_lei,
qtable.dpstatus as direct_parent_status,
qtable.uplei as ultimate_parent_lei,
qtable.upstatus as ultimate_parent_status,
qtable.iblei as international_branch_of_lei,
qtable.ibstatus as international_branch_status,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.leihrid as firef,
qtable.leihrid
from wca.leihr as qtable
inner join issur on qtable.blei = issur.lei
inner join scmst on issur.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
bond.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`secid`);
drop table if exists wfi.legal_entity_identifier_hierarchy;
rename table wfi.loadingtable to wfi.legal_entity_identifier_hierarchy;

--------------------------------------------------------------------

drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(


select distinct
scmst.secid,
qtable.leihrid as event_id,
qtable.blei as base_issuer_lei,
qtable.dplei as direct_parent_lei,
qtable.dpstatus as direct_parent_status,
qtable.uplei as ultimate_parent_lei,
qtable.upstatus as ultimate_parent_status,
qtable.iblei as international_branch_of_lei,
qtable.ibstatus as international_branch_status,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.leihrid as firef,
qtable.leihrid
from wca.leihr as qtable
inner join issur on qtable.blei = issur.lei
inner join scmst on issur.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
bond.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`secid`);
drop table if exists wfimatured.legal_entity_identifier_hierarchy;
rename table wfimatured.loadingtable to wfimatured.legal_entity_identifier_hierarchy;

--------------------------------------------------------------------

drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(


select distinct
qtable.secid,
qtable.pfdsid as event_id,
qtable.securitiesoffered as securities_offered,
qtable.greenshoeoption as green_shoe_option,
qtable.liquidationpreference as liquidation_preference,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.pfdsid as firef,
qtable.pfdsid
from wca.pfds as qtable
inner join scmst on qtable.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where
bond.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
);
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`);
drop table if exists wfi.preferreds;
rename table wfi.loadingtable to wfi.preferreds;

--------------------------------------------------------------------

drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(


select distinct
qtable.secid,
qtable.pfdsid as event_id,
qtable.securitiesoffered as securities_offered,
qtable.greenshoeoption as green_shoe_option,
qtable.liquidationpreference as liquidation_preference,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
qtable.pfdsid as firef,
qtable.pfdsid
from wca.pfds as qtable
inner join scmst on qtable.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
where
bond.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
);
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`);
drop table if exists wfimatured.preferreds;
rename table wfimatured.loadingtable to wfimatured.preferreds;

