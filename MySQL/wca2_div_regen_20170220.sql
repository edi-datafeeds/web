update wca.divpy set rationew=concat('0',rationew) where left(rationew,1) = '.';
update wca.divpy set ratioold=concat('0',ratioold) where left(ratioold,1) = '.';
update wca.divpy set rationew=concat(rationew,'.0') where instr(rationew,'.') = 0 and rationew<>'';
update wca.divpy set ratioold=concat(ratioold,'.0') where instr(ratioold,'.') = 0 and ratioold<>'';
drop table if exists wca2.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'dividend' as event,
case when optionid is not null
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),div_my.divid)
     else concat(lpad(scexhid,7,'0'),'01',div_my.divid)
     end as caref,
div_my.divid as eventid,
div_my.announcedate as created,
case when (divpy.acttime is not null) and (divpy.acttime > div_my.acttime) and (divpy.acttime > rd.acttime) and (divpy.acttime > exdt.acttime) and (divpy.acttime > pexdt.acttime) then divpy.acttime
     when (rd.acttime is not null) and (rd.acttime > div_my.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > div_my.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > div_my.acttime) then pexdt.acttime
     else div_my.acttime
     end as changed,
case when div_my.actflag = 'U' then 'Updated'
     when div_my.actflag = 'I' then 'Inserted'
     when div_my.actflag = 'D' then 'Deleted'
     when div_my.actflag = 'C' then 'Cancelled'
     when div_my.actflag = 'U' then 'Updated'
     when div_my.actflag = 'I' then 'Inserted'
     when div_my.actflag = 'D' then 'Deleted'
     when div_my.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
ifnull(sbx.sedol,'') as sedol,
ifnull(sbx.rcntrycd,'') as sedol_register_country,
ifnull(sbx.curencd,'') as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
ifnull(sbx.bbgcompid,'') as composite_global_id,
ifnull(sbx.bbgcomptk,'') as bloomberg_composite_ticker,
ifnull(sbx.bbgexhid,'') as bloomberg_global_id,
ifnull(sbx.bbgexhtk,'') as bloomberg_exchange_ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
rd.registrationdate as registration_date,
case when divpy.divtype= 's' and exdt.rdid is not null and exdt.acttime>'2014-07-21'
     then exdt.exdate2
     when divpy.divtype= 's' and pexdt.rdid is not null and pexdt.acttime>'2014-07-21'
     then pexdt.exdate2
     when exdt.rdid is not null
     then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when divpy.divtype= 's' and exdt.rdid is not null and exdt.acttime>'2014-07-21'
     then exdt.paydate2
     when divpy.divtype= 's' and pexdt.rdid is not null and pexdt.acttime>'2014-07-21'
     then pexdt.paydate2
     when exdt.rdid is not null
     then exdt.paydate
     else pexdt.paydate
     end as pay_date,
div_my.fyedate as fin_year_date,
case when div_my.marker= 'fnl' then 'Final'
     when div_my.divperiodcd= 'spl' then 'special'
     when div_my.divperiodcd= 'fnl' then 'final'
     when div_my.frequency= 'mnt' then 'monthly'
     when div_my.frequency= 'sma' then 'semi-annual'
     when div_my.frequency= 'ins' then 'installment'
     when div_my.frequency= 'int' then 'interim'
     when div_my.frequency= 'qtr' then 'quarterly'
     when div_my.frequency= 'fnl' then 'final'
     when div_my.frequency= 'anl' then 'annual'
     when div_my.frequency= 'reg' then 'regular'
     when div_my.frequency= 'un'  then 'unspecified'
     when div_my.frequency= 'bim' then 'bi-monthly'
     when div_my.frequency= 'spl' then 'special'
     when div_my.frequency= 'trm' then 'trimesterly'
     when div_my.frequency= 'mem' then 'memorial'
     when div_my.frequency= 'sup' then 'supplemental'
     when div_my.frequency= 'isc' then 'interest on sgc'
     when div_my.divperiodcd= 'mnt' then 'monthly'
     when div_my.divperiodcd= 'sma' then 'semi-annual'
     when div_my.divperiodcd= 'ins' then 'installment'
     when div_my.divperiodcd= 'int' then 'interim'
     when div_my.divperiodcd= 'qtr' then 'quarterly'
     when div_my.divperiodcd= 'anl' then 'annual'
     when div_my.divperiodcd= 'reg' then 'regular'
     when div_my.divperiodcd= 'un'  then 'unspecified'
     when div_my.divperiodcd= 'bim' then 'bi-monthly'
     when div_my.divperiodcd= 'trm' then 'trimesterly'
     when div_my.divperiodcd= 'mem' then 'memorial'
     when div_my.divperiodcd= 'sup' then 'supplemental'
     when div_my.divperiodcd= 'isc' then 'interest on sgc'
     else 'regular'
     end as dividend_period,
case when div_my.tbaflag= 't' then 'yes'
     else ''
     end as to_be_announced,
case when div_my.nildividend= 't' then 'yes'
     else ''
     end as nil_dividend,
case when optionid is not null
     then lpad(optionid,2,'0')
     else ''
     end as option_key,
case when divpy.defaultopt = 't' then 'yes'
     else ''
     end as default_option,
divpy.optelectiondate as option_election_date,
iractiondivpy.lookup as option_status,
case when div_my.nildividend= 'y' then 'nildividend'
     when divpy.divtype= 'b' then 'cash & stock'
     when divpy.divtype= 's' then 'stock'
     when divpy.divtype= 'c' then 'cash'
     else 'unspecified'
     end as dividend_type,
case when divpy.recindcashdiv='t' then '0' else divpy.grossdividend
     end as gross_dividend,
case when divpy.recindcashdiv='t' then '0' else divpy.netdividend
     end as net_dividend,
case when divpy.curencd is null then ''
     else divpy.curencd
     end as currency,
case when div_my.declcurencd is null then ''
     else div_my.declcurencd
     end as declared_currency,
case when div_my.declgrossamt is null then ''
     else div_my.declgrossamt
     end as Declared_Gross_Amount,
case when divpy.recindcashdiv= 't' then concat('yes: gross was: ',divpy.grossdividend,' net was: ',divpy.netdividend,' as on changed date')
     else ''
     end as cash_dividend_rescinded,
divpy.taxrate as tax_rate,
divpy.divrate as dividend_as_percent,
case when divpy.approxflag= 't' then 'yes'
     else ''
     end as approximate_dividend,
divpy.usdratetocurrency as usd_rate_to_currency,
case when divpy.fractions = '' then ''
     when irfractions.lookup is null and divpy.fractions <> ''
     then concat('[',divpy.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
divpy.coupon as coupon,
divpy.couponid as coupon_id,
divpy.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
case when ressedol.sedol is null then '' else ressedol.sedol end as resultant_sedol,
case when divpy.rationew is null or divpy.rationew = ''
     then ''
     else concat(divpy.rationew,':',divpy.ratioold)
     end as ratio,
exdt.paydate2 as stock_pay_date,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'div|divid|divnotes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid,
rd.rdid
from div_my
inner join rd on div_my.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid
       and scexh.exchgcd = exdt.exchgcd and 'DIV' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid
       and scmst.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
left outer join divpy on div_my.divid = divpy.divid
                   and 'D' <> divpy.actflag
left outer join scmst as resscmst on divpy.ressecid = resscmst.secid
left outer join sedol as ressedol on divpy.ressecid =ressedol.secid
                   and exchg.cntrycd=ressedol.cntrycd
                   and 'D'<>ressedol.actflag
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
left outer join irfractions on divpy.fractions = irfractions.code
left outer join iraction as iractiondivpy on divpy.actflag = iractiondivpy.code
where
div_my.acttime > '2012/01/01'
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
and scexh.actflag<>'D'
and ifnull(divpy.optionserialno,1)=1
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`trading_currency`,`sedol_register_country`,`resultant_sedol`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`),
add index `ix_exchgcd`(`exchange_code`);
drop table if exists wca2.evf_dividend;
rename table wca2.evf_loadingtable to wca2.evf_dividend;
drop table if exists wca2.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
SELECT DISTINCT
case when DP.optionid is not null
 then concat(lpad(SC.scexhid,7,'0'),lpad(DP.optionid,2,'0'),DM.divid)
 else concat(lpad(SC.scexhid,7,'0'),'01',DM.divid)
 end as caref,
DM.divid as eventid,
S.SecID,
SC.ExchgCD,
DM.divid AS Distrib,
DP.OptionID As Element,
DP.actflag As Action,
DM.announcedate as created,
DP.acttime As Changed,
case when SD.sedol is null then '' else SD.sedol end As Sedol,
I.CntryofIncorp As CntryofIncorp,
I.IssuerName As IssuerName,
S.sectycd As SecType,
S.SecurityDesc As Security,
S.Isin As Isin,
case when SD.sedol is null then '' else SD.curencd end As UnitofQuoteCurr,
E.ExDate As ExDate,
R.Recdate As RecDate,
E.PayDate As PayDate,
DP.GrossDividend As GrossRate,
DP.TaxRate,
DP.NetDividend As NetRate,
DP.Divtype As DivType,
case when DM.marker<>'' then DM.marker else DM.Divperiodcd end As Marker,
DP.DivType As StockType,
DP.DivType As EventType,
DP.CurenCD As OptionCurr,
case when RatioOld<>'' then CONCAT(DP.RatioOld,':',DP.RatioNew) else '' end As Ratio,
DP.OptElectionDate As OptionAcceptDate,
DP.TaxRate As UkTaxCurr,
DP.DivType As RateType,
XD.snpdate As Snpdate,
XD.sfpdate As Sfpdate,
DP.FXDate As CalculDate,
DM.FYEDate As PeriodStartDate,
DM.PeriodEndDate As PeriodEndDate,
DM.declarationdate As AnnounceDate,
XD.calculationdate As ConversionDate,
XD.curelecflag As ConversionStatus,
DP.stampaspercent As MaxPrice,
DP.ComAsPercent As Quantity,
SC.LocalCode,
'C' AS Confirmation,
case when XD.Detail_ID is null then 0 else XD.Detail_ID end as Detail_ID
From wca.div_my AS DM
INNER JOIN wca.rd AS R on DM.RdID=R.RdID
INNER JOIN wca.scmst As S on R.SecID = S.SecID
INNER JOIN wca.sectygrp AS SG on S.SectyCD =SG.SectyCD And SG.SecGrpId < 3
INNER JOIN wca.issur AS I on S.IssID=I.IssID
INNER JOIN wca.scexh AS SC on S.SecID=SC.SecID
INNER JOIN wca.exchg on SC.ExchgCD=wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol AS SD on S.SecID=SD.SecID AND 'GB'=SD.CntryCD and 'D'<>SD.actflag
LEFT OUTER JOIN wca.exdt AS E On R.RdID=E.RdID and SC.ExchgCD=E.ExchgCD and 'DIV'=E.eventtype
LEFT OUTER JOIN wca.divpy AS DP on DM.DivID = DP.DivID
left outer join xdes.detail AS XD on S.SecID = XD.secid AND R.Recdate=XD.Recdate
                       and case when DM.marker <>'' then DM.Marker else DM.divperiodcd end
                       = case when XD.marker <>'' then XD.Marker else XD.divperiodcd end
where
wca.exchg.cntrycd='GB'
and ifnull(DP.optionserialno,1)=1
and DM.acttime > '2012/01/01'
and (R.recdate < SC.acttime or SC.liststatus<>'D' or R.recdate is null)
and SC.actflag<>'D'
and (XD.actflag<>'D' or XD.actflag is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`,`UnitofQuoteCurr`,`detail_id`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_sedol`(`sedol`),
add index `ix_exchgcd`(`ExchgCD`);
drop table if exists wca2.ukd_Dividend;
rename table wca2.evf_loadingtable to wca2.ukd_Dividend;
drop table if exists wca2.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'initial_public_offering' as event,
case when scexh.scexhid is not null
     then concat(lpad(ipo.ipoid,7,'0'),scexh.scexhid)
     else lpad(ipo.ipoid,7,'0')
     end as caref,
ipo.ipoid as eventid,
ipo.announcedate as created,
ipo.acttime as changed,
case when ipo.actflag = 'U' then 'Updated'
     when ipo.actflag = 'I' then 'Inserted'
     when ipo.actflag = 'D' then 'Deleted'
     when ipo.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
ifnull(sbx.sedol,'') as sedol,
ifnull(sbx.rcntrycd,'') as sedol_register_country,
ifnull(sbx.curencd,'') as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
case when scexh.scexhid is null
     then ipo.exchgcd
     else scexh.exchgcd
     end as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
ifnull(sbx.bbgcompid,'') as composite_global_id,
ifnull(sbx.bbgcomptk,'') as bloomberg_composite_ticker,
ifnull(sbx.bbgexhid,'') as bloomberg_global_id,
ifnull(sbx.bbgexhtk,'') as bloomberg_exchange_ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
ipo.ipostatus as status,
ipo.subperiodfrom,
ipo.subperiodto,
ipo.tofcurrency,
ipo.sharepricelowest,
ipo.sharepricehighest,
ipo.proposedprice,
ipo.totaloffersize,
ipo.firsttradingdate,
ipo.initialprice,
ipo.minsharesoffered,
ipo.maxsharesoffered,
ipo.preso,
ipo.sharesoutstanding,
ipo.dealtype,
ipo.underwriter,
ipo.lawfirm,
ipo.transferagent,
ipo.whendate,
'ipo|ipoid|notes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid
from ipo
inner join scmst on ipo.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`trading_currency`,`sedol_register_country`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`),
add index `ix_exchgcd`(`exchange_code`);
drop table if exists wca2.evo_initial_public_offering;
rename table wca2.evf_loadingtable to wca2.evo_initial_public_offering;
drop table if exists wca2.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'ipo_rumour' as event,
concat(1, lpad(rum.rumid,7,'0')) as caref,
rum.rumid as eventid,
rum.announcedate as created,
rum.acttime as changed,
case when rum.actflag = 'U' then 'Updated'
     when rum.actflag = 'I' then 'Inserted'
     when rum.actflag = 'D' then 'Deleted'
     when rum.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
'' as country_of_incorporation,
rum.issuername as issuer_name,
'' as security_description,
'' as par_value,
'' as par_value_currency,
'' as status_flag,
rum.sectycd as security_type,
'' as security_structure,
'' as isin,
'' as us_code,
'' as sedol,
'' as sedol_register_country,
'' as trading_currency,
'' as primary_exchange,
rum.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
rum.localcode as local_code,
'' as composite_global_id,
'' as bloomberg_composite_ticker,
'' as bloomberg_global_id,
'' as bloomberg_exchange_ticker,
'' as listing_status,
'' as list_date,
'Rumour' as status,
'' as subperiodfrom,
'' as subperiodto,
'' as tofcurrency,
'' as sharepricelowest,
'' as sharepricehighest,
'' as proposedprice,
'' as totaloffersize,
'' as firsttradingdate,
'' as initialprice,
'' as minsharesoffered,
'' as maxsharesoffered,
'' as preso,
'' as sharesoutstanding,
'' as dealtype,
'' as underwriter,
'' as lawfirm,
'' as transferagent,
'' as whendate,
'rum|rumid|notes' as link_notes,
null as issid,
null as secid,
null as mktsgid
from rum
inner join exchg on rum.exchgcd = exchg.exchgcd
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`),
add index `ix_changed`(`changed`),
add index `ix_exchgcd`(`exchange_code`);
drop table if exists wca2.evo_IPO_Rumour;
rename table wca2.evf_loadingtable to wca2.evo_IPO_Rumour;
