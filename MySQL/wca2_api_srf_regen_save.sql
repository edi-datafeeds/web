drop table if exists wca2.api_loadingtable;
use wca;
create table wca2.api_loadingtable(
select distinct
concat(
(select case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end),
(select case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end),
(select case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end),
(select case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end),wca.scexh.ScexhID) as Record_ID,
case when exchg.actflag = 'D' then 'Deleted'
     when scmst.actflag = 'D' then 'Deleted'
     when scexh.actflag = 'D' then 'Deleted'
     else 'Updated' end as action_flag,
case when wca.scexh.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.exchg.acttime
     when sedolseq.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then sedolseq.acttime
     when bbeseq.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then bbeseq.acttime
     when bbcseq.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then bbcseq.acttime
     when bbebnd.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then bbebnd.acttime
     when wca.scxtc.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.mktsg.acttime
     when wca.dprcp.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.dprcp.acttime
     when wca.bond.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.bond.acttime
     when wca.warex.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.warex.acttime
     when wca.cowar.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.cowar.acttime
     when wca.rts.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.rts.acttime
     when wca.rd.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.rd.acttime
     when wca.scexh.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.exchg.acttime
     when sedolseq.acttime > (select max(feeddate) from wca.tbl_opslog) then sedolseq.acttime
     when bbeseq.acttime > (select max(feeddate) from wca.tbl_opslog) then bbeseq.acttime
     when bbcseq.acttime > (select max(feeddate) from wca.tbl_opslog) then bbcseq.acttime
     when bbebnd.acttime > (select max(feeddate) from wca.tbl_opslog) then bbebnd.acttime
     when wca.scxtc.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.mktsg.acttime
     when wca.dprcp.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.dprcp.acttime
     when wca.bond.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.bond.acttime
     when wca.warex.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.warex.acttime
     when wca.cowar.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.cowar.acttime
     when wca.rts.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.rts.acttime
     when wca.rd.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.rd.acttime
     when smf4.security.actdate > (select max(feeddate) from wca.tbl_opslog) then smf4.security.actdate
     else wca.scexh.acttime
     end as changed,
wca.scexh.AnnounceDate as Created,
case when wca.scmst.Statusflag = 'I' then 'Inactive'
     when wca.scmst.Statusflag = 'D' then 'In default'
     else 'Active' end as Global_Security_Status,
wca.scmst.SecID as Security_ID,
wca.scmst.IssID as Issuer_ID,
wca.issur.IssuerName as Issuer_Name,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else ''
     end as ISIN,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     else ''
     end as US_Code,
wca.issur.CntryofIncorp as Incorporation_Country,
ifnull(wca.issur.SIC,'') as SIC,
ifnull(wca.issur.CIK,'') as CIK,
ifnull(wca.scmst.CFI,'') as CFI,
ifnull(wca.issur.LEI,'') as LEI,
case when ifnull(wca.bond.bondtype,'')<>'' then wca.bond.bondtype
     when ifnull(wca.dprcp.drtype,'')<>'' then wca.dprcp.drtype
     else wca.scmst.SectyCD
     end as Security_Type_Code,
wca.scmst.StructCD as Security_Structure_Code,
wca.scmst.SecurityDesc as Security_Description,
wca.scmst.ParValue as Par_Value,
wca.scmst.CurenCD as Par_Value_Currency,
wca.scmst.PrimaryExchgCD as Primary_Exchange_Code,
wca.exchg.CntryCD as Listing_Country,
ifnull(sedolbbg.curencd,'') as Trading_Currency,
ifnull(sedolbbg.bbgcompid,'') as BBG_Composite_Global_ID,
ifnull(sedolbbg.bbgcomptk,'') as BBG_Composite_Ticker,
case when ifnull(sedolbbg.bbgexhid,'')<>'' then ifnull(sedolbbg.bbgexhid,'')
     else ifnull(bbebnd.bbgexhid,'')
     end as FIGI,
case when ifnull(sedolbbg.bbgexhtk,'')<>'' then ifnull(sedolbbg.bbgexhtk,'')
     else ifnull(bbebnd.bbgexhtk,'')
     end as FIGI_Ticker,
ifnull(sedolbbg.Sedol,'') as SEDOL,
case when ifnull(sedolbbg.SedolDefunct,'')='T' then 'Yes'
     else ''
     end as SEDOL_Defunct,
ifnull(sedolbbg.RcntryCD,'') as SEDOL_Register_Country,
ifnull(smf4.security.OPOL,'') as SEDOL_MIC,
wca.scexh.ExchgCD as Exchange_Code,
ifnull(wca.exchg.Mic,'') as MIC,
ifnull(wca.mktsg.mktsegment,'') as Segment_Name,
ifnull(wca.mktsg.mic,'') as Segment_MIC,
CASE when (wca.exchg.cntrycd<>'US' or (wca.scexh.exchgcd='USOTC' or wca.scexh.exchgcd='USTRCE' or wca.scexh.exchgcd='USBND')) and wca.scxtc.scexhid is null
     then wca.scexh.LocalCode
     when wca.scxtc.localCode<>''
     then wca.scxtc.LocalCode
     when wca.scexh.LocalCode='BPRAP'
     then wca.scexh.LocalCode
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),
                        replace(substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2),'PR','/PR'))
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,4) = 'PRCL'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-4),'/PR/CL')
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),
                        replace(substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3),'PR','/PR/'))
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-4),
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-3,4),'PR','/PR/')
     when wca.scmst.sectycd='PRF' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-4,2) = 'PR'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-5),'/PR/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,1),'/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2))
     when wca.scmst.sectycd='WAR' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,2) = 'WS'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WS/',
                        substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1))
     when wca.scmst.sectycd='WAR' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'WS'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),'/WS')
     when (wca.scmst.sectycd='CVR' or wca.scmst.sectycd='TRT') and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = '.RT'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/RT')
     when (wca.scmst.sectycd='CVR' or wca.scmst.sectycd='TRT') and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-1,2) = 'RT'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-2),'/RT')
     when (wca.scmst.sectycd='EQS' or wca.scmst.sectycd='DR') and wca.scmst.securitydesc like '%Class A%' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1) = 'A'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-1),'/A')
     when (wca.scmst.sectycd='EQS' or wca.scmst.sectycd='DR') and wca.scmst.securitydesc like '%Class B%' and substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ','')),1) = 'B'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-1),'/B')
     when substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = ' WI'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WI')
     when substring(replace(wca.scexh.localcode,' ',''),length(replace(wca.scexh.localcode,' ',''))-2,3) = ' WD'
     then concat(substring(replace(wca.scexh.localcode,' ',''),1,length(replace(wca.scexh.localcode,' ',''))-3),'/WD')
     else wca.scexh.LocalCode
     end as Local_Code,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'Pending'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'Pending'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'Pending'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'Listed'
     when wca.scexh.ListStatus='S' then 'Suspended'
     when wca.scexh.ListStatus='D' then 'Delisted'
     else 'Unknown'
     end as List_Status,
wca.scexh.listdate as List_Date,
wca.scexh.delistdate as Delist_Date,
ifnull(wca.ipo.IPOStatus,'') as IPO_Status,
wca.ipo.FirstTradingDate as First_Trading_Date,
case when wca.dprcp.unsecid is not null then wca.dprcp.unsecid
     when wca.warex.exersecid is not null then wca.warex.exersecid
     when wca.cowar.unsecid is not null then wca.cowar.unsecid
     when wca.rd.secid is not null then wca.rd.secid
     else ''
     end as Underlying_Security_ID,
case when ifnull(wca.dprcp.usratio,'')<>'' then wca.dprcp.usratio
     when ifnull(wca.warex.rationew,'')<>'' then wca.warex.rationew
     when ifnull(wca.cowar.unratio,'')<>'' then wca.cowar.unratio
     when ifnull(wca.rts.NewExerciseRatio,'')<>'' then wca.rts.NewExerciseRatio
     when wca.rd.secid is not null then '1'
     else ''
     end as Underlying_Ratio_Numerator,
case when ifnull(wca.dprcp.drratio,'')<>'' then wca.dprcp.drratio
     when ifnull(wca.warex.ratioold,'')<>'' then wca.warex.ratioold
     when ifnull(wca.cowar.warrantratio,'')<>'' then wca.cowar.warrantratio
     when ifnull(wca.rts.OldExerciseRatio,'')<>'' then wca.rts.OldExerciseRatio
     when wca.rd.secid is not null then '1'
     else ''
     end as Parent_Ratio_Denominator
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca.lcc on wca.scexh.secid = wca.lcc.secid and wca.scexh.exchgcd=wca.lcc.exchgcd 
                               and (select max(feeddate) from wca.tbl_opslog)=wca.lcc.effectivedate and sedolbbg.RcntryCD='GB' 
left outer join prices.GBLSE_Raw_Daily_Prices as gbp04 on sedolbbg.sedol=gbp04.Sedol
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.bbe as bbebnd on wca.scmst.secid =bbebnd.secid and ''=ifnull(bbebnd.exchgcd,'X') and ''<>ifnull(bbebnd.bbgexhid,'') and 'D'<>ifnull(bbebnd.bbgexhid,'')
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on sedolbbg.sedol=smf4.security.sedol
left outer join wca.dprcp on wca.scmst.secid=dprcp.secid and 'D'<>wca.dprcp.actflag
left outer join wca.scmst as unscmst on wca.dprcp.unsecid=unscmst.secid
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.warex on wca.scmst.secid = wca.warex.SecID and 'D'<> wca.warex.actflag and now()<ifnull(wca.warex.todate,'2001-01-01')
left outer join wca.cowar on wca.scmst.secid = wca.cowar.SecID and 'D'<> wca.cowar.actflag and now()<ifnull(wca.cowar.expirationdate,'2001-01-01')
left outer join wca.rts on wca.scmst.secid = wca.rts.trasecid and 'D'<> wca.rts.actflag and wca.scmst.sectycd='TRT'
left outer join wca.rd on wca.rts.rdid = wca.rd.rdid
where 
(wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='CW' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or wca.scmst.sectycd='IDX' or (wca.scmst.sectycd='BND' and (wca.scexh.localcode<>'' or substring(wca.scmst.isin,1,2)='XS')))
and substring(wca.exchg.exchgcd,3,3)<>'BND'
and wca.exchg.exchgcd<>'GBLSE'
and wca.exchg.exchgcd<>'USTRCE'
and wca.exchg.exchgcd<>'ALTSE'
and wca.exchg.exchgcd<>'AOASE'
and wca.exchg.exchgcd<>'AROEX'
and wca.exchg.exchgcd<>'ARRCX'
and wca.exchg.exchgcd<>'AUSIMV'
and wca.exchg.exchgcd<>'CHSCH'
and wca.exchg.exchgcd<>'CNSZHK'
and wca.exchg.exchgcd<>'CNSGHK'
and wca.exchg.exchgcd<>'DESCH'
and wca.exchg.exchgcd<>'DODRSX'
and wca.exchg.exchgcd<>'FINDX'
and wca.exchg.exchgcd<>'FOFSM'
and wca.exchg.exchgcd<>'GABVCA'
and wca.exchg.exchgcd<>'GBENLN'
and wca.exchg.exchgcd<>'GBOFX'
and wca.exchg.exchgcd<>'GBUT'
and wca.exchg.exchgcd<>'GIGSX'
and wca.exchg.exchgcd<>'HKHKSG'
and wca.exchg.exchgcd<>'HKHKSZ'
and wca.exchg.exchgcd<>'HNCASE'
and wca.exchg.exchgcd<>'HTHTSE'
and wca.exchg.exchgcd<>'JPNSE'
and wca.exchg.exchgcd<>'KNECSE'
and wca.exchg.exchgcd<>'LSMSM'
and wca.exchg.exchgcd<>'LUUL'
and wca.exchg.exchgcd<>'MVMSX'
and wca.exchg.exchgcd<>'MYLFX'
and wca.exchg.exchgcd<>'MZMSX'
and wca.exchg.exchgcd<>'NOOTC'
and wca.exchg.exchgcd<>'PLOTC'
and wca.exchg.exchgcd<>'SENDX'
and wca.exchg.exchgcd<>'TJCASE'
and wca.exchg.exchgcd<>'UAINX'
and wca.exchg.exchgcd<>'UAKISE'
and wca.exchg.exchgcd<>'UYUEX'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>date_sub(now(), interval 31 day))
and (wca.scmst.statusflag<>'I' or wca.scmst.acttime>date_sub(now(), interval 31 day))
and (wca.scmst.actflag<>'D' or wca.scmst.acttime>date_sub(now(), interval 31 day))
and (wca.scexh.actflag<>'D' or wca.scexh.acttime>date_sub(now(), interval 31 day))
and (wca.exchg.actflag<>'D' or wca.exchg.acttime>date_sub(now(), interval 31 day))
and (wca.warex.warexid is null or wca.warex.warexid = 
(select warexid from wca.warex 
where wca.scmst.secid = wca.warex.secid 
order by wca.warex.todate limit 1))
and (wca.rts.rtsid is null or wca.rts.rtsid = 
(select rtsid from wca.rts 
where wca.scmst.secid = wca.rts.trasecid 
order by wca.rts.rtsid desc limit 1))
);
alter table `wca2`.`api_loadingtable`
modify Record_ID bigint,
add primary key (`record_id`),
add index `ix_secid`(`security_id`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`),
add index `ix_figi`(`figi`);
drop table if exists wca2.api_security_reference;
rename table wca2.api_loadingtable to wca2.api_security_reference;
insert ignore into wca2.api_security_reference
select
concat(
(select case when sedolseq.seqnumreg is not null then sedolseq.seqnumreg
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end),
(select case when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when bbeseq.seqnum is not null then bbeseq.seqnum
     else '1'
     end),
(select case when bbcseq.seqnum is not null then bbcseq.seqnum 
     when bbeseq.seqnum is not null then bbeseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end),
(select case when bbeseq.seqnum is not null then bbeseq.seqnum 
     when bbcseq.seqnum is not null then bbcseq.seqnum
     when sedolseq.seqnumcur is not null then sedolseq.seqnumcur
     else '1'
     end),wca.scexh.ScexhID) as Record_ID,
case when exchg.actflag = 'D' then 'Deleted'
     when scmst.actflag = 'D' then 'Deleted'
     when scexh.actflag = 'D' then 'Deleted'
     else 'Updated' end as action_flag,
case when wca.scexh.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.exchg.acttime
     when sedolseq.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then sedolseq.acttime
     when bbeseq.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then bbeseq.acttime
     when bbcseq.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then bbcseq.acttime
     when bbebnd.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then bbebnd.acttime
     when wca.scxtc.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.mktsg.acttime
     when wca.dprcp.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.dprcp.acttime
     when wca.bond.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.bond.acttime
     when wca.warex.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.warex.acttime
     when wca.cowar.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.cowar.acttime
     when wca.rts.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.rts.acttime
     when wca.rd.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.rd.acttime
     when wca.scexh.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.exchg.acttime
     when sedolseq.acttime > (select max(feeddate) from wca.tbl_opslog) then sedolseq.acttime
     when bbeseq.acttime > (select max(feeddate) from wca.tbl_opslog) then bbeseq.acttime
     when bbcseq.acttime > (select max(feeddate) from wca.tbl_opslog) then bbcseq.acttime
     when bbebnd.acttime > (select max(feeddate) from wca.tbl_opslog) then bbebnd.acttime
     when wca.scxtc.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.mktsg.acttime
     when wca.dprcp.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.dprcp.acttime
     when wca.bond.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.bond.acttime
     when wca.warex.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.warex.acttime
     when wca.cowar.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.cowar.acttime
     when wca.rts.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.rts.acttime
     when wca.rd.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.rd.acttime
     when smf4.security.actdate > (select max(feeddate) from wca.tbl_opslog) then smf4.security.actdate
     else wca.scexh.acttime
     end as changed,
wca.scexh.AnnounceDate as Created,
case when wca.scmst.Statusflag = 'I' then 'Inactive'
     when wca.scmst.Statusflag = 'D' then 'In default'
     else 'Active' end as Global_Security_Status,
wca.scmst.SecID as Security_ID,
wca.scmst.IssID as Issuer_ID,
wca.issur.IssuerName as Issuer_Name,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else ''
     end as ISIN,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     else ''
     end as US_Code,
wca.issur.CntryofIncorp as Incorporation_Country,
ifnull(wca.issur.SIC,'') as SIC,
ifnull(wca.issur.CIK,'') as CIK,
ifnull(wca.scmst.CFI,'') as CFI,
ifnull(wca.issur.LEI,'') as LEI,
case when ifnull(wca.bond.bondtype,'')<>'' then wca.bond.bondtype
     when ifnull(wca.dprcp.drtype,'')<>'' then wca.dprcp.drtype
     else wca.scmst.SectyCD
     end as Security_Type_Code,
wca.scmst.StructCD as Security_Structure_Code,
wca.scmst.SecurityDesc as Security_Description,
wca.scmst.ParValue as Par_Value,
wca.scmst.CurenCD as Par_Value_Currency,
wca.scmst.PrimaryExchgCD as Primary_Exchange_Code,
wca.exchg.CntryCD as Listing_Country,
ifnull(sedolbbg.curencd,'') as Trading_Currency,
ifnull(sedolbbg.bbgcompid,'') as BBG_Composite_Global_ID,
ifnull(sedolbbg.bbgcomptk,'') as BBG_Composite_Ticker,
case when ifnull(sedolbbg.bbgexhid,'')<>'' then ifnull(sedolbbg.bbgexhid,'')
     else ifnull(bbebnd.bbgexhid,'')
     end as FIGI,
case when ifnull(sedolbbg.bbgexhtk,'')<>'' then ifnull(sedolbbg.bbgexhtk,'')
     else ifnull(bbebnd.bbgexhtk,'')
     end as FIGI_Ticker,
ifnull(sedolbbg.Sedol,'') as SEDOL,
case when ifnull(sedolbbg.SedolDefunct,'')='T' then 'Yes'
     else ''
     end as SEDOL_Defunct,
ifnull(sedolbbg.RcntryCD,'') as SEDOL_Register_Country,
ifnull(smf4.security.OPOL,'') as SEDOL_MIC,
wca.scexh.ExchgCD as Exchange_Code,
ifnull(wca.exchg.Mic,'') as MIC,
ifnull(wca.mktsg.mktsegment,'') as Segment_Name,
ifnull(wca.mktsg.mic,'') as Segment_MIC,
case when ifnull(gbp04.TIDM,'')<>'' and ifnull(sedolbbg.sedoldefunct,'')<>'T' and ifnull(smf4.security.statusflag,'')='T'
     then replace(gbp04.TIDM,'.L','')
     when ifnull(wca.scxtc.localCode,'')<>''
     then wca.scxtc.LocalCode
     else wca.scexh.LocalCode
     end as Local_Code,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'Pending'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'Pending'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'Pending'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'Listed'
     when wca.scexh.ListStatus='S' then 'Suspended'
     when wca.scexh.ListStatus='D' then 'Delisted'
     else 'Unknown'
     end as List_Status,
wca.scexh.listdate as List_Date,
wca.scexh.delistdate as Delist_Date,
ifnull(wca.ipo.IPOStatus,'') as IPO_Status,
wca.ipo.FirstTradingDate as First_Trading_Date,
case when wca.dprcp.unsecid is not null then wca.dprcp.unsecid
     when wca.warex.exersecid is not null then wca.warex.exersecid
     when wca.cowar.unsecid is not null then wca.cowar.unsecid
     when wca.rd.secid is not null then wca.rd.secid
     else ''
     end as Underlying_Security_ID,
case when ifnull(wca.dprcp.usratio,'')<>'' then wca.dprcp.usratio
     when ifnull(wca.warex.rationew,'')<>'' then wca.warex.rationew
     when ifnull(wca.cowar.unratio,'')<>'' then wca.cowar.unratio
     when ifnull(wca.rts.NewExerciseRatio,'')<>'' then wca.rts.NewExerciseRatio
     when wca.rd.secid is not null then '1'
     else ''
     end as Underlying_Ratio_Numerator,
case when ifnull(wca.dprcp.drratio,'')<>'' then wca.dprcp.drratio
     when ifnull(wca.warex.ratioold,'')<>'' then wca.warex.ratioold
     when ifnull(wca.cowar.warrantratio,'')<>'' then wca.cowar.warrantratio
     when ifnull(wca.rts.OldExerciseRatio,'')<>'' then wca.rts.OldExerciseRatio
     when wca.rd.secid is not null then '1'
     else ''
     end as Parent_Ratio_Denominator
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca2.sedolbbgseq as sedolbbg on wca.scexh.secid=sedolbbg.secid and wca.scexh.exchgcd = sedolbbg.exchgcd
left outer join wca.lcc on wca.scexh.secid = wca.lcc.secid and wca.scexh.exchgcd=wca.lcc.exchgcd 
                               and (select max(feeddate) from wca.tbl_opslog)=wca.lcc.effectivedate and sedolbbg.RcntryCD='GB' 
left outer join prices.GBLSE_Raw_Daily_Prices as gbp04 on sedolbbg.sedol=gbp04.Sedol
left outer join wca2.sedolseq as sedolseq on sedolbbg.sedolid=sedolseq.sedolid
left outer join wca2.bbcseq as bbcseq on sedolbbg.bbcid=bbcseq.bbcid
left outer join wca2.bbeseq as bbeseq on sedolbbg.bbeid=bbeseq.bbeid
left outer join wca.bbe as bbebnd on wca.scmst.secid =bbebnd.secid and ''=ifnull(bbebnd.exchgcd,'X') and ''<>ifnull(bbebnd.bbgexhid,'') and 'D'<>ifnull(bbebnd.bbgexhid,'')
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and sedolbbg.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on sedolbbg.sedol=smf4.security.sedol
left outer join wca.dprcp on wca.scmst.secid=dprcp.secid and 'D'<>wca.dprcp.actflag
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.warex on wca.scmst.secid = wca.warex.SecID and 'D'<> wca.warex.actflag and now()<ifnull(wca.warex.todate,'2001-01-01')
left outer join wca.cowar on wca.scmst.secid = wca.cowar.SecID and 'D'<> wca.cowar.actflag and now()<ifnull(wca.cowar.expirationdate,'2001-01-01')
left outer join wca.rts on wca.scmst.secid = wca.rts.trasecid and 'D'<> wca.rts.actflag and wca.scmst.sectycd='TRT'
left outer join wca.rd on wca.rts.rdid = wca.rd.rdid
where
(wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='CW' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or wca.scmst.sectycd='IDX' or (wca.scmst.sectycd='BND' and (wca.scexh.localcode<>'' or substring(wca.scmst.isin,1,2)='XS')))
and wca.scexh.exchgcd='GBLSE'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>date_sub(now(), interval 31 day))
and (wca.scmst.statusflag<>'I' or wca.scmst.acttime>date_sub(now(), interval 31 day))
and (wca.scmst.actflag<>'D' or wca.scmst.acttime>date_sub(now(), interval 31 day))
and (wca.scexh.actflag<>'D' or wca.scexh.acttime>date_sub(now(), interval 31 day))
and (wca.exchg.actflag<>'D' or wca.exchg.acttime>date_sub(now(), interval 31 day))
and (wca.warex.warexid is null or wca.warex.warexid = 
(select warexid from wca.warex 
where wca.scmst.secid = wca.warex.secid 
order by wca.warex.todate limit 1))
and (wca.rts.rtsid is null or wca.rts.rtsid = 
(select rtsid from wca.rts 
where wca.scmst.secid = wca.rts.trasecid 
order by wca.rts.rtsid desc limit 1));
insert ignore into wca2.api_security_reference
select
concat('1111',wca.scexh.ScexhID) as Record_ID,
case when exchg.actflag = 'D' then 'Deleted'
     when scmst.actflag = 'D' then 'Deleted'
     when scexh.actflag = 'D' then 'Deleted'
     else 'Updated' end as action_flag,
case when wca.scexh.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.exchg.acttime
     when wca_other.sedol.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca_other.sedol.acttime
     when wca.bbe.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.bbe.acttime
     when wca.bbc.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.bbc.acttime
     when wca.scxtc.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.mktsg.acttime
     when wca.dprcp.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.dprcp.acttime
     when wca.warex.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.warex.acttime
     when wca.cowar.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.cowar.acttime
     when wca.rts.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.rts.acttime
     when wca.rd.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.rd.acttime
     when wca.scexh.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.exchg.acttime
     when wca_other.sedol.acttime > (select max(feeddate) from wca.tbl_opslog) then wca_other.sedol.acttime
     when wca.bbe.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.bbe.acttime
     when wca.bbc.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.bbc.acttime
     when wca.scxtc.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.mktsg.acttime
     when wca.dprcp.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.dprcp.acttime
     when wca.warex.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.warex.acttime
     when wca.cowar.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.cowar.acttime
     when wca.rts.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.rts.acttime
     when wca.rd.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.rd.acttime
     when smf4.security.actdate > (select max(feeddate) from wca.tbl_opslog) then smf4.security.actdate
     else wca.scexh.acttime
     end as changed,
wca.scexh.AnnounceDate as Created,
case when wca.scmst.Statusflag = 'I' then 'Inactive'
     when wca.scmst.Statusflag = 'D' then 'In default'
     else 'Active' end as Global_Security_Status,
wca.scmst.SecID as Security_ID,
wca.scmst.IssID as Issuer_ID,
wca.issur.IssuerName as Issuer_Name,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else ''
     end as ISIN,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     else ''
     end as US_Code,
wca.issur.CntryofIncorp as Incorporation_Country,
ifnull(wca.issur.SIC,'') as SIC,
ifnull(wca.issur.CIK,'') as CIK,
ifnull(wca.scmst.CFI,'') as CFI,
ifnull(wca.issur.LEI,'') as LEI,
case when ifnull(wca.dprcp.drtype,'')<>'' then wca.dprcp.drtype
     else wca.scmst.SectyCD
     end as Security_Type_Code,
wca.scmst.StructCD as Security_Structure_Code,
wca.scmst.SecurityDesc as Security_Description,
wca.scmst.ParValue as Par_Value,
wca.scmst.CurenCD as Par_Value_Currency,
wca.scmst.PrimaryExchgCD as Primary_Exchange_Code,
wca.exchg.CntryCD as Listing_Country,
ifnull(wca_other.sedol.curencd,'') as Trading_Currency,
ifnull(wca.bbc.bbgcompid,'') as BBG_Composite_Global_ID,
ifnull(wca.bbc.bbgcomptk,'') as BBG_Composite_Ticker,
case when ifnull(wca.bbe.bbgexhid,'')<>'' then ifnull(wca.bbe.bbgexhid,'')
     else ''
     end as FIGI,
case when ifnull(wca.bbe.bbgexhtk,'')<>'' then ifnull(wca.bbe.bbgexhtk,'')
     else ''
     end as FIGI_Ticker,
ifnull(wca_other.sedol.sedol,'') as SEDOL,
case when ifnull(wca_other.sedol.defunct,'')='T' then 'Yes'
     else ''
     end as SEDOL_Defunct,
ifnull(smf4.security.cregcode,'') as SEDOL_Register_Country,
ifnull(smf4.security.OPOL,'') as SEDOL_MIC,
wca.scexh.ExchgCD as Exchange_Code,
ifnull(wca.exchg.Mic,'') as MIC,
ifnull(wca.mktsg.mktsegment,'') as Segment_Name,
ifnull(wca.mktsg.mic,'') as Segment_MIC,
wca.scxtc.LocalCode as Local_Code,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'Pending'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'Pending'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'Pending'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'Listed'
     when wca.scexh.ListStatus='S' then 'Suspended'
     when wca.scexh.ListStatus='D' then 'Delisted'
     else 'Unknown'
     end as List_Status,
wca.scexh.listdate as List_Date,
wca.scexh.delistdate as Delist_Date,
ifnull(wca.ipo.IPOStatus,'') as IPO_Status,
wca.ipo.FirstTradingDate as First_Trading_Date,
case when wca.dprcp.unsecid is not null then wca.dprcp.unsecid
     when wca.warex.exersecid is not null then wca.warex.exersecid
     when wca.cowar.unsecid is not null then wca.cowar.unsecid
     when wca.rd.secid is not null then wca.rd.secid
     else ''
     end as Underlying_Security_ID,
case when ifnull(wca.dprcp.usratio,'')<>'' then wca.dprcp.usratio
     when ifnull(wca.warex.rationew,'')<>'' then wca.warex.rationew
     when ifnull(wca.cowar.unratio,'')<>'' then wca.cowar.unratio
     when ifnull(wca.rts.NewExerciseRatio,'')<>'' then wca.rts.NewExerciseRatio
     when wca.rd.secid is not null then '1'
     else ''
     end as Underlying_Ratio_Numerator,
case when ifnull(wca.dprcp.drratio,'')<>'' then wca.dprcp.drratio
     when ifnull(wca.warex.ratioold,'')<>'' then wca.warex.ratioold
     when ifnull(wca.cowar.warrantratio,'')<>'' then wca.cowar.warrantratio
     when ifnull(wca.rts.OldExerciseRatio,'')<>'' then wca.rts.OldExerciseRatio
     when wca.rd.secid is not null then '1'
     else ''
     end as Parent_Ratio_Denominator
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.bbe on wca.scexh.secid=wca.bbe.secid and wca.scexh.exchgcd=ifnull(wca.bbe.exchgcd,'') and 'D'<>wca.bbe.actflag
left outer join wca.bbc on wca.bbe.secid=wca.bbc.secid and 'XG'=ifnull(wca.bbc.cntrycd,'') and 'D'<>wca.bbc.actflag
left outer join wca_other.sedol on wca.scexh.secid=wca_other.sedol.secid and 'CN'=wca_other.sedol.cntrycd and 'XH'=wca_other.sedol.rcntrycd
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and wca_other.sedol.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on wca_other.sedol.sedol=smf4.security.sedol
left outer join wca.dprcp on wca.scmst.secid=dprcp.secid and 'D'<>wca.dprcp.actflag
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.warex on wca.scmst.secid = wca.warex.SecID and 'D'<> wca.warex.actflag and now()<ifnull(wca.warex.todate,'2001-01-01')
left outer join wca.cowar on wca.scmst.secid = wca.cowar.SecID and 'D'<> wca.cowar.actflag and now()<ifnull(wca.cowar.expirationdate,'2001-01-01')
left outer join wca.rts on wca.scmst.secid = wca.rts.trasecid and 'D'<> wca.rts.actflag and wca.scmst.sectycd='TRT'
left outer join wca.rd on wca.rts.rdid = wca.rd.rdid
where
(wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='CW' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or wca.scmst.sectycd='IDX')
and wca.scexh.exchgcd='CNSGHK'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>date_sub(now(), interval 31 day))
and (wca.scmst.statusflag<>'I' or wca.scmst.acttime>date_sub(now(), interval 31 day))
and (wca.scmst.actflag<>'D' or wca.scmst.acttime>date_sub(now(), interval 31 day))
and (wca.scexh.actflag<>'D' or wca.scexh.acttime>date_sub(now(), interval 31 day))
and (wca.exchg.actflag<>'D' or wca.exchg.acttime>date_sub(now(), interval 31 day))
and (wca.warex.warexid is null or wca.warex.warexid = 
(select warexid from wca.warex 
where wca.scmst.secid = wca.warex.secid 
order by wca.warex.todate limit 1))
and (wca.rts.rtsid is null or wca.rts.rtsid = 
(select rtsid from wca.rts 
where wca.scmst.secid = wca.rts.trasecid 
order by wca.rts.rtsid desc limit 1));
insert ignore into wca2.api_security_reference
select
concat('1111',wca.scexh.ScexhID) as Record_ID,
case when exchg.actflag = 'D' then 'Deleted'
     when scmst.actflag = 'D' then 'Deleted'
     when scexh.actflag = 'D' then 'Deleted'
     else 'Updated' end as action_flag,
case when wca.scexh.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.exchg.acttime
     when wca_other.sedol.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca_other.sedol.acttime
     when wca.bbe.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.bbe.acttime
     when wca.bbc.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.bbc.acttime
     when wca.scxtc.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.mktsg.acttime
     when wca.dprcp.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.dprcp.acttime
     when wca.warex.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.warex.acttime
     when wca.cowar.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.cowar.acttime
     when wca.rts.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.rts.acttime
     when wca.rd.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.rd.acttime
     when wca.scexh.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.exchg.acttime
     when wca_other.sedol.acttime > (select max(feeddate) from wca.tbl_opslog) then wca_other.sedol.acttime
     when wca.bbe.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.bbe.acttime
     when wca.bbc.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.bbc.acttime
     when wca.scxtc.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.mktsg.acttime
     when wca.dprcp.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.dprcp.acttime
     when wca.warex.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.warex.acttime
     when wca.cowar.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.cowar.acttime
     when wca.rts.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.rts.acttime
     when wca.rd.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.rd.acttime
     when smf4.security.actdate > (select max(feeddate) from wca.tbl_opslog) then smf4.security.actdate
     else wca.scexh.acttime
     end as changed,
wca.scexh.AnnounceDate as Created,
case when wca.scmst.Statusflag = 'I' then 'Inactive'
     when wca.scmst.Statusflag = 'D' then 'In default'
     else 'Active' end as Global_Security_Status,
wca.scmst.SecID as Security_ID,
wca.scmst.IssID as Issuer_ID,
wca.issur.IssuerName as Issuer_Name,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else ''
     end as ISIN,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     else ''
     end as US_Code,
wca.issur.CntryofIncorp as Incorporation_Country,
ifnull(wca.issur.SIC,'') as SIC,
ifnull(wca.issur.CIK,'') as CIK,
ifnull(wca.scmst.CFI,'') as CFI,
ifnull(wca.issur.LEI,'') as LEI,
case when ifnull(wca.dprcp.drtype,'')<>'' then wca.dprcp.drtype
     else wca.scmst.SectyCD
     end as Security_Type_Code,
wca.scmst.StructCD as Security_Structure_Code,
wca.scmst.SecurityDesc as Security_Description,
wca.scmst.ParValue as Par_Value,
wca.scmst.CurenCD as Par_Value_Currency,
wca.scmst.PrimaryExchgCD as Primary_Exchange_Code,
wca.exchg.CntryCD as Listing_Country,
ifnull(wca_other.sedol.curencd,'') as Trading_Currency,
ifnull(wca.bbc.bbgcompid,'') as BBG_Composite_Global_ID,
ifnull(wca.bbc.bbgcomptk,'') as BBG_Composite_Ticker,
case when ifnull(wca.bbe.bbgexhid,'')<>'' then ifnull(wca.bbe.bbgexhid,'')
     else ''
     end as FIGI,
case when ifnull(wca.bbe.bbgexhtk,'')<>'' then ifnull(wca.bbe.bbgexhtk,'')
     else ''
     end as FIGI_Ticker,
ifnull(wca_other.sedol.sedol,'') as SEDOL,
case when ifnull(wca_other.sedol.defunct,'')='T' then 'Yes'
     else ''
     end as SEDOL_Defunct,
ifnull(smf4.security.cregcode,'') as SEDOL_Register_Country,
ifnull(smf4.security.OPOL,'') as SEDOL_MIC,
wca.scexh.ExchgCD as Exchange_Code,
ifnull(wca.exchg.Mic,'') as MIC,
ifnull(wca.mktsg.mktsegment,'') as Segment_Name,
ifnull(wca.mktsg.mic,'') as Segment_MIC,
wca.scxtc.LocalCode as Local_Code,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'Pending'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'Pending'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'Pending'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'Listed'
     when wca.scexh.ListStatus='S' then 'Suspended'
     when wca.scexh.ListStatus='D' then 'Delisted'
     else 'Unknown'
     end as List_Status,
wca.scexh.listdate as List_Date,
wca.scexh.delistdate as Delist_Date,
ifnull(wca.ipo.IPOStatus,'') as IPO_Status,
wca.ipo.FirstTradingDate as First_Trading_Date,
case when wca.dprcp.unsecid is not null then wca.dprcp.unsecid
     when wca.warex.exersecid is not null then wca.warex.exersecid
     when wca.cowar.unsecid is not null then wca.cowar.unsecid
     when wca.rd.secid is not null then wca.rd.secid
     else ''
     end as Underlying_Security_ID,
case when ifnull(wca.dprcp.usratio,'')<>'' then wca.dprcp.usratio
     when ifnull(wca.warex.rationew,'')<>'' then wca.warex.rationew
     when ifnull(wca.cowar.unratio,'')<>'' then wca.cowar.unratio
     when ifnull(wca.rts.NewExerciseRatio,'')<>'' then wca.rts.NewExerciseRatio
     when wca.rd.secid is not null then '1'
     else ''
     end as Underlying_Ratio_Numerator,
case when ifnull(wca.dprcp.drratio,'')<>'' then wca.dprcp.drratio
     when ifnull(wca.warex.ratioold,'')<>'' then wca.warex.ratioold
     when ifnull(wca.cowar.warrantratio,'')<>'' then wca.cowar.warrantratio
     when ifnull(wca.rts.OldExerciseRatio,'')<>'' then wca.rts.OldExerciseRatio
     when wca.rd.secid is not null then '1'
     else ''
     end as Parent_Ratio_Denominator
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.bbe on wca.scexh.secid=wca.bbe.secid and wca.scexh.exchgcd=ifnull(wca.bbe.exchgcd,'') and 'D'<>wca.bbe.actflag
left outer join wca.bbc on wca.bbe.secid=wca.bbc.secid and 'XZ'=ifnull(wca.bbc.cntrycd,'') and 'D'<>wca.bbc.actflag
left outer join wca_other.sedol on wca.scexh.secid=wca_other.sedol.secid and 'CN'=wca_other.sedol.cntrycd and 'XH'=wca_other.sedol.rcntrycd
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and wca_other.sedol.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on wca_other.sedol.sedol=smf4.security.sedol
left outer join wca.dprcp on wca.scmst.secid=dprcp.secid and 'D'<>wca.dprcp.actflag
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.warex on wca.scmst.secid = wca.warex.SecID and 'D'<> wca.warex.actflag and now()<ifnull(wca.warex.todate,'2001-01-01')
left outer join wca.cowar on wca.scmst.secid = wca.cowar.SecID and 'D'<> wca.cowar.actflag and now()<ifnull(wca.cowar.expirationdate,'2001-01-01')
left outer join wca.rts on wca.scmst.secid = wca.rts.trasecid and 'D'<> wca.rts.actflag and wca.scmst.sectycd='TRT'
left outer join wca.rd on wca.rts.rdid = wca.rd.rdid
where
(wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='CW' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or wca.scmst.sectycd='IDX')
and wca.scexh.exchgcd='CNSZHK'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>date_sub(now(), interval 31 day))
and (wca.scmst.statusflag<>'I' or wca.scmst.acttime>date_sub(now(), interval 31 day))
and (wca.scmst.actflag<>'D' or wca.scmst.acttime>date_sub(now(), interval 31 day))
and (wca.scexh.actflag<>'D' or wca.scexh.acttime>date_sub(now(), interval 31 day))
and (wca.exchg.actflag<>'D' or wca.exchg.acttime>date_sub(now(), interval 31 day))
and (wca.warex.warexid is null or wca.warex.warexid = 
(select warexid from wca.warex 
where wca.scmst.secid = wca.warex.secid 
order by wca.warex.todate limit 1))
and (wca.rts.rtsid is null or wca.rts.rtsid = 
(select rtsid from wca.rts 
where wca.scmst.secid = wca.rts.trasecid 
order by wca.rts.rtsid desc limit 1));
insert ignore into wca2.api_security_reference
select
concat('1111',wca.scexh.ScexhID) as Record_ID,
case when exchg.actflag = 'D' then 'Deleted'
     when scmst.actflag = 'D' then 'Deleted'
     when scexh.actflag = 'D' then 'Deleted'
     else 'Updated' end as action_flag,
case when wca.scexh.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.exchg.acttime
     when wca_other.sedol.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca_other.sedol.acttime
     when wca.bbe.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.bbe.acttime
     when wca.bbc.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.bbc.acttime
     when wca.scxtc.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.mktsg.acttime
     when wca.dprcp.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.dprcp.acttime
     when wca.warex.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.warex.acttime
     when wca.cowar.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.cowar.acttime
     when wca.rts.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.rts.acttime
     when wca.rd.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.rd.acttime
     when wca.scexh.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.exchg.acttime
     when wca_other.sedol.acttime > (select max(feeddate) from wca.tbl_opslog) then wca_other.sedol.acttime
     when wca.bbe.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.bbe.acttime
     when wca.bbc.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.bbc.acttime
     when wca.scxtc.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.mktsg.acttime
     when wca.dprcp.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.dprcp.acttime
     when wca.warex.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.warex.acttime
     when wca.cowar.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.cowar.acttime
     when wca.rts.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.rts.acttime
     when wca.rd.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.rd.acttime
     when smf4.security.actdate > (select max(feeddate) from wca.tbl_opslog) then smf4.security.actdate
     else wca.scexh.acttime
     end as changed,
wca.scexh.AnnounceDate as Created,
case when wca.scmst.Statusflag = 'I' then 'Inactive'
     when wca.scmst.Statusflag = 'D' then 'In default'
     else 'Active' end as Global_Security_Status,
wca.scmst.SecID as Security_ID,
wca.scmst.IssID as Issuer_ID,
wca.issur.IssuerName as Issuer_Name,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else ''
     end as ISIN,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     else ''
     end as US_Code,
wca.issur.CntryofIncorp as Incorporation_Country,
ifnull(wca.issur.SIC,'') as SIC,
ifnull(wca.issur.CIK,'') as CIK,
ifnull(wca.scmst.CFI,'') as CFI,
ifnull(wca.issur.LEI,'') as LEI,
case when ifnull(wca.dprcp.drtype,'')<>'' then wca.dprcp.drtype
     else wca.scmst.SectyCD
     end as Security_Type_Code,
wca.scmst.StructCD as Security_Structure_Code,
wca.scmst.SecurityDesc as Security_Description,
wca.scmst.ParValue as Par_Value,
wca.scmst.CurenCD as Par_Value_Currency,
wca.scmst.PrimaryExchgCD as Primary_Exchange_Code,
wca.exchg.CntryCD as Listing_Country,
ifnull(wca_other.sedol.curencd,'') as Trading_Currency,
ifnull(wca.bbc.bbgcompid,'') as BBG_Composite_Global_ID,
ifnull(wca.bbc.bbgcomptk,'') as BBG_Composite_Ticker,
case when ifnull(wca.bbe.bbgexhid,'')<>'' then ifnull(wca.bbe.bbgexhid,'')
     else ''
     end as FIGI,
case when ifnull(wca.bbe.bbgexhtk,'')<>'' then ifnull(wca.bbe.bbgexhtk,'')
     else ''
     end as FIGI_Ticker,
ifnull(wca_other.sedol.sedol,'') as SEDOL,
case when ifnull(wca_other.sedol.defunct,'')='T' then 'Yes'
     else ''
     end as SEDOL_Defunct,
ifnull(smf4.security.cregcode,'') as SEDOL_Register_Country,
ifnull(smf4.security.OPOL,'') as SEDOL_MIC,
wca.scexh.ExchgCD as Exchange_Code,
ifnull(wca.exchg.Mic,'') as MIC,
ifnull(wca.mktsg.mktsegment,'') as Segment_Name,
ifnull(wca.mktsg.mic,'') as Segment_MIC,
wca.scxtc.LocalCode as Local_Code,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'Pending'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'Pending'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'Pending'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'Listed'
     when wca.scexh.ListStatus='S' then 'Suspended'
     when wca.scexh.ListStatus='D' then 'Delisted'
     else 'Unknown'
     end as List_Status,
wca.scexh.listdate as List_Date,
wca.scexh.delistdate as Delist_Date,
ifnull(wca.ipo.IPOStatus,'') as IPO_Status,
wca.ipo.FirstTradingDate as First_Trading_Date,
case when wca.dprcp.unsecid is not null then wca.dprcp.unsecid
     when wca.warex.exersecid is not null then wca.warex.exersecid
     when wca.cowar.unsecid is not null then wca.cowar.unsecid
     when wca.rd.secid is not null then wca.rd.secid
     else ''
     end as Underlying_Security_ID,
case when ifnull(wca.dprcp.usratio,'')<>'' then wca.dprcp.usratio
     when ifnull(wca.warex.rationew,'')<>'' then wca.warex.rationew
     when ifnull(wca.cowar.unratio,'')<>'' then wca.cowar.unratio
     when ifnull(wca.rts.NewExerciseRatio,'')<>'' then wca.rts.NewExerciseRatio
     when wca.rd.secid is not null then '1'
     else ''
     end as Underlying_Ratio_Numerator,
case when ifnull(wca.dprcp.drratio,'')<>'' then wca.dprcp.drratio
     when ifnull(wca.warex.ratioold,'')<>'' then wca.warex.ratioold
     when ifnull(wca.cowar.warrantratio,'')<>'' then wca.cowar.warrantratio
     when ifnull(wca.rts.OldExerciseRatio,'')<>'' then wca.rts.OldExerciseRatio
     when wca.rd.secid is not null then '1'
     else ''
     end as Parent_Ratio_Denominator
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.bbe on wca.scexh.secid=wca.bbe.secid and wca.scexh.exchgcd=ifnull(wca.bbe.exchgcd,'') and 'D'<>wca.bbe.actflag
left outer join wca.bbc on wca.bbe.secid=wca.bbc.secid and 'XH'=ifnull(wca.bbc.cntrycd,'') and 'D'<>wca.bbc.actflag
left outer join wca_other.sedol on wca.scexh.secid=wca_other.sedol.secid and 'HK'=wca_other.sedol.cntrycd and 'XZ'=wca_other.sedol.rcntrycd
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and wca_other.sedol.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on wca_other.sedol.sedol=smf4.security.sedol
left outer join wca.dprcp on wca.scmst.secid=dprcp.secid and 'D'<>wca.dprcp.actflag
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.warex on wca.scmst.secid = wca.warex.SecID and 'D'<> wca.warex.actflag and now()<ifnull(wca.warex.todate,'2001-01-01')
left outer join wca.cowar on wca.scmst.secid = wca.cowar.SecID and 'D'<> wca.cowar.actflag and now()<ifnull(wca.cowar.expirationdate,'2001-01-01')
left outer join wca.rts on wca.scmst.secid = wca.rts.trasecid and 'D'<> wca.rts.actflag and wca.scmst.sectycd='TRT'
left outer join wca.rd on wca.rts.rdid = wca.rd.rdid
where
(wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='CW' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or wca.scmst.sectycd='IDX')
and wca.scexh.exchgcd='HKHKSZ'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>date_sub(now(), interval 31 day))
and (wca.scmst.statusflag<>'I' or wca.scmst.acttime>date_sub(now(), interval 31 day))
and (wca.scmst.actflag<>'D' or wca.scmst.acttime>date_sub(now(), interval 31 day))
and (wca.scexh.actflag<>'D' or wca.scexh.acttime>date_sub(now(), interval 31 day))
and (wca.exchg.actflag<>'D' or wca.exchg.acttime>date_sub(now(), interval 31 day))
and (wca.warex.warexid is null or wca.warex.warexid = 
(select warexid from wca.warex 
where wca.scmst.secid = wca.warex.secid 
order by wca.warex.todate limit 1))
and (wca.rts.rtsid is null or wca.rts.rtsid = 
(select rtsid from wca.rts 
where wca.scmst.secid = wca.rts.trasecid 
order by wca.rts.rtsid desc limit 1));
insert ignore into wca2.api_security_reference
select
concat('1111',wca.scexh.ScexhID) as Record_ID,
case when exchg.actflag = 'D' then 'Deleted'
     when scmst.actflag = 'D' then 'Deleted'
     when scexh.actflag = 'D' then 'Deleted'
     else 'Updated' end as action_flag,
case when wca.scexh.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.exchg.acttime
     when wca_other.sedol.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca_other.sedol.acttime
     when wca.bbe.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.bbe.acttime
     when wca.bbc.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.bbc.acttime
     when wca.scxtc.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.mktsg.acttime
     when wca.dprcp.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.dprcp.acttime
     when wca.warex.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.warex.acttime
     when wca.cowar.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.cowar.acttime
     when wca.rts.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.rts.acttime
     when wca.rd.acttime > (select date_sub(max(acttime_new), interval '20' minute) from wca.tbl_opslog) then wca.rd.acttime
     when wca.scexh.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scexh.acttime
     when wca.scmst.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scmst.acttime
     when wca.issur.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.issur.acttime
     when wca.exchg.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.exchg.acttime
     when wca_other.sedol.acttime > (select max(feeddate) from wca.tbl_opslog) then wca_other.sedol.acttime
     when wca.bbe.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.bbe.acttime
     when wca.bbc.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.bbc.acttime
     when wca.scxtc.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.scxtc.acttime
     when wca.ipo.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.ipo.acttime
     when wca.mktsg.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.mktsg.acttime
     when wca.dprcp.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.dprcp.acttime
     when wca.warex.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.warex.acttime
     when wca.cowar.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.cowar.acttime
     when wca.rts.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.rts.acttime
     when wca.rd.acttime > (select max(feeddate) from wca.tbl_opslog) then wca.rd.acttime
     when smf4.security.actdate > (select max(feeddate) from wca.tbl_opslog) then smf4.security.actdate
     else wca.scexh.acttime
     end as changed,
wca.scexh.AnnounceDate as Created,
case when wca.scmst.Statusflag = 'I' then 'Inactive'
     when wca.scmst.Statusflag = 'D' then 'In default'
     else 'Active' end as Global_Security_Status,
wca.scmst.SecID as Security_ID,
wca.scmst.IssID as Issuer_ID,
wca.issur.IssuerName as Issuer_Name,
case when wca.scxtc.scexhid is null
     then wca.scmst.isin
     when wca.scxtc.Isin<>''
     then wca.scxtc.Isin
     else ''
     end as ISIN,
case when wca.scxtc.scexhid is null
     then wca.scmst.uscode
     else ''
     end as US_Code,
wca.issur.CntryofIncorp as Incorporation_Country,
ifnull(wca.issur.SIC,'') as SIC,
ifnull(wca.issur.CIK,'') as CIK,
ifnull(wca.scmst.CFI,'') as CFI,
ifnull(wca.issur.LEI,'') as LEI,
case when ifnull(wca.dprcp.drtype,'')<>'' then wca.dprcp.drtype
     else wca.scmst.SectyCD
     end as Security_Type_Code,
wca.scmst.StructCD as Security_Structure_Code,
wca.scmst.SecurityDesc as Security_Description,
wca.scmst.ParValue as Par_Value,
wca.scmst.CurenCD as Par_Value_Currency,
wca.scmst.PrimaryExchgCD as Primary_Exchange_Code,
wca.exchg.CntryCD as Listing_Country,
ifnull(wca_other.sedol.curencd,'') as Trading_Currency,
ifnull(wca.bbc.bbgcompid,'') as BBG_Composite_Global_ID,
ifnull(wca.bbc.bbgcomptk,'') as BBG_Composite_Ticker,
case when ifnull(wca.bbe.bbgexhid,'')<>'' then ifnull(wca.bbe.bbgexhid,'')
     else ''
     end as FIGI,
case when ifnull(wca.bbe.bbgexhtk,'')<>'' then ifnull(wca.bbe.bbgexhtk,'')
     else ''
     end as FIGI_Ticker,
ifnull(wca_other.sedol.sedol,'') as SEDOL,
case when ifnull(wca_other.sedol.defunct,'')='T' then 'Yes'
     else ''
     end as SEDOL_Defunct,
ifnull(smf4.security.cregcode,'') as SEDOL_Register_Country,
ifnull(smf4.security.OPOL,'') as SEDOL_MIC,
wca.scexh.ExchgCD as Exchange_Code,
ifnull(wca.exchg.Mic,'') as MIC,
ifnull(wca.mktsg.mktsegment,'') as Segment_Name,
ifnull(wca.mktsg.mic,'') as Segment_MIC,
wca.scxtc.LocalCode as Local_Code,
case when ifnull(wca.scexh.listdate,'2001/01/01')>now()
     then 'Pending'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and ifnull(wca.ipo.firsttradingdate,'2001/01/01')>now()
     then 'Pending'
     when ifnull(wca.scexh.listdate,'2099/01/01')>now() and (ifnull(wca.ipo.ipostatus,'')='PENDING' or ifnull(wca.ipo.ipostatus,'')='NEW' 
        or ifnull(wca.ipo.ipostatus,'')='POSTPONED') and wca.ipo.firsttradingdate is null
     then 'Pending'
     when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='R' or wca.scexh.ListStatus='' then 'Listed'
     when wca.scexh.ListStatus='S' then 'Suspended'
     when wca.scexh.ListStatus='D' then 'Delisted'
     else 'Unknown'
     end as List_Status,
wca.scexh.listdate as List_Date,
wca.scexh.delistdate as Delist_Date,
ifnull(wca.ipo.IPOStatus,'') as IPO_Status,
wca.ipo.FirstTradingDate as First_Trading_Date,
case when wca.dprcp.unsecid is not null then wca.dprcp.unsecid
     when wca.warex.exersecid is not null then wca.warex.exersecid
     when wca.cowar.unsecid is not null then wca.cowar.unsecid
     when wca.rd.secid is not null then wca.rd.secid
     else ''
     end as Underlying_Security_ID,
case when ifnull(wca.dprcp.usratio,'')<>'' then wca.dprcp.usratio
     when ifnull(wca.warex.rationew,'')<>'' then wca.warex.rationew
     when ifnull(wca.cowar.unratio,'')<>'' then wca.cowar.unratio
     when ifnull(wca.rts.NewExerciseRatio,'')<>'' then wca.rts.NewExerciseRatio
     when wca.rd.secid is not null then '1'
     else ''
     end as Underlying_Ratio_Numerator,
case when ifnull(wca.dprcp.drratio,'')<>'' then wca.dprcp.drratio
     when ifnull(wca.warex.ratioold,'')<>'' then wca.warex.ratioold
     when ifnull(wca.cowar.warrantratio,'')<>'' then wca.cowar.warrantratio
     when ifnull(wca.rts.OldExerciseRatio,'')<>'' then wca.rts.OldExerciseRatio
     when wca.rd.secid is not null then '1'
     else ''
     end as Parent_Ratio_Denominator
from wca.scexh
inner join wca.scmst on wca.scexh.secid = wca.scmst.secid
left outer join wca.sectygrp on scmst.sectycd=wca.sectygrp.sectycd and 3>wca.sectygrp.secgrpid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
left outer join wca.mktsg on wca.scexh.mktsgid = wca.mktsg.mktsgid
left outer join wca.bbe on wca.scexh.secid=wca.bbe.secid and wca.scexh.exchgcd=ifnull(wca.bbe.exchgcd,'') and 'D'<>wca.bbe.actflag
left outer join wca.bbc on wca.bbe.secid=wca.bbc.secid and 'XH'=ifnull(wca.bbc.cntrycd,'') and 'D'<>wca.bbc.actflag
left outer join wca_other.sedol on wca.scexh.secid=wca_other.sedol.secid and 'HK'=wca_other.sedol.cntrycd and 'XG'=wca_other.sedol.rcntrycd
left outer join wca.ipo on wca.scmst.secid=wca.ipo.secid 
                       and wca.ipo.acttime>(date_sub(date_format(now(),'%Y-%m-%d'), interval 730 day))
                       and wca.ipo.actflag<>'D'
left outer join wca.scxtc on wca.scexh.scexhid = wca.scxtc.scexhid and wca_other.sedol.curencd=wca.scxtc.curencd and 'D'<>wca.scxtc.actflag
left outer join smf4.security on wca_other.sedol.sedol=smf4.security.sedol
left outer join wca.dprcp on wca.scmst.secid=dprcp.secid and 'D'<>wca.dprcp.actflag
left outer join wca.bond on wca.scmst.secid=wca.bond.secid
left outer join wca.warex on wca.scmst.secid = wca.warex.SecID and 'D'<> wca.warex.actflag and now()<ifnull(wca.warex.todate,'2001-01-01')
left outer join wca.cowar on wca.scmst.secid = wca.cowar.SecID and 'D'<> wca.cowar.actflag and now()<ifnull(wca.cowar.expirationdate,'2001-01-01')
left outer join wca.rts on wca.scmst.secid = wca.rts.trasecid and 'D'<> wca.rts.actflag and wca.scmst.sectycd='TRT'
left outer join wca.rd on wca.rts.rdid = wca.rd.rdid
where
(wca.sectygrp.secgrpid is not null or wca.scmst.sectycd='CW' or wca.scmst.sectycd='ETC' or wca.scmst.sectycd='ETN' 
     or wca.scmst.sectycd='IDX')
and wca.scexh.exchgcd='HKHKSG'
and (wca.scexh.liststatus<>'D' or wca.scexh.acttime>date_sub(now(), interval 31 day))
and (wca.scmst.statusflag<>'I' or wca.scmst.acttime>date_sub(now(), interval 31 day))
and (wca.scmst.actflag<>'D' or wca.scmst.acttime>date_sub(now(), interval 31 day))
and (wca.scexh.actflag<>'D' or wca.scexh.acttime>date_sub(now(), interval 31 day))
and (wca.exchg.actflag<>'D' or wca.exchg.acttime>date_sub(now(), interval 31 day))
and (wca.warex.warexid is null or wca.warex.warexid = 
(select warexid from wca.warex 
where wca.scmst.secid = wca.warex.secid 
order by wca.warex.todate limit 1))
and (wca.rts.rtsid is null or wca.rts.rtsid = 
(select rtsid from wca.rts 
where wca.scmst.secid = wca.rts.trasecid 
order by wca.rts.rtsid desc limit 1));
