drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select distinct
'frn ir adjustment frequency' as static_change,
case when oldfreq.lookup is null then qtable.oldfrnintadjfreq else oldfreq.lookup end as previous_value,
case when newfreq.lookup is null then qtable.newfrnintadjfreq else newfreq.lookup end as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(11,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldfreq on qtable.oldfrnintadjfreq = oldfreq.code
                      and 'freq' = oldfreq.typegroup
left outer join wfi.sys_lookup as newfreq on qtable.newfrnintadjfreq = newfreq.code
                      and 'freq' = newfreq.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldfrnintadjfreq <> qtable.newfrnintadjfreq
);
use wca;
insert into wfi.loadingtable
select distinct
'security name' as static_change,
qtable.secoldname as previous_value,
qtable.secnewname as current_value,
qtable.dateofchange as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(12,qtable.scchgid) as firef,
qtable.secid
from scchg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
	                        and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and secoldname <> secnewname;
use wca;
insert into wfi.loadingtable
select distinct
'isin' as static_change,
qtable.oldisin as previous_value,
qtable.newisin as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(13,qtable.iccid) as firef,
qtable.secid
from icc as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and oldisin <> newisin;
use wca;
insert into wfi.loadingtable
select distinct
'us code' as static_change,
qtable.olduscode as previous_value,
qtable.newuscode as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(14,qtable.iccid) as firef,
qtable.secid
from icc as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and olduscode <> newuscode;
use wca;
insert into wfi.loadingtable
select distinct
'common code' as static_change,
qtable.oldcommoncode as previous_value,
qtable.newcommoncode as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(15,qtable.iccid) as firef,
qtable.secid
from icc as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and oldcommoncode <> newcommoncode;
use wca;
insert into wfi.loadingtable
select distinct
'sedol' as static_change,
qtable.oldsedol as previous_value,
qtable.newsedol as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(16,qtable.sdchgid) as firef,
qtable.secid
from sdchg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and oldsedol <> newsedol;
use wca;
insert into wfi.loadingtable
select distinct
'incorporation' as static_change,
case when oldcntry.lookup is null then qtable.oldcntrycd else oldcntry.lookup end as previous_value,
case when newcntry.lookup is null then qtable.newcntrycd else newcntry.lookup end as current_value,
qtable.inchgdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(17,qtable.inchgid) as firef,
scmst.secid
from inchg as qtable
inner join scmst on qtable.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldcntry on qtable.oldcntrycd = oldcntry.code
                      and 'cntry' = oldcntry.typegroup
left outer join wfi.sys_lookup as newcntry on qtable.newcntrycd = newcntry.code
                      and 'cntry' = newcntry.typegroup
where
scmst.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and oldcntrycd <> newcntrycd;
use wca;
insert into wfi.loadingtable
select distinct
'company name' as static_change,
qtable.issoldname as previous_value,
qtable.issnewname as current_value,
qtable.namechangedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(18,qtable.ischgid) as firef,
scmst.secid
from ischg as qtable
inner join scmst on qtable.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
scmst.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and issoldname <> issnewname;
use wca;
insert into wfi.loadingtable
select distinct
'local code' as static_change,
qtable.oldlocalcode as previous_value,
qtable.newlocalcode as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(19,qtable.lccid) as firef,
qtable.secid
from lcc as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and oldlocalcode <> newlocalcode;
use wca;
insert into wfi.loadingtable
select distinct
'interest type' as static_change,
case when oldinttype.lookup is null then qtable.oldintbasis else oldinttype.lookup end as previous_value,
case when newinttype.lookup is null then qtable.newintbasis else newinttype.lookup end as current_value,
qtable.effectivedate as effective_date,
' ' as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(20,qtable.intbcid) as firef,
qtable.secid
from intbc as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as oldinttype on qtable.oldintbasis = oldinttype.code
                      and 'inttype' = oldinttype.typegroup
left outer join wfi.sys_lookup as newinttype on qtable.newintbasis = newinttype.code
                      and 'inttype' = newinttype.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and qtable.oldintbasis <> qtable.newintbasis;
use wca;
insert into wfi.loadingtable
select distinct
'interest currency' as static_change,
qtable.oldinterestcurrency as previous_value,
qtable.newinterestcurrency as current_value,
qtable.notificationdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(21,qtable.bschgid) as firef,
qtable.secid
from bschg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldinterestcurrency <> qtable.newinterestcurrency;
use wca;
insert into wfi.loadingtable
select distinct
'maturity currency' as static_change,
qtable.oldmaturitycurrency as previous_value,
qtable.newmaturitycurrency as current_value,
qtable.notificationdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(22,qtable.bschgid) as firef,
qtable.secid
from bschg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldmaturitycurrency <> qtable.newmaturitycurrency;
use wca;
insert into wfi.loadingtable
select distinct
'current currency' as static_change,
qtable.oldcurencd as previous_value,
qtable.newcurencd as current_value,
qtable.notificationdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(23,qtable.bschgid) as firef,
qtable.secid
from bschg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldcurencd <> qtable.newcurencd;
use wca;
insert into wfi.loadingtable
select distinct
'debt type' as static_change,
case when oldbondtype.lookup is null then qtable.oldbondtype else oldbondtype.lookup end as previous_value,
case when newbondtype.lookup is null then qtable.newbondtype else newbondtype.lookup end as current_value,
qtable.notificationdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(24,qtable.bschgid) as firef,
qtable.secid
from bschg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldbondtype on qtable.oldbondtype = oldbondtype.code
                      and 'bondtype' = oldbondtype.typegroup
left outer join wfi.sys_lookup as newbondtype on qtable.newbondtype = newbondtype.code
                      and 'bondtype' = newbondtype.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldbondtype <> qtable.newbondtype;
use wca;
insert into wfi.loadingtable
select distinct
'interest bdc' as static_change,
case when oldintbdc.lookup is null then qtable.oldintbusdayconv else oldintbdc.lookup end as previous_value,
case when newintbdc.lookup is null then qtable.newintbusdayconv else newintbdc.lookup end as current_value,
qtable.notificationdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(25,qtable.bschgid) as firef,
qtable.secid
from bschg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldintbdc on qtable.oldintbusdayconv = oldintbdc.code
                      and 'intbdc' = oldintbdc.typegroup
left outer join wfi.sys_lookup as newintbdc on qtable.newintbusdayconv = newintbdc.code
                      and 'intbdc' = newintbdc.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldintbusdayconv <> qtable.newintbusdayconv;
use wca;
insert into wfi.loadingtable
select distinct
'maturity bdc' as static_change,
case when oldmatbdc.lookup is null then qtable.oldmatbusdayconv else oldmatbdc.lookup end as previous_value,
case when newmatbdc.lookup is null then qtable.newmatbusdayconv else newmatbdc.lookup end as current_value,
qtable.notificationdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(26,qtable.bschgid) as firef,
qtable.secid
from bschg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldmatbdc on qtable.oldmatbusdayconv = oldmatbdc.code
                      and 'intbdc' = oldmatbdc.typegroup
left outer join wfi.sys_lookup as newmatbdc on qtable.newmatbusdayconv = newmatbdc.code
                      and 'intbdc' = newmatbdc.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldmatbusdayconv  <> qtable.newmatbusdayconv;
use wca;
insert into wfi.loadingtable
select distinct
'frn rounding' as static_change,
qtable.oldrounding as previous_value,
qtable.newrounding as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(27,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldrounding <> qtable.newrounding;
use wca;
insert into wfi.loadingtable
select distinct
'frn minimum ir' as static_change,
qtable.oldminimuminterestrate as previous_value,
qtable.newminimuminterestrate as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(28,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldminimuminterestrate <> qtable.newminimuminterestrate;
use wca;
insert into wfi.loadingtable
select distinct
'frn maximum ir' as static_change,
qtable.oldmaximuminterestrate as previous_value,
qtable.newmaximuminterestrate as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(29,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldmaximuminterestrate <> qtable.newmaximuminterestrate;
use wca;
insert into wfi.loadingtable
select distinct
'frn margin' as static_change,
qtable.oldmarkup as previous_value,
qtable.newmarkup as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(30,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldmarkup <> qtable.newmarkup;
use wca;
insert into wfi.loadingtable
select distinct
'frn index benchmark' as static_change,
case when oldindxben.lookup is null then qtable.oldfrnindexbenchmark else oldindxben.lookup end as current_value,
case when newindxben.lookup is null then qtable.newfrnindexbenchmark else newindxben.lookup end as previous_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(31,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldindxben on qtable.oldfrnindexbenchmark = oldindxben.code
                      and 'frnindxben' = oldindxben.typegroup
left outer join wfi.sys_lookup as newindxben on qtable.newfrnindexbenchmark = newindxben.code
                      and 'frnindxben' = newindxben.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldfrnindexbenchmark <> qtable.newfrnindexbenchmark;
use wca;
insert into wfi.loadingtable
select distinct
'frn type' as static_change,
case when oldtype.lookup is null then qtable.oldfrntype else oldtype.lookup end as previous_value,
case when newtype.lookup is null then qtable.newfrntype else newtype.lookup end as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(32,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldtype on qtable.oldfrntype = oldtype.code
                      and 'frntype' = oldtype.typegroup
left outer join wfi.sys_lookup as newtype on qtable.newfrntype = newtype.code
                      and 'frntype' = newtype.typegroup
where
qtable.secid in (select secid from wfi.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldfrntype <> qtable.newfrntype;
alter table `wfi`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`,`secid`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.static_change;
rename table wfi.loadingtable to wfi.static_change;
--
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select distinct
'frn ir adjustment frequency' as static_change,
case when oldfreq.lookup is null then qtable.oldfrnintadjfreq else oldfreq.lookup end as previous_value,
case when newfreq.lookup is null then qtable.newfrnintadjfreq else newfreq.lookup end as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(11,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldfreq on qtable.oldfrnintadjfreq = oldfreq.code
                      and 'freq' = oldfreq.typegroup
left outer join wfi.sys_lookup as newfreq on qtable.newfrnintadjfreq = newfreq.code
                      and 'freq' = newfreq.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldfrnintadjfreq <> qtable.newfrnintadjfreq
);
use wca;
insert into wfimatured.loadingtable
select distinct
'security name' as static_change,
qtable.secoldname as previous_value,
qtable.secnewname as current_value,
qtable.dateofchange as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(12,qtable.scchgid) as firef,
qtable.secid
from scchg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
	                        and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and secoldname <> secnewname;
use wca;
insert into wfimatured.loadingtable
select distinct
'isin' as static_change,
qtable.oldisin as previous_value,
qtable.newisin as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(13,qtable.iccid) as firef,
qtable.secid
from icc as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and oldisin <> newisin;
use wca;
insert into wfimatured.loadingtable
select distinct
'us code' as static_change,
qtable.olduscode as previous_value,
qtable.newuscode as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(14,qtable.iccid) as firef,
qtable.secid
from icc as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and olduscode <> newuscode;
use wca;
insert into wfimatured.loadingtable
select distinct
'common code' as static_change,
qtable.oldcommoncode as previous_value,
qtable.newcommoncode as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(15,qtable.iccid) as firef,
qtable.secid
from icc as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and oldcommoncode <> newcommoncode;
use wca;
insert into wfimatured.loadingtable
select distinct
'sedol' as static_change,
qtable.oldsedol as previous_value,
qtable.newsedol as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(16,qtable.sdchgid) as firef,
qtable.secid
from sdchg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and oldsedol <> newsedol;
use wca;
insert into wfimatured.loadingtable
select distinct
'incorporation' as static_change,
case when oldcntry.lookup is null then qtable.oldcntrycd else oldcntry.lookup end as previous_value,
case when newcntry.lookup is null then qtable.newcntrycd else newcntry.lookup end as current_value,
qtable.inchgdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(17,qtable.inchgid) as firef,
scmst.secid
from inchg as qtable
inner join scmst on qtable.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldcntry on qtable.oldcntrycd = oldcntry.code
                      and 'cntry' = oldcntry.typegroup
left outer join wfi.sys_lookup as newcntry on qtable.newcntrycd = newcntry.code
                      and 'cntry' = newcntry.typegroup
where
scmst.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and oldcntrycd <> newcntrycd;
use wca;
insert into wfimatured.loadingtable
select distinct
'company name' as static_change,
qtable.issoldname as previous_value,
qtable.issnewname as current_value,
qtable.namechangedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(18,qtable.ischgid) as firef,
scmst.secid
from ischg as qtable
inner join scmst on qtable.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
scmst.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and issoldname <> issnewname;
use wca;
insert into wfimatured.loadingtable
select distinct
'local code' as static_change,
qtable.oldlocalcode as previous_value,
qtable.newlocalcode as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(19,qtable.lccid) as firef,
qtable.secid
from lcc as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and oldlocalcode <> newlocalcode;
use wca;
insert into wfimatured.loadingtable
select distinct
'interest type' as static_change,
case when oldinttype.lookup is null then qtable.oldintbasis else oldinttype.lookup end as previous_value,
case when newinttype.lookup is null then qtable.newintbasis else newinttype.lookup end as current_value,
qtable.effectivedate as effective_date,
' ' as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(20,qtable.intbcid) as firef,
qtable.secid
from intbc as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as oldinttype on qtable.oldintbasis = oldinttype.code
                      and 'inttype' = oldinttype.typegroup
left outer join wfi.sys_lookup as newinttype on qtable.newintbasis = newinttype.code
                      and 'inttype' = newinttype.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and qtable.oldintbasis <> qtable.newintbasis;
use wca;
insert into wfimatured.loadingtable
select distinct
'interest currency' as static_change,
qtable.oldinterestcurrency as previous_value,
qtable.newinterestcurrency as current_value,
qtable.notificationdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(21,qtable.bschgid) as firef,
qtable.secid
from bschg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldinterestcurrency <> qtable.newinterestcurrency;
use wca;
insert into wfimatured.loadingtable
select distinct
'maturity currency' as static_change,
qtable.oldmaturitycurrency as previous_value,
qtable.newmaturitycurrency as current_value,
qtable.notificationdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(22,qtable.bschgid) as firef,
qtable.secid
from bschg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldmaturitycurrency <> qtable.newmaturitycurrency;
use wca;
insert into wfimatured.loadingtable
select distinct
'current currency' as static_change,
qtable.oldcurencd as previous_value,
qtable.newcurencd as current_value,
qtable.notificationdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(23,qtable.bschgid) as firef,
qtable.secid
from bschg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldcurencd <> qtable.newcurencd;
use wca;
insert into wfimatured.loadingtable
select distinct
'debt type' as static_change,
case when oldbondtype.lookup is null then qtable.oldbondtype else oldbondtype.lookup end as previous_value,
case when newbondtype.lookup is null then qtable.newbondtype else newbondtype.lookup end as current_value,
qtable.notificationdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(24,qtable.bschgid) as firef,
qtable.secid
from bschg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldbondtype on qtable.oldbondtype = oldbondtype.code
                      and 'bondtype' = oldbondtype.typegroup
left outer join wfi.sys_lookup as newbondtype on qtable.newbondtype = newbondtype.code
                      and 'bondtype' = newbondtype.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldbondtype <> qtable.newbondtype;
use wca;
insert into wfimatured.loadingtable
select distinct
'interest bdc' as static_change,
case when oldintbdc.lookup is null then qtable.oldintbusdayconv else oldintbdc.lookup end as previous_value,
case when newintbdc.lookup is null then qtable.newintbusdayconv else newintbdc.lookup end as current_value,
qtable.notificationdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(25,qtable.bschgid) as firef,
qtable.secid
from bschg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldintbdc on qtable.oldintbusdayconv = oldintbdc.code
                      and 'intbdc' = oldintbdc.typegroup
left outer join wfi.sys_lookup as newintbdc on qtable.newintbusdayconv = newintbdc.code
                      and 'intbdc' = newintbdc.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldintbusdayconv <> qtable.newintbusdayconv;
use wca;
insert into wfimatured.loadingtable
select distinct
'maturity bdc' as static_change,
case when oldmatbdc.lookup is null then qtable.oldmatbusdayconv else oldmatbdc.lookup end as previous_value,
case when newmatbdc.lookup is null then qtable.newmatbusdayconv else newmatbdc.lookup end as current_value,
qtable.notificationdate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(26,qtable.bschgid) as firef,
qtable.secid
from bschg as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldmatbdc on qtable.oldmatbusdayconv = oldmatbdc.code
                      and 'intbdc' = oldmatbdc.typegroup
left outer join wfi.sys_lookup as newmatbdc on qtable.newmatbusdayconv = newmatbdc.code
                      and 'intbdc' = newmatbdc.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldmatbusdayconv  <> qtable.newmatbusdayconv;
use wca;
insert into wfimatured.loadingtable
select distinct
'frn rounding' as static_change,
qtable.oldrounding as previous_value,
qtable.newrounding as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(27,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldrounding <> qtable.newrounding;
use wca;
insert into wfimatured.loadingtable
select distinct
'frn minimum ir' as static_change,
qtable.oldminimuminterestrate as previous_value,
qtable.newminimuminterestrate as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(28,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldminimuminterestrate <> qtable.newminimuminterestrate;
use wca;
insert into wfimatured.loadingtable
select distinct
'frn maximum ir' as static_change,
qtable.oldmaximuminterestrate as previous_value,
qtable.newmaximuminterestrate as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(29,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldmaximuminterestrate <> qtable.newmaximuminterestrate;
use wca;
insert into wfimatured.loadingtable
select distinct
'frn margin' as static_change,
qtable.oldmarkup as previous_value,
qtable.newmarkup as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(30,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldmarkup <> qtable.newmarkup;
use wca;
insert into wfimatured.loadingtable
select distinct
'frn index benchmark' as static_change,
case when oldindxben.lookup is null then qtable.oldfrnindexbenchmark else oldindxben.lookup end as current_value,
case when newindxben.lookup is null then qtable.newfrnindexbenchmark else newindxben.lookup end as previous_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(31,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldindxben on qtable.oldfrnindexbenchmark = oldindxben.code
                      and 'frnindxben' = oldindxben.typegroup
left outer join wfi.sys_lookup as newindxben on qtable.newfrnindexbenchmark = newindxben.code
                      and 'frnindxben' = newindxben.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldfrnindexbenchmark <> qtable.newfrnindexbenchmark;
use wca;
insert into wfimatured.loadingtable
select distinct
'frn type' as static_change,
case when oldtype.lookup is null then qtable.oldfrntype else oldtype.lookup end as previous_value,
case when newtype.lookup is null then qtable.newfrntype else newtype.lookup end as current_value,
qtable.effectivedate as effective_date,
case when qtable.eventtype is null then '' when relevent.lookup is null then qtable.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
qtable.actflag,
qtable.acttime as changed,
qtable.announcedate as created,
concat(32,qtable.frnfxid) as firef,
qtable.secid
from frnfx as qtable
inner join bond on qtable.secid = bond.secid
inner join scmst on qtable.secid = scmst.secid
left outer join wfi.sys_lookup as relevent on qtable.eventtype = relevent.code
                      and 'event' = relevent.typegroup
left outer join wfi.sys_lookup as oldtype on qtable.oldfrntype = oldtype.code
                      and 'frntype' = oldtype.typegroup
left outer join wfi.sys_lookup as newtype on qtable.newfrntype = newtype.code
                      and 'frntype' = newtype.typegroup
where
qtable.secid in (select secid from wfimatured.reference)
and qtable.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
and qtable.oldfrntype <> qtable.newfrntype;
alter table `wfimatured`.`loadingtable`
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`firef`,`secid`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.static_change;
rename table wfimatured.loadingtable to wfimatured.static_change;
