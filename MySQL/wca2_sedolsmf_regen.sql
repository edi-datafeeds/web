use wca2;
drop table if exists sedolsmf;
CREATE TABLE `sedolsmf` (
  `OpsTime` datetime DEFAULT NULL,
  `Opsflag` char(1) DEFAULT NULL,
  `Acttime` datetime DEFAULT NULL,
  `Actflag` char(1) DEFAULT NULL,
  `GCDateTime` datetime DEFAULT NULL,
  `SecID` int(10) NOT NULL DEFAULT '0',
  `CntryCD` char(2) DEFAULT NULL,
  `Sedol` char(7) DEFAULT NULL,
  `Defunct` char(1) DEFAULT NULL,
  `RcntryCD` char(2) DEFAULT NULL,
  `SedolId` int(10) NOT NULL DEFAULT '0',
  `CurenCD` char(3) DEFAULT '',
  `Source` varchar(20) DEFAULT NULL,
  `SourceDate` datetime DEFAULT NULL,
  `GCAction` char(1) DEFAULT NULL,
  `GCDate` datetime DEFAULT NULL,
  `GCTime` varchar(12) DEFAULT NULL,
  `AnnounceDate` datetime DEFAULT NULL,
  PRIMARY KEY (`SedolId`),
  KEY `ix_acttime_SEDOL` (`Acttime`),
  KEY `IX_SEDOL` (`Sedol`),
  KEY `IX_SecID` (`SecID`,`CntryCD`,`CurenCD`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

insert into sedolsmf select * from wca.sedol;

update wca2.sedolsmf as ss
inner join smf4.security as smf on ss.sedol=smf.sedol
set ss.cntrycd='GB'
where
ss.cntrycd<>'GB'
and smf.OPOL='XLON';

update wca2.sedolsmf as ss
inner join smf4.security as smf on ss.sedol=smf.sedol
set ss.curencd=smf.unitofqcurrcode
where
ss.curencd<>smf.unitofqcurrcode;
update wca2.sedolsmf as ss set ss.curencd='GBP' where ss.curencd='GBX';
update wca2.sedolsmf as ss set ss.curencd='USD' where ss.curencd='USX';

update wca2.sedolsmf as ss
inner join smf4.security as smf on ss.sedol=smf.sedol
set ss.defunct='F'
where
smf.statusflag<>'D'
and ss.defunct='T';

update wca2.sedolsmf as ss
inner join smf4.security as smf on ss.sedol=smf.sedol
set ss.defunct='T'
where
smf.statusflag='D'
and ss.defunct='F';

update wca2.sedolsmf as ss
inner join smf4.security as smf on ss.sedol=smf.sedol
set ss.rcntrycd=smf.cregcode
where
ss.rcntrycd<>smf.cregcode
and ss.rcntrycd<>'XG'
and ss.rcntrycd<>'XZ'
and smf.cregcode<>'XX';
