SELECT @q:= 
	CASE
		WHEN COUNT(`user`) > 0 
                THEN 
						( SELECT GROUP_CONCAT(CONCAT('KILL ',ID) SEPARATOR ';')  
						FROM information_schema.processlist AS MyXt
							WHERE 
								user = 'fdp_cpl'
						)
                ELSE ( CONCAT('SELECT ', CURDATE()) )
            END AS topicPart1
FROM information_schema.processlist 
WHERE 
	user = 'fdp_cpl'
;

PREPARE stmt FROM @q;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;