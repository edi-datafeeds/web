use wca2;
drop table if exists sedolbbgseq;
CREATE TABLE `sedolbbgseq` (
  `autoid` int(11) NOT NULL AUTO_INCREMENT,
  `sedolacttime` datetime DEFAULT NULL,
  `bbcacttime` datetime DEFAULT NULL,
  `bbeacttime` datetime DEFAULT NULL,
  `secid` int(11) NOT NULL DEFAULT '0',
  `cntrycd` char(2) NOT NULL DEFAULT '',
  `curencd` char(3) NOT NULL DEFAULT '',
  `rcntrycd` char(2) NOT NULL DEFAULT '',
  `exchgcd` char(6) NOT NULL DEFAULT '',
  `sedol` char(7) NOT NULL DEFAULT '',
  `sedoldefunct` char(1) NOT NULL DEFAULT '',
  `bbgcompid` varchar(12) DEFAULT NULL,
  `bbgcomptk` varchar(40) DEFAULT NULL,
  `bbgexhid` char(12) DEFAULT NULL,
  `bbgexhtk` varchar(40) DEFAULT NULL,
  `sedolid` int(11) DEFAULT NULL,
  `bbcid` int(11) DEFAULT NULL,
  `bbeid` int(11) DEFAULT NULL,
  PRIMARY KEY (`autoid`),
  KEY `ix_sedolid` (`sedolid`),
  KEY `ix_bbcid` (`bbcid`),
  KEY `ix_bbeid` (`bbeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `wca2`.`sedolbbgseq` 
ADD UNIQUE INDEX `ix_distinctnew` (`secid` ASC, `cntrycd` ASC, `rcntrycd` ASC, `exchgcd` ASC, `curencd` ASC);
use wca;
insert ignore into wca2.sedolbbgseq
select
null, 
wca_other.sedol.acttime,
bbc.acttime,
bbe.acttime, 
wca_other.sedol.secid, 
wca_other.sedol.cntrycd, 
wca_other.sedol.curencd, 
wca_other.sedol.rcntrycd,
bbe.exchgcd, 
wca_other.sedol.sedol, 
wca_other.sedol.defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk, 
wca_other.sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from wca_other.sedol
inner join bbe on wca_other.sedol.secid=bbe.secid
              and wca_other.sedol.cntrycd=substring(bbe.exchgcd,1,2)
              and wca_other.sedol.curencd=bbe.curencd
              and 'D'<>bbe.actflag
              and ''<>bbe.bbgexhid
inner join scmst on wca_other.sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
left outer join bbc on bbe.secid=bbc.secid
                   and wca_other.sedol.cntrycd=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and 'D'<>bbc.actflag
where 
wca_other.sedol.actflag<>'D'
and bbe.exchgcd<>''
and bbe.curenCD<>''
and wca_other.sedol.curenCD<>''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and wca_other.sedol.sedol<>''
and wca_other.sedol.cntrycd<>''
and wca_other.sedol.cntrycd<>'XS'
and wca_other.sedol.cntrycd<>'ZZ'
and wca_other.sedol.rcntrycd<>'XG'
and wca_other.sedol.rcntrycd<>'XH'
and wca_other.sedol.rcntrycd<>'XZ'
and bbc.cntrycd<>'XG'
and bbc.cntrycd<>'XH'
and bbc.cntrycd<>'XZ';
insert ignore into wca2.sedolbbgseq
select
null,
wca_other.sedol.acttime,
bbc.acttime,
bbe.acttime,
bbe.secid,
substring(bbe.exchgcd,1,2) as cntrycd,
wca_other.sedol.curencd,
wca_other.sedol.rcntrycd,
bbe.exchgcd,
wca_other.sedol.sedol,
wca_other.sedol.defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
wca_other.sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from bbe
inner join wca_other.sedol on bbe.secid=wca_other.sedol.secid
            and substring(bbe.exchgcd,1,2)=wca_other.sedol.cntrycd
            and ''<>ifnull(wca_other.sedol.curencd,'')
            and 'D'<>ifnull(wca_other.sedol.actflag,'')
            and ''<>ifnull(wca_other.sedol.sedol,'')
            and wca_other.sedol.cntrycd<>'XS'
            and wca_other.sedol.cntrycd<>'ZZ'
            and wca_other.sedol.rcntrycd<>'XG'
            and wca_other.sedol.rcntrycd<>'XH'
            and wca_other.sedol.rcntrycd<>'XZ'
inner join scmst on bbe.secid=scmst.secid
                and 'D'<>scmst.actflag
left outer join bbc on bbe.secid=bbc.secid
                   and substring(bbe.exchgcd,1,2)=bbc.cntrycd
                   and bbe.curencd=ifnull(bbc.curencd,'')
                   and 'D'<>ifnull(bbc.actflag,'')
where
bbe.curencd=''
and bbe.exchgcd<>''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and bbe.bbgexhid<>''
and bbe.actflag<>'D'
and concat(ifnull(bbe.secid,''),ifnull(bbe.exchgcd,''),ifnull(wca_other.sedol.curencd,''),ifnull(wca_other.sedol.rcntrycd,''))
not in (select concat(ifnull(subsdseq.secid,''),ifnull(subsdseq.exchgcd,''),ifnull(subsdseq.curencd,''),ifnull(subsdseq.rcntrycd,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=bbe.secid);
insert ignore into wca2.sedolbbgseq
select
null, 
wca_other.sedol.acttime,
bbc.acttime,
bbe.acttime, 
wca_other.sedol.secid, 
wca_other.sedol.cntrycd,
bbe.curencd, 
wca_other.sedol.rcntrycd,
bbe.exchgcd, 
wca_other.sedol.sedol, 
wca_other.sedol.defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
case when bbe.bbgexhid is null then '' else bbe.bbgexhid end as bbgexhid,
case when bbe.bbgexhtk is null then '' else bbe.bbgexhtk end as bbgexhtk, 
wca_other.sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from wca_other.sedol
inner join bbe on wca_other.sedol.secid=bbe.secid
                   and wca_other.sedol.cntrycd=substring(bbe.exchgcd,1,2)
                   and ''<>ifnull(bbe.curencd,'')
                   and 'D'<>ifnull(bbe.actflag,'')
inner join scmst on wca_other.sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
left outer join bbc on bbe.secid=bbc.secid
                   and wca_other.sedol.cntrycd=ifnull(bbc.cntrycd,'')
                   and bbe.curencd=ifnull(bbc.curencd,'')
                   and 'D'<>ifnull(bbc.actflag,'')
where 
wca_other.sedol.curencd=''
and bbe.exchgcd<>''
and bbe.bbgexhid<>''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and wca_other.sedol.actflag<>'D'
and wca_other.sedol.sedol<>''
and wca_other.sedol.cntrycd<>''
and wca_other.sedol.cntrycd<>'XS'
and wca_other.sedol.cntrycd<>'ZZ'
and wca_other.sedol.rcntrycd<>'XG'
and wca_other.sedol.rcntrycd<>'XH'
and wca_other.sedol.rcntrycd<>'XZ'
and concat(ifnull(bbe.secid,''),ifnull(bbe.exchgcd,''),ifnull(wca_other.sedol.curencd,''),ifnull(wca_other.sedol.rcntrycd,''))
not in (select concat(ifnull(subsdseq.secid,''),ifnull(subsdseq.exchgcd,''),ifnull(subsdseq.curencd,''),ifnull(subsdseq.rcntrycd,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=bbe.secid);

insert ignore into wca2.sedolbbgseq
select
null,
null, 
wca_other.sedol.acttime,
bbe.acttime,
wca_other.sedol.secid, 
wca_other.sedol.cntrycd, 
wca_other.sedol.curencd, 
wca_other.sedol.rcntrycd,
scexh.exchgcd, 
wca_other.sedol.sedol, 
wca_other.sedol.defunct,
'' as bbgcompid,
'' as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk, 
wca_other.sedol.sedolid,
null,
bbe.bbeid
from wca_other.sedol
inner join scexh on wca_other.sedol.secid=scexh.secid
              and wca_other.sedol.cntrycd=substring(scexh.exchgcd,1,2)
              and 'D'<>scexh.actflag
inner join scmst on wca_other.sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join bbe on scexh.secid=bbe.secid
                   and ''=ifnull(bbe.exchgcd,'')
                   and 'D'<>ifnull(bbe.actflag,'')
where 
wca_other.sedol.actflag<>'D'
and wca.scmst.sectycd='BND'
and wca_other.sedol.sedol<>''
and wca_other.sedol.cntrycd<>''
and wca_other.sedol.cntrycd<>'XS'
and wca_other.sedol.cntrycd<>'ZZ'
and wca_other.sedol.rcntrycd<>'XG'
and wca_other.sedol.rcntrycd<>'XH'
and wca_other.sedol.rcntrycd<>'XZ'
and substring(wca.scexh.exchgcd,3,3)<>'BND'
and wca.scexh.exchgcd<>'USTRCE'
and wca.scexh.exchgcd<>'ALTSE'
and wca.scexh.exchgcd<>'AOASE'
and wca.scexh.exchgcd<>'AROEX'
and wca.scexh.exchgcd<>'ARRCX'
and wca.scexh.exchgcd<>'AUSIMV'
and wca.scexh.exchgcd<>'CHSCH'
and wca.scexh.exchgcd<>'CNSZHK'
and wca.scexh.exchgcd<>'CNSGHK'
and wca.scexh.exchgcd<>'DESCH'
and wca.scexh.exchgcd<>'DODRSX'
and wca.scexh.exchgcd<>'FINDX'
and wca.scexh.exchgcd<>'FOFSM'
and wca.scexh.exchgcd<>'GABVCA'
and wca.scexh.exchgcd<>'GBENLN'
and wca.scexh.exchgcd<>'GBOFX'
and wca.scexh.exchgcd<>'GBUT'
and wca.scexh.exchgcd<>'GIGSX'
and wca.scexh.exchgcd<>'HKHKSG'
and wca.scexh.exchgcd<>'HKHKSZ'
and wca.scexh.exchgcd<>'HNCASE'
and wca.scexh.exchgcd<>'HTHTSE'
and wca.scexh.exchgcd<>'JPNSE'
and wca.scexh.exchgcd<>'KNECSE'
and wca.scexh.exchgcd<>'LSMSM'
and wca.scexh.exchgcd<>'LUUL'
and wca.scexh.exchgcd<>'MVMSX'
and wca.scexh.exchgcd<>'MYLFX'
and wca.scexh.exchgcd<>'MZMSX'
and wca.scexh.exchgcd<>'NOOTC'
and wca.scexh.exchgcd<>'PLOTC'
and wca.scexh.exchgcd<>'SENDX'
and wca.scexh.exchgcd<>'TJCASE'
and wca.scexh.exchgcd<>'UAINX'
and wca.scexh.exchgcd<>'UAKISE'
and wca.scexh.exchgcd<>'UYUEX'
and concat(ifnull(scexh.secid,''),ifnull(scexh.exchgcd,''),ifnull(wca_other.sedol.curencd,''),ifnull(wca_other.sedol.rcntrycd,''))
not in (select concat(ifnull(subsdseq.secid,''),ifnull(subsdseq.exchgcd,''),ifnull(subsdseq.curencd,''),ifnull(subsdseq.rcntrycd,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=bbe.secid);
insert ignore into wca2.sedolbbgseq
select
null, 
scexh.acttime,
null,
bbe.acttime,
bbe.secid,
substring(scexh.exchgcd,1,2) as cntrycd,
bbe.curencd, 
'' as rcntrycd,
scexh.exchgcd, 
'' as sedol, 
'' as defunct,
'' as bbgcompid,
'' as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk, 
null,
null,
bbe.bbeid
from scexh
inner join scmst on scexh.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join bbe on scexh.secid=bbe.secid
                   and ''=ifnull(bbe.exchgcd,'')
                   and 'D'<>ifnull(bbe.actflag,'')
left outer join wca_other.sedol on bbe.secid=wca_other.sedol.secid and 'D'<>wca_other.sedol.actflag
where 
wca_other.sedol.sedolid is null
and scexh.actflag<>'D'
and wca.scmst.sectycd='BND'
and substring(wca.scexh.exchgcd,3,3)<>'BND'
and wca.scexh.exchgcd<>'USTRCE'
and wca.scexh.exchgcd<>'ALTSE'
and wca.scexh.exchgcd<>'AOASE'
and wca.scexh.exchgcd<>'AROEX'
and wca.scexh.exchgcd<>'ARRCX'
and wca.scexh.exchgcd<>'AUSIMV'
and wca.scexh.exchgcd<>'CHSCH'
and wca.scexh.exchgcd<>'CNSZHK'
and wca.scexh.exchgcd<>'CNSGHK'
and wca.scexh.exchgcd<>'DESCH'
and wca.scexh.exchgcd<>'DODRSX'
and wca.scexh.exchgcd<>'FINDX'
and wca.scexh.exchgcd<>'FOFSM'
and wca.scexh.exchgcd<>'GABVCA'
and wca.scexh.exchgcd<>'GBENLN'
and wca.scexh.exchgcd<>'GBOFX'
and wca.scexh.exchgcd<>'GBUT'
and wca.scexh.exchgcd<>'GIGSX'
and wca.scexh.exchgcd<>'HKHKSG'
and wca.scexh.exchgcd<>'HKHKSZ'
and wca.scexh.exchgcd<>'HNCASE'
and wca.scexh.exchgcd<>'HTHTSE'
and wca.scexh.exchgcd<>'JPNSE'
and wca.scexh.exchgcd<>'KNECSE'
and wca.scexh.exchgcd<>'LSMSM'
and wca.scexh.exchgcd<>'LUUL'
and wca.scexh.exchgcd<>'MVMSX'
and wca.scexh.exchgcd<>'MYLFX'
and wca.scexh.exchgcd<>'MZMSX'
and wca.scexh.exchgcd<>'NOOTC'
and wca.scexh.exchgcd<>'PLOTC'
and wca.scexh.exchgcd<>'SENDX'
and wca.scexh.exchgcd<>'TJCASE'
and wca.scexh.exchgcd<>'UAINX'
and wca.scexh.exchgcd<>'UAKISE'
and wca.scexh.exchgcd<>'UYUEX'
and concat(ifnull(scexh.secid,''),ifnull(scexh.exchgcd,''))
not in (select concat(ifnull(subsdseq.secid,''),ifnull(subsdseq.exchgcd,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=bbe.secid);
insert ignore into wca2.sedolbbgseq
select
null,
wca_other.sedol.acttime,
bbc.acttime,
null,
bbc.secid,
bbc.cntrycd,
case when wca_other.sedol.curencd<>'' then wca_other.sedol.curencd
     else bbc.curencd
     end as curencd,
wca_other.sedol.rcntrycd,
scexh.exchgcd,
wca_other.sedol.sedol,
wca_other.sedol.defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
'' as bbgexhid,
'' as bbgexhtk,
wca_other.sedol.sedolid,
bbc.bbcid,
null
from bbc
left outer join bbe on bbc.secid=bbe.secid
                   and bbc.cntrycd=substring(bbe.exchgcd,1,2)
                   and 'CNSZHK'<>bbe.exchgcd
                   and 'CNSGHK'<>bbe.exchgcd
                   and 'HKHKSG'<>bbe.exchgcd
                   and 'HKHKSZ'<>bbe.exchgcd
                   and 'D'<>ifnull(bbe.actflag,'')
inner join wca_other.sedol on bbc.secid=wca_other.sedol.secid
            and bbc.cntrycd=wca_other.sedol.cntrycd
            and bbc.curencd=wca_other.sedol.curencd
            and 'D'<>ifnull(wca_other.sedol.actflag,'')
            and ''<>ifnull(wca_other.sedol.sedol,'')
            and wca_other.sedol.cntrycd<>'XS'
            and wca_other.sedol.cntrycd<>'ZZ'
            and wca_other.sedol.rcntrycd<>'XG'
            and wca_other.sedol.rcntrycd<>'XH'
            and wca_other.sedol.rcntrycd<>'XZ'
inner join scexh on wca_other.sedol.secid=scexh.secid
              and wca_other.sedol.cntrycd=substring(scexh.exchgcd,1,2)
              and 'CNSZHK'<>scexh.exchgcd
              and 'CNSGHK'<>scexh.exchgcd
              and 'HKHKSG'<>scexh.exchgcd
              and 'HKHKSZ'<>scexh.exchgcd
              and 'D'<>scexh.actflag
where
bbe.secid is null
and bbc.cntrycd<>'XG'
and bbc.cntrycd<>'XH'
and bbc.cntrycd<>'XZ'
and bbc.actflag<>'D'
and concat(ifnull(bbc.secid,''),ifnull(scexh.exchgcd,''),ifnull(wca_other.sedol.curencd,''),ifnull(wca_other.sedol.rcntrycd,''))
not in (select concat(ifnull(subsdseq.secid,''),ifnull(subsdseq.exchgcd,''),ifnull(subsdseq.curencd,''),ifnull(subsdseq.rcntrycd,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=bbc.secid);
insert ignore into wca2.sedolbbgseq
select
null,
wca_other.sedol.acttime,
bbc.acttime,
null,
bbc.secid,
bbc.cntrycd,
case when wca_other.sedol.curencd<>'' then wca_other.sedol.curencd
     else bbc.curencd
     end as curencd,
wca_other.sedol.rcntrycd,
scexh.exchgcd,
wca_other.sedol.sedol,
wca_other.sedol.defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
'' as bbgexhid,
'' as bbgexhtk,
wca_other.sedol.sedolid,
bbc.bbcid,
null
from bbc
left outer join bbe on bbc.secid=bbe.secid
                   and bbc.cntrycd=substring(bbe.exchgcd,1,2)
                   and 'CNSZHK'<>bbe.exchgcd
                   and 'CNSGHK'<>bbe.exchgcd
                   and 'HKHKSG'<>bbe.exchgcd
                   and 'HKHKSZ'<>bbe.exchgcd
                   and 'D'<>ifnull(bbe.actflag,'')
inner join wca_other.sedol on bbc.secid=wca_other.sedol.secid
            and bbc.cntrycd=wca_other.sedol.cntrycd
            and ''<>wca_other.sedol.curencd
            and 'D'<>ifnull(wca_other.sedol.actflag,'')
            and ''<>ifnull(wca_other.sedol.sedol,'')
            and wca_other.sedol.cntrycd<>'XS'
            and wca_other.sedol.cntrycd<>'ZZ'
            and wca_other.sedol.rcntrycd<>'XG'
            and wca_other.sedol.rcntrycd<>'XH'
            and wca_other.sedol.rcntrycd<>'XZ'
inner join scexh on wca_other.sedol.secid=scexh.secid
              and wca_other.sedol.cntrycd=substring(scexh.exchgcd,1,2)
              and 'CNSZHK'<>scexh.exchgcd
              and 'CNSGHK'<>scexh.exchgcd
              and 'HKHKSG'<>scexh.exchgcd
              and 'HKHKSZ'<>scexh.exchgcd
              and 'D'<>scexh.actflag
where
bbe.secid is null
and bbc.curencd=''
and bbc.cntrycd<>'XG'
and bbc.cntrycd<>'XH'
and bbc.cntrycd<>'XZ'
and bbc.actflag<>'D'
and concat(ifnull(bbc.secid,''),ifnull(scexh.exchgcd,''),ifnull(wca_other.sedol.curencd,''),ifnull(wca_other.sedol.rcntrycd,''))
not in (select concat(ifnull(subsdseq.secid,''),ifnull(subsdseq.exchgcd,''),ifnull(subsdseq.curencd,''),ifnull(subsdseq.rcntrycd,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=bbc.secid);
         
insert ignore into wca2.sedolbbgseq
select
null,
wca_other.sedol.acttime,
bbc.acttime,
null,
bbc.secid,
bbc.cntrycd,
case when wca_other.sedol.curencd<>'' then wca_other.sedol.curencd
     else bbc.curencd
     end as curencd,
wca_other.sedol.rcntrycd,
scexh.exchgcd,
wca_other.sedol.sedol,
wca_other.sedol.defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
'' as bbgexhid,
'' as bbgexhtk,
wca_other.sedol.sedolid,
bbc.bbcid,
null
from bbc
left outer join bbe on bbc.secid=bbe.secid
                   and bbc.cntrycd=substring(bbe.exchgcd,1,2)
                   and 'CNSZHK'<>bbe.exchgcd
                   and 'CNSGHK'<>bbe.exchgcd
                   and 'HKHKSG'<>bbe.exchgcd
                   and 'HKHKSZ'<>bbe.exchgcd
                   and 'D'<>ifnull(bbe.actflag,'')
inner join wca_other.sedol on bbc.secid=wca_other.sedol.secid
            and bbc.cntrycd=wca_other.sedol.cntrycd
            and ''=wca_other.sedol.curencd
            and 'D'<>ifnull(wca_other.sedol.actflag,'')
            and ''<>ifnull(wca_other.sedol.sedol,'')
            and wca_other.sedol.cntrycd<>'XS'
            and wca_other.sedol.cntrycd<>'ZZ'
            and wca_other.sedol.rcntrycd<>'XG'
            and wca_other.sedol.rcntrycd<>'XH'
            and wca_other.sedol.rcntrycd<>'XZ'
inner join scexh on wca_other.sedol.secid=scexh.secid
              and wca_other.sedol.cntrycd=substring(scexh.exchgcd,1,2)
              and 'CNSZHK'<>scexh.exchgcd
              and 'CNSGHK'<>scexh.exchgcd
              and 'HKHKSG'<>scexh.exchgcd
              and 'HKHKSZ'<>scexh.exchgcd
              and 'D'<>scexh.actflag
where
bbe.secid is null
and bbc.curencd<>''
and bbc.cntrycd<>'XG'
and bbc.cntrycd<>'XH'
and bbc.cntrycd<>'XZ'
and bbc.actflag<>'D'
and concat(ifnull(bbc.secid,''),ifnull(scexh.exchgcd,''),ifnull(wca_other.sedol.curencd,''),ifnull(wca_other.sedol.rcntrycd,''))
not in (select concat(ifnull(subsdseq.secid,''),ifnull(subsdseq.exchgcd,''),ifnull(subsdseq.curencd,''),ifnull(subsdseq.rcntrycd,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=bbc.secid);
insert ignore into wca2.sedolbbgseq
select
null, 
wca_other.sedol.acttime,
bbc.acttime,
bbe.acttime, 
wca_other.sedol.secid, 
wca_other.sedol.cntrycd, 
wca_other.sedol.curencd, 
wca_other.sedol.rcntrycd,
bbe.exchgcd, 
wca_other.sedol.sedol, 
wca_other.sedol.defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk, 
wca_other.sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from wca_other.sedol
inner join bbe on wca_other.sedol.secid=bbe.secid
              and wca_other.sedol.cntrycd=substring(bbe.exchgcd,1,2)
              and wca_other.sedol.curencd=bbe.curencd
              and 'D'<>bbe.actflag
              and ''<>bbe.bbgexhid
inner join scmst on wca_other.sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
left outer join bbc on bbe.secid=bbc.secid
                   and wca_other.sedol.cntrycd=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and 'D'<>bbc.actflag
                   and bbc.cntrycd<>'XG'
                   and bbc.cntrycd<>'XH'
                   and bbc.cntrycd<>'XZ'
where 
wca_other.sedol.actflag<>'D'
and bbe.exchgcd<>''
and wca_other.sedol.curencd=''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and wca_other.sedol.sedol<>''
and wca_other.sedol.cntrycd<>''
and wca_other.sedol.cntrycd<>'XS'
and wca_other.sedol.cntrycd<>'ZZ'
and wca_other.sedol.rcntrycd<>'XG'
and wca_other.sedol.rcntrycd<>'XH'
and wca_other.sedol.rcntrycd<>'XZ'
and concat(ifnull(bbe.secid,''),ifnull(bbe.exchgcd,''),ifnull(wca_other.sedol.curencd,''),ifnull(wca_other.sedol.rcntrycd,''))
not in (select concat(ifnull(subsdseq.secid,''),ifnull(subsdseq.exchgcd,''),ifnull(subsdseq.curencd,''),ifnull(subsdseq.rcntrycd,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=bbe.secid);
insert ignore into wca2.sedolbbgseq
select
null, 
wca_other.sedol.acttime,
null,
scexh.acttime, 
wca_other.sedol.secid, 
wca_other.sedol.cntrycd, 
wca_other.sedol.curencd, 
wca_other.sedol.rcntrycd,
scexh.exchgcd, 
wca_other.sedol.sedol, 
wca_other.sedol.defunct,
'' as bbgcompid,
'' as bbgcomptk,
'' as bbgexhid,
'' as bbgexhtk, 
wca_other.sedol.sedolid,
null,
null
from wca_other.sedol
inner join scexh on wca_other.sedol.secid=scexh.secid
              and wca_other.sedol.cntrycd=substring(scexh.exchgcd,1,2)
              and 'D'<>scexh.actflag
inner join scmst on wca_other.sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
where 
wca_other.sedol.actflag<>'D'
and wca_other.sedol.defunct<>'T'
and scexh.exchgcd<>'CNSZHK'
and scexh.exchgcd<>'CNSGHK'
and scexh.exchgcd<>'HKHKSG'
and scexh.exchgcd<>'HKHKSZ'
and wca_other.sedol.sedol<>''
and wca_other.sedol.cntrycd<>''
and wca_other.sedol.curencd<>''
and wca_other.sedol.cntrycd<>'XS'
and wca_other.sedol.cntrycd<>'ZZ'
and wca_other.sedol.rcntrycd<>'XG'
and wca_other.sedol.rcntrycd<>'XH'
and wca_other.sedol.rcntrycd<>'XZ'
and concat(ifnull(scexh.secid,''),ifnull(scexh.exchgcd,''),ifnull(wca_other.sedol.rcntrycd,''),ifnull(wca_other.sedol.curencd,''))
not in (select concat(ifnull(subsdseq.secid,''),ifnull(subsdseq.exchgcd,''),ifnull(subsdseq.rcntrycd,''),ifnull(subsdseq.curencd,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=scexh.secid);
