drop table if exists wca2.evf_loadingtable;  
use wca;
create table wca2.evf_loadingtable(
select distinct
'initial_public_offering' as event,
case when scexh.scexhid is not null
     then concat(lpad(ipo.ipoid,7,'0'),scexh.scexhid)
     else lpad(ipo.ipoid,7,'0')
     end as caref,
ipo.ipoid as eventid,
ipo.announcedate as created,
ipo.acttime as changed,
case when ipo.actflag = 'U' then 'Updated'
     when ipo.actflag = 'I' then 'Inserted'
     when ipo.actflag = 'D' then 'Deleted'
     when ipo.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sbx.secid is not null and sbx.curencd<>'' then sbx.sedol when sbc.secid is not null then sbc.sedol else '' end as sedol,
case when sbx.secid is not null and sbx.curencd<>'' then sbx.rcntrycd when sbc.secid is not null then sbc.rcntrycd else '' end as sedol_register_country,
case when sbx.secid is not null and sbx.curencd<>'' then sbx.curencd when sbc.secid is not null then sbc.curencd else '' end as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
case when scexh.scexhid is null
     then ipo.exchgcd
     else scexh.exchgcd
     end as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
case when sbx.secid is not null then sbx.bbgcompid when sbc.secid is not null then sbc.bbgcompid else '' end as composite_global_id,
case when sbx.secid is not null then sbx.bbgcomptk when sbc.secid is not null then sbc.bbgcomptk else '' end as bloomberg_composite_ticker,
case when sbx.secid is not null then sbx.bbgexhid else '' end as bloomberg_global_id,
case when sbx.secid is not null then sbx.bbgexhtk else '' end as bloomberg_exchange_ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
ipo.ipostatus as status,
ipo.subperiodfrom,
ipo.subperiodto,
ipo.tofcurrency,
ipo.sharepricelowest,
ipo.sharepricehighest,
ipo.proposedprice,
ipo.totaloffersize,
ipo.firsttradingdate,
ipo.initialprice,
ipo.minsharesoffered,
ipo.maxsharesoffered,
ipo.preso,
ipo.sharesoutstanding,
ipo.dealtype,
ipo.underwriter,
ipo.lawfirm,
ipo.transferagent,
ipo.whendate,
'ipo|ipoid|notes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid
from ipo
inner join scmst on ipo.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
left outer join wca2.sedolbbg as sbc on scmst.secid = sbc.secid
                   and '' = sbc.exchgcd
                   and exchg.cntrycd = sbc.cntrycd
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`trading_currency`,`sedol_register_country`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`),
add index `ix_exchgcd`(`exchange_code`);
drop table if exists wca2.evo_initial_public_offering;
rename table wca2.evf_loadingtable to wca2.evo_initial_public_offering;
drop table if exists wca2.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'ipo_rumour' as event,
concat(1, lpad(rum.rumid,7,'0')) as caref,
rum.rumid as eventid,
rum.announcedate as created,
rum.acttime as changed,
case when rum.actflag = 'U' then 'Updated'
     when rum.actflag = 'I' then 'Inserted'
     when rum.actflag = 'D' then 'Deleted'
     when rum.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
'' as country_of_incorporation,
rum.issuername as issuer_name,
'' as security_description,
'' as par_value,
'' as par_value_currency,
'' as status_flag,
rum.sectycd as security_type,
'' as security_structure,
'' as isin,
'' as us_code,
'' as sedol,
'' as sedol_register_country,
'' as trading_currency,
'' as primary_exchange,
rum.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
rum.localcode as local_code,
'' as composite_global_id,
'' as bloomberg_composite_ticker,
'' as bloomberg_global_id,
'' as bloomberg_exchange_ticker,
'' as listing_status,
'' as list_date,
'Rumour' as status,
'' as subperiodfrom,
'' as subperiodto,
'' as tofcurrency,
'' as sharepricelowest,
'' as sharepricehighest,
'' as proposedprice,
'' as totaloffersize,
'' as firsttradingdate,
'' as initialprice,
'' as minsharesoffered,
'' as maxsharesoffered,
'' as preso,
'' as sharesoutstanding,
'' as dealtype,
'' as underwriter,
'' as lawfirm,
'' as transferagent,
'' as whendate,
'rum|rumid|notes' as link_notes,
null as issid,
null as secid,
null as mktsgid
from rum
inner join exchg on rum.exchgcd = exchg.exchgcd
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`),
add index `ix_changed`(`changed`),
add index `ix_exchgcd`(`exchange_code`);
drop table if exists wca2.evo_IPO_Rumour;
rename table wca2.evf_loadingtable to wca2.evo_IPO_Rumour;
