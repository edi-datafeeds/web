select distinct
'ipo' as event,
case when scexh.scexhid is not null
     then concat(lpad(ipo.ipoid,7,'0'),scexh.scexhid)
     else lpad(ipo.ipoid,7,'0')
     end as caref,
ipo.ipoid as eventid,
ipo.announcedate as created,
ipo.acttime as changed,
case when ipo.actflag = 'U' then 'Updated'
     when ipo.actflag = 'I' then 'Inserted'
     when ipo.actflag = 'D' then 'Deleted'
     when ipo.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sbx.secid is not null then sbx.sedol when sbc.secid is not null then sbc.sedol else '' end as sedol,
case when sbx.secid is not null then sbx.rcntrycd when sbc.secid is not null then sbc.rcntrycd else '' end as sedol_register_country,
case when sbx.secid is not null then sbx.curencd when sbc.secid is not null then sbc.curencd else '' end as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
case when scexh.scexhid is null
     then ipo.exchgcd
     else scexh.exchgcd
     end as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
case when sbx.secid is not null then sbx.bbgcompid when sbc.secid is not null then sbc.bbgcompid else '' end as composite_global_id,
case when sbx.secid is not null then sbx.bbgcomptk when sbc.secid is not null then sbc.bbgcomptk else '' end as bloomberg_composite_ticker,
case when sbx.secid is not null then sbx.bbgexhid else '' end as bloomberg_global_id,
case when sbx.secid is not null then sbx.bbgexhtk else '' end as bloomberg_exchange_ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
ipo.ipostatus as status,
ipo.subperiodfrom,
ipo.subperiodto,
ipo.tofcurrency,
ipo.sharepricelowest,
ipo.sharepricehighest,
ipo.proposedprice,
ipo.totaloffersize,
ipo.firsttradingdate,
ipo.initialprice,
ipo.minsharesoffered,
ipo.maxsharesoffered,
ipo.preso,
ipo.sharesoutstanding,
ipo.dealtype,
ipo.underwriter,
ipo.lawfirm,
ipo.transferagent,
ipo.whendate,
'ipo|ipoid|notes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid
from ipo
inner join scmst on ipo.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
left outer join wca2.sedolbbg as sbc on scmst.secid = sbc.secid
                   and '' = sbc.exchgcd
                   and exchg.cntrycd = sbc.cntrycd
