drop table if exists wca2.evf_adjusted_sos;
use wca;
create table wca2.evf_adjusted_sos(
select distinct
'Capital Reduction' as event,
wca.caprd.caprdid as eventid,
wca.caprd.effectivedate as event_date,
format(wca.scmst.sharesoutstanding *(wca.caprd.newratio)/wca.caprd.oldratio,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.caprd.newratio," : ",wca.caprd.oldratio),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.caprd
inner join wca.scmst on wca.caprd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rd on wca.caprd.rdid = wca.rd.rdid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'SD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'RTS'=wca.rp5.eventtype
where
wca.caprd.effectivedate > (select max(feeddate) from wca.tbl_opslog where seq = 3)and wca.caprd.effectivedate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.caprd.oldratio <> ''
and wca.caprd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null);
insert into wca2.evf_adjusted_sos
select distinct
'Stock Dividend' as event,
wca.divpy.divpyid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.divpy.rationew + wca.divpy.rationew)/wca.divpy.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.divpy.rationew," : ",wca.divpy.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.div_my
left outer join wca.divpy on div_my.divid = divpy.divid and 1 = divpy.optionid
left outer join wca.divpy as divopt2 on div_my.divid = divopt2.divid and 2 = divopt2.optionid
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'div'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
wca.rd.secid=wca.divpy.ressecid
and divopt2.divid is null and divpy.divtype='S'
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.divpy.ratioold <> ''
and wca.divpy.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Divestment' as event,
wca.dvst.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.dvst.rationew)/wca.dvst.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.dvst.rationew," : ",wca.dvst.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.dvst
inner join wca.rd on wca.dvst.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'dvst'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and (wca.scmst.secid = wca.dvst.ressecid or wca.dvst.ressecid is null)
and wca.scmst.sharesoutstanding <> ''
and wca.dvst.rationew <> ''
and wca.dvst.ratioold <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Distribution' as event,
wca.dist.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.mpay.rationew + wca.mpay.rationew)/wca.mpay.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.mpay.rationew," : ",wca.mpay.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.dist
left outer join wca.mpay on dist.rdid = mpay.eventid and 1 = mpay.optionid and 'DIST'=mpay.sevent
left outer join wca.mpay as mpayser2 on dist.rdid = mpayser2.eventid and 1 = mpayser2.optionid and 2 = mpayser2.serialid and 'DIST'=mpay.sevent
left outer join wca.mpay as mpayopt2 on dist.rdid = mpayopt2.eventid and 2 = mpayopt2.optionid and 'DIST'=mpay.sevent
inner join wca.rd on wca.dist.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'DIST'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
mpayopt2.eventid is null
and mpayser2.eventid is null
and wca.mpay.ratioold <> '' and wca.mpay.rationew <> ''
and (wca.scmst.secid = wca.mpay.ressecid or wca.mpay.ressecid is null)
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),70)
and wca.scmst.sharesoutstanding <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Demerger' as event,
wca.dmrgr.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.mpay.rationew)/wca.mpay.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.mpay.rationew," : ",wca.mpay.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.dmrgr
left outer join wca.mpay on dmrgr.rdid = mpay.eventid and 1 = mpay.optionid and 'DMRGR'=mpay.sevent
left outer join wca.mpay as mpayser2 on dmrgr.rdid = mpayser2.eventid and 1 = mpayser2.optionid and 2 = mpayser2.serialid and 'dmrgr'=mpay.sevent
left outer join wca.mpay as mpayopt2 on dmrgr.rdid = mpayopt2.eventid and 2 = mpayopt2.optionid and 'dmrgr'=mpay.sevent
inner join wca.rd on wca.dmrgr.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'DMRGR'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
mpayopt2.eventid is null
and mpayser2.eventid is null
and wca.mpay.ratioold <> '' and wca.mpay.rationew <> ''
and (wca.scmst.secid = wca.mpay.ressecid or wca.mpay.ressecid is null)
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),70)
and wca.scmst.sharesoutstanding <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Subdivision' as event,
wca.sd.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.sd.newratio)/wca.sd.oldratio,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.sd.newratio," : ",wca.sd.oldratio),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.sd
inner join wca.rd on wca.sd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'SD'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'RTS'=wca.rp5.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.sd.oldratio <> ''
and wca.sd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Rights' as event,
wca.rts.rtsid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.rts.ratioold + wca.rts.rationew)/wca.rts.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.rts.rationew," : ",wca.rts.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.rts
inner join wca.rd on wca.rts.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'rts'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
wca.rd.secid=wca.rts.ressecid
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.rts.ratioold <> ''
and wca.rts.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Entitlement' as event,
wca.ent.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.ent.ratioold + wca.ent.rationew)/wca.ent.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.ent.rationew," : ",wca.ent.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.ent
inner join wca.rd on wca.ent.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'ent'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
wca.rd.secid=wca.ent.ressecid
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.ent.ratioold <> ''
and wca.ent.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Consolidation' as event,
wca.consd.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.consd.newratio)/wca.consd.oldratio,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.consd.newratio," : ",wca.consd.oldratio),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.consd
inner join wca.rd on wca.consd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'consd'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'ENT'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.consd.oldratio <> ''
and wca.consd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Bonus' as event,
wca.bon.bonid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.bon.ratioold + wca.bon.rationew)/wca.bon.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.bon.rationew," : ",wca.bon.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.bon
inner join wca.rd on wca.bon.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'bon'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
wca.rd.secid=wca.bon.ressecid
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.bon.ratioold <> ''
and wca.bon.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null;
ALTER TABLE `wca2`.`evf_adjusted_sos` ADD PRIMARY KEY (`event`, `eventid`);
