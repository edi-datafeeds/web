drop table if exists wca2.sedolbbg;
use wca;
create table wca2.sedolbbg
(
select
main.secid,
main.cntrycd,
main.curencd,
main.rcntrycd,
bbe.exchgcd,
main.sedol,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk
from sedol as main
inner join bbe on main.secid=bbe.secid
              and main.cntrycd=substring(bbe.exchgcd,1,2)
              and main.curencd=bbe.curencd
              and 'D'<>bbe.actflag
inner join scmst on main.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd 
                   and (3>sectygrp.secgrpid or 7=sectygrp.secgrpid)
left outer join bbc on bbe.secid=bbc.secid
                   and main.cntrycd=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and 'D'<>bbc.actflag
where
main.actflag<>'D'
and main.sedol<>''
and main.cntrycd<>''
and main.cntrycd<>'XS'
and main.cntrycd<>'ZZ'
and main.defunct<>'T'
);
alter table `wca2`.`sedolbbg`
add primary key (`secid`,`cntrycd`,`curencd`,`rcntrycd`,`exchgcd`);
insert into wca2.sedolbbg
select
main.secid,
substring(main.exchgcd,1,2) as cntrycd,
wca.sedol.curencd,
wca.sedol.rcntrycd,
main.exchgcd,
wca.sedol.sedol,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
main.bbgexhid,
main.bbgexhtk
from bbe as main
inner join sedol on main.secid=sedol.secid
            and substring(main.exchgcd,1,2)=sedol.cntrycd
            and ''<>ifnull(wca.sedol.curencd,'')
            and 'T'<>ifnull(sedol.defunct,'')
            and 'D'<>ifnull(sedol.actflag,'')
            and ''<>ifnull(sedol.sedol,'')
inner join scmst on main.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and (3>sectygrp.secgrpid or 7=sectygrp.secgrpid)
left outer join bbc on main.secid=bbc.secid
                   and substring(main.exchgcd,1,2)=bbc.cntrycd
                   and main.curencd=bbc.curencd
                   and 'D'<>ifnull(bbc.actflag,'')
where
main.curencd=''
and main.actflag<>'D'
and concat(main.secid,sedol.curencd,sedol.rcntrycd,main.exchgcd)
not in (select concat(secid,curencd,rcntrycd,exchgcd)
         from wca2.sedolbbg
         where 
         wca2.sedolbbg.secid=main.secid);
insert into wca2.sedolbbg
select
main.secid,
main.cntrycd,
bbe.curencd,
main.rcntrycd,
case when exchgcd is null then '' else exchgcd end as exchgcd,
main.sedol,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
case when bbe.bbgexhid is null then '' else bbe.bbgexhid end as bbgexhid,
case when bbe.bbgexhtk is null then '' else bbe.bbgexhtk end as bbgexhtk
from sedol as main
inner join bbe on main.secid=bbe.secid
                   and main.cntrycd=substring(bbe.exchgcd,1,2)
                   and ''<>ifnull(bbe.curencd,'')
                   and 'D'<>ifnull(bbe.actflag,'')
inner join scmst on main.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and (3>sectygrp.secgrpid or 7=sectygrp.secgrpid)
left outer join bbc on bbe.secid=bbc.secid
                   and main.cntrycd=ifnull(bbc.cntrycd,'')
                   and bbe.curencd=ifnull(bbc.curencd,'')
                   and 'D'<>ifnull(bbc.actflag,'')
where
main.curencd=''
and main.actflag<>'D'
and main.sedol<>''
and main.cntrycd<>''
and main.cntrycd<>'XS'
and main.cntrycd<>'ZZ'
and main.defunct<>'T'
and concat(main.secid,bbe.curencd,main.rcntrycd,bbe.exchgcd)
not in (select concat(secid,curencd,rcntrycd,exchgcd)
         from wca2.sedolbbg
         where 
         wca2.sedolbbg.secid=main.secid);
insert into wca2.sedolbbg
select
main.secid,
substring(main.exchgcd,1,2) as cntrycd,
main.curencd,
'' as rcntrycd,
main.exchgcd,
'' as sedol,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
main.bbgexhid,
main.bbgexhtk
from bbe as main
left outer join sedol on main.secid=sedol.secid
            and substring(main.exchgcd,1,2)=sedol.cntrycd
            and 'T'<>ifnull(sedol.defunct,'')
            and 'D'<>ifnull(sedol.actflag,'')
            and ''<>ifnull(sedol.sedol,'')
inner join scmst on main.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and (3>sectygrp.secgrpid or 7=sectygrp.secgrpid)
left outer join bbc on main.secid=bbc.secid
                   and substring(main.exchgcd,1,2)=bbc.cntrycd
                   and main.curencd=bbc.curencd
                   and 'D'<>ifnull(bbc.actflag,'')
where
sedol.sedolid is null
and main.actflag<>'D'
and concat(main.secid,main.exchgcd)
not in (select concat(secid,exchgcd)
         from wca2.sedolbbg
         where 
         wca2.sedolbbg.secid=main.secid);
insert into wca2.sedolbbg
select distinct
main.secid,
main.cntrycd,
main.curencd,
main.rcntrycd,
'' as exchgcd,
main.sedol,
'' as bbgcompid,
'' as bbgcomptk,
'' as bbgexhid,
'' as bbgexhtk
from sedol as main
left outer join bbe on main.secid=bbe.secid
                   and main.cntrycd=substring(bbe.exchgcd,1,2)
inner join scmst on main.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and (3>sectygrp.secgrpid or 7=sectygrp.secgrpid)
where
main.actflag<>'D'
and bbe.secid is null
and main.sedol<>''
and main.cntrycd<>''
and main.cntrycd<>'XS'
and main.cntrycd<>'ZZ'
and main.defunct<>'T'
and concat(main.secid,main.cntrycd)
not in (select concat(secid,cntrycd)
         from wca2.sedolbbg
         where 
         wca2.sedolbbg.secid=main.secid);
