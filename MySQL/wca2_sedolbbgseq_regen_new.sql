
insert ignore into wca2.sedolbbgseqnew1
select
null, 
wca_other.sedol.acttime,
null,
scexh.acttime, 
wca_other.sedol.secid, 
wca_other.sedol.cntrycd, 
wca_other.sedol.curencd, 
wca_other.sedol.rcntrycd,
scexh.exchgcd, 
wca_other.sedol.sedol, 
wca_other.sedol.defunct,
'' as bbgcompid,
'' as bbgcomptk,
'' as bbgexhid,
'' as bbgexhtk, 
wca_other.sedol.sedolid,
null,
null
from wca_other.sedol
inner join scexh on wca_other.sedol.secid=scexh.secid
              and wca_other.sedol.cntrycd=substring(scexh.exchgcd,1,2)
              and 'D'<>scexh.actflag
inner join scmst on wca_other.sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
where 
wca_other.sedol.actflag<>'D'
and scexh.exchgcd<>'CNSZHK'
and scexh.exchgcd<>'CNSGHK'
and scexh.exchgcd<>'HKHKSG'
and scexh.exchgcd<>'HKHKSZ'
and wca_other.sedol.sedol<>''
and wca_other.sedol.cntrycd<>''
and wca_other.sedol.curencd<>''
and wca_other.sedol.cntrycd<>'XS'
and wca_other.sedol.cntrycd<>'ZZ'
and wca_other.sedol.rcntrycd<>'XG'
and wca_other.sedol.rcntrycd<>'XH'
and wca_other.sedol.rcntrycd<>'XZ'
and concat(ifnull(scexh.secid,''),ifnull(scexh.exchgcd,''),ifnull(wca_other.sedol.curencd,''),ifnull(wca_other.sedol.rcntrycd,''))
not in (select concat(ifnull(subsdseq.secid,''),ifnull(subsdseq.exchgcd,''),ifnull(subsdseq.curencd,''),ifnull(subsdseq.rcntrycd,''))
         from wca2.sedolbbgseqnew1 as subsdseq
         where 
         subsdseq.secid=bbe.secid);
insert ignore into wca2.sedolbbgseqnew1
select
null,
null,
bbc.acttime,
bbe.acttime,
bbe.secid,
substring(bbe.exchgcd,1,2) as cntrycd,
bbe.curencd,
'' as rcntrycd,
bbe.exchgcd,
'' as sedol,
'' as defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
null,
bbc.bbcid,
bbe.bbeid
from bbe
inner join scmst on bbe.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
inner join bbc on bbe.secid=bbc.secid
                   and substring(bbe.exchgcd,1,2)=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and 'D'<>ifnull(bbc.actflag,'')
left outer join wca_other.sedol on bbe.secid=wca_other.sedol.secid
            and bbc.cntrycd=sedol.cntrycd
            and bbc.curencd=ifnull(wca_other.sedol.curencd,'')
            and 'D'<>ifnull(wca_other.sedol.actflag,'')
            and ''<>ifnull(wca_other.sedol.sedol,'')
            and wca_other.sedol.cntrycd<>'XS'
            and wca_other.sedol.cntrycd<>'ZZ'
            and wca_other.sedol.rcntrycd<>'XG'
            and wca_other.sedol.rcntrycd<>'XH'
            and wca_other.sedol.rcntrycd<>'XZ'
where
bbe.exchgcd<>''
and wca_other.sedol.sedolid is null
and bbe.curencd<>''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and bbe.actflag<>'D'
and bbe.bbgexhid<>''
and bbc.cntrycd<>'XG'
and bbc.cntrycd<>'XH'
and bbc.cntrycd<>'XZ'
and bbe.bbeid not in (select ifnull(subsdseq.bbeid,999999)
         from wca2.sedolbbgseqnew1 as subsdseq
         where 
         subsdseq.bbeid=bbe.bbeid);
insert ignore into wca2.sedolbbgseqnew1
select
null,
null,
null,
bbe.acttime,
bbe.secid,
substring(bbe.exchgcd,1,2) as cntrycd,
bbe.curencd,
'' as rcntrycd,
bbe.exchgcd,
'' as sedol,
'' as defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
null,
null,
bbe.bbeid
from bbe
inner join scmst on bbe.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
left outer join bbc on bbe.secid=bbc.secid
                   and substring(bbe.exchgcd,1,2)=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and 'D'<>ifnull(bbc.actflag,'')
                   and bbc.cntrycd<>'XG'
                   and bbc.cntrycd<>'XH'
                   and bbc.cntrycd<>'XZ'
left outer join wca_other.sedol on bbe.secid=wca_other.sedol.secid
            and bbc.cntrycd=sedol.cntrycd
            and bbc.curencd=ifnull(wca_other.sedol.curencd,'')
            and 'D'<>ifnull(wca_other.sedol.actflag,'')
            and ''<>ifnull(wca_other.sedol.sedol,'')
            and wca_other.sedol.cntrycd<>'XS'
            and wca_other.sedol.cntrycd<>'ZZ'
            and wca_other.sedol.rcntrycd<>'XG'
            and wca_other.sedol.rcntrycd<>'XH'
            and wca_other.sedol.rcntrycd<>'XZ'
where
bbe.exchgcd<>''
and bbe.curencd<>''
and wca_other.sedol.sedolid is null
and bbc.bbcid is null
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and bbe.actflag<>'D'
and bbe.bbgexhid<>''
and bbe.bbeid not in (select ifnull(subsdseq.bbeid,999999)
         from wca2.sedolbbgseqnew1 as subsdseq
         where 
         subsdseq.bbeid=bbe.bbeid);
insert ignore into wca2.sedolbbgseqnew1
select
null, 
wca_other.sedol.acttime,
bbc.acttime,
bbe.acttime, 
wca_other.sedol.secid, 
wca_other.sedol.cntrycd, 
wca_other.sedol.curencd, 
wca_other.sedol.rcntrycd,
bbe.exchgcd, 
wca_other.sedol.sedol, 
wca_other.sedol.defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk, 
wca_other.sedol.sedolid,
bbc.bbcid,
bbe.bbeid
from wca_other.sedol
inner join bbe on wca_other.sedol.secid=bbe.secid
              and wca_other.sedol.cntrycd=substring(bbe.exchgcd,1,2)
              and wca_other.sedol.curencd=bbe.curencd
              and 'D'<>bbe.actflag
              and ''<>bbe.bbgexhid
inner join scmst on wca_other.sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
left outer join bbc on bbe.secid=bbc.secid
                   and wca_other.sedol.cntrycd=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and 'D'<>bbc.actflag
                   and bbc.cntrycd<>'XG'
                   and bbc.cntrycd<>'XH'
                   and bbc.cntrycd<>'XZ'
where 
wca_other.sedol.actflag<>'D'
and bbe.exchgcd<>''
and wca_other.sedol.curencd=''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and wca_other.sedol.sedol<>''
and wca_other.sedol.cntrycd<>''
and wca_other.sedol.cntrycd<>'XS'
and wca_other.sedol.cntrycd<>'ZZ'
and wca_other.sedol.rcntrycd<>'XG'
and wca_other.sedol.rcntrycd<>'XH'
and wca_other.sedol.rcntrycd<>'XZ'
and bbe.bbeid not in (select ifnull(subsdseq.bbeid,999999)
         from wca2.sedolbbgseqnew1 as subsdseq
         where 
         subsdseq.bbeid=bbe.bbeid);
insert ignore into wca2.sedolbbgseqnew1
select
null, 
wca_other.sedol.acttime,
null,
scexh.acttime, 
wca_other.sedol.secid, 
wca_other.sedol.cntrycd, 
wca_other.sedol.curencd, 
wca_other.sedol.rcntrycd,
scexh.exchgcd, 
wca_other.sedol.sedol, 
wca_other.sedol.defunct,
'' as bbgcompid,
'' as bbgcomptk,
'' as bbgexhid,
'' as bbgexhtk, 
wca_other.sedol.sedolid,
null,
null
from wca_other.sedol
inner join scexh on wca_other.sedol.secid=scexh.secid
              and wca_other.sedol.cntrycd=substring(scexh.exchgcd,1,2)
              and 'D'<>scexh.actflag
inner join scmst on wca_other.sedol.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
where 
wca_other.sedol.actflag<>'D'
and scexh.exchgcd<>'CNSZHK'
and scexh.exchgcd<>'CNSGHK'
and scexh.exchgcd<>'HKHKSG'
and scexh.exchgcd<>'HKHKSZ'
and wca_other.sedol.sedol<>''
and wca_other.sedol.cntrycd<>''
and wca_other.sedol.cntrycd<>'XS'
and wca_other.sedol.cntrycd<>'ZZ'
and wca_other.sedol.rcntrycd<>'XG'
and wca_other.sedol.rcntrycd<>'XH'
and wca_other.sedol.rcntrycd<>'XZ'
and concat(ifnull(scexh.secid,''),ifnull(scexh.exchgcd,''))
not in (select concat(ifnull(subsdseq.secid,''),ifnull(subsdseq.exchgcd,''))
         from wca2.sedolbbgseq as subsdseq
         where 
         subsdseq.secid=scexh.secid);
insert ignore into wca2.sedolbbgseqnew1
select
null,
null,
bbc.acttime,
bbe.acttime,
bbe.secid,
substring(bbe.exchgcd,1,2) as cntrycd,
bbe.curencd,
'' as rcntrycd,
bbe.exchgcd,
'' as sedol,
'' as defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
null,
bbc.bbcid,
bbe.bbeid
from bbe
inner join scmst on bbe.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
inner join bbc on bbe.secid=bbc.secid
                   and substring(bbe.exchgcd,1,2)=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and 'D'<>ifnull(bbc.actflag,'')
left outer join wca_other.sedol on bbe.secid=wca_other.sedol.secid
            and bbc.cntrycd=sedol.cntrycd
            and bbc.curencd=ifnull(wca_other.sedol.curencd,'')
            and 'D'<>ifnull(wca_other.sedol.actflag,'')
            and ''<>ifnull(wca_other.sedol.sedol,'')
            and wca_other.sedol.cntrycd<>'XS'
            and wca_other.sedol.cntrycd<>'ZZ'
            and wca_other.sedol.rcntrycd<>'XG'
            and wca_other.sedol.rcntrycd<>'XH'
            and wca_other.sedol.rcntrycd<>'XZ'
where
bbe.exchgcd<>''
and wca_other.sedol.sedolid is null
and bbe.curencd=''
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and bbe.actflag<>'D'
and bbe.bbgexhid<>''
and bbc.cntrycd<>'XG'
and bbc.cntrycd<>'XH'
and bbc.cntrycd<>'XZ'
and bbe.bbeid not in (select ifnull(subsdseq.bbeid,999999)
         from wca2.sedolbbgseqnew1 as subsdseq
         where 
         subsdseq.bbeid=bbe.bbeid);
insert ignore into wca2.sedolbbgseqnew1
select
null,
null,
null,
bbe.acttime,
bbe.secid,
substring(bbe.exchgcd,1,2) as cntrycd,
bbe.curencd,
'' as rcntrycd,
bbe.exchgcd,
'' as sedol,
'' as defunct,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
null,
null,
bbe.bbeid
from bbe
inner join scmst on bbe.secid=scmst.secid
                and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
left outer join bbc on bbe.secid=bbc.secid
                   and substring(bbe.exchgcd,1,2)=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and 'D'<>ifnull(bbc.actflag,'')
                   and bbc.cntrycd<>'XG'
                   and bbc.cntrycd<>'XH'
                   and bbc.cntrycd<>'XZ'
left outer join wca_other.sedol on bbe.secid=wca_other.sedol.secid
            and bbc.cntrycd=sedol.cntrycd
            and bbc.curencd=ifnull(wca_other.sedol.curencd,'')
            and 'D'<>ifnull(wca_other.sedol.actflag,'')
            and ''<>ifnull(wca_other.sedol.sedol,'')
            and wca_other.sedol.cntrycd<>'XS'
            and wca_other.sedol.cntrycd<>'ZZ'
            and wca_other.sedol.rcntrycd<>'XG'
            and wca_other.sedol.rcntrycd<>'XH'
            and wca_other.sedol.rcntrycd<>'XZ'
where
bbe.exchgcd<>''
and bbe.curencd=''
and wca_other.sedol.sedolid is null
and bbc.bbcid is null
and bbe.exchgcd<>'CNSZHK'
and bbe.exchgcd<>'CNSGHK'
and bbe.exchgcd<>'HKHKSG'
and bbe.exchgcd<>'HKHKSZ'
and bbe.actflag<>'D'
and bbe.bbgexhid<>''
and bbe.bbeid not in (select ifnull(subsdseq.bbeid,999999)
         from wca2.sedolbbgseqnew1 as subsdseq
         where 
         subsdseq.bbeid=bbe.bbeid);
