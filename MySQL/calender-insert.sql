TRUNCATE `wfi`.`calender`;

INSERT INTO `wfi`.`calender` ( 
		`ISIN`, 
		`SecID`,
		 `USCode`,
		 `IssuerName`,
		 `SecurityDesc`,
		 `InterestBasis`,
		 `InterestRate`,
		 `AnlCoupRate`,
		 `NilInt`,
		 `Source`,
		 `Paydate`,
		 `InterestFromDate`,
		 `InterestToDate`,
		 `LatestAppliedINTPYAnlCpnRateDate`,
		 `LatestAppliedINTPYAnlCpnRate`,
		 `InterestPayDate1`,
		 `InterestPayDate2`,
		 `InterestPayDate3`,
		 `InterestPayDate4`,
		 `Interestpaymentfrequency`,
		 `FirstCouponDate`,
		 `LastCouponDate`,
		 `rdid`,
		 `intpy_actflag`,
		  `flagstatus`
  ) 
SELECT DISTINCT wca.scmst.ISIN, 
wca.bond.SecID, 
wca.scmst.USCode, 
wca.issur.IssuerName, 
wca.scmst.SecurityDesc, 
wca.bond.InterestBasis, 
wca.bond.InterestRate, 
wca.intpy.AnlCoupRate, 
wca.int_my.NilInt, 
wca.int_my.Source, 
wca.exdt.Paydate, 
wca.int_my.InterestFromDate, 
wca.int_my.InterestToDate, 
wca.bond.LatestAppliedINTPYAnlCpnRateDate, 
wca.bond.LatestAppliedINTPYAnlCpnRate, 
wca.bond.InterestPayDate1, 
wca.bond.InterestPayDate2, 
wca.bond.InterestPayDate3, 
wca.bond.InterestPayDate4, 
wca.bond.Interestpaymentfrequency, 
wca.bond.FirstCouponDate, 
wca.bond.LastCouponDate, 
wca.rd.rdid, 
wca.intpy.actflag, 
CASE WHEN wca.scmst.StatusFlag = '' THEN 'A' ELSE wca.scmst.StatusFlag end AS StatusFlag 
FROM wca.scmst INNER JOIN wca.bond ON wca.scmst.SecID = wca.bond.SecID AND wca.bond.InterestBasis = 'FR' 
LEFT OUTER JOIN wca.bondx ON wca.bond.SecID = wca.bondx.SecID 
LEFT OUTER JOIN wca.rd ON wca.scmst.SecID = wca.rd.SecID 
LEFT OUTER JOIN wca.int_my ON wca.rd.RdID = wca.int_my.RdID 
LEFT OUTER JOIN wca.intpy ON wca.int_my.RdID = wca.intpy.RDID 
LEFT OUTER JOIN wca.scexh ON wca.bond.SecID = wca.scexh.SecID 
LEFT OUTER JOIN wca.exdt ON wca.rd.RdID = wca.exdt.RdID 
INNER JOIN wca.issur ON wca.scmst.IssID = wca.issur.IssID 
WHERE wca.bond.actflag <> 'D' 
	AND wca.scmst.actflag <> 'D' 
	AND wca.issur.actflag <> 'D' 
	AND wca.intpy.actflag <> 'D' 
	AND wca.rd.actflag <> 'D' 
	AND wca.int_my.actflag <> 'D' 
	AND(wca.exdt.actflag <> 'D' OR wca.exdt.actflag IS NULL) 
	AND (wca.scexh.actflag <> 'D' OR wca.scexh.actflag IS NULL) 
	AND wca.scmst.statusflag <> 'I';