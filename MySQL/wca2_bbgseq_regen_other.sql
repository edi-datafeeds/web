use wca2;
drop table if exists bbgseq;
CREATE TABLE `bbgseq` (
  `autoid` int(11) NOT NULL AUTO_INCREMENT,
  `bbcacttime` datetime DEFAULT NULL,
  `bbeacttime` datetime DEFAULT NULL,
  `secid` int(11) NOT NULL DEFAULT '0',
  `cntrycd` char(2) NOT NULL DEFAULT '',
  `curencd` char(3) NOT NULL DEFAULT '',
  `exchgcd` char(6) NOT NULL DEFAULT '',
  `bbgcompid` varchar(12) DEFAULT NULL,
  `bbgcomptk` varchar(40) DEFAULT NULL,
  `bbgexhid` char(12) DEFAULT NULL,
  `bbgexhtk` varchar(40) DEFAULT NULL,
  `bbcid` int(11) DEFAULT NULL,
  `bbeid` int(11) DEFAULT NULL,
  PRIMARY KEY (`autoid`),
  KEY `ix_bbcid` (`bbcid`),
  KEY `ix_bbeid` (`bbeid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `wca2`.`bbgseq` 
ADD UNIQUE INDEX `ix_distinct` (`secid` ASC, `exchgcd` ASC, `curencd` ASC);
use wca;
insert ignore into wca2.bbgseq
select
null,
bbc.acttime,
bbe.acttime,
bbe.secid,
substring(bbe.exchgcd,1,2) as cntrycd,
bbe.curencd,
bbe.exchgcd,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
bbc.bbcid,
bbe.bbeid
from bbe
inner join scmst on bbe.secid=scmst.secid
                and 'D'<>scmst.actflag
left outer join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
left outer join bbc on bbe.secid=bbc.secid
                   and substring(bbe.exchgcd,1,2)=bbc.cntrycd
                   and bbe.curencd=bbc.curencd
                   and 'D'<>ifnull(bbc.actflag,'')
where
bbe.exchgcd<>''
and bbe.bbgexhid<>''
and bbe.actflag<>'D';
insert ignore into wca2.bbgseq
select
null,
bbc.acttime,
scexh.acttime,
scexh.secid,
'US' as cntrycd,
'USD' as curencd,
scexh.exchgcd,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
case when bbe.bbgexhid is null then '' else bbe.bbgexhid end as bbgexhid,
case when bbe.bbgexhtk is null then '' else bbe.bbgexhtk end as bbgexhtk,
bbc.bbcid,
bbe.bbeid
from scexh as scexh
inner join scmst on scexh.secid=scmst.secid
            and 'D'<>scmst.actflag
left outer join bbe on scexh.secid=bbe.secid
            and ''<>ifnull(bbe.bbgexhid,'')
            and 'D'<>ifnull(bbe.actflag,'')
left outer join bbc on scexh.secid=bbc.secid
            and 'US'=ifnull(bbc.cntrycd,'')
            and ''<>ifnull(bbc.bbgcompid,'')
            and 'D'<>ifnull(bbc.actflag,'')
where
scexh.actflag<>'D'
and wca.scmst.sectycd='BND'
and substring(scexh.exchgcd,1,2)='US'
and (scexh.exchgcd='USNYSE' or scexh.exchgcd='USNASD' or scexh.exchgcd='USAMEX' or scexh.exchgcd='USPAC' or scexh.exchgcd='USBATS')
and ord(substring(uscode,7,1)) between 48 and 57 
and ord(substring(uscode,8,1)) between 48 and 57
and (bbc.bbcid is not null or bbe.bbeid is not null);
insert ignore into wca2.bbgseq
select
null,
bbc.acttime,
bbc.acttime,
bbc.secid,
bbc.cntrycd,
bbc.curencd,
scexh.exchgcd,
case when bbc.bbgcompid is null then '' else bbc.bbgcompid end as bbgcompid,
case when bbc.bbgcomptk is null then '' else bbc.bbgcomptk end as bbgcomptk,
bbe.bbgexhid,
bbe.bbgexhtk,
bbc.bbcid,
bbe.bbeid
from scexh as scexh
inner join scmst on scexh.secid=scmst.secid
            and 'D'<>scmst.actflag
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
inner join bbc on scexh.secid=bbc.secid
            and substring(scexh.exchgcd,1,2)=bbc.cntrycd
            and ''<>ifnull(bbc.bbgcompid,'')
            and 'D'<>ifnull(bbc.actflag,'')
left outer join bbe on bbc.secid=bbe.secid
            and ''<>ifnull(bbe.bbgexhid,'')
            and 'D'<>ifnull(bbe.actflag,'')
where
scexh.actflag<>'D'
and bbeid is null
and bbcid is not null;
insert ignore into wca2.bbgseq
select
null,
null as acttime,
bbe.acttime,
bbe.secid,
substring(scmst.primaryexchgcd,1,2) as cntrycd,
bbe.curencd,
scmst.primaryexchgcd,
'' as bbgcompid,
'' as bbgcomptk,
case when bbe.bbgexhid is null then '' else bbe.bbgexhid end as bbgexhid,
case when bbe.bbgexhtk is null then '' else bbe.bbgexhtk end as bbgexhtk,
null as bbcid,
bbe.bbeid
from scmst
inner join sectygrp on scmst.sectycd=sectygrp.sectycd
                   and 4<>sectygrp.secgrpid
left outer join bbe on scmst.secid=bbe.secid
            and ''<>ifnull(bbe.bbgexhid,'')
            and 'D'<>ifnull(bbe.actflag,'')
where
scmst.actflag<>'D'
and scmst.primaryexchgcd<>''
and bbe.exchgcd=''
and bbeid is not null;
