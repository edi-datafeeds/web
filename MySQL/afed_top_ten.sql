delete from afed_data.marketing_topten;
ALTER TABLE afed_data.marketing_topten AUTO_INCREMENT = 1;

insert into afed_data.marketing_topten (country,indicators,value,frequency,value_date,sort)
Select C.value AS country,indicator_name As indicators,V.value ,F.freq As frequency,effective_date AS value_date,V.sort
FROM afed_data.key_indicator_value As V                    
	inner join afed_data.locality AS C on V.CntryCD=C.cd  and V.type_cd=C.type_cd
	left outer join afed_data.freq AS F on V.frq_id=F.frq_id  Where C.active=1 
	UNION                                 
	SELECT  DISTINCT
	C.`value` As Country, CONCAT('Exchange Rate ',R.curr,':US$ (av)') AS indicators, CASE WHEN R.rate =0 OR R.rate  IS NULL THEN 'N/A' ELSE R.rate  END AS value,'Daily' As frequency,max_date As value_date,'9' As sort   
	FROM    xrates.rates R
	INNER JOIN afed_data.locality As C On R.Curr=C.currency_code And C.type_cd='C'
	INNER JOIN (
		SELECT  Curr, MAX(Feeddate) max_date
		FROM   xrates.rates
			Where BASE = 'USD'
		GROUP   BY Curr
	) R2 ON  R.Curr = R2.Curr AND R.Feeddate = R2.max_date
	WHERE C.cd in (Select Distinct cntrycd from afed_data.key_indicator_value) And R.BASE = 'USD'
	GROUP BY C.currency_code,C.cd
	UNION
	SELECT C.`value` As Country, k.display_name AS indicators, R.value ,'Daily' As frequency,max_date As value_date,'10' As sort
	from irates.rates As R   
	INNER JOIN irates.key_irate As K On R.cntry =k.cntry And k.type_id =R.Type   
	INNER JOIN afed_data.locality As C On K.cntry=C.CD And C.type_cd='C'
	INNER JOIN 
	(
		SELECT DISTINCT cntry,Type, MAX(dte) max_date
		FROM    irates.rates
			Where active = 1
		GROUP   BY cntry,Type
	) x ON  R.cntry=x.cntry AND R.Type=x.Type AND R.dte = x.max_date
	WHERE R.cntry in (Select Distinct cntrycd from afed_data.key_indicator_value)
	GROUP BY R.cntry,R.Type
	Order By country, CAST(sort AS UNSIGNED) Asc;