drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select distinct
upper(actflag) as actflag,
lookupextra.acttime,
upper(typegroup) as typegroup,
upper(code) as code,
lookupextra.lookup
from lookupextra
);
alter table `wfi`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`typegroup`, `code`);
use wca;
insert ignore into wfi.loadingtable
select distinct
'I' as actflag,
null as acttime,
upper(typegroup) as typegroup,
upper(code) as code,
lookup.lookup
from lookup
where upper(code)<>'CORR';
use wca;
insert ignore into wfi.loadingtable
select distinct
upper('I') as actflag,
'2006/01/26' as acttime,
upper('divperiod') as typegroup,
dvprd.dvprdcd,
dvprd.divperiod
from dvprd
where wca.dvprd.dvprdcd not in
(select wfi.loadingtable.code from wfi.loadingtable where wfi.loadingtable.typegroup = 'FREQ');
use wca;
insert ignore into wfi.loadingtable
select distinct
upper('I') as actflag,
secty.acttime,
upper('sectype') as typegroup,
secty.sectycd,
secty.securitydescriptor
from secty;
use wca;
insert ignore into wfi.loadingtable
select distinct
exchg.actflag,
exchg.acttime,
upper('exchange') as typegroup,
exchg.exchgcd as code,
exchg.exchgname as lookup
from exchg;
use wca;
insert ignore into wfi.loadingtable
select distinct
exchg.actflag,
exchg.acttime,
upper('miccode') as typegroup,
exchg.mic as code,
exchg.exchgname as lookup
from exchg
where mic <> '' and mic is not null;
use wca;
insert ignore into wfi.loadingtable
select distinct
exchg.actflag,
exchg.acttime,
upper('micmap') as typegroup,
exchg.exchgcd as code,
exchg.mic as lookup
from exchg;
use wca;
insert ignore into wfi.loadingtable
select distinct
indus.actflag,
indus.acttime,
upper('indus') as typegroup,
indus.indusid as code,
indus.indusname as lookup
from indus;
use wca;
insert ignore into wfi.loadingtable
select distinct
cntry.actflag,
cntry.acttime,
upper('cntry') as typegroup,
cntry.cntrycd as code,
cntry.country as lookup
from cntry;
use wca;
insert ignore into wfi.loadingtable
select distinct
curen.actflag,
curen.acttime,
upper('curen') as typegroup,
curen.curencd as code,
curen.currency as lookup
from curen;
use wca;
insert ignore into wfi.loadingtable
select distinct
event.actflag,
event.acttime,
upper('event') as typegroup,
event.eventtype as code,
event.eventname as lookup
from event;
use wca;
insert ignore into wfi.loadingtable
select distinct
upper('I') as actflag,
'2006/01/26' as acttime,
upper('drtype') as typegroup,
sectygrp.sectycd as code,
sectygrp.securitydescriptor as lookup
from sectygrp
where secgrpid = 2;
drop table if exists wfi.sys_lookup;
rename table wfi.loadingtable to wfi.sys_lookup;
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select distinct
upper(actflag) as actflag,
lookupextra.acttime,
upper(typegroup) as typegroup,
upper(code) as code,
lookupextra.lookup
from lookupextra
);
alter table `wfimatured`.loadingtable
COLLATE='latin1_swedish_ci',
ENGINE=MyISAM,
add primary key (`typegroup`, `code`);
use wca;
insert ignore into wfimatured.loadingtable
select distinct
'I' as actflag,
null as acttime,
upper(typegroup) as typegroup,
upper(code) as code,
lookup.lookup
from lookup;
use wca;
insert ignore into wfimatured.loadingtable
select distinct
upper('I') as actflag,
'2006/01/26' as acttime,
upper('divperiod') as typegroup,
dvprd.dvprdcd,
dvprd.divperiod
from dvprd
where wca.dvprd.dvprdcd not in
(select wfimatured.loadingtable.code from wfimatured.loadingtable where wfimatured.loadingtable.typegroup = 'FREQ');
use wca;
insert ignore into wfimatured.loadingtable
select distinct
upper('I') as actflag,
secty.acttime,
upper('sectype') as typegroup,
secty.sectycd,
secty.securitydescriptor
from secty;
use wca;
insert ignore into wfimatured.loadingtable
select distinct
exchg.actflag,
exchg.acttime,
upper('exchange') as typegroup,
exchg.exchgcd as code,
exchg.exchgname as lookup
from exchg;
use wca;
insert ignore into wfimatured.loadingtable
select distinct
exchg.actflag,
exchg.acttime,
upper('miccode') as typegroup,
exchg.mic as code,
exchg.exchgname as lookup
from exchg
where mic <> '' and mic is not null;
use wca;
insert ignore into wfimatured.loadingtable
select distinct
exchg.actflag,
exchg.acttime,
upper('micmap') as typegroup,
exchg.exchgcd as code,
exchg.mic as lookup
from exchg;
use wca;
insert ignore into wfimatured.loadingtable
select distinct
indus.actflag,
indus.acttime,
upper('indus') as typegroup,
indus.indusid as code,
indus.indusname as lookup
from indus;
use wca;
insert ignore into wfimatured.loadingtable
select distinct
cntry.actflag,
cntry.acttime,
upper('cntry') as typegroup,
cntry.cntrycd as code,
cntry.country as lookup
from cntry;
use wca;
insert ignore into wfimatured.loadingtable
select distinct
curen.actflag,
curen.acttime,
upper('curen') as typegroup,
curen.curencd as code,
curen.currency as lookup
from curen;
use wca;
insert ignore into wfimatured.loadingtable
select distinct
event.actflag,
event.acttime,
upper('event') as typegroup,
event.eventtype as code,
event.eventname as lookup
from event;
use wca;
insert ignore into wfimatured.loadingtable
select distinct
upper('I') as actflag,
'2006/01/26' as acttime,
upper('drtype') as typegroup,
sectygrp.sectycd as code,
sectygrp.securitydescriptor as lookup
from sectygrp
where secgrpid = 2;
drop table if exists wfimatured.sys_lookup;
rename table wfimatured.loadingtable to wfimatured.sys_lookup;

-- wfi Web Complete Flag
-- update wfi.tbl_opslog
-- set comment= null
-- order by wfi.tbl_opslog.acttime desc limit 1;

