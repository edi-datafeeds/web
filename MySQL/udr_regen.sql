drop table if exists udr.loadingtable;
use wca;
create table udr.loadingtable(
select distinct
dprcp.announcedate as created,
dprcp.acttime as changed,
dprcp.actflag,
dprcp.secid as dr_secid,
drscmst.uscode as dr_uscode,
case when drscmst.statusflag = 'I' then 'Inactive' else 'Active' end as dr_status,
issur.issuername,
dprcp.drtype as dr_type,
drscmst.isin as dr_isin,
drscmst.securitydesc as dr_security_description,
drscmst.sharesoutstanding as dr_shares_outstanding,
substring(drscexh.exchgcd,1,2) as dr_listing_country,
drscexh.exchgcd as dr_exchange,
drscexh.localcode as dr_symbol,
concat(ltrim(dprcp.drratio),':',ltrim(dprcp.usratio)) as ratio,
dprcp.unsecid as un_secid,
unscmst.isin as un_isin,
unscmst.securitydesc as un_security_description,
unscmst.sharesoutstanding as un_shares_outstanding,
substring(unscexh.exchgcd,1,2) as un_listing_country,
unscexh.exchgcd as un_exchgcd,
unscexh.localcode as un_symbol,
unscexh.exchgcd as un_exchange,
concat(ltrim(dprcp.drratio),':',ltrim(dprcp.usratio)) as dr_un_ratio,
case when dprcp.spnflag = 't' then 's' when dprcp.spnflag = 'f' then 'u' else '' end as sponsored,
case when (dprcp.otherdepbank is not null and rtrim(dprcp.otherdepbank)<> '')
     then concat(rtrim(dprcp.depbank),', ',rtrim(dprcp.otherdepbank))
     else rtrim(dprcp.depbank) end as depository_bank,
dprcp.levdesc as level_description,
issur.cntryofincorp as country_of_incorporation,
indus.indusname as industry_sector,
'dprcp|secid|dprcpnotes' as link_notes
from dprcp
left outer join scmst as drscmst on dprcp.secid = drscmst.secid
left outer join scmst as unscmst on dprcp.unsecid = unscmst.secid
left outer join issur on unscmst.issid = issur.issid
left outer join indus on issur.indusid = indus.indusid
left outer join scexh as drscexh on drscmst.secid = drscexh.secid
                          and drscmst.primaryexchgcd = drscexh.exchgcd
                          and 'D' <> drscexh.liststatus
left outer join scexh as unscexh on unscmst.secid = unscexh.secid
                          and unscmst.primaryexchgcd = unscexh.exchgcd
                          and 'D' <> unscexh.liststatus
where
drscmst.uscode is not null
and issur.cntryofincorp<>'us'
and ltrim(drscmst.uscode) <> ''
and dprcp.drtype <> 'cdr'
and dprcp.drtype <> 'edr'
and dprcp.drtype <> 'nvd'
and dprcp.drtype <> 'rdr'
and dprcp.actflag <> 'd'
);
alter table `udr`.loadingtable
add primary key (`dr_secid`),
add index `ix_uscode` (`dr_uscode`),
add index `ix_issuername` (`issuername`),
add index `ix_dr_isin` (`dr_isin`),
add index `ix_dr_symbol` (`dr_symbol`),
add index `ix_un_isin` (`un_isin`),
add index `ix_un_symbol` (`un_symbol`);
drop table if exists udr.refdata;
rename table udr.loadingtable to udr.refdata;
