drop table if exists wca2.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'announcement' as event,
concat(lpad(scexhid,7,'0'),ann.annid) as caref,
annid as eventid,
ann.announcedate as created,
ann.acttime as changed,
case when ann.actflag = 'U' then 'Updated'
     when ann.actflag = 'I' then 'Inserted'
     when ann.actflag = 'D' then 'Deleted'
     when ann.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
ann.notificationdate as notification_date,
ann.eventtype as related_event,
'Mandatory' as choice,
'ann|annid|annnotes' as link_notes,
scmst.issid,
scmst.secid
from ann
inner join scmst on ann.issid = scmst.issid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and ann.announcedate between '2010/07/01' and '2099/01/01' 
and ((notificationdate >= scexh.listdate and scexh.listdate is not null) or notificationdate>=scexh.announcedate or notificationdate is null)
and (notificationdate < scexh.acttime or scexh.liststatus<>'D' or notificationdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_announcement;
rename table wca2.evf_loadingtable to wca2.evf_announcement;
--
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'bankruptcy' as event,
concat(lpad(scexhid,7,'0'),bkrp.bkrpid) as caref,
bkrpid as eventid,
bkrp.announcedate as created,
bkrp.acttime as changed,
case when bkrp.actflag = 'U' then 'Updated'
     when bkrp.actflag = 'I' then 'Inserted'
     when bkrp.actflag = 'D' then 'Deleted'
     when bkrp.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
bkrp.notificationdate as notification_date,
bkrp.filingdate as filing_date,
'Mandatory' as choice,
'bkrp|bkrpid|bkrpnotes' as link_notes,
scmst.issid,
scmst.secid
from bkrp
inner join scmst on bkrp.issid = scmst.issid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and bkrp.announcedate between '2010/07/01' and '2099/01/01' 
and ((notificationdate >= scexh.listdate and scexh.listdate is not null) or notificationdate>=scexh.announcedate or notificationdate is null)
and (notificationdate < scexh.acttime or scexh.liststatus<>'D' or notificationdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_bankruptcy;
rename table wca2.evf_loadingtable to wca2.evf_bankruptcy;
--
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'class action' as event,
concat(lpad(scexhid,7,'0'),lawst.lawstid) as caref,
lawstid as eventid,
lawst.announcedate as created,
lawst.acttime as changed,
case when lawst.actflag = 'U' then 'Updated'
     when lawst.actflag = 'I' then 'Inserted'
     when lawst.actflag = 'D' then 'Deleted'
     when lawst.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
lawst.effectivedate as effective_date,
'Mandatory' as choice,
'lawst|lawstid|lawstnotes' as link_notes,
scmst.issid,
scmst.secid
from lawst
inner join scmst on lawst.issid = scmst.issid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and lawst.announcedate between '2010/07/01' and '2099/01/01' 
and ((effectivedate >= scexh.listdate and scexh.listdate is not null) or effectivedate>=scexh.announcedate or effectivedate is null)
and (effectivedate < scexh.acttime or scexh.liststatus<>'D' or effectivedate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_class_action;
rename table wca2.evf_loadingtable to wca2.evf_class_action;
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'company meeting' as event,
concat(lpad(scexhid,7,'0'),agm.agmid) as caref,
agmid as eventid,
agm.announcedate as created,
agm.acttime as changed,
case when agm.actflag = 'U' then 'Updated'
     when agm.actflag = 'I' then 'Inserted'
     when agm.actflag = 'D' then 'Deleted'
     when agm.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
agm.agmdate as meeting_date,
agm.agmegm as meeting_type,
agm.agmno as meeting_number,
agm.fyedate as fin_year_date,
case when replace(agm.agmtime,' ','')=':'
     then ''
     else agm.agmtime
     end as meeting_time,
agm.add1 as address_1,
agm.add2 as address_2,
agm.add3 as address_3,
agm.add4 as address_4,
agm.add5 as address_5,
agm.add6 as address_6,
agm.city,
case when agm.cntrycd is null then ''
     when cntry.country is null and agm.cntrycd <> ''
     then concat('[',agm.cntrycd,'] not found')
     else cntry.country
     end as country,
'Mandatory' as choice,
scmst.issid,
scmst.secid
from agm
inner join scmst on agm.issid = scmst.issid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
     and exchg.cntrycd=sedol.cntrycd
     and 'f' = sedol.defunct
     and 'd' <> sedol.actflag
left outer join cntry on agm.cntrycd = cntry.cntrycd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and agm.announcedate between '2010/07/01' and '2099/01/01'
and agm.agmegm<>'bhm'
and ((agmdate >= scexh.listdate and scexh.listdate is not null) or agmdate>=scexh.announcedate or agmdate is null)
and (agmdate < scexh.acttime or scexh.liststatus<>'D' or agmdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_company_meeting;
rename table wca2.evf_loadingtable to wca2.evf_company_meeting;
------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'currency redenomination' as event,
concat(lpad(scexhid,7,'0'),currd.currdid) as caref,
currdid as eventid,
currd.announcedate as created,
currd.acttime as changed,
case when currd.actflag = 'U' then 'Updated'
     when currd.actflag = 'I' then 'Inserted'
     when currd.actflag = 'D' then 'Deleted'
     when currd.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
currd.effectivedate as effective_date,
currd.oldcurencd as old_currency,
currd.newcurencd as new_currency,
currd.oldparvalue as old_par_value,
currd.newparvalue as new_par_value,
case when currd.eventtype is null then ''
     when event.eventname is null and currd.eventtype <> ''
     then concat('[',currd.eventtype,'] not found')
     else event.eventname
     end as related_event,
'Mandatory' as choice,
'currd|currdid|currdnotes' as link_notes,
scmst.issid,
scmst.secid
from currd
inner join scmst on currd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join event on currd.eventtype = event.eventtype
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where 
sectygrp.secgrpid<3
and currd.announcedate between '2010/07/01' and '2099/01/01'
and ((effectivedate >= scexh.listdate and scexh.listdate is not null) or effectivedate>=scexh.announcedate or effectivedate is null)
and (effectivedate < scexh.acttime or scexh.liststatus<>'D' or effectivedate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_currency_redenomination;
rename table wca2.evf_loadingtable to wca2.evf_currency_redenomination;
---------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'financial year change' as event,
concat(lpad(scexhid,7,'0'),fychg.fychgid) as caref,
fychgid as eventid,
fychg.announcedate as created,
fychg.acttime as changed,
case when fychg.actflag = 'U' then 'Updated'
     when fychg.actflag = 'I' then 'Inserted'
     when fychg.actflag = 'D' then 'Deleted'
     when fychg.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
fychg.notificationdate as notification_date,
fychg.oldfystartdate as old_fin_year_start_date,
fychg.oldfyenddate as old_fin_year_date,
fychg.newfystartdate as new_fin_year_start_date,
fychg.newfyenddate as new_fin_year_date,
'Mandatory' as choice,
scmst.issid,
scmst.secid
from fychg
inner join scmst on fychg.issid = scmst.issid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
            and exchg.cntrycd=sedol.cntrycd
            and 'f' = sedol.defunct
            and 'd' <> sedol.actflag
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and fychg.announcedate between '2010/07/01' and '2099/01/01'
and ((notificationdate >= scexh.listdate and scexh.listdate is not null) or notificationdate>=scexh.announcedate or notificationdate is null)
and (notificationdate < scexh.acttime or scexh.liststatus<>'D' or notificationdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_financial_year_change;
rename table wca2.evf_loadingtable to wca2.evf_financial_year_change;
-------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'incorporation change' as event,
concat(lpad(scexhid,7,'0'),inchg.inchgid) as caref,
inchgid as eventid,
inchg.announcedate as created,
inchg.acttime as changed,
case when inchg.actflag = 'U' then 'Updated'
     when inchg.actflag = 'I' then 'Inserted'
     when inchg.actflag = 'D' then 'Deleted'
     when inchg.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
inchg.inchgdate as effective_date,
case when inchg.oldcntrycd is null then ''
     when oldcntry.country is null and inchg.oldcntrycd <> ''
     then concat('[',inchg.oldcntrycd,'] not found')
     else oldcntry.country 
     end as old_country,
case when inchg.newcntrycd is null then ''
     when newcntry.country is null and inchg.newcntrycd <> ''
     then concat('[',inchg.newcntrycd,'] not found')
     else newcntry.country 
     end as new_country,
case when inchg.eventtype is null then ''
     when event.eventname is null and inchg.eventtype <> ''
     then concat('[',inchg.eventtype,'] not found')
     else event.eventname
     end as related_event,
'Mandatory' as choice,
scmst.issid,
scmst.secid
from inchg
inner join scmst on inchg.issid = scmst.issid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join event on inchg.eventtype = event.eventtype
left outer join cntry as oldcntry on inchg.oldcntrycd = oldcntry.cntrycd
left outer join cntry as newcntry on inchg.newcntrycd = newcntry.cntrycd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and inchg.announcedate between '2010/07/01' and '2099/01/01' 
and ((inchgdate >= scexh.listdate and scexh.listdate is not null) or inchgdate>=scexh.announcedate or inchgdate is null)
and (inchgdate < scexh.acttime or scexh.liststatus<>'D' or inchgdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_incorporation_change;
rename table wca2.evf_loadingtable to wca2.evf_incorporation_change;
--------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'international code change' as event,
concat(lpad(scexhid,7,'0'),icc.iccid) as caref,
iccid as eventid,
icc.announcedate as created,
icc.acttime as changed,
case when icc.actflag = 'U' then 'Updated'
     when icc.actflag = 'I' then 'Inserted'
     when icc.actflag = 'D' then 'Deleted'
     when icc.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
icc.effectivedate as effective_date,
icc.oldisin as old_isin,
icc.newisin as new_isin,
icc.olduscode as old_us_code,
icc.newuscode as new_us_code,
case when icc.eventtype is null then ''
     when event.eventname is null and icc.eventtype <> ''
     then concat('[',icc.eventtype,'] not found')
     else event.eventname
     end as related_event,
'Mandatory' as choice,
scmst.issid,
scmst.secid
from icc
inner join scmst on icc.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join event on icc.eventtype = event.eventtype
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and icc.announcedate between '2011/07/01' and '2099/01/01'
and ((effectivedate >= scexh.listdate and scexh.listdate is not null) or effectivedate>=scexh.announcedate or effectivedate is null)
and (effectivedate < scexh.acttime or scexh.liststatus<>'D' or effectivedate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_international_code_change;
rename table wca2.evf_loadingtable to wca2.evf_international_code_change;
--------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'local code change' as event,
concat(lpad(scexhid,7,'0'),lcc.lccid) as caref,
lccid as eventid,
lcc.announcedate as created,
lcc.acttime as changed,
case when lcc.actflag = 'U' then 'Updated'
     when lcc.actflag = 'I' then 'Inserted'
     when lcc.actflag = 'D' then 'Deleted'
     when lcc.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
case when lcc.eventtype is null then ''
     when event.eventname is null and lcc.eventtype <> ''
     then concat('[',lcc.eventtype,'] not found')
     else event.eventname
     end as related_event,
lcc.effectivedate as effective_date,
lcc.oldlocalcode as old_code,
lcc.newlocalcode as new_code,
'Mandatory' as choice,
scmst.issid,
scmst.secid
from lcc
inner join scexh on lcc.secid = scexh.secid and lcc.exchgcd=scexh.exchgcd
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid 
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join event on lcc.eventtype = event.eventtype
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and lcc.announcedate between '2011/07/01' and '2099/01/01' 
and ((lcc.announcedate >= scexh.listdate and scexh.listdate is not null) or lcc.announcedate>=scexh.announcedate)
and (lcc.announcedate < scexh.acttime or scexh.liststatus<>'D')
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_local_code_change;
rename table wca2.evf_loadingtable to wca2.evf_local_code_change;
----------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'lot change' as event,
concat(lpad(scexhid,7,'0'),ltchg.ltchgid) as caref,
ltchgid as eventid,
ltchg.announcedate as created,
ltchg.acttime as changed,
case when ltchg.actflag = 'U' then 'Updated'
     when ltchg.actflag = 'I' then 'Inserted'
     when ltchg.actflag = 'D' then 'Deleted'
     when ltchg.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
ltchg.effectivedate as effective_date,
ltchg.oldlot as old_lot_size,
ltchg.newlot as new_lot_size,
ltchg.oldmintrdqty as old_min_trading_quant,
ltchg.newmintrdgqty as new_min_trading_quant,
'Mandatory' as choice,
scmst.issid,
scmst.secid
from ltchg
inner join scexh on ltchg.secid = scexh.secid and ltchg.exchgcd=scexh.exchgcd
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and ltchg.announcedate between '2010/07/01' and '2099/01/01' 
and ((ltchg.announcedate >= scexh.listdate and scexh.listdate is not null) or ltchg.announcedate>=scexh.announcedate)
and (ltchg.announcedate < scexh.acttime or scexh.liststatus<>'D')
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_lot_change;
rename table wca2.evf_loadingtable to wca2.evf_lot_change;
--
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'preference redemption' as event,
concat(lpad(scexhid,7,'0'),redem.redemid) as caref,
redemid as eventid,
redem.announcedate as created,
redem.acttime as changed,
case when redem.actflag = 'U' then 'Updated'
     when redem.actflag = 'I' then 'Inserted'
     when redem.actflag = 'D' then 'Deleted'
     when redem.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
'price' as rate_type,
redem.redemprice as rate,
redem.curencd as currency,
redem.redemdate as redemption_date,
case when redem.partfinal='p' then 'Part'
     when redem.partfinal='f' then 'Final'
     else ''
     end as part_final,
case when redem.mandoptflag = 'm' then 'Mandatory'
     when redem.mandoptflag = 'v' then 'Voluntary'
     else '' end as choice,
'redem|redemid|redemnotes' as link_notes,
scmst.issid,
scmst.secid
from redem
inner join scmst on redem.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where 
sectygrp.secgrpid<3
and redem.announcedate between '2010/07/01' and '2099/01/01'
and ((redemdate >= scexh.listdate and scexh.listdate is not null) or redemdate>=scexh.announcedate or redemdate is null)
and (redemdate < scexh.acttime or scexh.liststatus<>'D' or redemdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_preference_redemption;
rename table wca2.evf_loadingtable to wca2.evf_preference_redemption;
--------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'return of capital' as event,
concat(lpad(scexhid,7,'0'),rcap.rcapid) as caref,
rcapid as eventid,
rcap.announcedate as created,
case when rd.acttime > rcap.acttime then rd.acttime
     else rcap.acttime
     end as changed,
case when rcap.actflag = 'U' then 'Updated'
     when rcap.actflag = 'I' then 'Inserted'
     when rcap.actflag = 'D' then 'Deleted'
     when rcap.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
'cashback' as rate_type,
rcap.cashbak as rate,
rcap.curencd as currency,
rcap.effectivedate as effective_date,
rcap.cspydate as pay_date,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'rcap|rcapid|rcapnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from rcap
inner join scmst on rcap.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join rd on rcap.rdid = rd.rdid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and rcap.announcedate between '2010/07/01' and '2099/01/01'
and ((effectivedate >= scexh.listdate and scexh.listdate is not null) or effectivedate>=scexh.announcedate or effectivedate is null)
and (effectivedate < scexh.acttime or scexh.liststatus<>'D' or effectivedate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_return_of_capital;
rename table wca2.evf_loadingtable to wca2.evf_return_of_capital;
-----------------------------------------------------------------------------------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'security_description_change' as event,
concat(lpad(scexhid,7,'0'),scchg.scchgid) as caref,
scchgid as eventid,
scchg.announcedate as created,
scchg.acttime as changed,
case when scchg.actflag = 'U' then 'Updated'
     when scchg.actflag = 'I' then 'Inserted'
     when scchg.actflag = 'D' then 'Deleted'
     when scchg.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
scchg.dateofchange as effective_date,
case when scchg.eventtype is null then ''
     when event.eventname is null and scchg.eventtype <> ''
     then concat('[',scchg.eventtype,'] not found')
     else event.eventname
     end as related_event,
scchg.secoldname as old_name,
scchg.secnewname as new_name,
'Mandatory' as choice,
'scchg|scchgid|scchgnotes' as link_notes,
scmst.issid,
scmst.secid
from scchg
inner join scmst on scchg.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join event on scchg.eventtype = event.eventtype
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and scchg.announcedate between '2010/07/01' and '2099/01/01'
and ((dateofchange >= scexh.listdate and scexh.listdate is not null) or dateofchange>=scexh.announcedate or dateofchange is null)
and (dateofchange < scexh.acttime or scexh.liststatus<>'D' or dateofchange is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_security_description_change;
rename table wca2.evf_loadingtable to wca2.evf_security_description_change;
----------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'depository receipt change' as event,
concat(lpad(scexhid,7,'0'),drchg.drchgid) as caref,
drchg.drchgid as eventid,
drchg.announcedate as created,
drchg.acttime as changed,
case when drchg.actflag = 'U' then 'Updated'
     when drchg.actflag = 'I' then 'Inserted'
     when drchg.actflag = 'D' then 'Deleted'
     when drchg.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
drchg.effectivedate as effective_date,
case when drchg.olddrratio is null or drchg.olddrratio = ''
     then ''
     else concat(drchg.olddrratio,':',drchg.oldunratio)
     end as old_ratio,
case when drchg.newdrratio is null or drchg.newdrratio = ''
     then ''
     else concat(drchg.newdrratio,':',drchg.newunratio)
     end as new_ratio,
drchg.oldunsecid as old_un_secid,
drchg.newunsecid as new_un_secid,
case when drchg.eventtype is null then ''
     when event.eventname is null and drchg.eventtype <> ''
     then concat('[',drchg.eventtype,'] not found')
     else event.eventname
     end as related_event,
drchg.olddepbank as old_depository_bank,
drchg.newdepbank as new_depository_bank,
drchg.olddrtype as old_dr_type,
drchg.newdrtype as new_dr_type,
drchg.oldlevel as old_level,
drchg.newlevel as new_level,
'drchg|drchgid|drchgnotes' as link_notes,
' ' as choice,
scmst.issid,
scmst.secid
from drchg
inner join scmst on drchg.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join event on drchg.eventtype = event.eventtype
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and drchg.announcedate between '2010/07/01' and '2099/01/01'
and ((effectivedate >= scexh.listdate and scexh.listdate is not null) or effectivedate>=scexh.announcedate or effectivedate is null)
and (effectivedate < scexh.acttime or scexh.liststatus<>'D' or effectivedate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_depository_receipt_change;
rename table wca2.evf_loadingtable to wca2.evf_depository_receipt_change;
-----------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'call' as event,
concat(lpad(scexhid,7,'0'),call_my.callid) as caref,
callid as eventid,
call_my.announcedate as created,
case when rd.acttime > call_my.acttime then rd.acttime
     else call_my.acttime
     end as changed,
case when call_my.actflag = 'U' then 'Updated'
     when call_my.actflag = 'I' then 'Inserted'
     when call_my.actflag = 'D' then 'Deleted'
     when call_my.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
call_my.callnumber as call_number,
call_my.curencd as currency,
call_my.tofacevalue as apportionment_to_face_value,
call_my.topremium as apportionment_to_share_premium,
call_my.duedate as due_date,
'Voluntary' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'call|callid|callnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from call_my
inner join scmst on call_my.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join rd on call_my.rdid = rd.rdid
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and call_my.announcedate between '2010/07/01' and '2099/01/01'
and ((duedate >= scexh.listdate and scexh.listdate is not null) or duedate>=scexh.announcedate or duedate is null)
and (duedate < scexh.acttime or scexh.liststatus<>'D' or duedate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_call;
rename table wca2.evf_loadingtable to wca2.evf_call;
-------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'parvalue redenomination' as event,
concat(lpad(scexhid,7,'0'),pvrd.pvrdid) as caref,
pvrdid as eventid,
pvrd.announcedate as created,
pvrd.acttime as changed,
case when pvrd.actflag = 'U' then 'Updated'
     when pvrd.actflag = 'I' then 'Inserted'
     when pvrd.actflag = 'D' then 'Deleted'
     when pvrd.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
case when pvrd.eventtype is null then ''
     when event.eventname is null and pvrd.eventtype <> ''
     then concat('[',pvrd.eventtype,'] not found')
     else event.eventname
     end as related_event,
pvrd.effectivedate as effective_date,
pvrd.curencd as currency,
pvrd.oldparvalue as old_par_value,
pvrd.newparvalue as new_par_value,
'Mandatory' as choice,
'pvrd|pvrdid|pvrdnotes' as link_notes,
scmst.issid,
scmst.secid
from pvrd
inner join scmst on pvrd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join event on pvrd.eventtype = event.eventtype
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where 
sectygrp.secgrpid<3
and pvrd.announcedate between '2010/07/01' and '2099/01/01'
and ((effectivedate >= scexh.listdate and scexh.listdate is not null) or effectivedate>=scexh.announcedate or effectivedate is null)
and (effectivedate < scexh.acttime or scexh.liststatus<>'D' or effectivedate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_parvalue_redenomination;
rename table wca2.evf_loadingtable to wca2.evf_parvalue_redenomination;
---------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'arrangement' as event,
concat(lpad(scexhid,7,'0'),rd.rdid) as caref,
arr.rdid as eventid,
arr.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > arr.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > arr.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > arr.acttime) then pexdt.acttime
     else arr.acttime
     end as changed,
case when arr.actflag = 'U' then 'Updated'
     when arr.actflag = 'I' then 'Inserted'
     when arr.actflag = 'D' then 'Deleted'
     when arr.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
case when (icc.releventid is null or icc.oldisin = '') then isin
     else icc.oldisin
     end as isin,
case when (icc.releventid is null or icc.olduscode = '') then uscode
     else icc.olduscode
     end as us_code,
case when (sdchg.releventid is not null and sdchg.oldsedol <> '') then sdchg.oldsedol
     when sedol.secid is not null then sedol
     else ''
     end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
case when (lcc.releventid is null or lcc.oldlocalcode = '') then localcode
     else lcc.oldlocalcode
     end as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
' ' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'arr|rdid|arrnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from arr
inner join rd on arr.rdid = rd.secid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'ARR' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'ARR' = pexdt.eventtype
left outer join icc on arr.rdid = icc.releventid and 'ARR' = icc.eventtype
       and icc.actflag<>'d'
left outer join sdchg on arr.rdid = sdchg.releventid and 'ARR' = sdchg.eventtype
       and scexh.secid = sdchg.secid
       and exchg.cntrycd = sdchg.cntrycd
       and sedol.rcntrycd = sdchg.rcntrycd
       and sdchg.actflag<>'d'
left outer join lcc on arr.rdid = lcc.releventid and 'ARR' = lcc.eventtype
       and scexh.exchgcd = lcc.exchgcd
       and lcc.actflag<>'d'
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and arr.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
and (sdchg.secid is null or sedol=newsedol)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_arrangement;
rename table wca2.evf_loadingtable to wca2.evf_arrangement;
-------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'assimilation' as event,
concat(lpad(scexhid,7,'0'),assm.assmid) as caref,
assmid as eventid,
assm.announcedate as created,
assm.acttime as changed,
case when assm.actflag = 'U' then 'Updated'
     when assm.actflag = 'I' then 'Inserted'
     when assm.actflag = 'D' then 'Deleted'
     when assm.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
assm.assimilationdate as assimilation_date,
case when assm.sectycd is null then ''
     when secty.securitydescriptor is null and assm.sectycd <> ''
     then concat('[',assm.sectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
assm.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
'Mandatory' as choice,
scmst.issid,
scmst.secid
from assm
inner join scexh on assm.secid = scexh.secid and assm.exchgcd=scexh.exchgcd
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join scmst as resscmst on assm.ressecid = resscmst.secid
left outer join sedol as ressedol on assm.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join secty on assm.sectycd = secty.sectycd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and assm.announcedate between '2010/07/01' and '2099/01/01'
and ((assimilationdate >= scexh.listdate and scexh.listdate is not null) or assimilationdate>=scexh.announcedate or assimilationdate is null)
and (assimilationdate < scexh.acttime or scexh.liststatus<>'D' or assimilationdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_assimilation;
rename table wca2.evf_loadingtable to wca2.evf_assimilation;
------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'bonus' as event,
concat(lpad(scexhid,7,'0'),rd.rdid) as caref,
bon.rdid as eventid,
bon.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > bon.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > bon.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > bon.acttime) then pexdt.acttime
     else bon.acttime
     end as changed,
case when bon.actflag = 'U' then 'Updated'
     when bon.actflag = 'I' then 'Inserted'
     when bon.actflag = 'D' then 'Deleted'
     when bon.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
case when bon.rationew is null or bon.rationew = ''
     then ''
     else concat(bon.rationew,':',bon.ratioold)
     end as ratio,
case when bon.fractions is null then ''
     when irfractions.lookup is null and bon.fractions <> ''
     then concat('[',bon.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
bon.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
case when bon.sectycd is null then ''
     when secty.securitydescriptor is null and bon.sectycd <> ''
     then concat('[',bon.sectycd,'] not found')
     else secty.securitydescriptor
     end as offered_security_type,
bon.lapsedpremium as lapsed_entitlement_quant,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'bon|rdid|bonnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from bon
inner join rd on bon.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol. actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'BON' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'BON' = pexdt.eventtype
left outer join scmst as resscmst on bon.ressecid = resscmst.secid
left outer join sedol as ressedol on bon.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join irfractions on bon.fractions = irfractions.code
left outer join secty on bon.sectycd = secty.sectycd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and bon.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_bonus;
rename table wca2.evf_loadingtable to wca2.evf_bonus;
------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'bonus rights' as event,
concat(lpad(scexhid,7,'0'),rd.rdid) as caref,
br.rdid as eventid,
br.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > br.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > br.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > br.acttime) then pexdt.acttime
     else br.acttime
     end as changed,
case when br.actflag = 'U' then 'Updated'
     when br.actflag = 'I' then 'Inserted'
     when br.actflag = 'D' then 'Deleted'
     when br.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
case when br.rationew is null or br.rationew = ''
     then ''
     else concat(br.rationew,':',br.ratioold)
     end as ratio,
'issueprice' as rate_type,
br.issueprice as rate,
br.curencd as currency,
case when br.sectycd is null then ''
     when secty.securitydescriptor is null and br.sectycd <> ''
     then concat('[',br.sectycd,'] not found')
     else secty.securitydescriptor
     end as offered_security_type,
case when br.fractions is null then ''
     when irfractions.lookup is null and br.fractions <> ''
     then concat('[',br.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
br.startsubscription as subscription_period_start,
br.endsubscription as subscription_period_end,
br.splitdate as rights_split_date,
br.starttrade as rights_trade_begin,
br.endtrade as rights_trade_end,
br.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
br.trasecid as tradeable_secid,
trascmst.isin as tradeable_isin,
case when br.oversubscription = 't' then 'yes'
     else 'no'
     end as over_subscription,
' ' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'br|rdid|brnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from br
inner join rd on br.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'BR' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'BR' = pexdt.eventtype
left outer join scmst as resscmst on br.ressecid = resscmst.secid
left outer join scmst as trascmst on br.trasecid = trascmst.secid
left outer join sedol as ressedol on br.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join irfractions on br.fractions = irfractions.code
left outer join secty on br.sectycd = secty.sectycd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and br.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_bonus_rights;
rename table wca2.evf_loadingtable to wca2.evf_bonus_rights;
---------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'buy back' as event,
case when optionid is not null 
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),lpad(serialid,2,'0'),bb.bbid)
     else concat(lpad(scexhid,7,'0'),'0101',bb.bbid) 
     end as caref,
bbid as eventid,
bb.announcedate as created,
case when (mpay.acttime is not null) and (mpay.acttime > bb.acttime) and (mpay.acttime > rd.acttime) then mpay.acttime
     when rd.acttime > bb.acttime then rd.acttime
     else bb.acttime
     end as changed,
case when bb.actflag = 'U' then 'Updated'
     when bb.actflag = 'I' then 'Inserted'
     when bb.actflag = 'D' then 'Deleted'
     when bb.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when mpay.actflag = 'U' then 'Updated'
     when mpay.actflag = 'I' then 'Inserted'
     when mpay.actflag = 'D' then 'Deleted'
     when mpay.actflag = 'C' then 'Cancelled'
     else '' end as Pay_actflag,
case when optionid is not null 
     then concat(lpad(optionid,2,'0'),',',lpad(serialid,2,'0'))
     else ''
     end as option_key,
case when mpay.paytype is null then ''
     when irpaytype.lookup is null and mpay.actflag <> ''
     then concat('[',mpay.actflag,'] not found')
     else irpaytype.lookup
     end as pay_type,
case when mpay.curencd is null then ''
     else mpay.curencd
     end as currency,
case when mpay.rationew is null or mpay.rationew = ''
     then ''
     else concat(mpay.rationew,':',mpay.ratioold)
     end as ratio,
'minmaxprice' as rate_type,
case when mpay.maxprice is null or mpay.maxprice = ''
     then ''
     else concat(mpay.maxprice,':',mpay.minprice)
     end as rate,
mpay.minqlyqty as min_qualifying_quant,
mpay.maxqlyqty as max_qualifying_quant,
mpay.paydate as pay_date,
mpay.minofrqty as min_offer_quant,
mpay.maxofrqty as max_offer_quant,
mpay.tndrstrkprice as tender_strike_price,
mpay.tndrstrkstep as tender_strike_step,
case when bb.onoffflag is null then ''
     when ironoffmarket.lookup is null and bb.onoffflag <> ''
     then concat('[',bb.onoffflag,'] not found')
     else ironoffmarket.lookup
     end as on_off_market,
bb.startdate as start_date,
bb.enddate as end_date,
bb.minacpqty as min_acceptance_quant,
bb.maxacpqty as max_acceptance_quant,
bb.bbminpct as min_percent,
bb.bbmaxpct as max_percent,
'Voluntary' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'bb|bbid|bbnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from bb
inner join scmst on bb.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join rd on bb.rdid = rd.rdid
left outer join mpay on bb.bbid = mpay.eventid and 'BB' = mpay.sevent
left outer join ironoffmarket on bb.onoffflag = ironoffmarket.code
left outer join irpaytype on mpay.paytype = irpaytype.code
left outer join iraction as iractionmpay on mpay.actflag = iractionmpay.code
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and bb.announcedate between '2010/07/01' and '2099/01/01'
and ((enddate >= scexh.listdate and scexh.listdate is not null) or enddate>=scexh.announcedate or enddate is null)
and (enddate < scexh.acttime or scexh.liststatus<>'D' or enddate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_buy_back;
rename table wca2.evf_loadingtable to wca2.evf_buy_back;
-----------------------------------------------------------------------------------------------------
create table wca2.evf_loadingtable(
select distinct
'capital reduction' as event,
concat(lpad(scexhid,7,'0'),caprd.caprdid) as caref,
caprdid as eventid,
caprd.announcedate as created,
case when rd.acttime > caprd.acttime then rd.acttime
     else caprd.acttime
     end as changed,
case when caprd.actflag = 'U' then 'Updated'
     when caprd.actflag = 'I' then 'Inserted'
     when caprd.actflag = 'D' then 'Deleted'
     when caprd.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
case when (icc.releventid is null or icc.oldisin = '') then isin
     else icc.oldisin
     end as isin,
case when (icc.releventid is null or icc.olduscode = '') then uscode
     else icc.olduscode
     end as us_code,
case when (sdchg.releventid is not null and sdchg.oldsedol <> '') then sdchg.oldsedol
     when sedol.secid is not null then sedol
     else ''
     end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
case when (lcc.releventid is null or lcc.oldlocalcode = '') then localcode
     else lcc.oldlocalcode
     end as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when caprd.fractions is null then ''
     when irfractions.lookup is null and caprd.fractions <> ''
     then concat('[',caprd.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
caprd.effectivedate as effective_date,
case when caprd.newratio is null or caprd.newratio = ''
     then ''
     else concat(caprd.newratio,':',caprd.oldratio)
     end as ratio,
caprd.paydate as pay_date,
caprd.oldparvalue as old_par_value,
caprd.newparvalue as new_par_value,
icc.newisin as new_isin,
icc.newuscode as new_us_code,
sdchg.newsedol as new_sedol,
lcc.newlocalcode as new_local_code,
case when (icc.acttime is not null) and (icc.acttime > sdchg.acttime) then icc.acttime
     else sdchg.acttime
     end as new_code_date,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'caprd|caprdid|caprdnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from caprd
inner join scmst on caprd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join rd on caprd.rdid = rd.rdid
left outer join sedol on scmst.secid = sedol.secid
       and exchg.cntrycd=sedol.cntrycd
       and 'f' = sedol.defunct
       and 'd' <> sedol.actflag
left outer join icc on caprd.caprdid = icc.releventid and 'CAPRD' = icc.eventtype
       and icc.actflag<>'d'
left outer join sdchg on caprd.caprdid = sdchg.releventid and 'CAPRD' = sdchg.eventtype
       and scexh.secid = sdchg.secid
       and exchg.cntrycd = sdchg.cntrycd
       and sedol.rcntrycd = sdchg.rcntrycd
       and sdchg.actflag<>'d'
left outer join lcc on caprd.caprdid = lcc.releventid and 'CAPRD' = lcc.eventtype
       and scexh.exchgcd = lcc.exchgcd
       and lcc.actflag<>'d'
left outer join irfractions on caprd.fractions = irfractions.code
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and caprd.announcedate between '2010/07/01' and '2099/01/01'
and ((caprd.effectivedate >= scexh.listdate and scexh.listdate is not null) or caprd.effectivedate>=scexh.announcedate or caprd.effectivedate is null)
and (caprd.effectivedate < scexh.acttime or scexh.liststatus<>'D' or caprd.effectivedate is null)
and (sdchg.secid is null or sedol=newsedol)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_capital_reduction;
rename table wca2.evf_loadingtable to wca2.evf_capital_reduction;
----------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'certificate exchange' as event,
concat(lpad(scexhid,7,'0'),ctx.ctxid) as caref,
ctxid as eventid,
ctx.announcedate as created,
ctx.acttime as changed,
case when ctx.actflag = 'U' then 'Updated'
     when ctx.actflag = 'I' then 'Inserted'
     when ctx.actflag = 'D' then 'Deleted'
     when ctx.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
ctx.startdate as start_date,
ctx.enddate as end_date,
case when ctx.eventtype is null then ''
     when event.eventname is null and ctx.eventtype <> ''
     then concat('[',ctx.eventtype,'] not found')
     else event.eventname
     end as related_event,
ctx.ressecid as resultant_sec_id,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
case when ctx.sectycd is null then ''
     when secty.securitydescriptor is null and ctx.sectycd <> ''
     then concat('[',ctx.sectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
'Mandatory' as choice,
'ctx|ctxid|ctxnotes' as link_notes,
scmst.issid,
scmst.secid
from ctx
inner join scmst on ctx.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join scmst as resscmst on ctx.ressecid = resscmst.secid
left outer join sedol as ressedol on ctx.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join secty on ctx.sectycd = secty.sectycd
left outer join event on ctx.eventtype = event.eventtype
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and ctx.announcedate between '2010/07/01' and '2099/01/01'
and ((startdate >= scexh.listdate and scexh.listdate is not null) or startdate>=scexh.announcedate or startdate is null)
and (startdate < scexh.acttime or scexh.liststatus<>'D' or startdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_certificate_exchange;
rename table wca2.evf_loadingtable to wca2.evf_certificate_exchange;
---------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'consolidation' as event,
concat(lpad(scexhid,7,'0'),rd.rdid) as caref,
consd.rdid as eventid,
consd.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > consd.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > consd.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > consd.acttime) then pexdt.acttime
     else consd.acttime
     end as changed,
case when consd.actflag = 'U' then 'Updated'
     when consd.actflag = 'I' then 'Inserted'
     when consd.actflag = 'D' then 'Deleted'
     when consd.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
case when (icc.releventid is null or icc.oldisin = '') then isin
     else icc.oldisin
     end as isin,
case when (icc.releventid is null or icc.olduscode = '') then uscode
     else icc.olduscode
     end as us_code,
case when (sdchg.releventid is not null and sdchg.oldsedol <> '') then sdchg.oldsedol
     when sedol.secid is not null then sedol
     else ''
     end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
case when (lcc.releventid is null or lcc.oldlocalcode = '') then localcode
     else lcc.oldlocalcode
     end as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
consd.curencd as consolidation_currency,
consd.oldparvalue as old_par_value,
consd.newparvalue as new_par_value,
case when consd.newratio is null or consd.newratio = ''
     then ''
     else concat(consd.newratio,':',consd.oldratio)
     end as ratio,
case when consd.fractions is null then ''
     when irfractions.lookup is null and consd.fractions <> ''
     then concat('[',consd.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
icc.newisin as new_isin,
icc.newuscode as new_uscode,
sdchg.newsedol as new_sedol,
lcc.newlocalcode as new_local_code,
case when (icc.acttime is not null) and (icc.acttime > sdchg.acttime) then icc.acttime
     else sdchg.acttime
     end as new_code_date,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'consd|rdid|consdnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from consd
inner join rd on consd.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'CONSD' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'consd' = pexdt.eventtype
left outer join icc on rd.rdid = icc.releventid and 'CONSD' = icc.eventtype
       and icc.actflag<>'d'
left outer join sdchg on rd.rdid = sdchg.releventid and 'CONSD' = sdchg.eventtype
       and scmst.secid = sdchg.secid
       and exchg.cntrycd = sdchg.cntrycd
       and sedol.rcntrycd = sdchg.rcntrycd
       and sdchg.actflag<>'d'
left outer join lcc on rd.rdid = lcc.releventid and 'CONSD' = lcc.eventtype
       and scexh.exchgcd = lcc.exchgcd
       and lcc.actflag<>'d'
left outer join irfractions on consd.fractions = irfractions.code
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and consd.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
and (sdchg.secid is null or sedol=newsedol)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_consolidation;
rename table wca2.evf_loadingtable to wca2.evf_consolidation;
-----------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'demerger' as event,
case when optionid is not null 
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),lpad(serialid,2,'0'),rd.rdid)
     else concat(lpad(scexhid,7,'0'),'0101',rd.rdid) 
     end as caref,
dmrgr.rdid as eventid,
dmrgr.announcedate as created,
case when (mpay.acttime is not null) and (mpay.acttime > dmrgr.acttime) and (mpay.acttime > rd.acttime) and (mpay.acttime > exdt.acttime or exdt.acttime is null) and (mpay.acttime > pexdt.acttime or pexdt.acttime is null) then mpay.acttime
     when (rd.acttime is not null) and (rd.acttime > dmrgr.acttime) and (rd.acttime > exdt.acttime or exdt.acttime is null) and (rd.acttime > pexdt.acttime or pexdt.acttime is null) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > dmrgr.acttime) and (exdt.acttime > pexdt.acttime or pexdt.acttime is null) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > dmrgr.acttime) then pexdt.acttime
     else dmrgr.acttime
     end as changed,
case when dmrgr.actflag = 'U' then 'Updated'
     when dmrgr.actflag = 'I' then 'Inserted'
     when dmrgr.actflag = 'D' then 'Deleted'
     when dmrgr.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
case when mpay.actflag = 'U' then 'Updated'
     when mpay.actflag = 'I' then 'Inserted'
     when mpay.actflag = 'D' then 'Deleted'
     when mpay.actflag = 'C' then 'Cancelled'
     else '' end as Pay_actflag,
case when optionid is not null 
     then concat(lpad(optionid,2,'0'),',',lpad(serialid,2,'0'))
     else ''
     end as option_key,
case when mpay.paytype is null then ''
     when irpaytype.lookup is null and mpay.actflag <> ''
     then concat('[',mpay.actflag,'] not found')
     else irpaytype.lookup
     end as pay_type,
case when mpay.rationew is null or mpay.rationew = ''
     then ''
     else concat(mpay.rationew,':',mpay.ratioold)
     end as ratio,
case when mpay.fractions is null then ''
     when irfractions.lookup is null and mpay.fractions <> ''
     then concat('[',mpay.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
mpay.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
case when mpay.sectycd is null then ''
     when secty.securitydescriptor is null and mpay.sectycd <> ''
     then concat('[',mpay.sectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
dmrgr.effectivedate as effective_date,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'dmrgr|rdid|dmrgrnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from dmrgr
inner join rd on dmrgr.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'DMRGR' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'DMRGR' = pexdt.eventtype
left outer join mpay on dmrgr.rdid = mpay.eventid and 'DMRGR' = mpay.sevent
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
left outer join sedol as ressedol on mpay.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join secty on mpay.sectycd = secty.sectycd
left outer join irfractions on mpay.fractions = irfractions.code
left outer join irpaytype on mpay.paytype = irpaytype.code
left outer join iraction as iractionmpay on mpay.actflag = iractionmpay.code
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and dmrgr.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_demerger;
rename table wca2.evf_loadingtable to wca2.evf_demerger;
---------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'distribution' as event,
case when optionid is not null 
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),lpad(serialid,2,'0'),rd.rdid)
     else concat(lpad(scexhid,7,'0'),'0101',rd.rdid) 
     end as caref,
dist.rdid as eventid,
dist.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > dist.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > dist.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > dist.acttime) then pexdt.acttime
     else dist.acttime
     end as changed,
case when dist.actflag = 'U' then 'Updated'
     when dist.actflag = 'I' then 'Inserted'
     when dist.actflag = 'D' then 'Deleted'
     when dist.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
case when mpay.actflag = 'U' then 'Updated'
     when mpay.actflag = 'I' then 'Inserted'
     when mpay.actflag = 'D' then 'Deleted'
     when mpay.actflag = 'C' then 'Cancelled'
     else '' end as Pay_actflag,
case when optionid is not null 
     then concat(lpad(optionid,2,'0'),',',lpad(serialid,2,'0'))
     else ''
     end as option_key,
case when mpay.paytype is null then ''
     when irpaytype.lookup is null and mpay.actflag <> ''
     then concat('[',mpay.actflag,'] not found')
     else irpaytype.lookup
     end as pay_type,
case when mpay.sectycd is null then ''
     when secty.securitydescriptor is null and mpay.sectycd <> ''
     then concat('[',mpay.sectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
case when mpay.rationew is null or mpay.rationew = ''
     then ''
     else concat(mpay.rationew,':',mpay.ratioold)
     end as ratio,
case when mpay.fractions is null then ''
     when irfractions.lookup is null and mpay.fractions <> ''
     then concat('[',mpay.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
mpay.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'dist|rdid|distnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from dist
inner join rd on dist.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'DIST' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'DIST' = pexdt.eventtype
left outer join mpay on dist.rdid = mpay.eventid and 'DIST' = mpay.sevent
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
left outer join sedol as ressedol on mpay.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join secty on mpay.sectycd = secty.sectycd
left outer join irfractions on mpay.fractions = irfractions.code
left outer join irpaytype on mpay.paytype = irpaytype.code
left outer join iraction as iractionmpay on mpay.actflag = iractionmpay.code
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and dist.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_distribution;
rename table wca2.evf_loadingtable to wca2.evf_distribution;
--------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'divestment' as event,
concat(lpad(scexhid,7,'0'),rd.rdid) as caref,
dvst.rdid as eventid,
dvst.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > dvst.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > dvst.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > dvst.acttime) then pexdt.acttime
     else dvst.acttime
     end as changed,
case when dvst.actflag = 'U' then 'Updated'
     when dvst.actflag = 'I' then 'Inserted'
     when dvst.actflag = 'D' then 'Deleted'
     when dvst.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
case when dvst.rationew is null or dvst.rationew = ''
     then ''
     else concat(dvst.rationew,':',dvst.ratioold)
     end as ratio,
'min:maxprice' as rate_type,
case when dvst.maxprice is null or dvst.maxprice = ''
     then ''
     else concat(dvst.maxprice,':',dvst.minprice)
     end as rate,
dvst.curencd as currency,
case when dvst.fractions is null then ''
     when irfractions.lookup is null and dvst.fractions <> ''
     then concat('[',dvst.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
dvst.startsubscription as subscription_period_start,
dvst.endsubscription as subscription_period_end,
dvst.tndrstrkprice as tender_strike_price,
dvst.tndrpricestep as tender_price_step,
dvst.minqlyqty as min_qualifying_quant,
dvst.maxqlyqty as max_qualifying_quant,
dvst.minacpqty as min_acceptance_quant,
dvst.maxacpqty as max_acceptance_quant,
dvst.trasecid as tradeable_secid,
trascmst.isin as tradeable_isin,
dvst.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
case when dvst.sectycd is null then ''
     when secty.securitydescriptor is null and dvst.sectycd <> ''
     then concat('[',dvst.sectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'dvst|rdid|dvstnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from dvst
inner join rd on dvst.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'DVST' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'DVST' = pexdt.eventtype
left outer join scmst as resscmst on dvst.ressecid = resscmst.secid
left outer join scmst as trascmst on dvst.trasecid = trascmst.secid
left outer join sedol as ressedol on dvst.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join secty on dvst.sectycd = secty.sectycd
left outer join irfractions on dvst.fractions = irfractions.code
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and dvst.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_divestment;
rename table wca2.evf_loadingtable to wca2.evf_divestment;
---------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'entitlement' as event,
concat(lpad(scexhid,7,'0'),rd.rdid) as caref,
ent.rdid as eventid,
ent.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > ent.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > ent.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > ent.acttime) then pexdt.acttime
     else ent.acttime
     end as changed,
case when ent.actflag = 'U' then 'Updated'
     when ent.actflag = 'I' then 'Inserted'
     when ent.actflag = 'D' then 'Deleted'
     when ent.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
case when ent.sectycd is null then ''
     when secty.securitydescriptor is null and ent.sectycd <> ''
     then concat('[',ent.sectycd,'] not found')
     else secty.securitydescriptor
     end as offered_security_type,
case when ent.rationew is null or ent.rationew = ''
     then ''
     else concat(ent.rationew,':',ent.ratioold)
     end as ratio,
'issueprice' as rate_type,
ent.entissueprice as rate,
ent.curencd as currency,
case when ent.fractions is null then ''
     when irfractions.lookup is null and ent.fractions <> ''
     then concat('[',ent.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
ent.entissueprice as issue_price,
ent.startsubscription as subscription_period_opens,
ent.endsubscription as subscription_period_closes ,
ent.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
case when ent.oversubscription = 't' then 'yes'
     else 'no'
     end as over_subscription,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'ent|rdid|entnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from ent
inner join rd on ent.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'ENT' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'ENT' = pexdt.eventtype
left outer join scmst as resscmst on ent.ressecid = resscmst.secid
left outer join sedol as ressedol on ent.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join irfractions on ent.fractions = irfractions.code
left outer join secty on ent.sectycd = secty.sectycd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and ent.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_entitlement;
rename table wca2.evf_loadingtable to wca2.evf_entitlement;
---------------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'issuer name change' as event,
concat(lpad(scexhid,7,'0'),ischg.ischgid) as caref,
ischgid as eventid,
ischg.announcedate as created,
ischg.acttime as changed,
case when ischg.actflag = 'U' then 'Updated'
     when ischg.actflag = 'I' then 'Inserted'
     when ischg.actflag = 'D' then 'Deleted'
     when ischg.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
case when (icc.releventid is null or icc.oldisin = '') then isin
     else icc.oldisin
     end as isin,
case when (icc.releventid is null or icc.olduscode = '') then uscode
     else icc.olduscode
     end as us_code,
case when (sdchg.releventid is not null and sdchg.oldsedol <> '') then sdchg.oldsedol
     when sedol.secid is not null then sedol
     else ''
     end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
case when (lcc.releventid is null or lcc.oldlocalcode = '') then localcode
     else lcc.oldlocalcode
     end as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
case when ischg.eventtype is null then ''
     when event.eventname is null and ischg.eventtype <> ''
     then concat('[',ischg.eventtype,'] not found')
     else event.eventname
     end as related_event,
ischg.namechangedate as effective_date,
ischg.issoldname as old_name,
ischg.issnewname as new_name,
ischg.legalname as legal_name,
'Mandatory' as choice,
'ischg|ischgid|ischgnotes' as link_notes,
scmst.issid,
scmst.secid
from ischg
inner join scmst on ischg.issid = scmst.issid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
inner join sedol on scmst.secid = sedol.secid and exchg.cntrycd=sedol.cntrycd
left outer join icc on ischg.ischgid = icc.releventid and 'ISCHG' = icc.eventtype
       and icc.actflag<>'d'
       and scmst.secid = icc.secid
left outer join sdchg on ischg.ischgid = sdchg.releventid and 'ISCHG' = sdchg.eventtype
       and scmst.secid = sdchg.secid
       and exchg.cntrycd = sdchg.cntrycd
       and sedol.rcntrycd = sdchg.rcntrycd
       and sdchg.actflag<>'d'
left outer join lcc on ischg.ischgid = lcc.releventid and 'ISCHG' = lcc.eventtype
       and scexh.exchgcd = lcc.exchgcd
       and scmst.secid = lcc.secid
       and lcc.actflag<>'d'
left outer join event on ischg.eventtype = event.eventtype
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and ischg.announcedate between '2010/07/01' and '2099/01/01' 
and ((namechangedate >= scexh.listdate and scexh.listdate is not null) or namechangedate>=scexh.announcedate or namechangedate is null)
and (namechangedate < scexh.acttime or scexh.liststatus<>'D' or namechangedate is null)
and (sdchg.secid is null or sedol=newsedol)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_issuer_name_change;
rename table wca2.evf_loadingtable to wca2.evf_issuer_name_change;
-----------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'liquidation' as event,
case when optionid is not null 
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),lpad(serialid,2,'0'),liq.liqid)
     else concat(lpad(scexhid,7,'0'),'0101',liq.liqid) 
     end as caref,
liqid as eventid,
liq.announcedate as created,
case when (mpay.acttime is not null) and (mpay.acttime > liq.acttime) then mpay.acttime
     else liq.acttime
     end as changed,
case when liq.actflag = 'U' then 'Updated'
     when liq.actflag = 'I' then 'Inserted'
     when liq.actflag = 'D' then 'Deleted'
     when liq.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
case when mpay.actflag = 'U' then 'Updated'
     when mpay.actflag = 'I' then 'Inserted'
     when mpay.actflag = 'D' then 'Deleted'
     when mpay.actflag = 'C' then 'Cancelled'
     else '' end as Pay_actflag,
case when optionid is not null 
     then concat(lpad(optionid,2,'0'),',',lpad(serialid,2,'0'))
     else ''
     end as option_key,
case when mpay.paytype is null then ''
     when irpaytype.lookup is null and mpay.actflag <> ''
     then concat('[',mpay.actflag,'] not found')
     else irpaytype.lookup
     end as pay_type,
mpay.paydate as liquidation_date,
'price' as rate_type,
mpay.maxprice as rate,
case when mpay.curencd is null then ''
     else mpay.curencd
     end as currency,
liq.liquidator,
liq.liqadd1 as address_1,
liq.liqadd2 as address_2,
liq.liqadd3 as address_3,
liq.liqadd4 as address_4,
liq.liqadd5 as address_5,
liq.liqadd6 as address_6,
liq.liqcity as city,
case when liq.liqcntrycd is null then ''
     when cntry.country is null and liq.liqcntrycd <> ''
     then concat('[',liq.liqcntrycd,'] not found')
     else cntry.country
     end as country,
liq.liqtel as telephone,
liq.liqfax as fax,
liq.liqemail as email,
'Mandatory' as choice,
'liq|liqid|liquidationterms' as link_terms,
scmst.issid,
scmst.secid
from liq
inner join scmst on liq.issid = scmst.issid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join mpay on liq.liqid = mpay.eventid and 'LIQ' = mpay.sevent
left outer join irpaytype on mpay.paytype = irpaytype.code
left outer join cntry on liq.liqcntrycd = cntry.cntrycd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and liq.announcedate between '2010/07/01' and '2099/01/01'
and ((liq.announcedate >= scexh.listdate and scexh.listdate is not null) or liq.announcedate>=scexh.announcedate)
and (liq.announcedate < scexh.acttime or scexh.liststatus<>'D')
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_liquidation;
rename table wca2.evf_loadingtable to wca2.evf_liquidation;
--------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'listing status change' as event,
concat(lpad(scexhid,7,'0'),lstat.lstatid) as caref,
lstat.lstatid as eventid,
lstat.announcedate as created,
lstat.acttime as changed,
case when lstat.actflag = 'U' then 'Updated'
     when lstat.actflag = 'I' then 'Inserted'
     when lstat.actflag = 'D' then 'Deleted'
     when lstat.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
case when lstat.eventtype is null then ''
     when event.eventname is null and lstat.eventtype <> ''
     then concat('[',lstat.eventtype,'] not found')
     else event.eventname
     end as related_event,
lstat.lstatstatus as listing_status_change_to,
lstat.notificationdate as notification_date,
lstat.effectivedate as effective_date,
'Mandatory' as choice,
'lstat|lstatid|reason' as link_reason,
scmst.issid,
scmst.secid
from lstat
inner join scexh on lstat.secid = scexh.secid and lstat.exchgcd=scexh.exchgcd
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join event on lstat.eventtype = event.eventtype
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and lstat.announcedate between '2011/07/01' and '2099/01/01'
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_listing_status_change;
rename table wca2.evf_loadingtable to wca2.evf_listing_status_change;
------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'merger' as event,
case when optionid is not null 
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),lpad(serialid,2,'0'),rd.rdid)
     else concat(lpad(scexhid,7,'0'),'0101',rd.rdid) 
     end as caref,
mrgr.rdid as eventid,
mrgr.announcedate as created,
case when (mpay.acttime is not null) and (mpay.acttime > mrgr.acttime) and (mpay.acttime > rd.acttime) and (mpay.acttime > exdt.acttime) and (mpay.acttime > pexdt.acttime) then mpay.acttime
     when (rd.acttime is not null) and (rd.acttime > mrgr.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > mrgr.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > mrgr.acttime) then pexdt.acttime
     else mrgr.acttime
     end as changed,
case when mrgr.actflag = 'U' then 'Updated'
     when mrgr.actflag = 'I' then 'Inserted'
     when mrgr.actflag = 'D' then 'Deleted'
     when mrgr.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
case when mpay.actflag = 'U' then 'Updated'
     when mpay.actflag = 'I' then 'Inserted'
     when mpay.actflag = 'D' then 'Deleted'
     when mpay.actflag = 'C' then 'Cancelled'
     else '' end as Pay_actflag,
case when optionid is not null 
     then concat(lpad(optionid,2,'0'),',',lpad(serialid,2,'0'))
     else ''
     end as option_key,
case when mpay.paytype is null then ''
     when irpaytype.lookup is null and mpay.actflag <> ''
     then concat('[',mpay.actflag,'] not found')
     else irpaytype.lookup
     end as pay_type,
case when mpay.rationew is null or mpay.rationew = ''
     then ''
     else concat(mpay.rationew,':',mpay.ratioold)
     end as ratio,
case when mpay.fractions is null then ''
     when irfractions.lookup is null and mpay.fractions <> ''
     then concat('[',mpay.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
mpay.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
case when mpay.sectycd is null then ''
     when secty.securitydescriptor is null and mpay.sectycd <> ''
     then concat('[',mpay.sectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
mpay.maxprice as max_price,
mpay.minprice as min_price,
mrgr.effectivedate as effective_date,
mrgr.appointeddate as merge_date ,
case when mrgr.mrgrstatus is null then ''
     when irmrgrstat.lookup is null and mrgr.mrgrstatus <> ''
     then concat('[',mrgr.mrgrstatus,'] not found')
     else irmrgrstat.lookup
     end as merger_status,
'Mandatory' as choice,
'mrgr|rdid|companies' as link_companies,
'mrgr|rdid|approvalstatus' as link_approval_status,
'rd|rdid|rdnotes' as link_record_date_notes,
'mrgr|rdid|mrgrterms' as link_terms,
scmst.issid,
scmst.secid,
rd.rdid
from mrgr
inner join rd on mrgr.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'MRGR' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'MRGR' = pexdt.eventtype
left outer join mpay on mrgr.rdid = mpay.eventid and 'MRGR' = mpay.sevent
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
left outer join sedol as ressedol on mpay.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join secty on mpay.sectycd = secty.sectycd
left outer join irfractions on mpay.fractions = irfractions.code
left outer join irmrgrstat on mrgr.mrgrstatus = irmrgrstat.code
left outer join irpaytype on mpay.paytype = irpaytype.code
left outer join iraction as iractionmpay on mpay.actflag = iractionmpay.code
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and mrgr.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_merger;
rename table wca2.evf_loadingtable to wca2.evf_merger;
-----------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'new listing' as event,
concat(lpad(nlist.scexhid,7,'0')) as caref,
nlist.scexhid as eventid,
nlist.announcedate as created,
nlist.acttime as changed,
case when nlist.actflag = 'U' then 'Updated'
     when nlist.actflag = 'I' then 'Inserted'
     when nlist.actflag = 'D' then 'Deleted'
     when nlist.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
'Mandatory' as choice,
scmst.issid,
scmst.secid
from nlist
inner join scexh on nlist.scexhid = scexh.scexhid
inner join scmst on scexh.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and nlist.announcedate between '2011/07/01' and '2099/01/01'
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_new_listing;
rename table wca2.evf_loadingtable to wca2.evf_new_listing;
--------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'preference conversion' as event,
concat(lpad(scexhid,7,'0'),conv.convid) as caref,
conv.convid as eventid,
conv.announcedate as created,
conv.acttime as changed,
case when conv.actflag = 'U' then 'Updated'
     when conv.actflag = 'I' then 'Inserted'
     when conv.actflag = 'D' then 'Deleted'
     when conv.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
case when conv.rationew is null or conv.rationew = ''
     then ''
     else concat(conv.rationew,':',conv.ratioold)
     end as ratio,
'price' as rate_type,
conv.price as rate,
conv.curencd as currency,
conv.fromdate as record_date,
conv.todate as conv_period_end,
conv.ressecid as resultant_sec_id,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
case when conv.ressectycd is null then ''
     when secty.securitydescriptor is null and conv.ressectycd <> ''
     then concat('[',conv.ressectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
case when conv.mandoptflag = 'm' then 'Mandatory'
     when conv.mandoptflag = 'v' then 'Voluntary'
     else ''
     end as choice,
case when conv.partfinalflag='p' then 'Part'
     when conv.partfinalflag='f' then 'Final'
     else ''
     end as part_final,
'conv|convid|convnotes' as link_notes,
scmst.issid,
scmst.secid
from conv
inner join scmst on conv.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol. actflag
left outer join scmst as resscmst on conv.ressecid = resscmst.secid
left outer join sedol as ressedol on conv.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join secty on conv.ressectycd = secty.sectycd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and conv.announcedate between '2010/07/01' and '2099/01/01'
and ((fromdate >= scexh.listdate and scexh.listdate is not null) or fromdate>=scexh.announcedate or fromdate is null)
and (fromdate < scexh.acttime or scexh.liststatus<>'D' or fromdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_preference_conversion;
rename table wca2.evf_loadingtable to wca2.evf_preference_conversion;
--------------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'rights' as event,
concat(lpad(scexhid,7,'0'),rd.rdid) as caref,
rts.rdid as eventid,
rts.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > rts.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > rts.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > rts.acttime) then pexdt.acttime
     else rts.acttime
     end as changed,
case when rts.actflag = 'U' then 'Updated'
     when rts.actflag = 'I' then 'Inserted'
     when rts.actflag = 'D' then 'Deleted'
     when rts.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
case when rts.sectycd is null then ''
     when secty.securitydescriptor is null and rts.sectycd <> ''
     then concat('[',rts.sectycd,'] not found')
     else secty.securitydescriptor
     end as offered_security_type,
case when rts.rationew is null or rts.rationew = ''
     then ''
     else concat(rts.rationew,':',rts.ratioold)
     end as ratio,
'issueprice' as rate_type,
rts.issueprice as rate,
rts.curencd as currency,
case when rts.fractions is null then ''
     when irfractions.lookup is null and rts.fractions <> ''
     then concat('[',rts.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
rts.startsubscription as subscription_period_open,
rts.endsubscription as subscription_period_close,
rts.splitdate as split_date,
rts.starttrade as rights_trading_open,
rts.endtrade as rights_trading_close,
rts.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
rts.trasecid as tradeable_secid,
trascmst.isin as tradeable_isin,
rts.lapsedpremium,
case when rts.oversubscription = 't' then 'yes'
     else 'no'
     end as over_subscription,
'Voluntary' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'rts|rdid|rtsnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from rts
inner join rd on rts.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'RTS' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'RTS' = pexdt.eventtype
left outer join scmst as resscmst on rts.ressecid = resscmst.secid
left outer join sedol as ressedol on rts.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join scmst as trascmst on rts.trasecid = trascmst.secid
left outer join irfractions on rts.fractions = irfractions.code
left outer join secty on rts.sectycd = secty.sectycd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and rts.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_rights;
rename table wca2.evf_loadingtable to wca2.evf_rights;
-------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'security reclassification' as event,
concat(lpad(scexhid,7,'0'),secrc.secrcid) as caref,
secrcid as eventid,
secrc.announcedate as created,
secrc.acttime as changed,
case when secrc.actflag = 'U' then 'Updated'
     when secrc.actflag = 'I' then 'Inserted'
     when secrc.actflag = 'D' then 'Deleted'
     when secrc.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
case when secrc.eventtype is null then ''
     when event.eventname is null and secrc.eventtype <> ''
     then concat('[',secrc.eventtype,'] not found')
     else event.eventname
     end as related_event,
secrc.effectivedate as effective_date,
case when secrc.sectycd is null then ''
     when secty.securitydescriptor is null and secrc.sectycd <> ''
     then concat('[',secrc.sectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
case when secrc.rationew is null or secrc.rationew = ''
     then ''
     else concat(secrc.rationew,':',secrc.ratioold)
     end as ratio,
secrc.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
'Mandatory' as choice,
'secrc|secrcid|secrcnotes' as link_notes,
scmst.issid,
scmst.secid
from secrc
inner join scmst on secrc.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join scmst as resscmst on secrc.ressecid = resscmst.secid
left outer join sedol as ressedol on secrc.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.curencd = ressedol.curencd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and 'd' <> ressedol.actflag
left outer join secty on secrc.sectycd = secty.sectycd
left outer join event on secrc.eventtype = event.eventtype
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and secrc.announcedate between '2010/07/01' and '2099/01/01'
and ((effectivedate >= scexh.listdate and scexh.listdate is not null) or effectivedate>=scexh.announcedate or effectivedate is null)
and (effectivedate < scexh.acttime or scexh.liststatus<>'D' or effectivedate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_security_reclassification;
rename table wca2.evf_loadingtable to wca2.evf_security_reclassification;
-------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'sedol change' as event,
concat(lpad(scexhid,7,'0'),sdchg.sdchgid) as caref,
sdchg.sdchgid as eventid,
sdchg.announcedate as created,
sdchg.acttime as changed,
case when sdchg.actflag = 'U' then 'Updated'
     when sdchg.actflag = 'I' then 'Inserted'
     when sdchg.actflag = 'D' then 'Deleted'
     when sdchg.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
case when sdchg.eventtype is null then ''
     when event.eventname is null and sdchg.eventtype <> ''
     then concat('[',sdchg.eventtype,'] not found')
     else event.eventname
     end as related_event,
sdchg.cntrycd as cntry_of_sedol,
sdchg.effectivedate as effective_date,
sdchg.oldsedol as old_sedol,
sdchg.newsedol as new_sedol,
sdchg.cntrycd as old_country,
sdchg.newcntrycd as new_country,
sdchg.rcntrycd as old_register_country,
sdchg.newrcntrycd as new_register_country,
'Mandatory' as choice,
'sdchg|sdchgid|sdchgnotes' as link_notes,
scmst.issid,
scmst.secid
from sdchg
inner join scmst on sdchg.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
left outer join sedol on scmst.secid = sedol.secid
                and sdchg.newcntrycd=sedol.cntrycd
                and 'f' =sedol.defunct
                and 'd' <> sedol.actflag
inner join exchg on sdchg.cntrycd = exchg.cntrycd
inner join scexh on scmst.secid = scexh.secid and exchg.exchgcd = scexh.exchgcd
left outer join event on sdchg.eventtype = event.eventtype
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and sdchg.announcedate between '2011/07/01' and '2099/01/01' 
and ((effectivedate >= scexh.listdate and scexh.listdate is not null) or effectivedate>=scexh.announcedate or effectivedate is null)
and (effectivedate < scexh.acttime or scexh.liststatus<>'D' or effectivedate is null)
and (sdchg.secid is null or sedol=newsedol)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_sedol_change;
rename table wca2.evf_loadingtable to wca2.evf_sedol_change;
------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'subdivision' as event,
concat(lpad(scexhid,7,'0'),rd.rdid) as caref,
sd.rdid as eventid,
sd.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > sd.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > sd.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > sd.acttime) then pexdt.acttime
     else sd.acttime
     end as changed,
case when sd.actflag = 'U' then 'Updated'
     when sd.actflag = 'I' then 'Inserted'
     when sd.actflag = 'D' then 'Deleted'
     when sd.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
case when (icc.releventid is null or icc.oldisin = '') then isin
     else icc.oldisin
     end as isin,
case when (sdchg.releventid is not null and sdchg.oldsedol <> '') then sdchg.oldsedol
     when sedol.secid is not null then sedol
     else ''
     end as sedol,
case when (icc.releventid is null or icc.olduscode = '') then uscode
     else icc.olduscode
     end as us_code,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as Security_Structure,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
case when (lcc.releventid is null or lcc.oldlocalcode = '') then scexh.localcode
     else lcc.oldlocalcode
     end as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
case when sd.newratio is null or sd.newratio = ''
     then ''
     else concat(sd.newratio,':',sd.oldratio)
     end as ratio,
sd.oldcurencd as old_currency,
sd.newcurencd as new_currency,
sd.oldparvalue as old_par_value,
sd.newparvalue as new_par_value,
icc.newisin as new_isin,
icc.newuscode as new_uscode,
sdchg.newsedol as new_sedol,
lcc.newlocalcode as new_local_code,
case when (icc.acttime is not null) and (icc.acttime > sdchg.acttime) then icc.acttime
     else sdchg.acttime
     end as newcodedate,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'sd|rdid|sdnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from sd
inner join rd on sd.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'SD' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'SD' = pexdt.eventtype
left outer join icc on rd.rdid = icc.releventid and 'SD' = icc.eventtype
       and icc.actflag<>'d'
left outer join sdchg on rd.rdid = sdchg.releventid and 'SD' = sdchg.eventtype
       and scmst.secid = sdchg.secid
       and exchg.cntrycd = sdchg.cntrycd
       and sedol.rcntrycd = sdchg.rcntrycd
       and sdchg.actflag<>'d'
left outer join lcc on rd.rdid = lcc.releventid and 'SD' = lcc.eventtype
       and scexh.exchgcd = lcc.exchgcd
       and lcc.actflag<>'d'
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and sd.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
and (sdchg.secid is null or sedol=newsedol)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_subdivision;
rename table wca2.evf_loadingtable to wca2.evf_subdivision;
-------------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'takeover' as event,
case when optionid is not null 
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),lpad(serialid,2,'0'),tkovr.tkovrid)
     else concat(lpad(scexhid,7,'0'),'0101',tkovr.tkovrid) 
     end as caref,
tkovrid as eventid,
tkovr.announcedate as created,
case when (mpay.acttime is not null) and (mpay.acttime > tkovr.acttime) and (mpay.acttime > rd.acttime) then mpay.acttime
     when rd.acttime > tkovr.acttime then rd.acttime
     else tkovr.acttime
     end as changed,
case when tkovr.actflag = 'U' then 'Updated'
     when tkovr.actflag = 'I' then 'Inserted'
     when tkovr.actflag = 'D' then 'Deleted'
     when tkovr.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when mpay.actflag = 'U' then 'Updated'
     when mpay.actflag = 'I' then 'Inserted'
     when mpay.actflag = 'D' then 'Deleted'
     when mpay.actflag = 'C' then 'Cancelled'
     else '' end as Pay_actflag,
case when optionid is not null 
     then concat(lpad(optionid,2,'0'),',',lpad(serialid,2,'0'))
     else ''
     end as option_key,
case when mpay.paytype is null then ''
     when irpaytype.lookup is null and mpay.actflag <> ''
     then concat('[',mpay.actflag,'] not found')
     else irpaytype.lookup
     end as pay_type,
case when mpay.rationew is null or mpay.rationew = ''
     then ''
     else concat(mpay.rationew,':',mpay.ratioold)
     end as ratio,
'minpricemaxprice' as rate_type,
case when mpay.maxprice is null or mpay.maxprice = ''
     then ''
     else concat(mpay.maxprice,':',mpay.minprice)
     end as rate,
case when mpay.curencd is null then ''
     else mpay.curencd
     end as currency,
mpay.paydate as pay_date,
case when mpay.fractions is null then ''
     when irfractions.lookup is null and mpay.fractions <> ''
     then concat('[',mpay.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
mpay.minqlyqty as min_qualifying_quant,
mpay.maxqlyqty as max_qualifying_quant,
mpay.minofrqty as min_offer_quant,
mpay.maxofrqty as max_offer_quant,
mpay.tndrstrkprice as tender_strike_price,
mpay.tndrstrkstep as tender_strike_step,
mpay.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
case when mpay.sectycd is null then ''
     when secty.securitydescriptor is null and mpay.sectycd <> ''
     then concat('[',mpay.sectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
case when tkovr.hostile is null then ''
     when irhostile.lookup is null and tkovr.hostile <> ''
     then concat('[',tkovr.hostile,'] not found')
     else irhostile.lookup
     end as takeover_type,
case when tkovr.tkovrstatus is null then ''
     when irtkovrstat.lookup is null and tkovr.tkovrstatus <> ''
     then concat('[',tkovr.tkovrstatus,'] not found')
     else irtkovrstat.lookup
     end as takeover_status,
tkovr.offerorissid as offeror_issid,
tkovr.offerorname as offeror_name,
tkovr.opendate as offer_opens,
tkovr.closedate as offer_closes,
tkovr.preofferqty as pre_offer_quant,
tkovr.preofferpercent as pre_offer_percentage,
tkovr.targetquantity as target_quant,
tkovr.targetpercent as target_percentage,
tkovr.unconditionaldate as offer_unconditional_date,
tkovr.cmacqdate as compulsory_acq_date,
tkovr.minacpqty as min_acceptance_quant,
tkovr.maxacpqty as max_acceptance_quant,
case when (tkovr.cmacqdate is null) then 'Voluntary'
     else 'Mandatory'
     end as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'tkovr|tkovrid|tkovrnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from tkovr
inner join scmst on tkovr.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join rd on tkovr.rdid = rd.rdid
left outer join mpay on tkovr.tkovrid = mpay.eventid and 'TKOVR' = mpay.sevent
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
left outer join sedol as ressedol on mpay.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join secty on mpay.sectycd = secty.sectycd
left outer join irfractions on mpay.fractions = irfractions.code
left outer join irhostile on tkovr.hostile = irhostile.code
left outer join irtkovrstat on tkovr.tkovrstatus = irtkovrstat.code
left outer join irpaytype on mpay.paytype = irpaytype.code
left outer join iraction as iractionmpay on mpay.actflag = iractionmpay.code
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and tkovr.acttime between '2010/07/01' and '2099/01/01'
and ((tkovr.announcedate >= scexh.listdate and scexh.listdate is not null) or tkovr.announcedate>=scexh.announcedate)
and (tkovr.announcedate < scexh.acttime or scexh.liststatus<>'D')
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_takeover;
rename table wca2.evf_loadingtable to wca2.evf_takeover;
-----------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'preferential offer' as event,
concat(lpad(scexhid,7,'0'),rd.rdid) as caref,
prf.rdid as eventid,
prf.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > prf.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > prf.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > prf.acttime) then pexdt.acttime
     else prf.acttime
     end as changed,
case when prf.actflag = 'U' then 'Updated'
     when prf.actflag = 'I' then 'Inserted'
     when prf.actflag = 'D' then 'Deleted'
     when prf.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
prf.offereeissid as offeree_issid,
prf.offereename as offeree_name,
case when prf.sectycd is null then ''
     when secty.securitydescriptor is null and prf.sectycd <> ''
     then concat('[',prf.sectycd,'] not found')
     else secty.securitydescriptor
     end as offered_security_type,
case when prf.fractions is null then ''
     when irfractions.lookup is null and prf.fractions <> ''
     then concat('[',prf.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
case when prf.rationew is null or prf.rationew = ''
     then ''
     else concat(prf.rationew,':',prf.ratioold)
     end as ratio,
'minprice:maxprice' as rate_type,
case when prf.maxprice is null or prf.maxprice = ''
     then ''
     else concat(prf.maxprice,':',prf.minprice)
     end as rate,
prf.curencd as currency,
prf.startsubscription as subscription_period_open,
prf.endsubscription as subscription_period_close,
prf.tndrstrkprice as strike_price,
prf.tndrpricestep as tender_price_step,
prf.minqlyqty as min_qualifying_quant,
prf.maxqlyqty as max_qualifying_quant,
prf.minacpqty as min_acceptance_quant,
prf.maxacpqty as max_acceptance_quant,
prf.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
'Voluntary' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'prf|rdid|prfnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from prf
inner join rd on prf.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'PRF' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'PRF' = pexdt.eventtype
left outer join scmst as resscmst on prf.ressecid = resscmst.secid
left outer join sedol as ressedol on prf.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join irfractions on prf.fractions = irfractions.code
left outer join secty on prf.sectycd = secty.sectycd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and prf.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_preferential_offer;
rename table wca2.evf_loadingtable to wca2.evf_preferential_offer;
-----------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'purchase offer' as event,
concat(lpad(scexhid,7,'0'),rd.rdid) as caref,
po.rdid as eventid,
po.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > po.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > po.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > po.acttime) then pexdt.acttime
     else po.acttime end as changed,
case when po.actflag = 'U' then 'Updated'
     when po.actflag = 'I' then 'Inserted'
     when po.actflag = 'D' then 'Deleted'
     when po.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
'minmaxprice' as rate_type,
case when po.maxprice is null or po.maxprice = ''
     then ''
     else concat(po.maxprice,':',po.minprice)
     end as rate,
po.curencd as currency,
po.negotiatedprice as negotiated_price,
po.offeropens as offer_opens,
po.offercloses as offer_closes,
po.pominpercent as min_percent,
po.pomaxpercent as max_percent,
po.minofrqty as min_offer_quant,
po.maxofrqty as max_offer_quant,
po.tndrstrkprice as tender_strike_price,
po.tndrpricestep as tender_price_step,
po.minqlyqty as min_qualifying_quant,
po.maxqlyqty as max_qualifying_quant,
po.minacpqty as min_acceptance_quant,
po.maxacpqty as max_acceptance_quant,
'Voluntary' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'po|rdid|ponotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from po
inner join rd on po.rdid = rd.rdid 
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'PO' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'PO' = pexdt.eventtype
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and po.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_purchase_offer;
rename table wca2.evf_loadingtable to wca2.evf_purchase_offer;
--------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'dividend reinvestment plan' as event,
concat(lpad(scexhid,7,'0'),div_my.divid) as caref,
div_my.divid as eventid,
drip.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > drip.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > drip.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > drip.acttime) then pexdt.acttime
     else drip.acttime
     end as changed,
case when drip.actflag = 'U' then 'Updated'
     when drip.actflag = 'I' then 'Inserted'
     when drip.actflag = 'D' then 'Deleted'
     when drip.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
drip.drippaydate as drip_pay_date,
case when div_my.divperiodcd= 'mnt' then 'monthly'
     when div_my.divperiodcd= 'sma' then 'semi-annual'
     when div_my.divperiodcd= 'ins' then 'installment'
     when div_my.divperiodcd= 'int' then 'interim'
     when div_my.divperiodcd= 'qtr' then 'quarterly'
     when div_my.divperiodcd= 'fnl' then 'final'
     when div_my.divperiodcd= 'anl' then 'annual'
     when div_my.divperiodcd= 'reg' then 'regular'
     when div_my.divperiodcd= 'un'  then 'unspecified'
     when div_my.divperiodcd= 'bim' then 'bi-monthly'
     when div_my.divperiodcd= 'spl' then 'special'
     when div_my.divperiodcd= 'trm' then 'trimesterly'
     when div_my.divperiodcd= 'mem' then 'memorial'
     when div_my.divperiodcd= 'sup' then 'supplemental'
     when div_my.divperiodcd= 'isc' then 'interest on sgc'
     else ''
     end as dividend_period,
case when div_my.tbaflag= 't' then 'yes'
     else ''
     end as to_be_announced,
case when drip.cntrycd is null then ''
     when cntry.country is null and drip.cntrycd <> ''
     then concat('[',drip.cntrycd,'] not found')
     else cntry.country
     end as country,
drip.crestdate as crest_date,
drip.driplastdate as drip_last_date,
drip.dripreinvprice as drip_reinv_price,
case when divpy.recindcashdiv='t' then '0' else divpy.grossdividend
     end as gross_cash_dividend,
case when divpy.recindcashdiv='t' then '0' else divpy.netdividend
     end as net_cash_dividend,
case when divpy.curencd is null then ''
     else divpy.curencd
     end as dividend_currency,
case when divpy.recindcashdiv= 't' then concat('yes: gross was: ',divpy.grossdividend,' net was: ',divpy.netdividend,' as on changed date')
     else ''
     end as cash_dividend_rescinded,
'Voluntary' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'div|divid|divnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from drip
inner join div_my on drip.divid = div_my.divid
left outer join divpy on drip.divid = divpy.divid and drip.cntrycd = substring(divpy.curencd,1,2) and 's' <> divpy.divtype and 'b' <> divpy.divtype
inner join rd on div_my.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid  and drip.cntrycd = substring(scexh.exchgcd,1,2)
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'DIV' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
left outer join cntry on drip.cntrycd = cntry.cntrycd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and drip.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_dividend_reinvestment_plan;
rename table wca2.evf_loadingtable to wca2.evf_dividend_reinvestment_plan;
-------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'franking' as event,
concat(lpad(scexhid,7,'0'),div_my.divid) as caref,
div_my.divid as eventid,
frank.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > frank.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > frank.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > frank.acttime) then pexdt.acttime
     else frank.acttime
     end as changed,
case when frank.actflag = 'U' then 'Updated'
     when frank.actflag = 'I' then 'Inserted'
     when frank.actflag = 'D' then 'Deleted'
     when frank.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
case when div_my.divperiodcd= 'mnt' then 'monthly'
     when div_my.divperiodcd= 'sma' then 'semi-annual'
     when div_my.divperiodcd= 'ins' then 'installment'
     when div_my.divperiodcd= 'int' then 'interim'
     when div_my.divperiodcd= 'qtr' then 'quarterly'
     when div_my.divperiodcd= 'fnl' then 'final'
     when div_my.divperiodcd= 'anl' then 'annual'
     when div_my.divperiodcd= 'reg' then 'regular'
     when div_my.divperiodcd= 'un'  then 'unspecified'
     when div_my.divperiodcd= 'bim' then 'bi-monthly'
     when div_my.divperiodcd= 'spl' then 'special'
     when div_my.divperiodcd= 'trm' then 'trimesterly'
     when div_my.divperiodcd= 'mem' then 'memorial'
     when div_my.divperiodcd= 'sup' then 'supplemental'
     when div_my.divperiodcd= 'isc' then 'interest on sgc'
     else ''
     end as dividend_period,
case when div_my.tbaflag= 't' then 'yes'
     else ''
     end as to_be_announced,
case when frank.cntrycd is null then ''
     when cntry.country is null and frank.cntrycd <> ''
     then concat('[',frank.cntrycd,'] not found')
     else cntry.country
     end as country,
case when frank.frankflag = 'f' then 'Fully Franked'
     when frank.frankflag = 'p' then 'Partially Franked'
     when frank.frankflag = 'u' then 'Unfranked'
     when frank.frankflag = 'n' then 'Not known'
     else ''
     end as franking_type,
case when frank.frankdiv<>'' then frank.frankdiv
     when frank.frankflag = 'f' then divpy.grossdividend
     else ''
     end as amount_franked,
case when frank.unfrankdiv<>'' then frank.unfrankdiv
     when frank.frankflag = 'u' then divpy.grossdividend
     else ''
     end as amount_unfranked,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'div|divid|divnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from frank
inner join div_my on frank.divid = div_my.divid
inner join rd on div_my.rdid = rd.rdid
left outer join divpy on frank.divid = divpy.divid and frank.cntrycd = substring(divpy.curencd,1,2) and 's' <> divpy.divtype and 'b' <> divpy.divtype
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid  and frank.cntrycd = substring(scexh.exchgcd,1,2)
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'DIV' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
left outer join cntry on frank.cntrycd = cntry.cntrycd
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and frank.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_franking;
rename table wca2.evf_loadingtable to wca2.evf_franking;
-----------------------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'security swap' as event,
concat(lpad(scexhid,7,'0'),rd.rdid) as caref,
scswp.rdid as eventid,
scswp.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > scswp.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > scswp.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > scswp.acttime) then pexdt.acttime
     else scswp.acttime
     end as changed,
case when scswp.actflag = 'U' then 'Updated'
     when scswp.actflag = 'I' then 'Inserted'
     when scswp.actflag = 'D' then 'Deleted'
     when scswp.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
case when scswp.newratio is null or scswp.newratio = ''
     then ''
     else concat(scswp.newratio,':',scswp.oldratio)
     end as ratio,
case when scswp.fractions is null then ''
     when irfractions.lookup is null and scswp.fractions <> ''
     then concat('[',scswp.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
scswp.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
case when scswp.sectycd is null then ''
     when secty.securitydescriptor is null and scswp.sectycd <> ''
     then concat('[',scswp.sectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'scswp|rdid|scswpnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from scswp
inner join rd on scswp.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'SCSWP' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'SCSWP' = pexdt.eventtype
left outer join scmst as resscmst on scswp.ressecid = resscmst.secid
left outer join sedol as ressedol on scswp.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join secty on scswp.sectycd = secty.sectycd
left outer join irfractions on scswp.fractions = irfractions.code
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and scswp.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_security_swap;
rename table wca2.evf_loadingtable to wca2.evf_security_swap;
-------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'odd lot offer' as event,
case when optionid is not null 
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),lpad(serialid,2,'0'),rd.rdid)
     else concat(lpad(scexhid,7,'0'),'0101',rd.rdid) 
     end as caref,
oddlt.rdid as eventid,
oddlt.announcedate as created,
case when (rd.acttime is not null) and (rd.acttime > oddlt.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > oddlt.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > oddlt.acttime) then pexdt.acttime
     else oddlt.acttime
     end as changed,
case when oddlt.actflag = 'U' then 'Updated'
     when oddlt.actflag = 'I' then 'Inserted'
     when oddlt.actflag = 'D' then 'Deleted'
     when oddlt.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     when pexdt.paydate is not null then pexdt.paydate
     else mpay.paydate
     end as pay_date,
case when mpay.actflag = 'U' then 'Updated'
     when mpay.actflag = 'I' then 'Inserted'
     when mpay.actflag = 'D' then 'Deleted'
     when mpay.actflag = 'C' then 'Cancelled'
     else '' end as Pay_actflag,
case when optionid is not null 
     then concat(lpad(optionid,2,'0'),',',lpad(serialid,2,'0'))
     else ''
     end as option_key,
case when mpay.paytype is null then ''
     when irpaytype.lookup is null and mpay.actflag <> ''
     then concat('[',mpay.actflag,'] not found')
     else irpaytype.lookup
     end as pay_type,
case when mpay.rationew is null or mpay.rationew = ''
     then ''
     else concat(mpay.rationew,':',mpay.ratioold)
     end as ratio,
case when mpay.sectycd is null then ''
     when secty.securitydescriptor is null and mpay.sectycd <> ''
     then concat('[',mpay.sectycd,'] not found')
     else secty.securitydescriptor
     end as resultant_security_type,
case when mpay.fractions is null then ''
     when irfractions.lookup is null and mpay.fractions <> ''
     then concat('[',mpay.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
case when mpay.curencd is null then ''
     else mpay.curencd
     end as currency,
'minmaxprice' as rate_type,
case when mpay.maxprice is null or mpay.maxprice = ''
     then ''
     else concat(mpay.maxprice,':',mpay.minprice)
     end as rate,
mpay.minqlyqty as min_qualifying_quant,
mpay.maxqlyqty as max_qualifying_quant,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
'Mandatory' as choice,
oddlt.startdate as start_date,
oddlt.enddate as end_date,
oddlt.minacpqty as min_acceptance_qty,
oddlt.maxacpqty as max_acceptance_qty,
oddlt.buyin as buy_in,
oddlt.buyincurencd as buy_in_currency,
oddlt.buyinprice as buy_in_price,
'rd|rdid|rdnotes' as link_record_date_notes,
'oddlt|rdid|notes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from oddlt
inner join rd on oddlt.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd = sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'ODDLT' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'ODDLT' = pexdt.eventtype
left outer join mpay on oddlt.rdid = mpay.eventid and 'ODDLT' = mpay.sevent
left outer join scmst as resscmst on mpay.ressecid = resscmst.secid
left outer join sedol as ressedol on mpay.ressecid = ressedol.secid
                   and exchg.cntrycd = ressedol.cntrycd
                   and sedol.rcntrycd = ressedol.rcntrycd
                   and sedol.curencd = ressedol.curencd
                   and 'd' <> ressedol.actflag
left outer join secty on mpay.sectycd = secty.sectycd
left outer join irfractions on mpay.fractions = irfractions.code
left outer join iraction as iractionmpay on mpay.actflag = iractionmpay.code
left outer join irpaytype on mpay.paytype = irpaytype.code
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and oddlt.announcedate between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_odd_lot_offer;
rename table wca2.evf_loadingtable to wca2.evf_odd_lot_offer;
--------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'dividend' as event,
case when optionid is not null
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),div_my.divid)
     else concat(lpad(scexhid,7,'0'),'01',div_my.divid)
     end as caref,
div_my.divid as eventid,
div_my.announcedate as created,
case when (divpy.acttime is not null) and (divpy.acttime > div_my.acttime) and (divpy.acttime > rd.acttime) and (divpy.acttime > exdt.acttime) and (divpy.acttime > pexdt.acttime) then divpy.acttime
     when (rd.acttime is not null) and (rd.acttime > div_my.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > div_my.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > div_my.acttime) then pexdt.acttime
     else div_my.acttime
     end as changed,
case when div_my.actflag = 'U' then 'Updated'
     when div_my.actflag = 'I' then 'Inserted'
     when div_my.actflag = 'D' then 'Deleted'
     when div_my.actflag = 'C' then 'Cancelled'
     when div_my.actflag = 'U' then 'Updated'
     when div_my.actflag = 'I' then 'Inserted'
     when div_my.actflag = 'D' then 'Deleted'
     when div_my.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
rd.registrationdate as registration_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
div_my.fyedate as fin_year_date,
case when div_my.divperiodcd= 'mnt' then 'monthly'
     when div_my.divperiodcd= 'sma' then 'semi-annual'
     when div_my.divperiodcd= 'ins' then 'installment'
     when div_my.divperiodcd= 'int' then 'interim'
     when div_my.divperiodcd= 'qtr' then 'quarterly'
     when div_my.divperiodcd= 'fnl' then 'final'
     when div_my.divperiodcd= 'anl' then 'annual'
     when div_my.divperiodcd= 'reg' then 'regular'
     when div_my.divperiodcd= 'un'  then 'unspecified'
     when div_my.divperiodcd= 'bim' then 'bi-monthly'
     when div_my.divperiodcd= 'spl' then 'special'
     when div_my.divperiodcd= 'trm' then 'trimesterly'
     when div_my.divperiodcd= 'mem' then 'memorial'
     when div_my.divperiodcd= 'sup' then 'supplemental'
     when div_my.divperiodcd= 'isc' then 'interest on sgc'
     else ''
     end as dividend_period,
case when div_my.tbaflag= 't' then 'yes'
     else ''
     end as to_be_announced,
case when div_my.nildividend= 't' then 'yes'
     else ''
     end as nil_dividend,
case when optionid is not null 
     then concat(lpad(optionid,2,'0'),',01')
     else ''
     end as option_key,
case when divpy.defaultopt = 't' then 'yes'
     else ''
     end as default_option,
divpy.optelectiondate as option_election_date,
iractiondivpy.lookup as option_status,
case when div_my.nildividend= 'y' then 'nildividend'
     when divpy.divtype= 'b' then 'cash & stock'
     when divpy.divtype= 's' then 'stock'
     when divpy.divtype= 'c' then 'cash'
     else 'unspecified'
     end as dividend_type,
case when divpy.recindcashdiv='t' then '0' else divpy.grossdividend
     end as gross_dividend,
case when divpy.recindcashdiv='t' then '0' else divpy.netdividend
     end as net_dividend,
case when divpy.curencd is null then ''
     else divpy.curencd
     end as currency,
case when divpy.recindcashdiv= 't' then concat('yes: gross was: ',divpy.grossdividend,' net was: ',divpy.netdividend,' as on changed date')
     else ''
     end as cash_dividend_rescinded,
divpy.taxrate as tax_rate,
case when divpy.approxflag= 't' then 'yes'
     else ''
     end as approximate_dividend,
divpy.usdratetocurrency as usd_rate_to_currency,
case when divpy.fractions is null then ''
     when irfractions.lookup is null and divpy.fractions <> ''
     then concat('[',divpy.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
divpy.coupon as coupon,
divpy.couponid as coupon_id,
divpy.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
case when divpy.rationew is null or divpy.rationew = ''
     then ''
     else concat(divpy.rationew,':',divpy.ratioold)
     end as ratio,
exdt.paydate2 as stock_pay_date,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'div|divid|divnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from div_my
inner join rd on div_my.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid
       and scexh.exchgcd = exdt.exchgcd and 'DIV' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid
       and scmst.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
left outer join divpy on div_my.divid = divpy.divid
left outer join scmst as resscmst on divpy.ressecid = resscmst.secid
left outer join sedol on scmst.secid = sedol.secid
       and exchg.cntrycd = sedol.cntrycd
       and 'f' = sedol.defunct
       and 'd' <> sedol.actflag
left outer join sedol as ressedol on divpy.ressecid = ressedol.secid
       and sedol.rcntrycd = ressedol.rcntrycd
       and exchg.cntrycd = ressedol.cntrycd
       and sedol.curencd = ressedol.curencd
       and 'd' <> ressedol.actflag
left outer join irfractions on divpy.fractions = irfractions.code
left outer join iraction as iractiondivpy on divpy.actflag = iractiondivpy.code
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and div_my.acttime between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_dividend;
rename table wca2.evf_loadingtable to wca2.evf_dividend;
------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'shares_outstanding_change' as event,
case when shoch.shochid is not null 
     then concat(lpad(scexhid,7,'0'),shoch.shochid)
     else concat(lpad(scexhid,7,'0'),scmst.secid) 
     end as caref,
case when shoch.shochid is not null
     then shoch.shochid
     else scmst.secid
     end as eventid,
case when shoch.shochid is not null then shoch.announcedate
     else scmst.announcedate
     end as created,
case when shoch.shochid is not null then shoch.acttime
     else scmst.acttime
     end as changed,
case when shoch.actflag = 'U' then 'Updated'
     when shoch.actflag = 'I' then 'Inserted'
     when shoch.actflag = 'D' then 'Deleted'
     when shoch.actflag = 'C' then 'Cancelled'
     when scmst.actflag = 'U' then 'Updated'
     when scmst.actflag = 'I' then 'Inserted'
     when scmst.actflag = 'D' then 'Deleted'
     when scmst.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
shoch.effectivedate as effective_date,
case when shoch.shochid is not null then shoch.oldsos
     else null
     end as old_shares_outstanding,
case when shoch.shochid is not null then shoch.newsos
     else scmst.sharesoutstanding
     end as new_shares_outstanding,
'Mandatory' as choice,
'shoch|shochid|shochnotes' as link_notes,
scmst.issid,
scmst.secid
from scmst
left outer join shoch on scmst.secid = shoch.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join sedol on scmst.secid = sedol.secid
                   and exchg.cntrycd=sedol.cntrycd
                   and 'f' = sedol.defunct
                   and 'd' <> sedol.actflag
left outer join sectygrp on scmst.sectycd = sectygrp.sectycd
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
sectygrp.secgrpid<3
and scmst.sharesoutstanding <> ''
and (shoch.acttime between '2010/07/01' and '2099/01/01' or shoch.acttime is null)
and ((effectivedate >= scexh.listdate and scexh.listdate is not null) or effectivedate>=scexh.announcedate or effectivedate is null)
and (effectivedate < scexh.acttime or scexh.liststatus<>'D' or effectivedate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_shares_outstanding_change;
rename table wca2.evf_loadingtable to wca2.evf_shares_outstanding_change;
-----------------------------
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_loadingtable(
select distinct
'dividend unittrust' as event,
case when optionid is not null
     then concat(lpad(scexhid,7,'0'),lpad(optionid,2,'0'),div_my.divid)
     else concat(lpad(scexhid,7,'0'),'01',div_my.divid)
     end as caref,
div_my.divid as eventid,
div_my.announcedate as created,
case when (divpy.acttime is not null)
and (divpy.acttime > div_my.acttime)
and (divpy.acttime > rd.acttime)
and (divpy.acttime > exdt.acttime)
and (divpy.acttime > pexdt.acttime) then divpy.acttime
    when (rd.acttime is not null)
and (rd.acttime > div_my.acttime) and (rd.acttime > exdt.acttime)
and (rd.acttime > pexdt.acttime) then rd.acttime
    when (exdt.acttime is not null)
and (exdt.acttime > div_my.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
    when (pexdt.acttime is not null)
and (pexdt.acttime > div_my.acttime) then pexdt.acttime
    else div_my.acttime end as changed,
case when divpy.actflag = 'U' then 'Updated'
     when divpy.actflag = 'I' then 'Inserted'
     when divpy.actflag = 'D' then 'Deleted'
     when divpy.actflag = 'C' then 'Cancelled'
     when div_my.actflag = 'U' then 'Updated'
     when div_my.actflag = 'I' then 'Inserted'
     when div_my.actflag = 'D' then 'Deleted'
     when div_my.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sedol.secid is null then '' else sedol.sedol end as sedol,
case when sedol.secid is null then '' else sedol.curencd end as sedol_currency,
case when sedol.secid is null then '' else sedol.rcntrycd end as sedol_register_country,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
bbe.bbgexhid as Bloomberg_Global_ID,
bbe.bbgexhtk as Bloomberg_Exchange_Ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
case when exdt.exdate is not null then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when exdt.paydate is not null then exdt.paydate
     else pexdt.paydate
     end as pay_date,
div_my.fyedate as fin_year_date,
case when div_my.divperiodcd= 'mnt' then 'monthly'
     when div_my.divperiodcd= 'sma' then 'semi-annual'
     when div_my.divperiodcd= 'ins' then 'installment'
     when div_my.divperiodcd= 'int' then 'interim'
     when div_my.divperiodcd= 'qtr' then 'quarterly'
     when div_my.divperiodcd= 'fnl' then 'final'
     when div_my.divperiodcd= 'anl' then 'annual'
     when div_my.divperiodcd= 'reg' then 'regular'
     when div_my.divperiodcd= 'un'  then 'unspecified'
     when div_my.divperiodcd= 'bim' then 'bi-monthly'
     when div_my.divperiodcd= 'spl' then 'special'
     when div_my.divperiodcd= 'trm' then 'trimesterly'
     when div_my.divperiodcd= 'mem' then 'memorial'
     when div_my.divperiodcd= 'sup' then 'supplemental'
     when div_my.divperiodcd= 'isc' then 'interest on sgc'
     else ''
     end as dividend_frequency,
case when div_my.tbaflag= 't' then 'yes'
     else ''
     end as to_be_announced,
case when div_my.nildividend= 't' then 'yes'
     else ''
     end as nil_dividend,
case when optionid is not null 
     then concat(lpad(optionid,2,'0'),',01')
     else ''
     end as option_key,
case when divpy.actflag is null then ''
     when iractiondivpy.lookup is null and divpy.actflag <> ''
     then concat('[',divpy.actflag,'] not found')
     else iractiondivpy.lookup
     end as option_status,
case when div_my.nildividend= 'y' then 'nildividend'
     when divpy.divtype= 'b' then 'cash & stock'
     when divpy.divtype= 's' then 'stock'
     when divpy.divtype= 'c' then 'cash'
     else 'unspecified'
     end as dividend_type,
divpy.grossdividend as gross_dividend,
divpy.netdividend as net_dividend,
divpy.equalisation,
case when rtrim(divpy.equalisation) <> '' and divpy.group2grossdiv = '' then '0'
     else divpy.group2grossdiv
     end as group2_gross,
case when rtrim(divpy.equalisation) <> '' and divpy.group2netdiv = '' then '0'
     else divpy.group2netdiv
     end as group2_net,
case when divpy.curencd is null then ''
     else divpy.curencd
     end as currency,
case when divpy.recindcashdiv= 't' then 'yes'
     else ''
     end as cash_dividend_rescinded,
divpy.taxrate,
case when divpy.approxflag= 't' then 'yes'
     else ''
     end as approximate_dividend,
divpy.usdratetocurrency as usd_rate_to_currency,
case when divpy.fractions is null then ''
     when irfractions.lookup is null and divpy.fractions <> ''
     then concat('[',divpy.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
divpy.coupon as coupon,
divpy.couponid as coupon_id,
divpy.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
ressedol.sedol as resultant_sedol,
case when divpy.rationew is null or divpy.rationew = ''
     then ''
     else concat(divpy.rationew,':',divpy.ratioold)
     end as ratio,
exdt.paydate2 as stock_pay_date,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'div|divid|divnotes' as link_notes,
scmst.issid,
scmst.secid,
rd.rdid
from div_my
inner join rd on div_my.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid and 'MF'= scmst.sectycd
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid and scexh.exchgcd = exdt.exchgcd and 'DIV' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid and scmst.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
left outer join divpy on div_my.divid = divpy.divid
left outer join scmst as resscmst on divpy.ressecid = resscmst.secid
left outer join sedol on scmst.secid = sedol.secid
       and exchg.cntrycd = sedol.cntrycd
       and 'f' = sedol.defunct
       and 'd' <> sedol.actflag
left outer join sedol as ressedol on divpy.ressecid = ressedol.secid
       and exchg.cntrycd = ressedol.cntrycd
       and sedol.rcntrycd = ressedol.rcntrycd
       and sedol.curencd = ressedol.curencd
       and 'd' <> ressedol.actflag
left outer join irfractions on divpy.fractions = irfractions.code
left outer join iraction as iractiondivpy on divpy.actflag = iractiondivpy.code
left outer join bbe on scexh.exchgcd = bbe.exchgcd and scmst.secid = bbe.secid and sedol.curencd=bbe.curencd and 'D'<>bbe.actflag
where
div_my.acttime between '2010/07/01' and '2099/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
);
alter table `wca2`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`),
add index `ix_secid`(`secid`),
add index `ix_issid`(`issid`),
add index `ix_changed`(`changed`);
drop table if exists wca2.evf_dividend_unittrust;
rename table wca2.evf_loadingtable to wca2.evf_dividend_unittrust;
insert into wca2.tbl_opslog
select * from wca.tbl_opslog
order by wca.tbl_opslog.acttime desc limit 1;
drop table if exists wca2.evf_adjusted_sos;
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2.evf_adjusted_sos(
select distinct
'Capital Reduction' as event,
wca.caprd.caprdid as eventid,
wca.caprd.effectivedate as event_date,
format(wca.scmst.sharesoutstanding *(wca.caprd.newratio)/wca.caprd.oldratio,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.caprd.newratio," : ",wca.caprd.oldratio),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.caprd
inner join wca.scmst on wca.caprd.secid = wca.scmst.secid
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rd on wca.caprd.rdid = wca.rd.rdid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'SD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'RTS'=wca.rp5.eventtype
where
wca.caprd.effectivedate > (select max(feeddate) from wca.tbl_opslog where seq = 3)and wca.caprd.effectivedate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.caprd.oldratio <> ''
and wca.caprd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null);
insert into wca2.evf_adjusted_sos
select distinct
'Stock Dividend' as event,
wca.divpy.divid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.divpy.rationew + wca.divpy.rationew)/wca.divpy.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.divpy.rationew," : ",wca.divpy.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.div_my
left outer join wca.divpy on div_my.divid = divpy.divid and 1 = divpy.optionid
left outer join wca.divpy as divopt2 on div_my.divid = divopt2.divid and 2 = divopt2.optionid
inner join wca.rd on wca.div_my.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'div'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
divopt2.divid is null and divpy.divtype='S'
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.divpy.ratioold <> ''
and wca.divpy.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Divestment' as event,
wca.dvst.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.dvst.rationew)/wca.dvst.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.dvst.rationew," : ",wca.dvst.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.dvst
inner join wca.rd on wca.dvst.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'dvst'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and (wca.scmst.secid = wca.dvst.ressecid or wca.dvst.ressecid is null)
and wca.scmst.sharesoutstanding <> ''
and wca.dvst.rationew <> ''
and wca.dvst.ratioold <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Distribution' as event,
wca.dist.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.mpay.rationew + wca.mpay.rationew)/wca.mpay.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.mpay.rationew," : ",wca.mpay.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.dist
left outer join wca.mpay on dist.rdid = mpay.eventid and 1 = mpay.optionid and 'DIST'=mpay.sevent
left outer join wca.mpay as mpayser2 on dist.rdid = mpayser2.eventid and 1 = mpayser2.optionid and 2 = mpayser2.serialid and 'DIST'=mpay.sevent
left outer join wca.mpay as mpayopt2 on dist.rdid = mpayopt2.eventid and 2 = mpayopt2.optionid and 'DIST'=mpay.sevent
inner join wca.rd on wca.dist.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'DIST'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
mpayopt2.eventid is null
and mpayser2.eventid is null
and wca.mpay.ratioold <> '' and wca.mpay.rationew <> ''
and (wca.scmst.secid = wca.mpay.ressecid or wca.mpay.ressecid is null)
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),70)
and wca.scmst.sharesoutstanding <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Demerger' as event,
wca.dmrgr.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.mpay.rationew)/wca.mpay.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.mpay.rationew," : ",wca.mpay.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.dmrgr
left outer join wca.mpay on dmrgr.rdid = mpay.eventid and 1 = mpay.optionid and 'DMRGR'=mpay.sevent
left outer join wca.mpay as mpayser2 on dmrgr.rdid = mpayser2.eventid and 1 = mpayser2.optionid and 2 = mpayser2.serialid and 'dmrgr'=mpay.sevent
left outer join wca.mpay as mpayopt2 on dmrgr.rdid = mpayopt2.eventid and 2 = mpayopt2.optionid and 'dmrgr'=mpay.sevent
inner join wca.rd on wca.dmrgr.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.scexh on wca.scmst.secid = wca.scexh.secid
inner join wca.exchg on wca.scexh.exchgcd = wca.exchg.exchgcd
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'DMRGR'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
left outer join wca.rdprt as rp6 on wca.rd.rdid = wca.rp6.rdid and 'BON'=wca.rp6.eventtype
where
mpayopt2.eventid is null
and mpayser2.eventid is null
and wca.mpay.ratioold <> '' and wca.mpay.rationew <> ''
and (wca.scmst.secid = wca.mpay.ressecid or wca.mpay.ressecid is null)
and wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),70)
and wca.scmst.sharesoutstanding <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null
and wca.rp6.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Subdivision' as event,
wca.sd.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.sd.newratio)/wca.sd.oldratio,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.sd.newratio," : ",wca.sd.oldratio),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.sd
inner join wca.rd on wca.sd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'SD'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'RTS'=wca.rp5.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.sd.oldratio <> ''
and wca.sd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Rights' as event,
wca.rts.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.rts.ratioold + wca.rts.rationew)/wca.rts.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.rts.rationew," : ",wca.rts.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.rts
inner join wca.rd on wca.rts.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'rts'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'ENT'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.rts.ratioold <> ''
and wca.rts.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Entitlement' as event,
wca.ent.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.ent.ratioold + wca.ent.rationew)/wca.ent.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.ent.rationew," : ",wca.ent.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.ent
inner join wca.rd on wca.ent.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'ent'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.ent.ratioold <> ''
and wca.ent.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Consolidation' as event,
wca.consd.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.consd.newratio)/wca.consd.oldratio,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.consd.newratio," : ",wca.consd.oldratio),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.consd
inner join wca.rd on wca.consd.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'consd'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'BON'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'ENT'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.consd.oldratio <> ''
and wca.consd.newratio <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null;
insert into wca2.evf_adjusted_sos
select distinct
'Bonus' as event,
wca.bon.rdid as eventid,
wca.exdt.exdate as event_date,
format(wca.scmst.sharesoutstanding *(wca.bon.ratioold + wca.bon.rationew)/wca.bon.ratioold,0) as adjusted_amount,
format(wca.scmst.sharesoutstanding,0) as current_amount,
replace(concat(wca.bon.rationew," : ",wca.bon.ratioold),".0000000","") as ratio,
wca.issur.issuername as issuer_name,
wca.scmst.isin as isin,
wca.scmst.securitydesc as security_description,
wca.scmst.sectycd as security_type,
wca.scmst.secid
from wca.bon
inner join wca.rd on wca.bon.rdid = wca.rd.rdid
inner join wca.scmst on wca.rd.secid = wca.scmst.secid
inner join wca.exdt on wca.rd.rdid = wca.exdt.rdid and wca.scmst.primaryexchgcd = wca.exdt.exchgcd and 'bon'=wca.exdt.eventtype
inner join wca.issur on wca.scmst.issid = wca.issur.issid
left outer join wca.rdprt as rp1 on wca.rd.rdid = wca.rp1.rdid and 'ENT'=wca.rp1.eventtype
left outer join wca.rdprt as rp2 on wca.rd.rdid = wca.rp2.rdid and 'CAPRD'=wca.rp2.eventtype
left outer join wca.rdprt as rp3 on wca.rd.rdid = wca.rp3.rdid and 'CONSD'=wca.rp3.eventtype
left outer join wca.rdprt as rp4 on wca.rd.rdid = wca.rp4.rdid and 'RTS'=wca.rp4.eventtype
left outer join wca.rdprt as rp5 on wca.rd.rdid = wca.rp5.rdid and 'SD'=wca.rp5.eventtype
where
wca.exdt.exdate > (select max(feeddate) from wca.tbl_opslog where seq = 3) and wca.exdt.exdate < adddate((select max(feeddate) from wca.tbl_opslog where seq = 3),365)
and wca.scmst.sharesoutstanding <> ''
and wca.bon.ratioold <> ''
and wca.bon.rationew <> ''
and wca.rp1.rdid is null
and wca.rp2.rdid is null
and wca.rp3.rdid is null
and wca.rp4.rdid is null
and wca.rp5.rdid is null;
ALTER TABLE `wca2`.`evf_adjusted_sos` ADD PRIMARY KEY (`event`, `eventid`);
