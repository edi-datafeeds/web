drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2arch.evf_loadingtable(
select distinct
'dividend' as event,
case when divpy.optionid is not null and divpy.OptionSerialNo is not null
 then concat(lpad(scexh.scexhid,7,'0'),lpad(divpy.optionid,2,'0'),lpad(divpy.OptionSerialNo,2,'0'),div_my.divid)
 when divpy.optionid is not null
 then concat(lpad(scexh.scexhid,7,'0'),lpad(divpy.optionid,2,'0'),'01',div_my.divid)
 else concat(lpad(scexh.scexhid,7,'0'),'0101',div_my.divid)
 end as caref,
div_my.divid as eventid,
div_my.announcedate as created,
case when (divpy.acttime is not null) and (divpy.acttime > div_my.acttime) and (divpy.acttime > rd.acttime) and (divpy.acttime > exdt.acttime) and (divpy.acttime > pexdt.acttime) then divpy.acttime
     when (rd.acttime is not null) and (rd.acttime > div_my.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > div_my.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > div_my.acttime) then pexdt.acttime
     else div_my.acttime
     end as changed,
case when div_my.actflag = 'U' then 'Updated'
     when div_my.actflag = 'I' then 'Inserted'
     when div_my.actflag = 'D' then 'Deleted'
     when div_my.actflag = 'C' then 'Cancelled'
     when div_my.actflag = 'U' then 'Updated'
     when div_my.actflag = 'I' then 'Inserted'
     when div_my.actflag = 'D' then 'Deleted'
     when div_my.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
scmst.statusflag as status_flag,
scmst.sectycd as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
case when sbx.secid is not null then sbx.sedol when sbc.secid is not null then sbc.sedol else '' end as sedol,
case when sbx.secid is not null then sbx.rcntrycd when sbc.secid is not null then sbc.rcntrycd else '' end as sedol_register_country,
case when sbx.secid is not null then sbx.curencd when sbc.secid is not null then sbc.curencd else '' end as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
scexh.exchgcd as exchange_code,
exchg.mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
case when sbx.secid is not null then sbx.bbgcompid when sbc.secid is not null then sbc.bbgcompid else '' end as composite_global_id,
case when sbx.secid is not null then sbx.bbgcomptk when sbc.secid is not null then sbc.bbgcomptk else '' end as bloomberg_composite_ticker,
case when sbx.secid is not null then sbx.bbgexhid else '' end as bloomberg_global_id,
case when sbx.secid is not null then sbx.bbgexhtk else '' end as bloomberg_exchange_ticker,
scexh.liststatus as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
rd.registrationdate as registration_date,
case when divpy.divtype= 's' and exdt.rdid is not null and exdt.acttime>'2014-07-21'
     then exdt.exdate2
     when divpy.divtype= 's' and pexdt.rdid is not null and pexdt.acttime>'2014-07-21'
     then pexdt.exdate2
     when exdt.rdid is not null
     then exdt.exdate
     else pexdt.exdate
     end as ex_date,
case when divpy.divtype= 's' and exdt.rdid is not null and exdt.acttime>'2014-07-21'
     then exdt.paydate2
     when divpy.divtype= 's' and pexdt.rdid is not null and pexdt.acttime>'2014-07-21'
     then pexdt.paydate2
     when exdt.rdid is not null
     then exdt.paydate
     else pexdt.paydate
     end as pay_date,
case when divpy.divtype= 'b' and exdt.rdid is not null and exdt.acttime>'2014-07-21'
     then exdt.exdate2
     when divpy.divtype= 'b' and pexdt.rdid is not null and pexdt.acttime>'2014-07-21'
     then pexdt.exdate2
     when divpy.divtype= 'b' and exdt.rdid is not null and exdt.acttime<'2014-07-21'
     then exdt.exdate
     when divpy.divtype= 'b' and pexdt.rdid is not null and pexdt.acttime<'2014-07-21'
     then pexdt.exdate
     else null
     end as ex_date_stock_with_cash,
case when divpy.divtype= 'b' and exdt.rdid is not null and exdt.acttime>'2014-07-21'
     then exdt.paydate2
     when divpy.divtype= 'b' and pexdt.rdid is not null and pexdt.acttime>'2014-07-21'
     then pexdt.paydate2
     when divpy.divtype= 'b' and exdt.rdid is not null and exdt.acttime<'2014-07-21'
     then exdt.paydate
     when divpy.divtype= 'b' and pexdt.rdid is not null and pexdt.acttime<'2014-07-21'
     then pexdt.paydate
     else null
     end as pay_date_stock_with_cash,
div_my.fyedate as fin_year_date,
case when div_my.marker= 'fnl' then 'Final'
     when div_my.divperiodcd= 'spl' then 'special'
     when div_my.divperiodcd= 'fnl' then 'final'
     when div_my.frequency= 'mnt' then 'monthly'
     when div_my.frequency= 'sma' then 'semi-annual'
     when div_my.frequency= 'ins' then 'installment'
     when div_my.frequency= 'int' then 'interim'
     when div_my.frequency= 'qtr' then 'quarterly'
     when div_my.frequency= 'fnl' then 'final'
     when div_my.frequency= 'anl' then 'annual'
     when div_my.frequency= 'reg' then 'regular'
     when div_my.frequency= 'un'  then 'unspecified'
     when div_my.frequency= 'bim' then 'bi-monthly'
     when div_my.frequency= 'spl' then 'special'
     when div_my.frequency= 'trm' then 'trimesterly'
     when div_my.frequency= 'mem' then 'memorial'
     when div_my.frequency= 'sup' then 'supplemental'
     when div_my.frequency= 'isc' then 'interest on sgc'
     when div_my.divperiodcd= 'mnt' then 'monthly'
     when div_my.divperiodcd= 'sma' then 'semi-annual'
     when div_my.divperiodcd= 'ins' then 'installment'
     when div_my.divperiodcd= 'int' then 'interim'
     when div_my.divperiodcd= 'qtr' then 'quarterly'
     when div_my.divperiodcd= 'anl' then 'annual'
     when div_my.divperiodcd= 'reg' then 'regular'
     when div_my.divperiodcd= 'un'  then 'unspecified'
     when div_my.divperiodcd= 'bim' then 'bi-monthly'
     when div_my.divperiodcd= 'trm' then 'trimesterly'
     when div_my.divperiodcd= 'mem' then 'memorial'
     when div_my.divperiodcd= 'sup' then 'supplemental'
     when div_my.divperiodcd= 'isc' then 'interest on sgc'
     else 'regular'
     end as dividend_period,
frank.frankid as subevent_link,
drip.dripid as drip_link,
case when div_my.tbaflag= 't' then 'yes'
     else ''
     end as to_be_announced,
case when div_my.nildividend= 't' then 'yes'
     else ''
     end as nil_dividend,
case when optionid is not null
     then lpad(optionid,2,'0')
     else ''
     end as option_key,
case when divpy.defaultopt = 't' then 'yes'
     else ''
     end as default_option,
divpy.optelectiondate as option_election_date,
iractiondivpy.lookup as option_status,
case when div_my.nildividend= 'y' then 'nildividend'
     when divpy.divtype= 'b' then 'cash & stock'
     when divpy.divtype= 's' then 'stock'
     when divpy.divtype= 'c' then 'cash'
     else 'unspecified'
     end as dividend_type,
case when divpy.recindcashdiv='t' then '0' else divpy.grossdividend
     end as gross_dividend,
case when divpy.recindcashdiv='t' then '0' else divpy.netdividend
     end as net_dividend,
case when divpy.curencd is null then ''
     else divpy.curencd
     end as currency,
case when div_my.declcurencd is null then ''
     else div_my.declcurencd
     end as declared_currency,
case when div_my.declgrossamt is null then ''
     else div_my.declgrossamt
     end as declared_gross_amount,
case when divpy.recindcashdiv= 't' then concat('yes: gross was: ',divpy.grossdividend,' net was: ',divpy.netdividend,' as on changed date')
     else ''
     end as cash_dividend_rescinded,
divpy.taxrate as tax_rate,
divpy.depfees as depository_fees,
divpy.divrate as dividend_as_percent,
divpy.comfee as commission_fee,
divpy.comaspercent as commission_fee_as_percent,
divpy.stampduty as stamp_duty_fee,
divpy.stampaspercent as stamp_duty_fee_as_percent,
case when divpy.approxflag= 't' then 'yes'
     else ''
     end as approximate_dividend,
divpy.usdratetocurrency as usd_rate_to_currency,
case when divpy.fractions = '' then ''
     when irfractions.lookup is null and divpy.fractions <> ''
     then concat('[',divpy.fractions,'] not found')
     else irfractions.lookup
     end as fractions,
divpy.coupon as coupon,
divpy.couponid as coupon_id,
divpy.ressecid as resultant_secid,
resscmst.isin as resultant_isin,
'' as resultant_sedol,
case when divpy.rationew is null or divpy.rationew = ''
     then ''
     else concat(divpy.rationew,':',divpy.ratioold)
     end as ratio,
exdt.paydate2 as stock_pay_date,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'div|divid|divnotes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid,
rd.rdid
from div_my
inner join rd on div_my.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join sectygrp on scmst.sectycd = sectygrp.sectycd and 3>sectygrp.secgrpid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid
       and scexh.exchgcd = exdt.exchgcd and 'DIV' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid
       and scmst.primaryexchgcd = pexdt.exchgcd and 'DIV' = pexdt.eventtype
left outer join divpy on div_my.divid = divpy.divid
                   and 'D' <> divpy.actflag
left outer join drip on div_my.divid = drip.divid
                   and exchg.cntrycd=drip.cntrycd
left outer join frank on div_my.divid = frank.divid
                   and exchg.cntrycd=frank.cntrycd
LEFT OUTER JOIN DRIP ON div_my.DivID = DRIP.DivID and exchg.CntryCD = DRIP.CntryCD
LEFT OUTER JOIN FRANK ON div_my.DivID = FRANK.DivID and exchg.CntryCD = FRANK.CntryCD
left outer join scmst as resscmst on divpy.ressecid = resscmst.secid
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
left outer join wca2.sedolbbg as sbc on scmst.secid = sbc.secid
                   and '' = sbc.exchgcd
                   and exchg.cntrycd = sbc.cntrycd
left outer join irfractions on divpy.fractions = irfractions.code
left outer join iraction as iractiondivpy on divpy.actflag = iractiondivpy.code
where
div_my.acttime < '2015/01/01'
and ((recdate >= scexh.listdate and scexh.listdate is not null) or recdate>=scexh.announcedate or recdate is null)
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
and ifnull(divpy.optionserialno,1)=1
and scexh.actflag<>'D'
and div_my.actflag<>'D'
);
alter table `wca2arch`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`trading_currency`,`sedol_register_country`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`);
drop table if exists wca2arch.evf_dividend;
rename table wca2arch.evf_loadingtable to wca2arch.evf_dividend;
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2arch.evf_loadingtable(
SELECT DISTINCT
case when DP.optionid is not null and DP.OptionSerialNo is not null
 then concat(lpad(SC.scexhid,7,'0'),lpad(DP.optionid,2,'0'),lpad(DP.OptionSerialNo,2,'0'),DM.divid)
 when DP.optionid is not null
 then concat(lpad(SC.scexhid,7,'0'),lpad(DP.optionid,2,'0'),'01',DM.divid)
 else concat(lpad(SC.scexhid,7,'0'),'0101',DM.divid)
 end as caref,
DM.divid as eventid,
S.SecID,
SC.ExchgCD,
DM.divid AS Distrib,
DP.OptionID As Element,
DP.actflag As Action,
DM.announcedate as created,
DP.acttime As Changed,
case when SD.sedol is null then '' else SD.sedol end As Sedol,
I.CntryofIncorp As CntryofIncorp,
I.IssuerName As IssuerName,
S.sectycd As SecType,
S.SecurityDesc As Security,
S.Isin As Isin,
case when SD.sedol is null then '' else SD.curencd end As UnitofQuoteCurr,
E.ExDate As ExDate,
R.Recdate As RecDate,
E.PayDate As PayDate,
DP.GrossDividend As GrossRate,
DP.TaxRate,
DP.NetDividend As NetRate,
DP.Divtype As DivType,
case when DM.marker<>'' then DM.marker else DM.Divperiodcd end As Marker,
DP.DivType As StockType,
DP.DivType As EventType,
DP.CurenCD As OptionCurr,
case when RatioOld<>'' then CONCAT(DP.RatioOld,':',DP.RatioNew) else '' end As Ratio,
DP.OptElectionDate As OptionAcceptDate,
DP.TaxRate As UkTaxCurr,
DP.DivType As RateType,
XD.snpdate As Snpdate,
XD.sfpdate As Sfpdate,
DP.FXDate As CalculDate,
DM.FYEDate As PeriodStartDate,
DM.PeriodEndDate As PeriodEndDate,
DM.declarationdate As AnnounceDate,
XD.calculationdate As ConversionDate,
XD.curelecflag As ConversionStatus,
DP.stampaspercent As MaxPrice,
DP.ComAsPercent As Quantity,
SC.LocalCode,
'C' AS Confirmation,
case when XD.Detail_ID is null then 0 else XD.Detail_ID end as Detail_ID
From wca.div_my AS DM
INNER JOIN wca.rd AS R on DM.RdID=R.RdID
INNER JOIN wca.scmst As S on R.SecID = S.SecID
INNER JOIN wca.sectygrp AS SG on S.SectyCD =SG.SectyCD And SG.SecGrpId < 3
INNER JOIN wca.issur AS I on S.IssID=I.IssID
INNER JOIN wca.scexh AS SC on S.SecID=SC.SecID
INNER JOIN wca.exchg on SC.ExchgCD=wca.exchg.exchgcd
LEFT OUTER JOIN wca.sedol AS SD on S.SecID=SD.SecID AND 'GB'=SD.CntryCD and 'D'<>SD.actflag
LEFT OUTER JOIN wca.exdt AS E On R.RdID=E.RdID and SC.ExchgCD=E.ExchgCD and 'DIV'=E.eventtype
LEFT OUTER JOIN wca.divpy AS DP on DM.DivID = DP.DivID
left outer join xdes.detail AS XD on S.SecID = XD.secid AND R.Recdate=XD.Recdate
                       and case when DM.marker <>'' then DM.Marker else DM.divperiodcd end
                       = case when XD.marker <>'' then XD.Marker else XD.divperiodcd end
where
wca.exchg.cntrycd='GB'
and DM.acttime < '2015/01/01'
and DM.actflag<>'D'
and (R.recdate < SC.acttime or SC.liststatus<>'D' or R.recdate is null)
and SC.actflag<>'D'
and (XD.actflag<>'D' or XD.actflag is null)
);
alter table `wca2arch`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`sedol`,`UnitofQuoteCurr`,`detail_id`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_sedol`(`sedol`),
add index `ix_exchgcd`(`ExchgCD`);
drop table if exists wca2arch.ukd_Dividend;
rename table wca2arch.evf_loadingtable to wca2arch.ukd_Dividend;
drop table if exists wca2arch.evf_loadingtable;
use wca;
create table wca2arch.evf_loadingtable(
select distinct
'interest_dividend' as event,
case when intpy.optionid is not null
     then concat(lpad(scexh.scexhid,7,'0'),lpad(intpy.optionid,2,'0'),'01',int_my.rdid)
     else concat(lpad(scexh.scexhid,7,'0'),'0101',int_my.rdid)
     end as caref,
int_my.rdid as eventid,
int_my.announcedate as created,
case when (intpy.acttime is not null) and (intpy.acttime > int_my.acttime) and (intpy.acttime > rd.acttime) and (intpy.acttime > exdt.acttime) and (intpy.acttime > pexdt.acttime) then intpy.acttime
     when (rd.acttime is not null) and (rd.acttime > int_my.acttime) and (rd.acttime > exdt.acttime) and (rd.acttime > pexdt.acttime) then rd.acttime
     when (exdt.acttime is not null) and (exdt.acttime > int_my.acttime) and (exdt.acttime > pexdt.acttime) then exdt.acttime
     when (pexdt.acttime is not null) and (pexdt.acttime > int_my.acttime) then pexdt.acttime
     else int_my.acttime
     end as changed,
case when int_my.actflag = 'U' then 'Updated'
     when int_my.actflag = 'I' then 'Inserted'
     when int_my.actflag = 'D' then 'Deleted'
     when int_my.actflag = 'C' then 'Cancelled'
     when intpy.actflag = 'U' then 'Updated'
     when intpy.actflag = 'I' then 'Inserted'
     when intpy.actflag = 'D' then 'Deleted'
     when intpy.actflag = 'C' then 'Cancelled'
     else '' end as actflag,
issur.cntryofincorp as country_of_incorporation,
issur.issuername as issuer_name,
scmst.securitydesc as security_description,
scmst.parvalue as par_value,
scmst.curencd as par_value_currency,
case when scmst.StatusFlag='' then upper('A') else scmst.StatusFlag end as status_flag,
case when wca.scmst.SectyCD = 'BND' then 'PRF' else wca.scmst.SectyCD end as security_type,
scmst.structcd as security_structure,
scmst.isin,
scmst.uscode as us_code,
ifnull(sbx.sedol,'') as sedol,
ifnull(sbx.rcntrycd,'') as sedol_register_country,
ifnull(sbx.curencd,'') as trading_currency,
case when scexh.exchgcd is null or scmst.primaryexchgcd is null or scmst.primaryexchgcd='' then ''
     when scmst.primaryexchgcd=scexh.exchgcd then 'yes'
     else 'no'
     end as primary_exchange,
case when scexh.ExchgCD = 'USTRCE' then 'USNYSE' else scexh.ExchgCD end as exchange_code,
case when exchg.Mic = 'FINR' then 'XNYS' else exchg.Mic end as mic,
exchg.cntrycd as listing_country,
scexh.localcode as local_code,
ifnull(sbx.bbgcompid,'') as composite_global_id,
ifnull(sbx.bbgcomptk,'') as bloomberg_composite_ticker,
ifnull(sbx.bbgexhid,'') as bloomberg_global_id,
ifnull(sbx.bbgexhtk,'') as bloomberg_exchange_ticker,
case when wca.scexh.ListStatus='N' or wca.scexh.ListStatus='' then upper('L') else wca.scexh.ListStatus end as listing_status,
scexh.listdate as list_date,
rd.recdate as record_date,
CASE WHEN exdt.rdid IS NOT NULL and (intpy.inttype='B' or intpy.inttype='C')
     THEN exdt.ExDate
     WHEN exdt.rdid IS NOT NULL and intpy.inttype='S'
     THEN exdt.ExDate2
     WHEN pexdt.rdid IS NOT NULL and (intpy.inttype='B' or intpy.inttype='C')
     THEN pexdt.ExDate
     WHEN pexdt.rdid IS NOT NULL and intpy.inttype='S'
     THEN pexdt.ExDate2
     ELSE pexdt.exdate END as ex_date,
CASE WHEN exdt.rdid IS NOT NULL and (intpy.inttype='B' or intpy.inttype='C')
     THEN exdt.Paydate
     WHEN exdt.rdid IS NOT NULL and intpy.inttype='S'
     THEN exdt.Paydate2
     WHEN pexdt.rdid IS NOT NULL and (intpy.inttype='B' or intpy.inttype='C')
     THEN pexdt.Paydate
     WHEN pexdt.rdid IS NOT NULL and intpy.inttype='S'
     THEN pexdt.Paydate2
     ELSE pexdt.Paydate END as pay_date,
'' as to_be_announced,
case when int_my.nilint= 't' then 'yes'
     else ''
     end as nil_dividend,
case when intpy.optionid is not null
     then lpad(intpy.optionid,2,'0')
     else ''
     end as option_key,
case when intpy.defaultopt = 't' then 'yes'
     else ''
     end as default_option,
iractionintpy.lookup as option_status,
case when int_my.nilint= 'Y' then 'nil'
     when intpy.inttype= 'B' then 'cash & stock'
     when intpy.inttype= 'S' then 'stock'
     when intpy.inttype= 'C' then 'cash'
     else 'unspecified'
     end as dividend_type,
intpy.grossinterest*scmst.parvalue as gross_dividend,
case when intpy.curencd is null then ''
     else intpy.curencd
     end as currency,
case when intpy.rescindinterest= 'T' then 'yes'
     else ''
     end as cash_dividend_rescinded,
intpy.DomesticTaxRate as tax_rate,
intpy.intrate as dividend_as_percent,
intpy.couponno as coupon_ID,
'Mandatory' as choice,
'rd|rdid|rdnotes' as link_record_date_notes,
'int|rdid|intnotes' as link_notes,
scmst.issid,
scmst.secid,
scexh.mktsgid,
rd.rdid
from int_my
inner join rd on int_my.rdid = rd.rdid
inner join bond on rd.secid = bond.secid
inner join scmst on rd.secid = scmst.secid
inner join issur on scmst.issid = issur.issid
inner join scexh on scmst.secid = scexh.secid
inner join exchg on scexh.exchgcd = exchg.exchgcd
left outer join exdt on rd.rdid = exdt.rdid
       and scexh.exchgcd = exdt.exchgcd and 'INT' = exdt.eventtype
left outer join exdt as pexdt on rd.rdid = pexdt.rdid
       and scmst.primaryexchgcd = pexdt.exchgcd and 'INT' = pexdt.eventtype
left outer join intpy on int_my.rdid = intpy.rdid
                   and 'D' <> intpy.actflag
left outer join wca2.sedolbbg as sbx on scmst.secid = sbx.secid
                   and exchg.exchgcd = sbx.exchgcd
left outer join iraction as iractionintpy on intpy.actflag = iractionintpy.code
where
int_my.actflag<>'D'
and int_my.acttime<'2015/01/01'
and ((wca.scexh.liststatus<>'D' or wca.scexh.acttime>int_my.announcedate) and wca.scexh.actflag<>'D')
and (wca.scmst.sectycd='BND' and (ord(substring(wca.scmst.uscode,7,1)) between 48 and 57 and ord(substring(wca.scmst.uscode,8,1)) between 48 and 57))
and wca.exchg.cntrycd='US'
and (recdate < scexh.acttime or scexh.liststatus<>'D' or recdate is null)
and scexh.actflag<>'D'
);
alter table `wca2arch`.`evf_loadingtable`
modify caref bigint,
add primary key (`caref`,`trading_currency`,`sedol_register_country`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`),
add index `ix_isin`(`isin`),
add index `ix_uscode`(`us_code`),
add index `ix_sedol`(`sedol`),
add index `ix_exchgcd`(`exchange_code`);
drop table if exists wca2arch.evf_interest_dividend;
rename table wca2arch.evf_loadingtable to wca2arch.evf_interest_dividend;
