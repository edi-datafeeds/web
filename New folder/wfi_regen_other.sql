drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
rd.recdate as record_date,
exdt.exdate as ex_date,
exdt.paydate as pay_date,
cosnt.expirydate as expiry_date,
cosnt.expirytime as expiry_time,
SUBSTRING(cosnt.TimeZone, 1,3) AS Time_Zone,
case when ynblank.lookup is null then cosnt.collateralrelease else ynblank.lookup end as collateral_release,
cosnt.currency as redemption_currency,
cosnt.fee as fee,
'wca.dbo.cosnt|rdid|notes' as link_notes,
cosnt.actflag,
cosnt.acttime as changed,
cosnt.announcedate as created,
cosnt.rdid as firef,
rd.secid,
rd.rdid
from cosnt
inner join rd on cosnt.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
left outer join exdt on rd.rdid = exdt.rdid and scmst.primaryexchgcd = exdt.exchgcd and 'cosnt' = exdt.eventtype
left outer join wfi.sys_lookup as ynblank on cosnt.collateralrelease = ynblank.typegroup
                      and 'ynblank' = ynblank.code
where
rd.secid in (select secid from wfi.reference)
and cosnt.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfi`.loadingtable
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.consent;
rename table wfi.loadingtable to wfi.consent;
--
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
rd.recdate as record_date,
exdt.exdate as ex_date,
exdt.paydate as pay_date,
cosnt.expirydate as expiry_date,
cosnt.expirytime as expiry_time,
cosnt.timezone as time_zone,
case when ynblank.lookup is null then cosnt.collateralrelease else ynblank.lookup end as collateral_release,
cosnt.currency as redemption_currency,
cosnt.fee as fee,
'wca.dbo.cosnt|rdid|notes' as link_notes,
cosnt.actflag,
cosnt.acttime as changed,
cosnt.announcedate as created,
cosnt.rdid as firef,
rd.secid,
rd.rdid
from cosnt
inner join rd on cosnt.rdid = rd.rdid
inner join scmst on rd.secid = scmst.secid
inner join bond on scmst.secid = bond.secid
left outer join exdt on rd.rdid = exdt.rdid and scmst.primaryexchgcd = exdt.exchgcd and 'cosnt' = exdt.eventtype
left outer join wfimatured.sys_lookup as ynblank on cosnt.collateralrelease = ynblank.typegroup
                      and 'ynblank' = ynblank.code
where
rd.secid in (select secid from wfimatured.reference)
and cosnt.actflag<>'d'
and eventtype<>'CLEAN'
and eventtype<>'CORR'
);
alter table `wfimatured`.loadingtable
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.consent;
rename table wfimatured.loadingtable to wfimatured.consent;
--
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
liq.liquidator as liquidator,
liq.liqadd1 as address1,
liq.liqadd2 as address2,
liq.liqadd3 as address3,
liq.liqadd4 as address4,
liq.liqadd5 as address5,
liq.liqadd6 as address6,
liq.liqcity as city,
case when cntry.lookup is null then liq.liqcntrycd else cntry.lookup end as cntry,
liq.liqtel as telephone,
liq.liqfax as fax,
liq.liqemail as email,
liq.rddate as record_date,
'wca.dbo.liq|liqid|liquidationterms' as link_notes,
liq.actflag,
liq.acttime as changed,
liq.announcedate as created,
concat(lpad(liq.liqid,7,'0'),scmst.secid) as firef,
scmst.secid,
liq.liqid
from liq   
inner join scmst on liq.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
left outer join wfi.sys_lookup as cntry on liq.liqcntrycd = cntry.typegroup
                      and 'cntry' = cntry.code
where
bond.secid in (select secid from wfi.reference)
and bond.bondtype <> 'prf' and bond.actflag<>'d'
and liq.actflag<>'d'
);
alter table `wfi`.loadingtable
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.liquidation;
rename table wfi.loadingtable to wfi.liquidation;
--
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
liq.liquidator as liquidator,
liq.liqadd1 as address1,
liq.liqadd2 as address2,
liq.liqadd3 as address3,
liq.liqadd4 as address4,
liq.liqadd5 as address5,
liq.liqadd6 as address6,
liq.liqcity as city,
case when cntry.lookup is null then liq.liqcntrycd else cntry.lookup end as cntry,
liq.liqtel as telephone,
liq.liqfax as fax,
liq.liqemail as email,
liq.rddate as record_date,
'wca.dbo.liq|liqid|liquidationterms' as link_notes,
liq.actflag,
liq.acttime as changed,
liq.announcedate as created,
concat(lpad(liq.liqid,7,'0'),scmst.secid) as firef,
scmst.secid,
liq.liqid
from liq   
inner join scmst on liq.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
left outer join wfi.sys_lookup as cntry on liq.liqcntrycd = cntry.typegroup
                      and 'cntry' = cntry.code
where
bond.secid in (select secid from wfimatured.reference)
and bond.bondtype <> 'prf' and bond.actflag<>'d'
and liq.actflag<>'d'
);
alter table `wfimatured`.loadingtable
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.liquidation;
rename table wfimatured.loadingtable to wfimatured.liquidation;
--
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
bkrp.notificationdate as notification_date,
bkrp.filingdate as filing_date,
'wca.dbo.bkrp|bkrpid|bkrpnotes' as link_notes,
bkrp.actflag,
bkrp.acttime as changed,
bkrp.announcedate as created,
concat(lpad(bkrp.bkrpid,7,'0'),scmst.secid) as firef,
scmst.secid,
scmst.issid,
bkrp.bkrpid
from bkrp
inner join scmst on bkrp.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
bond.secid in (select secid from wfi.reference)
and bond.bondtype <> 'prf' and bond.actflag<>'d'
and bkrp.actflag<>'d'
);
alter table `wfi`.loadingtable
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.bankruptcy;
rename table wfi.loadingtable to wfi.bankruptcy;
--
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
bkrp.notificationdate as notification_date,
bkrp.filingdate as filing_date,
'wca.dbo.bkrp|bkrpid|bkrpnotes' as link_notes,
bkrp.actflag,
bkrp.acttime as changed,
bkrp.announcedate as created,
concat(lpad(bkrp.bkrpid,7,'0'),scmst.secid) as firef,
scmst.secid,
scmst.issid,
bkrp.bkrpid
from bkrp
inner join scmst on bkrp.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
bond.secid in (select secid from wfimatured.reference)
and bond.bondtype <> 'prf' and bond.actflag<>'d'
and bkrp.actflag<>'d'
);
alter table `wfimatured`.loadingtable
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.bankruptcy;
rename table wfimatured.loadingtable to wfimatured.bankruptcy;
--
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
lawst.effectivedate as effective_date,
case when lawst.latype= 'clsact' then 'class action' else 'other' end as legal_action_type,
lawst.regdate as registration_date,
'wca.dbo.lawst|lawstid|lawstnotes' as link_notes,
lawst.actflag,
lawst.acttime as changed,
lawst.announcedate as created,
concat(lpad(lawst.lawstid,7,'0'),scmst.secid) as firef,
scmst.secid,
lawst.lawstid
from lawst
inner join scmst on lawst.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
bond.secid in (select secid from wfi.reference)
and bond.bondtype <> 'prf' and bond.actflag<>'d'
and lawst.actflag<>'d'
);
alter table `wfi`.loadingtable
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.legal_action;
rename table wfi.loadingtable to wfi.legal_action;
--
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
lawst.effectivedate as effective_date,
case when lawst.latype= 'clsact' then 'class action' else 'other' end as legal_action_type,
lawst.regdate as registration_date,
'wca.dbo.lawst|lawstid|lawstnotes' as link_notes,
lawst.actflag,
lawst.acttime as changed,
lawst.announcedate as created,
concat(lpad(lawst.lawstid,7,'0'),scmst.secid) as firef,
scmst.secid,
lawst.lawstid
from lawst
inner join scmst on lawst.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
where
bond.secid in (select secid from wfimatured.reference)
and bond.bondtype <> 'prf' and bond.actflag<>'d'
and lawst.actflag<>'d'
);
alter table `wfimatured`.loadingtable
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.legal_action;
rename table wfimatured.loadingtable to wfimatured.legal_action;
--
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select 
agm.agmdate as agm_date,
case when agmegm.lookup is null then agm.agmegm else agmegm.lookup end as meeting_type,
agm.agmno as agm_number,
agm.fyedate as fin_year_end_date,
case when agm.agmtime=':' then '' else agm.agmtime end as agm_time,
agm.add1 as address1,
agm.add2 as address2,
agm.add3 as address3,
agm.add4 as address4,
agm.add5 as address5,
agm.add6 as address6,
agm.city as city,
case when cntry.lookup is null then agm.cntrycd else cntry.lookup end as cntry,
agm.actflag,
agm.acttime as changed,
agm.announcedate as created,
agm.agmid as firef,
agm.bondsecid as secid
from agm
inner join bond on agm.bondsecid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as agmegm on agm.agmegm = agmegm.typegroup
                      and 'agmegm' = agmegm.code
left outer join wfi.sys_lookup as cntry on agm.cntrycd = cntry.typegroup
                      and 'cntry' = cntry.code
where
bond.secid in (select secid from wfi.reference)
and bond.bondtype <> 'prf' and bond.actflag<>'d' and bondsecid is not null and bondsecid>0
and agm.actflag<>'d'
);
alter table `wfi`.loadingtable
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.bondholder_meeting;
rename table wfi.loadingtable to wfi.bondholder_meeting;
--
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select 
agm.agmdate as agm_date,
case when agmegm.lookup is null then agm.agmegm else agmegm.lookup end as meeting_type,
agm.agmno as agm_number,
agm.fyedate as fin_year_end_date,
case when agm.agmtime=':' then '' else agm.agmtime end as agm_time,
agm.add1 as address1,
agm.add2 as address2,
agm.add3 as address3,
agm.add4 as address4,
agm.add5 as address5,
agm.add6 as address6,
agm.city as city,
case when cntry.lookup is null then agm.cntrycd else cntry.lookup end as cntry,
agm.actflag,
agm.acttime as changed,
agm.announcedate as created,
agm.agmid as firef,
agm.bondsecid as secid
from agm
inner join bond on agm.bondsecid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join wfi.sys_lookup as agmegm on agm.agmegm = agmegm.typegroup
                      and 'agmegm' = agmegm.code
left outer join wfi.sys_lookup as cntry on agm.cntrycd = cntry.typegroup
                      and 'cntry' = cntry.code
where
bond.secid in (select secid from wfimatured.reference)
and bond.bondtype <> 'prf' and bond.actflag<>'d' and bondsecid is not null and bondsecid>0
and agm.actflag<>'d'
);
alter table `wfimatured`.loadingtable
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.bondholder_meeting;
rename table wfimatured.loadingtable to wfimatured.bondholder_meeting;
--
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select
sachg.EffectiveDate as Effective_Date,
OLDAGNCY.RegistrarName as Old_Agency_Name,
NEWAGNCY.RegistrarName as New_Agency_Name,
OLDAGNCY.CntryCD as Old_Agency_Country,
NEWAGNCY.CntryCD as New_Agency_Country,
sachg.OldSpStartDate as Old_Surety_Start_Date,
sachg.NewSpStartDate as New_Surety_Start_Date,
sachg.OldSpEndDate as Old_Surety_End_Date,
sachg.NewSpEndDate as New_Surety_End_Date,
case when OLDRELATION.Lookup is null then sachg.OldRelationship else OLDRELATION.Lookup end as Old_Relationship,
case when NEWRELATION.Lookup is null then sachg.NewRelationship else NEWRELATION.Lookup end as New_Relationship,
case when OLDGTYPE.Lookup is null then sachg.OldGuaranteeType else OLDGTYPE.Lookup end as Old_Guarantee_Type,  
case when NEWGTYPE.Lookup is null then sachg.NewGuaranteeType else NEWGTYPE.Lookup end as New_Guarantee_Type,
sachg.Actflag,
sachg.Acttime as changed,
sachg.AnnounceDate as created,
sachg.sachgID as firef,
scagy.secid
from sachg
inner join scagy on sachg.scagyid = scagy.scagyid
inner join bond on scagy.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join agncy as OLDAGNCY on sachg.OldAgncyID = OLDAGNCY.agncyid
left outer join agncy as NEWAGNCY on sachg.OldAgncyID = NEWAGNCY.agncyid
left outer join wfi.sys_lookup as OLDRELATION on sachg.oldrelationship = OLDRELATION.Code
                      and 'RELATION' = OLDRELATION.TypeGroup
left outer join wfi.sys_lookup as NEWRELATION on sachg.newrelationship = NEWRELATION.Code
                      and 'RELATION' = NEWRELATION.TypeGroup
left outer join wfi.sys_lookup as OLDGTYPE on sachg.OldGuaranteeType = OLDGTYPE.Code
                      and 'GTYPE' = OLDGTYPE.TypeGroup
left outer join wfi.sys_lookup as NEWGTYPE on sachg.NewGuaranteeType = NEWGTYPE.Code
                      and 'GTYPE' = NEWGTYPE.TypeGroup
where
bond.secid in (select secid from wfi.reference)
);
alter table `wfi`.`loadingtable`
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfi.agency_change;
rename table wfi.loadingtable to wfi.agency_change;
--
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select
sachg.EffectiveDate as Effective_Date,
OLDAGNCY.RegistrarName as Old_Agency_Name,
NEWAGNCY.RegistrarName as New_Agency_Name,
OLDAGNCY.CntryCD as Old_Agency_Country,
NEWAGNCY.CntryCD as New_Agency_Country,
sachg.OldSpStartDate as Old_Surety_Start_Date,
sachg.NewSpStartDate as New_Surety_Start_Date,
sachg.OldSpEndDate as Old_Surety_End_Date,
sachg.NewSpEndDate as New_Surety_End_Date,
case when OLDRELATION.Lookup is null then sachg.OldRelationship else OLDRELATION.Lookup end as Old_Relationship,
case when NEWRELATION.Lookup is null then sachg.NewRelationship else NEWRELATION.Lookup end as New_Relationship,
case when OLDGTYPE.Lookup is null then sachg.OldGuaranteeType else OLDGTYPE.Lookup end as Old_Guarantee_Type,  
case when NEWGTYPE.Lookup is null then sachg.NewGuaranteeType else NEWGTYPE.Lookup end as New_Guarantee_Type,
sachg.Actflag,
sachg.Acttime as changed,
sachg.AnnounceDate as created,
sachg.sachgID as firef,
scagy.secid
from sachg
inner join scagy on sachg.scagyid = scagy.scagyid
inner join bond on scagy.secid = bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join agncy as OLDAGNCY on sachg.OldAgncyID = OLDAGNCY.agncyid
left outer join agncy as NEWAGNCY on sachg.OldAgncyID = NEWAGNCY.agncyid
left outer join wfi.sys_lookup as OLDRELATION on sachg.oldrelationship = OLDRELATION.Code
                      and 'RELATION' = OLDRELATION.TypeGroup
left outer join wfi.sys_lookup as NEWRELATION on sachg.newrelationship = NEWRELATION.Code
                      and 'RELATION' = NEWRELATION.TypeGroup
left outer join wfi.sys_lookup as OLDGTYPE on sachg.OldGuaranteeType = OLDGTYPE.Code
                      and 'GTYPE' = OLDGTYPE.TypeGroup
left outer join wfi.sys_lookup as NEWGTYPE on sachg.NewGuaranteeType = NEWGTYPE.Code
                      and 'GTYPE' = NEWGTYPE.TypeGroup
where
bond.secid in (select secid from wfimatured.reference)
and (sachg.actflag<>'d' or sachg.acttime>ADDDATE(curdate(), -14))
);
alter table `wfimatured`.`loadingtable`
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfimatured.agency_change;
rename table wfimatured.loadingtable to wfimatured.agency_change;
--
drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select
rd.recdate as record_date,
exdt.exdate as ex_date,
exdt.paydate as pay_date,
intpy.curencd as announced_interest_currency,
intpy.intrate as interest_rate,
intpy.grossinterest as gross_interest,
intpy.netinterest as net_interest,
intpy.rescindinterest as rescinded_interest,
int_my.interestfromdate as interest_from_date,
int_my.interesttodate as interest_to_date,
int_my.days,
case when indef.defaulttype<>'' then 'full default' else '' end as interest_default,
intpy.domestictaxrate as domestic_tax_rate,
intpy.agencyfees as agency_fees,
intpy.couponno as coupon_number,
intpy.optelectiondate as election_date,
intpy.anlcouprate as annual_coupon_rate,
int_my.nilint as nil_interest,
intpy.bcurencd as announced_current_currency,
intpy.bparvalue as announced_nominal_value,
'wca.dbo.indef|indefid|notes' as link_default_notes,
'int|rdid|intnotes' as link_notes,
case when int_my.actflag = 'd' or int_my.actflag='c' or intpy.actflag is null then int_my.actflag else intpy.actflag end as actflag,
case when (rd.acttime is not null) and (rd.acttime > int_my.acttime) and (rd.acttime > exdt.acttime) then rd.acttime when (exdt.acttime is not null) and (exdt.acttime > int_my.acttime) then exdt.acttime else int_my.acttime end as changed,
int_my.announcedate as created,
case when optionid is not null and scexhid is not null
     then concat(lpad(scexhid,7,'0'),optionid,int_my.rdid)
     when optionid is null and scexhid is not null
     then concat(lpad(scexhid,7,'0'),'1',int_my.rdid)
     when optionid is not null and scexhid is null
     then concat(optionid,int_my.rdid)
     else concat('1',int_my.rdid)
     end as firef,
rd.secid,
scexh.exchgcd,
rd.rdid,
indef.indefid
from int_my
inner join rd on int_my.rdid = rd.rdid
inner join bond on rd.secid =bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join scexh on rd.secid = scexh.secid
left outer join exdt on rd.rdid = exdt.rdid 
     and scexh.exchgcd = exdt.exchgcd and 'int' = exdt.eventtype
     and exdt.actflag<>'d'
left outer join intpy on int_my.rdid = intpy.rdid
left outer join indef on rd.secid = indef.secid and exdt.paydate = indef.defaultdate and 'intdefa' = indef.defaulttype and 'd' <> indef.actflag
where
rd.secid in (select secid from wfi.reference)
);
alter table `wfi`.`loadingtable`
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfi.interest_announcement;
rename table wfi.loadingtable to wfi.interest_announcement;
--
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select
rd.recdate as record_date,
exdt.exdate as ex_date,
exdt.paydate as pay_date,
intpy.curencd as announced_interest_currency,
intpy.intrate as interest_rate,
intpy.grossinterest as gross_interest,
intpy.netinterest as net_interest,
intpy.rescindinterest as rescinded_interest,
int_my.interestfromdate as interest_from_date,
int_my.interesttodate as interest_to_date,
int_my.days,
case when indef.defaulttype<>'' then 'full default' else '' end as interest_default,
intpy.domestictaxrate as domestic_tax_rate,
intpy.agencyfees as agency_fees,
intpy.couponno as coupon_number,
intpy.optelectiondate as election_date,
intpy.anlcouprate as annual_coupon_rate,
int_my.nilint as nil_interest,
intpy.bcurencd as announced_current_currency,
intpy.bparvalue as announced_nominal_value,
'wca.dbo.indef|indefid|notes' as link_default_notes,
'int|rdid|intnotes' as link_notes,
case when int_my.actflag = 'd' or int_my.actflag='c' or intpy.actflag is null then int_my.actflag else intpy.actflag end as actflag,
case when (rd.acttime is not null) and (rd.acttime > int_my.acttime) and (rd.acttime > exdt.acttime) then rd.acttime when (exdt.acttime is not null) and (exdt.acttime > int_my.acttime) then exdt.acttime else int_my.acttime end as acttime,
int_my.announcedate as created,
case when optionid is not null and scexhid is not null
     then concat(lpad(scexhid,7,'0'),optionid,int_my.rdid)
     when optionid is null and scexhid is not null
     then concat(lpad(scexhid,7,'0'),'1',int_my.rdid)
     when optionid is not null and scexhid is null
     then concat(optionid,int_my.rdid)
     else concat('1',int_my.rdid)
     end as firef,
rd.secid,
scexh.exchgcd,
rd.rdid,
indef.indefid
from int_my
inner join rd on int_my.rdid = rd.rdid
inner join bond on rd.secid =bond.secid
inner join scmst on bond.secid = scmst.secid
left outer join scexh on rd.secid = scexh.secid
left outer join exdt on rd.rdid = exdt.rdid 
     and scexh.exchgcd = exdt.exchgcd and 'int' = exdt.eventtype
     and exdt.actflag<>'d'
left outer join intpy on int_my.rdid = intpy.rdid
left outer join indef on rd.secid = indef.secid and exdt.paydate = indef.defaultdate and 'intdefa' = indef.defaulttype and 'd' <> indef.actflag
where
rd.secid in (select secid from wfimatured.reference)
);
alter table `wfimatured`.`loadingtable`
add primary key (`firef`),
add index `ix_secid` (`secid`);
drop table if exists wfimatured.interest_announcement;
rename table wfimatured.loadingtable to wfimatured.interest_announcement;
