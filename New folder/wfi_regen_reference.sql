-- wfi Web Loading Flag
-- insert into wca2.tbl_opslog
-- select * from wca.tbl_opslog
-- order by wca.tbl_opslog.acttime desc limit 1;
-- update wca2.tbl_opslog
-- set comment='Currently loading - Searches could be disrupted during this process'
-- order by wca2.tbl_opslog.acttime desc limit 1;



drop table if exists wfi.loadingtable2;
use wca;
create table wfi.loadingtable2(
select
parentscmst.isin as Parent_ISIN,
case when scmst.statusflag='D' then 'In default' when scmst.statusflag='I' then 'Inactive' else 'Active' end as Security_Status,
case when bond.municipal='y' then 'yes' when bond.municipal='n' then 'no' else '' end as municipal,
case when bond.privateplacement='y' then 'yes' when bond.privateplacement='n' then 'no' else '' end as private_placement,
case when bond.syndicated='y' then 'yes' when bond.syndicated='n' then 'no' else '' end as syndicated,
case when bondtype.lookup is null then bond.bondtype else bondtype.lookup end as security_type,
case when structcd.lookup is null then scmst.structcd else structcd.lookup end as debt_structure,
scmst.regs144a as regulation_code,
bond.curencd as current_currency,
case when holding.lookup is null then scmst.holding else holding.lookup end as holding,
case when bond.largeparvalue<>'' and bond.largeparvalue<>'0' then bond.largeparvalue when bond.parvalue<>'' and bond.parvalue<>'0' then bond.parvalue else scmst.parvalue end as nominal_value,
bondx.denomination1 as denom_1,
bondx.denomination2 as denom_2,
bondx.denomination3 as denom_3,
bondx.denomination4 as denom_4,
bondx.denomination5 as denom_5,
bondx.denomination6 as denom_6,
bondx.denomination7 as denom_7,
bondx.minimumdenomination as min_denom, 
bondx.denominationmultiple as denom_multiple, 


-- bond.issuedate as issue_date, 
case when bond.secid in (select secid from wca.trnch) then wca.trnch.tranchedate else bond.issuedate end as issue_date,


bond.issuecurrency as issue_currency, 
bond.issueprice as issue_price, 
bond.priceaspercent as price_as_percent,


-- bond.issueamount as issue_amount_in_millions, 
case when bond.secid in (select secid from wca.trnch) then wca.trnch.trancheamount else bond.issueamount end as issue_amount_in_millions,



-- bond.issueamountdate as issue_amount_date,
case when bond.secid in (select secid from wca.trnch) then wca.trnch.tranchedate else bond.issueamountdate end as issue_amount_date,


case when (bond.IndicativeIssAmt is null or bond.IndicativeIssAmt = 0) then ''
else bond.IndicativeIssAmt end as indicative_issue_amount, 
bond.IndicativeIssAmtDate as indicative_issue_amount_date,
bondx.series, 
bondx.class, 
case when bond.seniorjunior='s' then 'senior' when bond.seniorjunior='j' then 'junior' when bond.seniorjunior='m' then 'mezzanine' when bond.seniorjunior='P' then 'Senior Preferred' when bond.seniorjunior='n' then 'Senior Non Preferred' else '' end as financing_structure,
bond.outstandingamount as outstanding_in_millions,
case when bond.outstandingamountdate = '1800/01/01' then null else bond.outstandingamountdate end as outstanding_change_date, 
case when inttype.lookup is null then bond.interestbasis else inttype.lookup end as interest_basis, 
case when frntype.lookup is null then bond.frntype else frntype.lookup end as frn_type,  
bond.interestcurrency as interest_currency, 
case when frnindxben.lookup is null then bond.frnindexbenchmark else frnindxben.lookup end as frn_index_benchmark,  
bond.markup as margin,
bond.rounding, 
bond.interestrate as interest_rate,
bond.minimuminterestrate as min_interest_rate, 
bond.maximuminterestrate as max_interest_rate, 
bond.intcommencementdate as interest_commencement_date, 
bond.firstcoupondate as first_coupon_date, 
case when bond.oddfirstcoupon='S' then 'Short' when bond.oddfirstcoupon='L' then 'Long' else '' end as odd_first_coupon,
case when bond.oddlastcoupon='S' then 'Short' when bond.oddlastcoupon='L' then 'Long' else '' end as odd_last_coupon,
bond.lastcoupondate as last_coupon_date,
case when bond.collateral='Y' then 'yes' else '' end as collateral_flag,
coverpool.lookup as cover_pool,
bond.pikpay as payment_in_kind,
cocotrigger.lookup as cocotrigger,
cocoaction.lookup as cocoaction,
case when (bond.CoCoCapRatioTriggerPercent is null or bond.CoCoCapRatioTriggerPercent = 0) then ''
else bond.CoCoCapRatioTriggerPercent end as coco_capital_ratio_trigger_percentage,
nonviability.lookup as  nonviability,
case when bondx.taxability='TXFR' then 'TAXABILITY' else '' end as taxability,
case when bond.guaranteed='t' then 'yes' else 'no' end as guaranteed,
case when bond.securedby='y' then 'yes' when bond.securedby='n' then 'no' else '' end as secured_by,
case when bond.warrantattached='y' then 'yes' when bond.warrantattached='n' then 'no' else '' end as warrant_attached,
case when bond.subordinate='y' then 'yes' when bond.subordinate='n' then 'no' else '' end as subordinate,
case when assetbkd.lookup is null then bond.securitycharge else assetbkd.lookup end as security_charge, 
case when intaccrual.lookup is null then bond.interestaccrualconvention else intaccrual.lookup end as interest_accrual_convention,
case when intbdc.lookup is null then bond.intbusdayconv else intbdc.lookup end as int_business_day_convention,  
conventionmethod as convention_method,  
case when strip.lookup is null then bond.strip else strip.lookup end as strip, 
bond.stripinterestnumber as strip_interest_number,
case when freq.lookup is null then bond.interestpaymentfrequency else freq.lookup end as interest_payment_freq,  
case when bond.varintpaydate='y' then 'yes' when bond.varintpaydate='n' then 'no' else '' end as variable_interest_paydate,
bond.frnintadjfreq as frn_interest_adjustment_freq, 
bond.interestpaydate1 as interest_payddmm_1,
bond.interestpaydate2 as interest_payddmm_2,
bond.interestpaydate3 as interest_payddmm_3,
bond.interestpaydate4 as interest_payddmm_4,
case when bond.perpetual='i' then 'irredeemable' when bond.perpetual='p' then 'perpetual' when bond.perpetual='u' then 'undated' else '' end as perpetual,
case when debtstatus.lookup is null then bond.maturitystructure else debtstatus.lookup end as maturity_structure,  
-- bond.maturitydate as maturity_date,
case when bond.secid in (select secid from wca.trnch) then ''
     when (bond.MaturityDate = '' or bond.MaturityDate is null) then 'Perpetual'
     else bond.MaturityDate end as Maturity_Date,
bond.ExpMatDate as expected_maturity_date,
case when bond.maturityextendible='y' then 'yes' when bond.maturityextendible='n' then 'no' else '' end as maturity_extendible,
bond.maturitycurrency as maturity_currency,
bond.matprice as maturity_price,
bond.matpriceaspercent as maturity_price_as_percent,
case when matbnhmrk.lookup is null then bond.maturitybenchmark else matbnhmrk.lookup end as maturity_benchmark,  
case when matbdc.lookup is null then bond.matbusdayconv else matbdc.lookup end as mat_business_day_convention,  
case when bond.cumulative='y' then 'yes' when bond.cumulative='n' then 'no' else '' end as cumulative,
case when bond.callable='y' then 'yes' when bond.callable='n' then 'no' else '' end as callable,
case when bond.puttable='y' then 'yes' when bond.puttable='n' then 'no' else '' end as puttable,
case when bond.sinkingfund='y' then 'yes' when bond.sinkingfund='n' then 'no' else '' end as sinking_fund,
case when payout.lookup is null then bond.payoutmode else payout.lookup end as payout_mode,
bond.debtmarket as debt_grouping, 
case when bondx.ontap='t' then 'yes' else 'no' end as on_tap,
bondx.maximumtapamount as max_tap_amount, 
bondx.tapexpirydate as tap_expiry_date, 
case when issuedate>'2001/02/28' and issueamountdate>'2002/03/01'
and interestbasis<>'zc' and issur.cntryofincorp in (select cntryofincorp from wfi.sys_eusd) then 'yes'
else 'no' end as eusd_applicable,
bondx.domestictaxrate as domestic_tax_rate,
bondx.nonresidenttaxrate as non_resident_tax_rate,
bondx.taxrules as tax_rules,
case when bond.bondsrc is null or bond.bondsrc='' or bond.bondsrc='de' or bond.bondsrc='pp' or bond.bondsrc='sr' then '' else 'yes' end as backup_document, 
'bondx|secid|governinglaw' as link_governing_law,
'bond|secid|notes' as link_notes,
bond.tier,
bond.firstannouncementdate,
bond.performancebenchmarkisin,
case when mifid.MiFIDID is not null and mifid.enddate > curdate() or mifid.MiFIDID is not null and mifid.enddate is null then 'yes' else 'no' end as mifid_eligibility,
bond.LatestAppliedINTPYAnlCpnRateDate as latest_applicable_payment_date,
bondx.interestgracedays as interest_grace_period_days,
case when bondx.interestgracedayconvention='bd' then 'business days' when bondx.interestgracedayconvention='cd' then 'calendar days' else '' end as interest_grace_period_convention,
bondx.matgracedays as maturity_grace_period_days,
case when bondx.matgracedayconvention='bd' then 'business days' when bondx.matgracedayconvention='cd' then 'calendar days' else '' end as maturity_grace_period_convention,
case when bond.GreenBond='y' then 'Yes' else '' end as Green_Bond,

wca.convt.ResSecID as resultant_secid,
wca.convt.SectyCD as resultant_security_type,
wca.resscmst.ISIN as resultant_isin,
wca.resissur.Issuername as resultant_issuer_name,
wca.resscmst.SecurityDesc as resultant_security_description,

wca.scmst.SharesOutstanding as shares_outstanding,
wca.scmst.SharesOutstandingDate as shares_outstanding_date,

bond.actflag,
bond.acttime as changed,
bond.announcedate as created,
bond.secid
from bond
left outer join bondx on bond.secid = bondx.secid
inner join scmst on bond.secid = scmst.secid
left outer join scmst as parentscmst on scmst.parentsecid = parentscmst.secid
inner join issur on scmst.issid = issur.issid
left outer join mifid on bond.secid = mifid.secid

left outer join wca.convt on wca.bond.secid = wca.convt.secid
left outer join wca.scmst as resscmst on wca.convt.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid

left outer join wca.trnch on wca.bond.secid = wca.trnch.secid


left outer join wfi.sys_lookup as holding on scmst.holding = holding.code
                      and 'holding' = holding.typegroup
left outer join wfi.sys_lookup as structcd on scmst.structcd = structcd.code
                      and 'structcd' = structcd.typegroup
left outer join wfi.sys_lookup as bondtype on bond.bondtype = bondtype.code
                      and 'bondtype' = bondtype.typegroup
left outer join wfi.sys_lookup as freq on bond.interestpaymentfrequency = freq.code
                      and 'freq' = freq.typegroup
left outer join wfi.sys_lookup as inttype on bond.interestbasis = inttype.code
                      and 'inttype' = inttype.typegroup
left outer join wfi.sys_lookup as intaccrual on bond.interestaccrualconvention = intaccrual.code
                      and 'intaccrual' = intaccrual.typegroup
left outer join wfi.sys_lookup as frntype on bond.frntype = frntype.code
                      and 'frntype' = frntype.typegroup
left outer join wfi.sys_lookup as frnindxben on bond.frnindexbenchmark = frnindxben.code
                      and 'frnindxben' = frnindxben.typegroup
left outer join wfi.sys_lookup as assetbkd on bond.securitycharge = assetbkd.code
                      and 'assetbkd' = assetbkd.typegroup
left outer join wfi.sys_lookup as matbdc on bond.intbusdayconv = matbdc.code
                      and 'matbdc' = matbdc.typegroup
left outer join wfi.sys_lookup as intbdc on bond.intbusdayconv = intbdc.code
                      and 'intbdc' = intbdc.typegroup
left outer join wfi.sys_lookup as debtstatus on bond.maturitystructure = debtstatus.code
                      and 'debtstatus' = debtstatus.typegroup
left outer join wfi.sys_lookup as strip on bond.strip = strip.code
                      and 'strip' = strip.typegroup
left outer join wfi.sys_lookup as matbnhmrk on bond.maturitybenchmark = matbnhmrk.code
                      and 'matbnhmrk' = matbnhmrk.typegroup
left outer join wfi.sys_lookup as payout on bond.payoutmode = payout.code
                      and 'payout' = payout.typegroup
left outer join wfi.sys_lookup as coverpool on bond.coverpool = coverpool.code
                      and 'coverpool' = coverpool.typegroup
left outer join wfi.sys_lookup as cocotrigger on bond.cocotrigger = cocotrigger.code
                      and 'coco' = cocotrigger.typegroup
left outer join wfi.sys_lookup as cocoaction on bond.cocoact = cocoaction.code
                      and 'cocoaction' = cocoaction.typegroup 
left outer join wfi.sys_lookup as nonviability on bond.nonviability = nonviability.code
                      and 'nonviable' = nonviability.typegroup                     

where

(wca.convt.convtid = (SELECT t2.convtid
                 FROM wca.convt t2
                 WHERE t2.secid = wca.convt.secid
                 order by t2.convtid desc limit 1)
                 or wca.convt.convtid is null)


and scmst.statusflag<>'I'
and bond.actflag<>'d'
);
alter table `wfi`.`loadingtable`
add primary key (`secid`),
add index `ix_changed`(`changed`),
add index `ix_maturity_date`(`maturity_date`);
drop table if exists wfi.reference;
rename table wfi.loadingtable to wfi.reference;
--
drop table if exists wfimatured.loadingtable2;
use wca;
create table wfimatured.loadingtable2(
select
parentscmst.isin as Parent_ISIN,
case when scmst.statusflag='D' then 'In default' when scmst.statusflag='I' then 'Inactive' else 'Active' end as Security_Status,
case when bond.municipal='y' then 'yes' when bond.municipal='n' then 'no' else '' end as municipal,
case when bond.privateplacement='y' then 'yes' when bond.privateplacement='n' then 'no' else '' end as private_placement,
case when bond.syndicated='y' then 'yes' when bond.syndicated='n' then 'no' else '' end as syndicated,
case when bondtype.lookup is null then bond.bondtype else bondtype.lookup end as security_type,
case when structcd.lookup is null then scmst.structcd else structcd.lookup end as debt_structure,
scmst.regs144a as regulation_code,
bond.curencd as current_currency,
case when holding.lookup is null then scmst.holding else holding.lookup end as holding,
case when bond.largeparvalue<>'' and bond.largeparvalue<>'0' then bond.largeparvalue when bond.parvalue<>'' and bond.parvalue<>'0' then bond.parvalue else scmst.parvalue end as nominal_value,
bondx.denomination1 as denom_1,
bondx.denomination2 as denom_2,
bondx.denomination3 as denom_3,
bondx.denomination4 as denom_4,
bondx.denomination5 as denom_5,
bondx.denomination6 as denom_6,
bondx.denomination7 as denom_7,
bondx.minimumdenomination as min_denom, 
bondx.denominationmultiple as denom_multiple, 

-- bond.issuedate as issue_date, 
case when bond.secid in (select secid from wca.trnch) then wca.trnch.tranchedate else bond.issuedate end as issue_date,


bond.issuecurrency as issue_currency, 
bond.issueprice as issue_price, 
bond.priceaspercent as price_as_percent,


-- bond.issueamount as issue_amount_in_millions, 
case when bond.secid in (select secid from wca.trnch) then wca.trnch.trancheamount else bond.issueamount end as issue_amount_in_millions,



-- bond.issueamountdate as issue_amount_date,
case when bond.secid in (select secid from wca.trnch) then wca.trnch.tranchedate else bond.issueamountdate end as issue_amount_date,



case when (bond.IndicativeIssAmt is null or bond.IndicativeIssAmt = 0) then ''
else bond.IndicativeIssAmt end as indicative_issue_amount, 
bond.IndicativeIssAmtDate as indicative_issue_amount_date,
bondx.series, 
bondx.class, 
case when bond.seniorjunior='s' then 'senior' when bond.seniorjunior='j' then 'junior' when bond.seniorjunior='m' then 'mezzanine' when bond.seniorjunior='P' then 'Senior Preferred' when bond.seniorjunior='n' then 'Senior Non Preferred' else '' end as financing_structure,
case when bond.outstandingamount ='' or  bond.outstandingamount is null
     then '0'
     else  bond.outstandingamount
     end as outstanding_in_millions,
case when bond.outstandingamountdate = '1800/01/01' then null else bond.outstandingamountdate end as outstanding_change_date, 
case when inttype.lookup is null then bond.interestbasis else inttype.lookup end as interest_basis, 
case when frntype.lookup is null then bond.frntype else frntype.lookup end as frn_type,  
bond.interestcurrency as interest_currency, 
case when frnindxben.lookup is null then bond.frnindexbenchmark else frnindxben.lookup end as frn_index_benchmark,  
bond.markup as margin,
bond.rounding, 
bond.interestrate as interest_rate,
bond.minimuminterestrate as min_interest_rate, 
bond.maximuminterestrate as max_interest_rate, 
bond.intcommencementdate as interest_commencement_date, 
bond.firstcoupondate as first_coupon_date, 
case when bond.oddfirstcoupon='S' then 'Short' when bond.oddfirstcoupon='L' then 'Long' else '' end as odd_first_coupon,
case when bond.oddlastcoupon='S' then 'Short' when bond.oddlastcoupon='L' then 'Long' else '' end as odd_last_coupon,
bond.lastcoupondate as last_coupon_date,
case when bond.collateral='Y' then 'yes' else '' end as collateral_flag,
coverpool.lookup  as cover_pool,
bond.pikpay as payment_in_kind,
cocotrigger.lookup as cocotrigger,
cocoaction.lookup as cocoaction,
case when (bond.CoCoCapRatioTriggerPercent is null or bond.CoCoCapRatioTriggerPercent = 0) then ''
else bond.CoCoCapRatioTriggerPercent end as coco_capital_ratio_trigger_percentage,
nonviability.lookup as  nonviability,
case when bondx.taxability='TXFR' then 'TAXABILITY' else '' end as taxability,
case when bond.guaranteed='t' then 'yes' else 'no' end as guaranteed,
case when bond.securedby='y' then 'yes' when bond.securedby='n' then 'no' else '' end as secured_by,
case when bond.warrantattached='y' then 'yes' when bond.warrantattached='n' then 'no' else '' end as warrant_attached,
case when bond.subordinate='y' then 'yes' when bond.subordinate='n' then 'no' else '' end as subordinate,
case when assetbkd.lookup is null then bond.securitycharge else assetbkd.lookup end as security_charge, 
case when intaccrual.lookup is null then bond.interestaccrualconvention else intaccrual.lookup end as interest_accrual_convention,
case when intbdc.lookup is null then bond.intbusdayconv else intbdc.lookup end as int_business_day_convention,  
conventionmethod as convention_method,  
case when strip.lookup is null then bond.strip else strip.lookup end as strip, 
bond.stripinterestnumber as strip_interest_number,
case when freq.lookup is null then bond.interestpaymentfrequency else freq.lookup end as interest_payment_freq,  
case when bond.varintpaydate='y' then 'yes' when bond.varintpaydate='n' then 'no' else '' end as variable_interest_paydate,
bond.frnintadjfreq as frn_interest_adjustment_freq, 
bond.interestpaydate1 as interest_payddmm_1,
bond.interestpaydate2 as interest_payddmm_2,
bond.interestpaydate3 as interest_payddmm_3,
bond.interestpaydate4 as interest_payddmm_4,
case when bond.perpetual='i' then 'irredeemable' when bond.perpetual='p' then 'perpetual' when bond.perpetual='u' then 'undated' else '' end as perpetual,
case when debtstatus.lookup is null then bond.maturitystructure else debtstatus.lookup end as maturity_structure,  
-- bond.maturitydate as maturity_date,
case when bond.secid in (select secid from wca.trnch) then ''
     when (bond.MaturityDate = '' or bond.MaturityDate is null) then 'Perpetual'
     else bond.MaturityDate end as Maturity_Date,
bond.ExpMatDate as expected_maturity_date,
case when bond.maturityextendible='y' then 'yes' when bond.maturityextendible='n' then 'no' else '' end as maturity_extendible,
bond.maturitycurrency as maturity_currency,
bond.matprice as maturity_price,
bond.matpriceaspercent as maturity_price_as_percent,
case when matbnhmrk.lookup is null then bond.maturitybenchmark else matbnhmrk.lookup end as maturity_benchmark,  
case when matbdc.lookup is null then bond.matbusdayconv else matbdc.lookup end as mat_business_day_convention,  
case when bond.cumulative='y' then 'yes' when bond.cumulative='n' then 'no' else '' end as cumulative,
case when bond.callable='y' then 'yes' when bond.callable='n' then 'no' else '' end as callable,
case when bond.puttable='y' then 'yes' when bond.puttable='n' then 'no' else '' end as puttable,
case when bond.sinkingfund='y' then 'yes' when bond.sinkingfund='n' then 'no' else '' end as sinking_fund,
case when payout.lookup is null then bond.payoutmode else payout.lookup end as payout_mode,
bond.debtmarket as debt_grouping, 
case when bondx.ontap='t' then 'yes' else 'no' end as on_tap,
bondx.maximumtapamount as max_tap_amount, 
bondx.tapexpirydate as tap_expiry_date, 
case when issuedate>'2001/02/28' and issueamountdate>'2002/03/01'
and interestbasis<>'zc' and issur.cntryofincorp in (select cntryofincorp from wfimatured.sys_eusd) then 'yes'
else 'no' end as eusd_applicable,
bondx.domestictaxrate as domestic_tax_rate,
bondx.nonresidenttaxrate as non_resident_tax_rate,
bondx.taxrules as tax_rules,
case when bond.bondsrc is null or bond.bondsrc='' or bond.bondsrc='de' or bond.bondsrc='pp' or bond.bondsrc='sr' then '' else 'yes' end as backup_document, 
'bondx|secid|governinglaw' as link_governing_law,
'bond|secid|notes' as link_notes,
bond.tier,
bond.firstannouncementdate,
bond.performancebenchmarkisin,
case when mifid.MiFIDID is not null and mifid.enddate > curdate() or mifid.MiFIDID is not null and mifid.enddate is null then 'yes' else 'no' end as mifid_eligibility,
bond.LatestAppliedINTPYAnlCpnRateDate as latest_applicable_payment_date, 
bondx.interestgracedays as interest_grace_period_days,
case when bondx.interestgracedayconvention='bd' then 'business days' when bondx.interestgracedayconvention='cd' then 'calendar days' else '' end as interest_grace_period_convention,
bondx.matgracedays as maturity_grace_period_days,
case when bondx.matgracedayconvention='bd' then 'business days' when bondx.matgracedayconvention='cd' then 'calendar days' else '' end as maturity_grace_period_convention,
case when bond.GreenBond='y' then 'Yes' else '' end as Green_Bond,

wca.convt.ResSecID as resultant_secid,
wca.convt.SectyCD as resultant_security_type,
wca.resscmst.ISIN as resultant_isin,
wca.resissur.Issuername as resultant_issuer_name,
wca.resscmst.SecurityDesc as resultant_security_description,

wca.scmst.SharesOutstanding as shares_outstanding,
wca.scmst.SharesOutstandingDate as shares_outstanding_date,

bond.actflag,
bond.acttime as changed,
bond.announcedate as created,
bond.secid
from bond
left outer join bondx on bond.secid = bondx.secid
inner join scmst on bond.secid = scmst.secid
left outer join scmst as parentscmst on scmst.parentsecid = parentscmst.secid
inner join issur on scmst.issid = issur.issid
left outer join mifid on bond.secid = mifid.secid

left outer join wca.convt on wca.bond.secid = wca.convt.secid
left outer join wca.scmst as resscmst on wca.convt.ressecid = wca.resscmst.secid
left outer join wca.issur as resissur on wca.resscmst.issid = wca.resissur.issid

left outer join wca.trnch on wca.bond.secid = wca.trnch.secid


left outer join wfimatured.sys_lookup as holding on scmst.holding = holding.code
                      and 'holding' = holding.typegroup
left outer join wfimatured.sys_lookup as structcd on scmst.structcd = structcd.code
                      and 'structcd' = structcd.typegroup
left outer join wfimatured.sys_lookup as bondtype on bond.bondtype = bondtype.code
                      and 'bondtype' = bondtype.typegroup
left outer join wfimatured.sys_lookup as freq on bond.interestpaymentfrequency = freq.code
                      and 'freq' = freq.typegroup
left outer join wfimatured.sys_lookup as inttype on bond.interestbasis = inttype.code
                      and 'inttype' = inttype.typegroup
left outer join wfimatured.sys_lookup as intaccrual on bond.interestaccrualconvention = intaccrual.code
                      and 'intaccrual' = intaccrual.typegroup
left outer join wfimatured.sys_lookup as frntype on bond.frntype = frntype.code
                      and 'frntype' = frntype.typegroup
left outer join wfimatured.sys_lookup as frnindxben on bond.frnindexbenchmark = frnindxben.code
                      and 'frnindxben' = frnindxben.typegroup
left outer join wfimatured.sys_lookup as assetbkd on bond.securitycharge = assetbkd.code
                      and 'assetbkd' = assetbkd.typegroup
left outer join wfimatured.sys_lookup as matbdc on bond.intbusdayconv = matbdc.code
                      and 'matbdc' = matbdc.typegroup
left outer join wfimatured.sys_lookup as intbdc on bond.intbusdayconv = intbdc.code
                      and 'intbdc' = intbdc.typegroup
left outer join wfimatured.sys_lookup as debtstatus on bond.maturitystructure = debtstatus.code
                      and 'debtstatus' = debtstatus.typegroup
left outer join wfimatured.sys_lookup as strip on bond.strip = strip.code
                      and 'strip' = strip.typegroup
left outer join wfimatured.sys_lookup as matbnhmrk on bond.maturitybenchmark = matbnhmrk.code
                      and 'matbnhmrk' = matbnhmrk.typegroup
left outer join wfimatured.sys_lookup as payout on bond.payoutmode = payout.code
                      and 'payout' = payout.typegroup
left outer join wfi.sys_lookup as coverpool on bond.coverpool = coverpool.code
                      and 'coverpool' = coverpool.typegroup 
left outer join wfi.sys_lookup as cocotrigger on bond.cocotrigger = cocotrigger.code
                      and 'coco' = cocotrigger.typegroup
left outer join wfi.sys_lookup as cocoaction on bond.cocoact = cocoaction.code
                      and 'cocoaction' = cocoaction.typegroup
left outer join wfi.sys_lookup as nonviability on bond.nonviability = nonviability.code
                      and 'nonviable' = nonviability.typegroup                        
where

(wca.convt.convtid = (SELECT t2.convtid
                 FROM wca.convt t2
                 WHERE t2.secid = wca.convt.secid
                 order by t2.convtid desc limit 1)
                 or wca.convt.convtid is null)

and scmst.statusflag='I'
and bond.actflag<>'d'

);
alter table `wfimatured`.`loadingtable`
add primary key (`secid`),
add index `ix_changed`(`changed`),
add index `ix_maturity_date`(`maturity_date`);
drop table if exists wfimatured.reference;
rename table wfimatured.loadingtable to wfimatured.reference;