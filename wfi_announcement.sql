drop table if exists wfi.loadingtable;
use wca;
create table wfi.loadingtable(
select
case when relevent.lookup is null then ann.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
ann.notificationdate as notification_date,
'wca.dbo.ann|annid|annnotes' as link_notes,
ann.actflag,
ann.acttime as changed,
ann.announcedate as created,
concat(lpad(ann.annid,7,'0'),scmst.secid) as firef,
scmst.secid
from ann
inner join scmst on ann.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
left outer join wfi.sys_lookup as relevent on ann.eventtype = relevent.typegroup
                      and 'event' = relevent.code
where
not ((maturitydate>ADDDATE(curdate(), -1) or maturitydate is null)
or (maturitydate<curdate() and scmst.statusflag<>'I'))
and bond.bondtype <> 'prf' and bond.actflag<>'d'
and ann.actflag<>'d'
);
alter table `wfi`.loadingtable
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfi.announcement;
rename table wfi.loadingtable to wfi.announcement;
------------------------------------------------------------------------------------------------------------------------------
drop table if exists wfimatured.loadingtable;
use wca;
create table wfimatured.loadingtable(
select
case when relevent.lookup is null then ann.eventtype when relevent.lookup = 'clean' then 'batch correction' else relevent.lookup end as reason,
ann.notificationdate as notification_date,
'wca.dbo.ann|annid|annnotes' as link_notes,
ann.actflag,
ann.acttime as changed,
ann.announcedate as created,
concat(lpad(ann.annid,7,'0'),scmst.secid) as firef,
scmst.secid
from ann
inner join scmst on ann.issid = scmst.issid
inner join bond on scmst.secid = bond.secid
left outer join wfi.sys_lookup as relevent on ann.eventtype = relevent.typegroup
                      and 'event' = relevent.code
where
not ((maturitydate>ADDDATE(curdate(), -1) or maturitydate is null)
or (maturitydate<curdate() and scmst.statusflag<>'I'))
and bond.bondtype <> 'prf' and bond.actflag<>'d'
and ann.actflag<>'d'
);
alter table `wfimatured`.loadingtable
add primary key (`firef`),
add index `ix_secid`(`secid`),
add index `ix_changed`(`changed`);
drop table if exists wfimatured.announcement;
rename table wfimatured.loadingtable to wfimatured.announcement;
------------------------------------------------------------------------------------------------------------------------------
